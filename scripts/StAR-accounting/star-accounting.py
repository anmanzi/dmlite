#!/usr/bin/python2
###########################################################################
#
# star-accounting
#
# A script that produces an accounting record following the EMI StAR specs
# in the version 1.2
#
# Syntax:
#
# star-accounting [-h] [--help]
# .. to get the help screen
#
# Dependencies:
#  yum install MySQL-python python-lxml python-uuid
#
#  v1.0.0 initial release
#  v1.0.2 removed site debug printouts that were screwing up the output
#  v1.0.3 avoid summing the size fields for directories. Fixes EGI doublecounting
#  v1.0.4 marks the report as regarding the past period, not the next one; parse
#         the DPM group names into VO and role
#  v1.3.0 Petr Vokac (petr.vokac@cern.ch), February 7, 2019
#         * replace SQL join with simple queries to improve performance
#         * compatibility with python 3
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

__version__ = "1.3.0"
__author__  = "Fabrizio Furano"

import sys
import os
import socket
import re
from optparse import OptionParser
import MySQLdb
import lxml.builder as lb
from lxml import etree
import uuid
import datetime
import logging


_log = logging.getLogger('DPMDUMP')

default_cns_db = 'cns_db'
default_dpm_db = 'dpm_db'


def get_conn_data(config, default_db):
    """ Returns connection data from {DPM,NS}CONFIG"""
    retval = {}

    _log.debug("getting connection info from %s", config)
    try:
        config_line = open(config).readline().strip()
    except Exception as e:
        _log.error("Cannot open DPM config file %s: %s", config, str(e))
        sys.exit(-1)

    nsre = re.compile(r"(.*)/(.*)@([^/]*)(?:/(.*))?")
    m = nsre.match(config_line)
    if m == None:
        _log.error("Bad line in DPM config '%s', doesn't match re '%s'", config, nsre)
        sys.exit(-1)
    retval['user'] = m.group(1)
    retval['pass'] = m.group(2)
    retval['host'] = m.group(3)
    if m.group(4):
        retval['db'] = m.group(4)
    else:
        retval['db'] = default_db

    _log.debug("database connection: host=%s, database=%s, user=%s", retval['host'], retval['db'], retval['user'])

    return retval



def addrecord(xmlroot, hostname, group, user, site, filecount, resourcecapacityused, logicalcapacityused, validduration, recordid = None):
  # update XML 
  rec = etree.SubElement(xmlroot, SR+'StorageUsageRecord')
  rid = etree.SubElement(rec, SR+'RecordIdentity')
  rid.set(SR+"createTime", datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))

  if hostname:
    ssys = etree.SubElement(rec, SR+"StorageSystem")
    ssys.text = hostname

  recid = recordid  
  if not recid:
    recid = hostname+"-"+str(uuid.uuid1())
  rid.set(SR+"recordId", recid)

  subjid = etree.SubElement(rec, SR+'SubjectIdentity')

  if group:
    grouproles = group.split('/')

    # If the last token is Role=... then we fetch the role and add it to the record
    tmprl = grouproles[-1]
    if tmprl.find('Role=') != -1:
      splitroles = tmprl.split('=')
      if (len(splitroles) > 1):
        role = splitroles[1]
        grp = etree.SubElement(subjid, SR+"GroupAttribute" )
        grp.set( SR+"attributeType", "role" )
        grp.text = role
      # Now drop this last token, what remains is the vo identifier
      grouproles.pop()


    # The voname is the first token
    voname = grouproles.pop(0)
    grp = etree.SubElement(subjid, SR+"Group")
    grp.text = voname

    # If there are other tokens, they are a subgroup
    if len(grouproles) > 0:
      subgrp = '/'.join(grouproles)
      grp = etree.SubElement(subjid, SR+"GroupAttribute" )
      grp.set( SR+"attributeType", "subgroup" )
      grp.text = subgrp

  if user:
    usr = etree.SubElement(subjid, SR+"User")
    usr.text = user

  if site:
    st = etree.SubElement(subjid, SR+"Site")
    st.text = site

  e = etree.SubElement(rec, SR+"StorageMedia")
  e.text = "disk"

  if validduration:
    e = etree.SubElement(rec, SR+"StartTime")
    d = datetime.datetime.utcnow() - datetime.timedelta(seconds=validduration)
    e.text = d.strftime("%Y-%m-%dT%H:%M:%SZ")

  e = etree.SubElement(rec, SR+"EndTime")
  e.text = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")


  if filecount:
    e = etree.SubElement(rec, SR+"FileCount")
    e.text = str(filecount)


  if not resourcecapacityused:
    resourcecapacityused = 0

  e1 = etree.SubElement(rec, SR+"ResourceCapacityUsed")
  e1.text = str(resourcecapacityused)

  e3 = etree.SubElement(rec, SR+"ResourceCapacityAllocated")
  e3.text = str(resourcecapacityused)

  if not logicalcapacityused:
    logicalcapacityused = 0

  e2 = etree.SubElement(rec, SR+"LogicalCapacityUsed")
  e2.text = str(logicalcapacityused)



#
# Return dictionary with reserved space by given column (s_uid, s_gid)
#
def getreservedspace(dbconn, column):
  cursor = dbconn.cursor()
  cursor.execute('SELECT {0}, SUM(t_space) FROM dpm_space_reserv GROUP BY {0}'.format(column))

  ret = {}
  for row in cursor:
      ret[row[0]] = row[1]

  cursor.close()

  return ret



#
# Return dictionary with key / value for given table
#
def getdbkv(dbconn, table, ckey, cval):
  cursor = dbconn.cursor()
  cursor.execute('SELECT {0}, {1} FROM {2}'.format(ckey, cval, table))

  ret = {}
  for row in cursor:
      ret[row[0]] = row[1]

  cursor.close()

  return ret



def getdnsnames(name): 
  d = socket.gethostbyaddr(name) 
  names = [ d[0] ] + d[1] + d[2] 
  return names 

def resolve(name): 
  names = getdnsnames(name) 
  for dnsname in names: 
    if '.' in dnsname: 
      fullname = dnsname 
      break 
    else: 
      fullname = name 
  return fullname 

def gethostname(): 
  fullname = socket.gethostname() 
  if '.' not in fullname: 
    fullname = resolve(fullname) 
  return fullname

#############
# Main code #
#############

# basic logging configuration
streamHandler = logging.StreamHandler(sys.stderr)
streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
_log.addHandler(streamHandler)
_log.setLevel(logging.WARN)

parser = OptionParser()
parser.add_option('--reportgroups', dest='reportgroups', action='store_true', default=False, help="Report about all groups")
parser.add_option('--reportusers', dest='reportusers', action='store_true', default=False, help="Report about all users")
parser.add_option('-v', '--debug', dest='verbose', action='count', default=0, help='Increase verbosity level for debugging (on stderr)')
parser.add_option('--hostname', dest='hostname', default=gethostname(), help="The hostname string to use in the record. Default: this host.")
parser.add_option('--site', dest='site', default="", help="The site string to use in the record. Default: none.")
parser.add_option('--recordid', dest='recordid', default=None, help="The recordid string to use in the record. Default: a newly computed unique string.")
parser.add_option('--nsconfig', dest='nsconfig', default=None, help="Path to the NSCONFIG file where to take the db login info")
parser.add_option('--dpmconfig', dest='dpmconfig', default=None, help="Path to the DPMCONFIG file where to take the db login info")
parser.add_option('--dbhost', dest='dbhost', default=None, help="Database host, if no NSCONFIG given")
parser.add_option('--dbuser', dest='dbuser', default=None, help="Database user, if no NSCONFIG given")
parser.add_option('--dbpwd', dest='dbpwd', default=None, help="Database password, if no NSCONFIG given")
parser.add_option('--nsdbname', dest='nsdbname', default='cns_db', help="NS Database name, if no NSCONFIG given")
parser.add_option('--dpmdbname', dest='dpmdbname', default='dpm_db', help="DPM Database name, if no DPMCONFIG given")
parser.add_option('--validduration', dest='validduration', default=86400, help="Valid duration of this record, in seconds (default: 1 day)")
options, args = parser.parse_args()

if options.verbose == 0: _log.setLevel(logging.ERROR)
elif options.verbose == 1: _log.setLevel(logging.WARN)
elif options.verbose == 2: _log.setLevel(logging.INFO)
else: _log.setLevel(logging.DEBUG)

record_id = options.recordid
site = options.site
conn_data = {
    'host': options.dbhost,
    'user': options.dbuser,
    'pass': options.dbpwd,
    'cns_db': options.nsdbname,
    'dpm_db': options.dpmdbname,
}

if options.nsconfig:
  # Parse the NSCONFIG line, extract the db login info from it
  for k, v in get_conn_data(options.nsconfig, default_cns_db).items():
    if k == 'db': conn_data['cns_db'] = v
    else: conn_data[k] = v

if options.dpmconfig:
  # Parse the DPMCONFIG line, extract the db login info from it
  for k, v in get_conn_data(options.nsconfig, default_dpm_db).items():
    if k == 'db': conn_data['dpm_db'] = v
    else: conn_data[k] = v


#
# Connect to the db
#
try:
  nsconn = MySQLdb.connect(host=conn_data['host'], user=conn_data['user'], passwd=conn_data['pass'], db=conn_data['cns_db'])
except MySQLdb.Error as e:
  _log.error("Error Connecting to mysql. %d: %s", e.args[0], e.args[1])
  sys.exit (1)


try:
  dpmconn = MySQLdb.connect(host=conn_data['host'], user=conn_data['user'], passwd=conn_data['pass'], db=conn_data['dpm_db'])
except MySQLdb.Error as e:
  _log.error("Error Connecting to mysql. %d: %s", e.args[0], e.args[1])
  sys.exit (1)



#
# Init the xml generator
#
SR_NAMESPACE = "http://eu-emi.eu/namespaces/2011/02/storagerecord"
SR = "{%s}" % SR_NAMESPACE
NSMAP = {"sr": SR_NAMESPACE}
xmlroot = etree.Element(SR+"StorageUsageRecords", nsmap=NSMAP)



if options.reportgroups:
  #
  # Report about groups
  #
  _log.debug("Groups reporting: starting")

  gid2space = getreservedspace(dpmconn, 's_gid')
  gid2name = getdbkv(nsconn, 'Cns_groupinfo', 'gid', 'groupname')

  cursor = nsconn.cursor()
  cursor.execute('SELECT gid, COUNT(*), SUM(filesize) FROM Cns_file_metadata WHERE filemode & 16384 = 0 GROUP BY gid')

  for row in cursor:
    _log.debug(row)
    if row[0] not in gid2name: continue

    # update XML
    addrecord(xmlroot, options.hostname, gid2name[row[0]], None, site, row[1], gid2space.get(row[0], 0), row[2], options.validduration, record_id)

  _log.debug("Groups reporting: number of rows returned: %d", cursor.rowcount)

  cursor.close()



if options.reportusers:
  #
  # Report about users
  #
  _log.debug("Users reporting: starting")

  uid2space = getreservedspace(dpmconn, 's_uid')
  uid2name = getdbkv(nsconn, 'Cns_userinfo', 'userid', 'username')

  cursor = nsconn.cursor()
  cursor.execute ('SELECT owner_uid, COUNT(*), SUM(filesize) FROM Cns_file_metadata WHERE filemode & 16384 = 0 GROUP BY owner_uid')

  for row in cursor:
    _log.debug(row)
    if row[0] not in uid2name: continue

    # update XML
    addrecord(xmlroot, options.hostname, None, uid2name[row[0]], site, row[1], uid2space.get(row[0], 0), row[2], options.validduration, record_id)

  _log.debug("Users reporting: number of rows returned: %d", cursor.rowcount)

  cursor.close()


nsconn.close()
dpmconn.close()


# pretty string
out = sys.stdout
if sys.version_info >= (3,):
    out = sys.stdout.buffer
et = etree.ElementTree(xmlroot)
et.write(out, pretty_print=True, encoding="utf-8")


_log.debug('done')
