#!/bin/sh

# usage: externalstat.sh <lfn>

# This is an example script for the DOME file pull hooks
#
# If the filename ends with "dir" then this script will report that it's
# a directory. Otherwise it will be a regular file# that has 123456 as its size
#
# The companion pulling script will have to create such a file when invoked
#

if [[ $1 =~ .*dir$ ]] || [[ $1 =~ .*dir/$ ]]; then
    # It's a directory
    echo ">>>>> STAT 0 16384"
else
    # It's a file
    echo ">>>>> STAT 123456"
fi



