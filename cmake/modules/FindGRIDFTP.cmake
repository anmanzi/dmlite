#
# This module detects if Globus FTP (GridFTP) is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# GRIDFTP_LIBRARIES   = full path to the globus ftp libraries
# GRIDFTP_INCLUDE_DIR = include dir to be used when using the globus ftp library
# GLOBUS_CONFIG_DIR   = directory containing the globus_config header
# GRIDFTP_FOUND       = set to true if globus ftp was found successfully
#
# GLOBUS_LOCATION
#   setting this enables search for globus ftp libraries / headers in this location


# -----------------------------------------------------
# GRIDFTP Libraries
# -----------------------------------------------------
find_library(GRIDFTP_LIBRARIES
    NAMES globus_gridftp_server
    HINTS $ENV{GLOBUS_LOCATION}/lib $ENV{GLOBUS_LOCATION}/lib64 $ENV{GLOBUS_LOCATION}/lib32
    DOC "The main globus ftp server library"
)

# -----------------------------------------------------
# GRIDFTP Include Directories
# -----------------------------------------------------
find_path(GRIDFTP_INCLUDE_DIR
    NAMES globus/globus_gridftp_server.h
    HINTS $ENV{GLOBUS_LOCATION} $ENV{GLOBUS_LOCATION}/include
    DOC "The globus ftp server include directory"
)
if(GRIDFTP_INCLUDE_DIR)
    message(STATUS "globus ftp includes found in ${GRIDFTP_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# GLOBUS Config Include Directory
# -----------------------------------------------------
find_path(GLOBUS_CONFIG_DIR
    NAMES globus_config.h
    HINTS $ENV{GLOBUS_LOCATION} $ENV{GLOBUS_LOCATION}/include /usr/include/globus
	/usr/lib64/globus/include /usr/lib/globus/include /usr/lib32/globus/include
    DOC "The globus config header directory"
)
if(GLOBUS_CONFIG_DIR)
    message(STATUS "globus config header found in ${GLOBUS_CONFIG_DIR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set GRIDFTP_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gridftp DEFAULT_MSG GRIDFTP_LIBRARIES GRIDFTP_INCLUDE_DIR GLOBUS_CONFIG_DIR)
mark_as_advanced(GLOBUS_CONFIG_DIR GRIDFTP_INCLUDE_DIR GRIDFTP_LIBRARIES)
