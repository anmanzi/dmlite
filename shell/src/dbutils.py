from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import logging
try:
    import pymysql as MySQLdb
except ImportError:
    try:
        import MySQLdb
    except ImportError:
        sys.exit("Could not import MySQLdb, please install the MySQL Python module.")

_log = logging.getLogger('dmlite-shell')


class DPMDB(object):

    def __init__(self, interpreter):
        #check where to get the mysql conf from
        username = None
        password = None
        host = None
        port = 0
        nsDBName = 'cns_db'
        dpmDBName = 'dpm_db'
        self.dirhash = {}

        catalogImpl = interpreter.catalog.getImplId()
        if 'DomeAdapterHeadCatalog' not in catalogImpl:
            conf = open('/etc/dmlite.conf.d/mysql.conf', 'r')
            for line in conf:
                if line.startswith("MySqlHost"):
                    host = line[len("MySqlHost")+1:len(line)].strip()
                if line.startswith("MySqlUsername"):
                    username = line[len("MySqlUsername")+1:len(line)].strip()
                if line.startswith("MySqlPassword"):
                    password = line[len("MySqlPassword")+1:len(line)].strip()
                if line.startswith("MySqlPort"):
                    port = line[len("MySqlPort")+1:len(line)].strip()
                if line.startswith("NsDatabase"):
                    nsDBName = line[len("NsDatabase")+1:len(line)].strip()
                if line.startswith("DpmDatabase"):
                    dpmDBName = line[len("DpmDatabase")+1:len(line)].strip()
            conf.close()
        else:
            conf = open('/etc/domehead.conf', 'r')
            for line in conf:
                if line.startswith("head.db.host:"):
                    host = line[len("head.db.host:"):len(line)].strip()
                if line.startswith("head.db.user:"):
                    username = line[len("head.db.user:"):len(line)].strip()
                if line.startswith("head.db.password:"):
                    password = line[len("head.db.password:"):len(line)].strip()
                if line.startswith("head.db.port:"):
                    port = line[len("head.db.port:"):len(line)].strip()
                if line.startswith("head.db.cnsdbname:"):
                    nsDBName = line[len("head.db.cnsdbname:"):len(line)].strip()
                if line.startswith("head.db.dpmdbname:"):
                    dpmDBName = line[len("head.db.dpmdbname:"):len(line)].strip()
            conf.close()

        if int(port) == 0:
            port = 3306
        else:
            port = int(port)
        try:
            dpmdb = MySQLdb.connect(host=host, user=username, passwd=password, db=dpmDBName, port=port)
            self.dpmdb_c = dpmdb.cursor()
        except MySQLdb.Error as e:
            _log.error("Database connection to %s failed, error %d: %s", dpmDBName, e.args[0], e.args[1])
            raise e
        try:
            nsdb = MySQLdb.connect(host=host, user=username, passwd=password, db=nsDBName, port=port)
            self.nsdb_c = nsdb.cursor()
        except MySQLdb.Error as e:
            _log.error("Database connection to %s failed, error %d: %s", nsDBName, e.args[0], e.args[1])
            raise e

    def getReplicasInServer(self, server):
        """Method to get all the file replica for a single server."""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.host = '%(host)s'
                ''' % {"host": server})
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database server '%s' replica query failed, error %d: %s", server, e.args[0], e.args[1])
            raise e

    def getReplicasInFS(self, fsname, server):
        """Method to get all the file replica for a FS."""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.fs = '%(fs)s' AND r.host= '%(host)s'
                ''' % {"fs": fsname, "host": server})
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database server '%s' filesystem '%s' replica query failed, error %d: %s", server, fsname, e.args[0], e.args[1])
            raise e

    def getReplicaInFSFolder(self, fsname, server, folder):
        """Method to get all the file replica for a FS folder"""
        try:
            folder = server + ':' + fsname + '%' + folder + '%'
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.fs = '%(fs)s' AND r.host= '%(host)s' AND r.sfn LIKE '%(fold)s'
                ''' % {"fs": fsname, "host": server, "fold": folder})
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database server '%s' filesystem '%s' folder '%s' replica query failed, error %d: %s", server, fsname, folder, e.args[0], e.args[1])
            raise e

    def getReplicasInPool(self, poolname):
        """Method to get all the file replica for a pool"""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.poolname = '%(poolname)s'
                ''' % {"poolname": poolname})
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database pool '%s' replica query failed, error %d: %s", poolname, e.args[0], e.args[1])
            raise e

    def getPoolFromReplica(self, sfn):
        """Method to get the pool name from a sfn"""
        try:
            self.nsdb_c.execute('''
                SELECT r.poolname
                FROM Cns_file_replica r
                WHERE r.sfn= '%(sfn)s'
                ''' % {"sfn": sfn})
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(row[0])
            return ret
        except MySQLdb.Error as e:
            _log.error("Database pool from replica '%s' query failed, error %d: %s", sfn, e.args[0], e.args[1])
            raise e

    def getFilesystems(self, poolname):
        """get all filesystems in a pool"""
        try:
            self.dpmdb_c.execute('''
                SELECT poolname, server, fs, status, weight
                FROM dpm_fs
                WHERE poolname = '%(pool)s'
                ''' % {"pool": poolname})
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(FileSystem(row[0], row[1], row[2], row[3], row[4]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database query for pool '%s' filesystems failed, error %d: %s", poolname, e.args[0], e.args[1])
            raise e

    def getFilesystemsInServer(self, server):
        """get all filesystems in a server"""
        try:
            self.dpmdb_c.execute('''
                SELECT poolname, server, fs, status, weight
                FROM dpm_fs
                WHERE server = '%(server)s'
                ''' % {"server": server})
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(FileSystem(row[0], row[1], row[2], row[3], row[4]))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database query for server '%s' filesystems failed, error %d: %s", server, e.args[0], e.args[1])
            raise e

    def getServers(self, pool):
        """get all server in a server"""
        try:
            self.dpmdb_c.execute('''
                SELECT server
                FROM dpm_fs
                WHERE poolname = '%(pool)s'
                ''' % {"pool": pool})
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(row[0])
            return ret
        except MySQLdb.Error as e:
            _log.error("Database query for pool '%s' servers failed, error %d: %s", pool, e.args[0], e.args[1])
            raise e

    def getPoolFromFS(self, server, fsname):
        """get the pool related to FS"""
        try:
            self.dpmdb_c.execute('''
                SELECT poolname
                FROM dpm_fs
                WHERE server = '%(server)s'  AND fs= '%(fsname)s'
                ''' % {"server": server, "fsname": fsname})
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(row[0])
            return ret
        except MySQLdb.Error as e:
            _log.error("Database query for pool from server '%s' and filesystem '%s' failed, error %d: %s", server, fsname, e.args[0], e.args[1])
            raise e

    def getGroupByGID(self, gid):
        """get groupname by gid """
        try:
            self.nsdb_c.execute('''
                SELECT groupname
                FROM Cns_groupinfo
                WHERE gid = '%(gid)s'
                ''' % {"gid": gid})
            row = self.nsdb_c.fetchone()
            return row[0]
        except MySQLdb.Error as e:
            _log.error("Database query for group by gid '%i' failed, error %d: %s", gid, e.args[0], e.args[1])
            raise e

    def getGroupIdByName(self, groupname):
        """get groupid by name """
        try:
            self.nsdb_c.execute('''
                SELECT gid
                FROM Cns_groupinfo
                WHERE groupname = '%(groupname)s'
                ''' % {"groupname": groupname})
            row = self.nsdb_c.fetchone()
            return row[0]
        except MySQLdb.Error as e:
            _log.error("Database query for gid by group '%s' failed, error %d: %s", groupname, e.args[0], e.args[1])
            raise e

    def find(self, pattern, folder):
        """retrieve a list of sfn matching a pattern"""
        comparison = '='
        ret = list()
        if folder:
            comparison = '!='

        try:
            self.nsdb_c.execute('''
                select name, fileid, parent_fileid, filemode from Cns_file_metadata
                where name like '%%%(pattern)s%%' and filemode&32768 %(comparison)s 32768
                ''' % {"pattern": pattern, "comparison": comparison})
            for row in self.nsdb_c.fetchall():
                name, fileid, parent_fileid, filemode = row
                namelist = self.getLFN(parent_fileid) + [str(name)]
                ret.append('/'.join(namelist))
            return ret
        except MySQLdb.Error as e:
            _log.error("Database query for file pattern '%s' failed, error %d: %s", pattern, e.args[0], e.args[1])
            raise e
        except ValueError as v:
            _log.error("Failed to evaluate pattern '%s': %s", pattern, str(v))
            raise v

    def getLFNFromSFN(self, sfn):
        """get LFN from sfn"""
        sfn = sfn.replace('"', '')
        try:
            self.nsdb_c.execute('''
                SELECT parent_fileid, name
                FROM Cns_file_replica JOIN Cns_file_metadata ON Cns_file_replica.fileid = Cns_file_metadata.fileid
                WHERE Cns_file_replica.sfn="%s"''' % sfn)
            row = self.nsdb_c.fetchone()
            if row == None:
                raise ValueError("no replica metadata")
            parent_fileid, name = row
            namelist = self.getLFN(parent_fileid) + [str(name)]
            return '/'.join(namelist)
        except MySQLdb.Error as e:
            _log.error("Database query for sfn '%s' failed, error %d: %s", sfn, e.args[0], e.args[1])
            return None
        except ValueError as e:
            _log.error("Failed to get LFN from sfn '%s': %s", sfn, str(e))
            return None

    def getLFNFromIno(self, fileid):
        """get LFN from sfn"""
        try:
            namelist = self.getLFN(fileid)
            return '/'.join(namelist)
        except MySQLdb.Error as e:
            _log.error("Database query for fileid '%s' failed, error %d: %s", fileid, e.args[0], e.args[1])
            return None
        except ValueError as e:
            _log.error("Failed to get LFN from fileid '%s': %s", fileid, str(e))
            return None

    def getLFN(self, fileid):
        """get LFN from fileid"""
        namelist = []
        parent_fileid = fileid
        while parent_fileid > 0:
            key = str(parent_fileid)
            if key in self.dirhash:
                name, parent_fileid = self.dirhash[key]
                namelist.append(str(name))
            else:
                self.nsdb_c.execute('''SELECT parent_fileid, name FROM Cns_file_metadata WHERE fileid = %s''' % parent_fileid)
                row = self.nsdb_c.fetchone()
                if row == None:
                    raise ValueError("no parent for %s" % parent_fileid)
                parent_fileid, name = row
                self.dirhash[key] = (name, parent_fileid)
                namelist.append(str(name))
        namelist.reverse() #put entries in "right" order for joining together
        return [''] + namelist[1:] #and sfn and print dpns name (minus srm bits)


# Get a filesytem information from db
class FileReplica(object):

    def __init__(self, name, poolname, fsname, host, sfn, size, gid, status, replicastatus, setname, ptime):
        self.name = name
        self.poolname = poolname
        self.fsname = fsname
        self.host = host
        self.sfn = sfn
        self.size = size
        self.gid = gid
        self.status = status
        self.replicastatus = replicastatus
        self.setname = setname
        self.pinnedtime = ptime
        self.lfn = None

    def __repr__(self):
        return "FileReplica(name=" + self.name + ", poolname=" + self.poolname + ", server=" + self.host + ", fsname=" + self.fsname + ", sfn=" + self.sfn + ", size=" + str(self.size) + ", gid=" + str(self.gid) + ", status=" + self.status + ", replicastatus=" + self.replicastatus + ", setname=" + self.setname + ", pinnedtime=" + str(self.pinnedtime) + ")"


# Get a filesytem information from db
class FileSystem(object):

    def __init__(self, poolname, server, name, status, weight):
        self.poolname = poolname
        self.server = server
        self.name = name
        self.status = status
        self.files = list()
        # Filled in by annotateFreeSpace()
        self.size = None
        self.avail = None
        self.status = status
        self.weight = weight

    def desc(self):
        """Short description of this location - i.e. a minimal human readable description of what this is"""
        return str(self.server) + str(self.name)

    def fileCount(self):
        """Support for using these to track dirs at a time"""
        return len(self.files)

    def __repr__(self):
        return "FileSystem(poolname=" + self.poolname + ", server=" + self.server + ", name=" + self.name + ", status=" + str(self.status) + ", weight=" + str(self.weight) + ", with " + str(len(self.files)) + " files and " + str(self.avail) + " 1k blocks avail)"
