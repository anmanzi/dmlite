#!/usr/bin/python2
#
# Dump DPM DOME information in a format that can be scraped
# by prometheus Node exporter text collector
#
# Usage:
# * headnode
#   dmlite-prom.py --config /etc/domehead.conf --headnode -o /var/lib/prometheus-exporters/node_exporter/textfile_collector/dmlite_metrics.prom
# * disknode
#   dmlite-prom.py --config /etc/domedisk.conf -o /var/lib/prometheus-exporters/node_exporter/textfile_collector/dmlite_metrics.prom
# * node_exporter
#   node_exporter --collector.textfile.directory /var/lib/prometheus-exporters/node_exporter/textfile_collector ...
#
import os, sys, re
import optparse
import socket
import logging, logging.handlers
from io import StringIO
try:
    from urllib.parse import urlparse
    from urllib.request import urlopen
except ImportError:
    from urlparse import urlparse
    from urllib2 import urlopen

__version__ = '0.0.1'
__author__ = 'Petr Vokac'

_log = logging.getLogger('DPMPROM')

DEFAULT_HOST = socket.getfqdn()
DEFAULT_HEAD_PORT = 1094
DEFAULT_DISK_PORT = 1095
DEFAULT_DOME_CONFIG = '/etc/domehead.conf'
DEFAULT_URLPREFIX = '/domehead/'
DEFAULT_CERT = '/etc/grid-security/dpmmgr/dpmcert.pem'
DEFAULT_KEY = '/etc/grid-security/dpmmgr/dpmkey.pem'
DEFAULT_CAPATH = '/etc/grid-security/certificates/'


def parseConfig(filename):
    _log.debug("parsing config file %s", filename)
    ret = {}
    with open(filename) as f:
        reComment = re.compile('^ *#')
        reKeyValue = re.compile('^(.*?):\s*(.*?)\s*$')
        for line in f.readlines():
            if reComment.match(line): continue
            res = reKeyValue.match(line)
            if res == None: continue
            k = res.group(1)
            v = res.group(2)
            ret[k] = v
    return ret


def getDomeInfo(url):
    _log.debug("get info from DOME: %s", url)

    ret = {}

    f = urlopen(url)
    for line in f.readlines():
        res = re.match(b'^dome \[(.*?)\] running as (\S+)', line)
        if res != None:
            ret['version'] = res.group(1).decode('utf-8')
            ret['flavor'] = res.group(2).decode('utf-8')
        res = re.match(b'^Total: (\d+) .*Free: (\d+)', line)
        if res != None:
            ret['space_total'] = int(res.group(1))
            ret['space_free'] = int(res.group(2))
        res = re.match(b'^Server PID: (\d+)', line)
        if res != None:
            ret['pid'] = int(res.group(1))
        res = re.match(b'^Request rate: (.*?)Hz \(Peak: (.*?)Hz\) .*DB queries: (.*?)Hz .*DB transactions: (.*?)Hz .*Intercluster messages: (.*?)Hz', line)
        if res != None:
            ret['request_rate'] = float(res.group(1))
            ret['request_rate_peak'] = float(res.group(2))
            ret['db_query_rate'] = float(res.group(3))
            ret['db_transaction_rate'] = float(res.group(4))
            ret['intercluster_rate'] = float(res.group(5))
        res = re.match(b'^Queue checksum: (\d+) .*Queue file pull: (\d+)', line)
        if res != None:
            ret['queue_checksum'] = int(res.group(1))
            ret['queue_filepull'] = int(res.group(2))

    if 'version' not in ret:
        raise Exception("retreived data not DOME info")

    return ret


#=====================================================================
# main
#=====================================================================
if __name__ == '__main__':
    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    # parse options from command line
    def opt_set_loglevel(option, opt, value, parser):
        loglevel = option.default
        if value != None:
            loglevel = int({
                'CRITICAL': logging.CRITICAL,
                'DEBUG': logging.DEBUG,
                'ERROR': logging.ERROR,
                'FATAL': logging.FATAL,
                'INFO': logging.INFO,
                'NOTSET': logging.NOTSET,
                'WARN': logging.WARN,
                'WARNING': logging.WARNING,
            }.get(value, value))
        if loglevel < 0:
            # relative log level for multiple -vvv argument
            loglevel += _log.getEffectiveLevel()
            if loglevel < 1: loglevel = 1
        _log.setLevel(loglevel)
        setattr(parser.values, option.dest, loglevel)

    # command line arguments
    usage = "usage: %prog [options]"
    description = "DPM DOME info for prometheus Node exporter text collector"
    epilog = """Example:\n{0} --config=/etc/domehead.conf --headnode""".format(sys.argv[0])
    parser = optparse.OptionParser(usage=usage, description=description, epilog=epilog, version="%prog")
    parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=-10, help="each \"v\" increases the verbosity level")
    parser.add_option("--debug", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    #parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    parser.add_option("-q", "--quiet", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.ERROR, help="set log level to ERROR")
    parser.add_option("--log-level", dest="loglevel", action="callback", callback=opt_set_loglevel, type="string", help="set log level, default: %default")
    parser.add_option("--log-file", dest="logfile", metavar="FILE", help="set log file, default: %default")
    parser.add_option("--log-size", dest="logsize", type="int", default=10*1024*1024, help="maximum size of log file, default: %default")
    parser.add_option("--log-backup", dest="logbackup", type="int", default=4, help="number of log backup files, default: %default")
    parser.add_option("-c", "--config", dest="config", help="DOME config file")
    parser.add_option('--host', dest='host', help="DOME host, if no DOME config given")
    parser.add_option('--port', dest='port', help="DOME port, if no DOME config given")
    parser.add_option('--urlprefix', dest='urlprefix', help="DOME base url prefix, if no config given")
    parser.add_option('--headnode', dest='headnode', action='store_true', default=False, help="DOME headnode, default: %default")
    parser.add_option("--cert", dest='cert', help="DOME host certificate, if no config given")
    parser.add_option("--key", dest='key', help="DOME host key, if no config given")
    #parser.add_option('--dbhost', dest='dbhost', help="database host, if no config given")
    #parser.add_option('--dbport', dest='dbport', help="database port, if no config given")
    #parser.add_option('--dbuser', dest='dbuser', help="database user, if no config given")
    #parser.add_option('--dbpwd', dest='dbpwd', help="database password, if no config given")
    #parser.add_option('--dbname', dest='dbname', help="database name, if no config given")
    #parser.add_option('--dbdpm', dest='dbdpm', help="DPM database name, if no config given")
    parser.add_option("-o", "--output", dest="output", default='<stdout>', help="Output file, default: %default")


    (options, args) = parser.parse_args()

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    import getpass
    import inspect
    import hashlib
    _log.info("command: %s", " ".join(sys.argv))
    _log.debug("script: %s", os.path.abspath(inspect.getfile(inspect.currentframe())))
    _log.debug("version: %s", __version__)
    _log.debug("sha256: %s", hashlib.sha256(open(__file__).read().encode('utf-8')).hexdigest())
    _log.debug("python: %s", str(sys.version_info))
    _log.debug("user: %s@%s", getpass.getuser(), socket.gethostname())
    _log.debug("system load: %s", str(os.getloadavg()))

    # validate command line options
    if not options.config and not options.host:
        _log.error("Missing required configuration options \"config\" or \"host\"")
        sys.exit(1)

    config = {}
    if options.config:
        if not os.path.exists(options.config):
            _log.error("DOME configuration file %s doesn't exist", options.config)
            sys.exit(1)
        config = parseConfig(options.config)

    dome_headnode = options.headnode or config.get('glb.role') == 'head'
    dome_host = options.host if options.host else DEFAULT_HOST
    dome_port = options.port if options.port else DEFAULT_HEAD_PORT if dome_headnode else DEFAULT_DISK_PORT
    dome_urlprefix = options.urlprefix if options.urlprefix else config.get('glb.auth.urlprefix', DEFAULT_URLPREFIX)
    if not dome_urlprefix.startswith('/'): dome_urlprefix = "/{1}".format(dome_urlprefix)
    if not dome_urlprefix.endswith('/'): dome_urlprefix = "{1}/".format(dome_urlprefix)

    dome_cert = options.cert if options.cert else config.get('glb.restclient.cli_certificate', DEFAULT_CERT)
    dome_key = options.key if options.key else config.get('glb.restclient.cli_private_key', DEFAULT_KEY)
    dome_capath = DEFAULT_CAPATH

    _log.debug("start")

    out = StringIO()

    # get dome_info data directly from running DOME
    try:
        dome_info_url = "https://{0}:{1}{2}dome_info".format(dome_host, dome_port, dome_urlprefix)
        data = getDomeInfo(dome_info_url)

        out.write(u'# HELP dmlite_version DPM DOME dmlite version\n')
        out.write(u'# TYPE dmlite_version gauge\n')
        out.write(u'dmlite_version{version="%s",flavor="%s"} 1\n' % (data['version'], data['flavor']))

        for key in ['space_total', 'space_free', 'pid', 'request_rate', 'request_rate_peak', 'db_query_rate', 'db_transaction_rate', 'intercluster_rate', 'queue_checksum', 'queue_filepull']:
            if key not in data: continue
            out.write(u'# HELP dmlite_%s DPM DOME %s\n' % (key, key))
            out.write(u'# TYPE dmlite_%s gauge\n' % key)
            out.write(u'dmlite_%s %s\n' % (key, data[key]))

    except Exception as e:
        _log.error("unable to get DPM DOME info: %s", str(e))

    # talk directly with DOME to get additional headnode/global info
    if dome_headnode:
        _log.debug("headnode")

        try:
            import json
            import dmliteshell.executor

            dome_command_base = "https://{0}:{1}{2}command".format(dome_host, dome_port, dome_urlprefix)
            dshell = dmliteshell.executor.DomeExecutor(dome_cert, dome_key, dome_capath, '', '')

            si, err = dshell.getSpaceInfo(dome_command_base)
            if err:
                raise Exception("Unable to get DPM DOME storage info ({0})".format(str(si)))

            jsi = json.loads(si)

            out.write(u'# HELP dmlite_fsinfo_size Physical size of disknodes filesystems\n')
            out.write(u'# TYPE dmlite_fsinfo_size gauge\n')
            for disknode, fsdata in sorted(jsi.get('fsinfo', {}).items()):
                for fspath, pathdata in sorted(fsdata.items()):
                    out.write(u"dmlite_fsinfo_size{disknode=\"%s\",filesystem=\"%s\",pool=\"%s\",status=\"%s\"} %s\n" % (disknode, fspath, pathdata['poolname'], pathdata['fsstatus'], pathdata['physicalsize']))
            out.write(u'# HELP dmlite_fsinfo_free Free space on disknode filesystems\n')
            out.write(u'# TYPE dmlite_fsinfo_free gauge\n')
            for disknode, fsdata in sorted(jsi.get('fsinfo', {}).items()):
                for fspath, pathdata in sorted(fsdata.items()):
                    out.write(u"dmlite_fsinfo_free{disknode=\"%s\",filesystem=\"%s\",pool=\"%s\",status=\"%s\"} %s\n" % (disknode, fspath, pathdata['poolname'], pathdata['fsstatus'], pathdata['freespace']))

            out.write(u'# HELP dmlite_poolinfo_size Size of DPM pool\n')
            out.write(u'# TYPE dmlite_poolinfo_size gauge\n')
            for poolname, pooldata in sorted(jsi.get('poolinfo', {}).items()):
                out.write(u"dmlite_poolinfo_size{pool=\"%s\",status=\"%s\"} %s\n" % (poolname, pooldata['poolstatus'], pooldata['physicalsize']))
            out.write(u'# HELP dmlite_poolinfo_free Free space on DPM pool\n')
            out.write(u'# TYPE dmlite_poolinfo_free gauge\n')
            for poolname, pooldata in sorted(jsi.get('poolinfo', {}).items()):
                out.write(u"dmlite_poolinfo_free{pool=\"%s\",status=\"%s\"} %s\n" % (poolname, pooldata['poolstatus'], pooldata['freespace']))
            out.write(u'# HELP dmlite_poolinfo_reserved Reserved min free space on DPM pool\n')
            out.write(u'# TYPE dmlite_poolinfo_reserved gauge\n')
            for poolname, pooldata in sorted(jsi.get('poolinfo', {}).items()):
                out.write(u"dmlite_poolinfo_reserved{pool=\"%s\",status=\"%s\"} %s\n" % (poolname, pooldata['poolstatus'], pooldata['defsize']))

            qt, err = dshell.getquotatoken(dome_command_base, '/', 0, 1)
            if err:
                raise Exception("Unable to get DPM DOME quotatoken ({0})".format(str(qt)))

            jqt = json.loads(qt)

            out.write(u'# HELP dmlite_quotatoken_size Quotatoken size\n')
            out.write(u'# TYPE dmlite_quotatoken_size gauge\n')
            for qtdata in sorted(jqt.values(), key=lambda x: x['path']):
                out.write(u"dmlite_quotatoken_size{name=\"%s\",pool=\"%s\",path=\"%s\"} %s\n" % (qtdata['quotatkname'], qtdata['quotatkpoolname'], qtdata['path'], qtdata['quotatktotspace']))
            out.write(u'# HELP dmlite_quotatoken_free Quotatoken free space\n')
            out.write(u'# TYPE dmlite_quotatoken_free gauge\n')
            for qtdata in sorted(jqt.values(), key=lambda x: x['path']):
                out.write(u"dmlite_quotatoken_free{name=\"%s\",pool=\"%s\",path=\"%s\"} %s\n" % (qtdata['quotatkname'], qtdata['quotatkpoolname'], qtdata['path'], qtdata['pathfreespace']))
            out.write(u'# HELP dmlite_quotatoken_used Quotatoken used space\n')
            out.write(u'# TYPE dmlite_quotatoken_used gauge\n')
            for qtdata in sorted(jqt.values(), key=lambda x: x['path']):
                out.write(u"dmlite_quotatoken_used{name=\"%s\",pool=\"%s\",path=\"%s\"} %s\n" % (qtdata['quotatkname'], qtdata['quotatkpoolname'], qtdata['path'], qtdata['pathusedspace']))

            out.write(u'# HELP dmlite_directory_used Directory size\n')
            out.write(u'# TYPE dmlite_directory_used gauge\n')
            for qtdata in sorted(jqt.values(), key=lambda x: x['path']):
                path = qtdata['path']
                try:
                    ds, err = dshell.getdirspaces(dome_command_base, path)
                    if err:
                        raise Exception("Unable to get DPM DOME dirspaces ({0})".format(str(ds)))
                    dirdata = json.loads(ds)
                    out.write(u"dmlite_directory_used{name=\"%s\",pool=\"%s\",path=\"%s\"} %s\n" % (dirdata['quotatoken'], dirdata['poolname'], path, dirdata['dirusedspace']))
                except Exception as e:
                    _log.error("uanble to get DOME directory details for %s: %s", path, str(e))

        except Exception as e:
            _log.error("unable to get DOME additional info: %s", str(e))

    _log.debug("write output")

    if options.output in [None, '-', '<stdout>']:
        sys.stdout.write(out.getvalue())
    elif options.output == '<stderr>':
        sys.stderr.write(out.getvalue())
    else:
        try:
            f = open(options.output, 'w')
            f.write(out.getvalue())
            f.close()
        except Exception as e:
            _log.error("unable to write output to %s: %s", options.output, str(e))
            if os.path.exists(options.output):
                try:
                    os.unlink(options.output)
                except Exception as e:
                    _log.error("unable to remove output file %s: %s", options.output, str(e))

    _log.debug("done")
