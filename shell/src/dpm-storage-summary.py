#!/usr/bin/python2
# WLCG Storage Resource Reporting implementation for DPM
# * https://docs.google.com/document/d/1yzCvKpxsbcQC5K9MyvXc-vBF1HGPBk4vhjw3MEXoXf8/edit
# * https://twiki.cern.ch/twiki/bin/view/LCG/AccountingTaskForce
# Configuration
# * https://twiki.cern.ch/twiki/bin/view/DPM/DpmSetupManualInstallation#Publishing_space_usage
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import json
import time
import socket
import argparse
import tempfile
import os, sys
import logging, logging.handlers
from dmliteshell.infoutils import SystemInfo

__version__ = '0.1.1'

_log = logging.getLogger('DPMSRR')


# The top level object
class storageService:

    def __init__(self, config):
        self.config = config
        self.entry = {}
        info = SystemInfo(self.config)
        self.entry["name"] = self.config.host
        self.entry["implementation"] = "DPM"
        self.entry["implementationversion"] = info.getsysinfo("dome")
        self.entry["qualitylevel"] = "production"
        self.entry["storageshares"] = []
        self.entry["storageendpoints"] = []
        self.entry["datastores"] = []
        self.entry["latestupdate"] = int(time.time())

    def addshares(self):
        info = SystemInfo(self.config)
        jgqt, totalcapacity, totalused, totalgroups = info.getspaces()
        for space, qt in jgqt.items():
            quotatktotspace = max(0, int(qt["quotatktotspace"]))
            pathusedspace = max(0, int(qt["pathusedspace"]))
            self.entry["storageshares"].append(storageShare(qt["quotatkname"], quotatktotspace, pathusedspace, qt["path"], qt["groups"]))

    def addendpoints(self):
        self.entry["storageendpoints"].append(storageEndpoint("gsiftp", "gsiftp://" + self.config.host + "/", "gsiftp"))
        self.entry["storageendpoints"].append(storageEndpoint("https", "https://" + self.config.host + "/", "https"))
        self.entry["storageendpoints"].append(storageEndpoint("xrootd", "root://" + self.config.host + "/", "xrootd"))
        self.entry["storageendpoints"].append(storageEndpoint("srm", "srm://" + self.config.host + ":8446/srm/managerv2?SFN=/", "srm"))

    def adddataStores(self):
        self.entry["datastores"].append(dataStore("DPM data store"))

    def printjson(self, out=sys.stdout):
        out.write(json.dumps({"storageservice": self.entry}, indent=4))

    def publish_https(self):
        try:
            import pycurl
        except ImportError as e:
            raise Exception("unable to import pycurl module (install python2-pycurl or python3-pycurl package): {0}".format(str(e)))

        urlbase = "https://%s%s" % (self.config.host, self.config.path)
        if self.config.davport != 443:
            urlbase = "https://%s:%s%s" % (self.config.host, self.config.davport, self.config.path)
        urlfile = "%s/%s" % (urlbase, self.config.file)
        urlfilenew = "%s/%s.%i" % (urlbase, self.config.file, int(time.time()))

        c = pycurl.Curl()
        c.setopt(c.SSLCERT, self.config.cert)
        c.setopt(c.SSLKEY, self.config.key)
        c.setopt(c.SSL_VERIFYPEER, 0)
        c.setopt(c.SSL_VERIFYHOST, 2)
        c.setopt(c.FOLLOWLOCATION, 1)
        if _log.getEffectiveLevel() < logging.DEBUG:
            c.setopt(c.VERBOSE, True)

        try:
            _log.debug("check if base path %s exists", urlbase)
            c.setopt(c.URL, urlbase)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "HEAD")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 200:
                raise Exception("base path %s not found" % urlbase)

            _log.debug("put the new file to %s", urlfilenew)
            c.setopt(c.URL, urlfilenew)
            c.setopt(c.NOBODY, False)
            c.setopt(c.CUSTOMREQUEST, "PUT")
            # suppress the response body
            c.setopt(c.WRITEFUNCTION, lambda x: None)
            c.setopt(c.POSTFIELDS, json.dumps({"storageservice": self.entry}, indent=4))
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 201:
                raise Exception("unable to put new file %s (HTTP code %s)" % (urlfilenew, c.getinfo(c.HTTP_CODE)))

            _log.debug("delete existing file %s", urlfile)
            c.setopt(c.URL, urlfile)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "DELETE")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 204 and c.getinfo(c.HTTP_CODE) != 404:
                raise Exception("unable to delete file %s (HTTP code %i)" % (urlfile, c.getinfo(c.HTTP_CODE)))

            _log.debug("rename the new file %s to %s", urlfilenew, urlfile)
            c.setopt(c.URL, urlfilenew)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "MOVE")
            c.setopt(c.HTTPHEADER, ["Destination:%s" % urlfile])
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 201:
                raise Exception("unable to rename %s to %s (HTTP code %i)" % (urlfilenew, urlfile, c.getinfo(c.HTTP_CODE)))

            _log.info("%s written successfully" % urlfile)

        except Exception as e:
            _log.error("file writing error: %s", str(e))

            _log.debug("delete temporary file %s", urlfilenew)
            c.setopt(c.URL, urlfilenew)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "DELETE")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 204:
                if c.getinfo(c.HTTP_CODE) != 404:
                    _log.error("unable to cleanup temporary file %s (http code %i)", urlfilenew, c.getinfo(c.HTTP_CODE))
                else:
                    _log.info("temporary file %s doesn't exist", urlfilenew)

            raise Exception("failed to upload %s with HTTP protocol".format(urlfile))

        finally:
            _log.debug("cleanup")
            c.close()

    def publish_xrootd(self):
        # set environment for XRootD transfers
        # XRD_* env variables must be set before importing XRootD module
        if _log.getEffectiveLevel() < logging.DEBUG:
            os.putenv('XRD_LOGLEVEL', 'Dump')
            os.putenv('XRD_LOGMASK', 'All')
            #os.putenv('XRD_LOGFILE', '/tmp/xrootd.debug')
        os.putenv('XRD_CONNECTIONWINDOW', '10') # by default connection timeouts after 300s
        #os.putenv('XRD_REQUESTTIMEOUT', '10') # can be set individually for each operation

        # set X509_* env variable used by XRootD authentication
        os.putenv('X509_USER_CERT', self.config.cert)
        os.putenv('X509_USER_KEY', self.config.key)

        try:
            import XRootD.client
        except ImportError as e:
            raise Exception("unable to import XRootD module (install python2-xrootd or python34-xrootd package): {0}".format(str(e)))

        urlhost = "root://{0}".format(self.config.host)
        if self.config.port != 1094:
            urlhost = "{0}:{1}".format(self.config.host, self.config.port)
        filename = "{0}/{1}".format(self.config.path, self.config.file)
        filenamenew = "{0}.{1}".format(filename, int(time.time()))
        filenameold = "{0}.bak".format(filenamenew)
        urlfile = "{0}/{1}".format(urlhost, filename)
        urlfilenew = "{0}/{1}".format(urlhost, filenamenew)
        urlfileold = "{0}/{1}".format(urlhost, filenameold)

        cleanup = []
        try:
            _log.debug("create temporary file with storage json")
            fd, tmpfile = tempfile.mkstemp()
            cleanup.append(tmpfile)
            with os.fdopen(fd, 'w') as f:
                f.write(json.dumps({"storageservice": self.entry}, indent=4))

            xrdc = XRootD.client.FileSystem(urlhost)

            _log.debug("refresh storage json uploading the temporary file")

            status, details = xrdc.copy(tmpfile, urlfilenew, force=True)
            if not status.ok:
                raise Exception("unable to copy data to {0}: {1}".format(urlfilenew, status.message))
            cleanup.append(urlfilenew)

            status, detail = xrdc.stat(filename)
            if status.ok:
                status, details = xrdc.mv(filename, filenameold)
                if not status.ok:
                    raise Exception("unable to move {0} to {1}: {2}".format(filename, filenameold, status.message))
                cleanup.append(urlfileold)
            else:
                _log.info("no previous version of %s: %s", filename, status.message)

            status, details = xrdc.mv(filenamenew, filename)
            if not status.ok:
                # move old file back in case of problems with new file
                status1, details1 = xrdc.mv(filenameold, filename)
                if status1.ok:
                    cleanup.remove(urlfileold)
                raise Exception("unable to move {0} to {1}: {2}".format(filenamenew, filename, status.message))
            cleanup.remove(urlfilenew)

            _log.info("%s written successfully", urlfile)

        except Exception as e:
            _log.error(str(e))
            raise Exception("failed to upload {0} with XRootD protocol".format(filename))

        finally:
            # cleanup temporary files
            for c in cleanup:
                try:
                    if c.startswith('root://'): xrdc.rm(c)
                    else: os.unlink(c)
                except Exception as e:
                    _log.debug("unable to remove temporary file %s: %s", c, str(e))


# Derive from dict for easy serialisation
class storageEndpoint(dict):

    def __init__(self, name, endpointurl, interfacetype):
        self["name"] = name
        self["endpointurl"] = endpointurl
        self["interfacetype"] = interfacetype
        self["qualitylevel"] = "production"
        self["assignedshares"] = ["all"]


# Derive from dict for easy serialisation
class storageShare(dict):

    def __init__(self, name, totalsize, usedsize, path, groups):
        self["name"] = name
        self["timestamp"] = int(time.time())
        self["totalsize"] = totalsize
        self["usedsize"] = usedsize
        self["path"] = [path]
        self["vos"] = groups
        self["assignedendpoints"] = ["all"]
        self["servingstate"] = "open"


# Derive from dict for easy serialisation
class dataStore(dict):

    def __init__(self, name):
        self["name"] = name
        self["message"] = "Not used"


#=====================================================================
# main
#=====================================================================
if __name__ == '__main__':
    import inspect

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    fqdn = socket.getfqdn()
    domain = '.'.join(fqdn.split('.')[1:])
    if domain == '': domain = 'cern.ch'

    # parse options from command line
    class VerbosityAction(argparse.Action):

        def __call__(self, parser, namespace, values, option_string=None):
            loglevel = self.default
            if len(values) > 0:
                loglevel = int({
                    'CRITICAL': logging.CRITICAL,
                    'DEBUG': logging.DEBUG,
                    'ERROR': logging.ERROR,
                    'FATAL': logging.FATAL,
                    'INFO': logging.INFO,
                    'NOTSET': logging.NOTSET,
                    'WARN': logging.WARN,
                    'WARNING': logging.WARNING,
                }.get(values[0].upper(), values[0]))
            _log.setLevel(loglevel)
            setattr(namespace, self.dest, loglevel)

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", dest="loglevel", action=VerbosityAction, default=logging.DEBUG, nargs=0, help="set log level to DEBUG")
    parser.add_argument("-q", "--quiet", dest="loglevel", action=VerbosityAction, default=logging.ERROR, nargs=0, help="set log level to ERROR")
    parser.add_argument("--log-level", dest="loglevel", action=VerbosityAction, nargs=1, help="set log level, default: %(default)s")
    parser.add_argument("--log-file", dest="logfile", metavar="FILE", help="set log file (default: %(default)s)")
    parser.add_argument("--log-size", dest="logsize", type=int, default=10 * 1024 * 1024, help="maximum size of log file, default: %(default)s")
    parser.add_argument("--log-backup", dest="logbackup", type=int, default=4, help="number of log backup files, default: %(default)s")
    # storage summary arguments
    parser.add_argument("--path", help="Path to publish the summary file, default: %(default)s", default="/dpm/{0}/home/dteam".format(domain))
    parser.add_argument("--file", help="Name of the summary file, default: %(default)s", default="storagesummary.json")
    parser.add_argument("--cert", help="Path to host certificate, default: %(default)s", default="/etc/grid-security/hostcert.pem")
    parser.add_argument("--key", help="Path to host key, default: %(default)s", default="/etc/grid-security/hostkey.pem")
    parser.add_argument("--host", help="FQDN, default: %(default)s", default=fqdn)
    parser.add_argument("--print", help="Just print, don't publish", action="store_true", dest="prnt")
    parser.add_argument("--proto", help="Publish protocol, default %(default)s", default="https")
    parser.add_argument("--port", help="Dome's port number, default: %(default)s", default=1094)
    parser.add_argument("--davport", help="Webdav's port, default: %(default)s", default=443)

    options = parser.parse_args()

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    _log.info("command: %s", " ".join(sys.argv))
    _log.info("script: %s", os.path.abspath(inspect.getfile(inspect.currentframe())))
    _log.info("version: %s", __version__)
    _log.info("python: %s", str(sys.version_info))

    try:
        # Create object
        dpm = storageService(options)
        dpm.addshares()

        # Which endpoints are supposed to be there?
        dpm.addendpoints()
        # Don't bother with this for now
        #dpm.adddataStores()

        # Print or publish
        if options.prnt:
            dpm.printjson()
        elif options.proto in ['https', 'davs']:
            dpm.publish_https()
        elif options.proto in ['root', 'xroot']:
            dpm.publish_xrootd()
        else:
            _log.error("unknown publish protocol %s", options.proto)

    except Exception as e:
        _log.error(str(e))
        sys.exit(1)

    sys.exit(os.EX_OK)
