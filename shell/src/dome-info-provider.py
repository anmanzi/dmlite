#!/usr/bin/python2

import argparse
import socket

from dmliteshell.infoutils import *


def create_shares(config):
    """Find all the StorageShares on this DPM
    and return an array of them"""
    sysinfo = SystemInfo(config)
    jgqt, totalcapacity, totalused, totalgroups = sysinfo.getspaces()
    ret = []
    for space, qt in jgqt.items():
        share = Share(qt["path"], qt["quotatkpoolname"], qt["groups"], qt["quotatkname"], config.host)
        scap = ShareCapacity(qt["quotatkname"], int(qt["quotatktotspace"]), int(qt["pathusedspace"]), config.host)
        scap.set_foreign_key(share.getname())
        share.add_child(scap)

        mapp = MappingPolicy(qt["quotatkname"], qt["groups"], config.host)
        mapp.set_foreign_key(share.getname())
        share.add_child(mapp)

        ret.append(share)
    return ret


def create_endpoints(config):
    """Find all the StorageEndpoints on this DPM
    and return an array of them"""
    sysinfo = SystemInfo()

    cert_subject = None
    try:
        from M2Crypto import X509
        x509 = X509.load_cert(config.cert, X509.FORMAT_PEM)
        cert_subject = "/%s" % '/'.join(x509.get_issuer().as_text().split(', '))
    except:
        pass

    ret = []
    if 2811 in sysinfo.ports:
        ret.append(Endpoint("gsiftp", config.host, 2811, "", cert_subject, *sysinfo.getprotinfo("gsiftp")))
    if 443 in sysinfo.ports:
        ret.append(Endpoint("https", config.host, 443, "", cert_subject, *sysinfo.getprotinfo("https")))
    if 1094 in sysinfo.ports:
        ret.append(Endpoint("xroot", config.host, 1094, "/", cert_subject, *sysinfo.getprotinfo("root")))
    if 8446 in sysinfo.ports:
        ret.append(Endpoint("httpg", config.host, 8446, "/srm/managerv2?SFN=/", cert_subject, *sysinfo.getprotinfo("srm")))

    return ret


def create_accessprotocols(config):
    """Find all the StorageEndpoints on this DPM
    and return an array of them"""
    ret = []
    sysinfo = SystemInfo()
    if 2811 in sysinfo.ports:
        ret.append(AccessProtocol(config.host, *sysinfo.getprotinfo("gsiftp")))
    if 443 in sysinfo.ports:
        ret.append(AccessProtocol(config.host, *sysinfo.getprotinfo("https")))
    if 1094 in sysinfo.ports:
        ret.append(AccessProtocol(config.host, *sysinfo.getprotinfo("root")))
    if 8446 in sysinfo.ports:
        ret.append(AccessProtocol(config.host, *sysinfo.getprotinfo("gsiftp")))
    return ret


def create_manager(config):
    """Create a StorageManager"""
    sysinfo = SystemInfo(config)
    jgqt, totalcapacity, totalused, totalgroups = sysinfo.getspaces()
    mgr = Manager(sysinfo.getsysinfo("dome"), sysinfo.getsysinfo("dmlite"), config.host)
    ds = DataStore(config.host, totalcapacity, totalused)
    ds.set_foreign_key(mgr.getname())
    mgr.add_child(ds)
    return mgr


def main():
    # Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--cert", help="Path to host certificate", default="/etc/grid-security/hostcert.pem")
    parser.add_argument("--key", help="Path to host key", default="/etc/grid-security/hostkey.pem")
    parser.add_argument("--host", help="fqdn", default=socket.getfqdn())
    parser.add_argument("--sitename", help="site name", required=True)
    args = parser.parse_args()

    # Create the top of the tree
    top = Service(args)
    sysinfo = SystemInfo(args)
    jgqt, totalcapacity, totalused, totalgroups = sysinfo.getspaces()
    ssc = StorageServiceCapacity(args.host, totalcapacity, totalused)
    ssc.set_foreign_key(top.getname())
    top.add_child(ssc)

    # Add entries underneath
    for share in create_shares(args):
        share.set_foreign_key(top.getname())
        top.add_child(share)

    for endpoint in create_endpoints(args):
        endpoint.set_foreign_key(top.getname())
        ap = AccessPolicy(totalgroups, args.host)
        ap.set_foreign_key(endpoint.getname())
        endpoint.add_child(ap)
        top.add_child(endpoint)

    for accessprotocol in create_accessprotocols(args):
        accessprotocol.set_foreign_key(top.getname())
        top.add_child(accessprotocol)

    manager = create_manager(args)
    manager.set_foreign_key(top.getname())
    top.add_child(manager)

    # Print everything out
    prefix = "GLUE2GroupID=resource,o=glue"
    top.print_out(prefix)


# Excecute
if __name__ == '__main__':
    main()
