#!/usr/bin/python2
# Simplified Argus banning data extracted from server configuration
# read directly by its WSDL interface.
#
# This simplified version takes into account pure banning rules
# that applies only to all resources ".*" and all actions ".*", see
# pap-admin list-policies --action '.*' --resource '.*' --all
# Details about available options for Argus banning in documentation
# https://argus-documentation.readthedocs.io/en/stable/pap/simplified_policy_language.html
#
# It is necessary to configure access privileges on Argus server
# CONFIGURATION_READ|POLICY_READ_LOCAL|POLICY_READ_REMOTE
# Use DPM headnode host certificate with following subject
# `openssl x509 -in /etc/grid-security/hostcert.pem -noout -subject`
# and on Argus server add this subject in the [dn] section
# of the /etc/argus/pap/pap_authorization.ini
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import pycurl
import io

import xml.etree.ElementTree as ET


class ArgusBan(object):

    _PAP_NS = {
        'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
        'pap': 'http://services.pap.authz.glite.org',
        'xacml': 'urn:oasis:names:tc:xacml:2.0:policy:schema:os',
    }

    def __init__(self, host, port=8150, cert='/etc/grid-security/hostcert.pem', key='/etc/grid-security/hostkey.pem', capath='/etc/grid-security/certificates'):
        self._host = host
        self._port = port
        self._cert = cert
        self._key = key
        self._capath = capath


    def _soap_request(self, service, data):
        # Post the SOAP request and return response as XML tree
        url = "https://%s:%i/pap/services/%s?wsdl" % (self._host, self._port, service)

        curl = pycurl.Curl()
        curl.setopt(curl.URL, url)
        curl.setopt(curl.POSTFIELDS, data)
        curl.setopt(curl.CAPATH, self._capath)
        curl.setopt(curl.SSLCERT, self._cert)
        curl.setopt(curl.SSLKEY, self._key)
        curl.setopt(curl.HTTPHEADER, ['SOAPAction: ""'])
        body = io.BytesIO()
        curl.setopt(curl.WRITEFUNCTION, body.write)

        try:
            curl.perform()
        except pycurl.error as e:
            raise IOError("Failed to query Argus %s: %s" % (url, str(e)))

        code = curl.getinfo(pycurl.HTTP_CODE)
        if code != 200:
            raise IOError("Failed to query Argus %s (Code: %d)" % (url, code))

        # sys.stdout.write("%s\n" % str(body.getvalue()))

        return ET.fromstring(body.getvalue())


    def pap_aliases(self):
        # requires Argus PAP CONFIGURATION_READ privileges
        aliases = []

        PAP_GET_ALL = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pap="http://services.pap.authz.glite.org" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <soapenv:Header/>
         <soapenv:Body>
           <pap:getAllPaps/>
         </soapenv:Body>
</soapenv:Envelope>
"""

        root = self._soap_request('PAPManagementService', PAP_GET_ALL)
        if sys.version_info[:2] >= (2, 7):
            paps = root.find("soapenv:Body/pap:getAllPapsResponse/pap:getAllPapsReturn", ArgusBan._PAP_NS)
            for pap in paps:
                alias = pap.find('pap:alias', ArgusBan._PAP_NS).text
                enabled = pap.find('pap:enabled', ArgusBan._PAP_NS).text == 'true'
                if not enabled: continue
                aliases.append(alias)
        else:  # python 2.6 xml.etree explicit namespaces and missing xpath support
            paps = root.find("{%(soapenv)s}Body/{%(pap)s}getAllPapsResponse/{%(pap)s}getAllPapsReturn" % ArgusBan._PAP_NS)
            for pap in paps:
                alias = pap.find("{%(pap)s}alias" % ArgusBan._PAP_NS).text
                enabled = pap.find("{%(pap)s}enabled" % ArgusBan._PAP_NS).text == 'true'
                if not enabled: continue
                aliases.append(alias)

        return aliases


    def pap_policy_sets(self, alias):
        # requires Argus PAP POLICY_READ_LOCAL|POLICY_READ_REMOTE privileges
        ret = {}

        PAP_POLICY_SETS = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pap="http://services.pap.authz.glite.org" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <soapenv:Header/>
         <soapenv:Body>
           <pap:listPolicySets><papAlias>%s</papAlias></pap:listPolicySets>
         </soapenv:Body>
</soapenv:Envelope>
""" % alias

        root = self._soap_request('XACMLPolicyManagementService', PAP_POLICY_SETS)
        if sys.version_info[:2] >= (2, 7):
            policy_sets = root.findall("soapenv:Body/pap:listPolicySetsResponse/pap:listPolicySetsReturn/xacml:PolicySet", ArgusBan._PAP_NS)
            for policy_set in policy_sets:
                reference = policy_set.find('xacml:PolicyIdReference', ArgusBan._PAP_NS)
                if reference == None: continue  # comparison with None necessary
                resources = policy_set.findall('xacml:Target/xacml:Resources/xacml:Resource/xacml:ResourceMatch/xacml:AttributeValue', ArgusBan._PAP_NS)
                ret[reference.text] = [x.text for x in resources]
        else:  # python 2.6 xml.etree explicit namespaces and missing xpath support
            policy_sets = root.findall("{%(soapenv)s}Body/{%(pap)s}listPolicySetsResponse/{%(pap)s}listPolicySetsReturn/{%(xacml)s}PolicySet" % ArgusBan._PAP_NS)
            for policy_set in policy_sets:
                reference = policy_set.find("{%(xacml)s}PolicyIdReference" % ArgusBan._PAP_NS)
                if reference == None: continue  # comparison with None necessary
                resources = policy_set.findall("{%(xacml)s}Target/{%(xacml)s}Resources/{%(xacml)s}Resource/{%(xacml)s}ResourceMatch/{%(xacml)s}AttributeValue" % ArgusBan._PAP_NS)
                ret[reference.text] = [x.text for x in resources]

        return ret


    def pap_simple_banlist(self, alias):
        # requires Argus PAP POLICY_READ_LOCAL|POLICY_READ_REMOTE privileges
        ret = {}

        PAP_LIST_POLICIES = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pap="http://services.pap.authz.glite.org" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <soapenv:Header/>
         <soapenv:Body>
           <pap:listPolicies><papAlias>%s</papAlias></pap:listPolicies>
         </soapenv:Body>
</soapenv:Envelope>
""" % alias

        root = self._soap_request('XACMLPolicyManagementService', PAP_LIST_POLICIES)
        policy_sets = self.pap_policy_sets(alias)
        if sys.version_info[:2] >= (2, 7):
            policies = root.findall("soapenv:Body/pap:listPoliciesResponse/pap:listPoliciesReturn/xacml:Policy", ArgusBan._PAP_NS)
            for policy in policies:
                policy_id = policy.get('PolicyId')
                # ignore policies without corresponding resources
                if policy_id not in policy_sets: continue
                # ignore policies that don't applies on all resources
                if policy_sets[policy_id] != ['.*']: continue
                # ignore policies that don't applies on all actions
                actions = policy.findall('xacml:Target/xacml:Actions/xacml:Action/xacml:ActionMatch/xacml:AttributeValue', ArgusBan._PAP_NS)
                if [x.text for x in actions] != ['.*']: continue
                # ignore policies with permit rules
                if len(policy.findall("xacml:Rule[@Effect='Permit']", ArgusBan._PAP_NS)) > 0: continue
                for subject_match in policy.findall("xacml:Rule[@Effect='Deny']/xacml:Target/xacml:Subjects/xacml:Subject/xacml:SubjectMatch", ArgusBan._PAP_NS):
                    subject_type = subject_match.find('xacml:SubjectAttributeDesignator', ArgusBan._PAP_NS)
                    subject_value = subject_match.find('xacml:AttributeValue', ArgusBan._PAP_NS)
                    if subject_type.get('AttributeId') == 'urn:oasis:names:tc:xacml:1.0:subject:subject-id':
                        dtype = 'subject'
                    elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/subject-issuer':
                        dtype = 'subject-issuer'
                    elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/fqan':
                        dtype = 'fqan'
                    elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/fqan/primary':
                        dtype = 'pfqan'
                    elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/virtual-organization':
                        dtype = 'vo'
                    else:
                        #raise Exception("deny rule data type %s not supported" % subject_type.get('AttributeId'))
                        continue
                    if dtype in ['subject', 'subject-issuer']:
                        if subject_type.get('DataType') != 'urn:oasis:names:tc:xacml:1.0:data-type:x500Name':
                            continue
                        user_dn = subject_value.text
                        dn_parts = [x.strip() for x in user_dn.split(', ')]
                        dn_parts.reverse()
                        ret.setdefault(dtype, []).append('/%s' % '/'.join(dn_parts))
                    else:
                        ret.setdefault(dtype, []).append(subject_value.text)
        else:  # python 2.6 xml.etree explicit namespaces and missing xpath support
            policies = root.findall("{%(soapenv)s}Body/{%(pap)s}listPoliciesResponse/{%(pap)s}listPoliciesReturn/{%(xacml)s}Policy" % ArgusBan._PAP_NS)
            for policy in policies:
                policy_id = policy.get('PolicyId')
                # ignore policies without corresponding resources
                if policy_id not in policy_sets: continue
                # ignore policies that don't applies on all resources
                if policy_sets[policy_id] != ['.*']: continue
                # ignore policies that don't applies on all actions
                actions = policy.findall("{%(xacml)s}Target/{%(xacml)s}Actions/{%(xacml)s}Action/{%(xacml)s}ActionMatch/{%(xacml)s}AttributeValue" % ArgusBan._PAP_NS)
                if [x.text for x in actions] != ['.*']: continue
                # ignore policies with permit rules
                rules = policy.findall("{%(xacml)s}Rule" % ArgusBan._PAP_NS)
                if len(filter(lambda x: x.get('Effect') == 'Permit', rules)) > 0: continue
                for rules in filter(lambda x: x.get('Effect') == 'Deny', rules):
                    for subject_match in rules.findall("{%(xacml)s}Target/{%(xacml)s}Subjects/{%(xacml)s}Subject/{%(xacml)s}SubjectMatch" % ArgusBan._PAP_NS):
                        subject_type = subject_match.find("{%(xacml)s}SubjectAttributeDesignator" % ArgusBan._PAP_NS)
                        subject_value = subject_match.find("{%(xacml)s}AttributeValue" % ArgusBan._PAP_NS)
                        if subject_type.get('AttributeId') == 'urn:oasis:names:tc:xacml:1.0:subject:subject-id':
                            dtype = 'subject'
                        elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/subject-issuer':
                            dtype = 'subject-issuer'
                        elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/fqan':
                            dtype = 'fqan'
                        elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/fqan/primary':
                            dtype = 'pfqan'
                        elif subject_type.get('AttributeId') == 'http://glite.org/xacml/attribute/virtual-organization':
                            dtype = 'vo'
                        else:
                            #raise Exception("deny rule data type %s not supported" % subject_type.get('AttributeId'))
                            continue
                        if dtype in ['subject', 'subject-issuer']:
                            if subject_type.get('DataType') != 'urn:oasis:names:tc:xacml:1.0:data-type:x500Name':
                                continue
                            user_dn = subject_value.text
                            dn_parts = [x.strip() for x in user_dn.split(', ')]
                            dn_parts.reverse()
                            ret.setdefault(dtype, []).append('/%s' % '/'.join(dn_parts))
                        else:
                            ret.setdefault(dtype, []).append(subject_value.text)

        return ret



if __name__ == '__main__':

    if len(sys.argv) == 1 or sys.argv[1] in ['-h', '--help', 'help']:
        print("usage: %s argus.fqdn[:port[#alias]] [cert [key [capath]]]" % sys.argv[0])
        print("examples:")
        print("  %s argus1.farm.particle.cz" % sys.argv[0])
        print("  %s argus1.farm.particle.cz:8150" % sys.argv[0])
        print("  %s argus1.farm.particle.cz:8150#default" % sys.argv[0])
        print("  %s argus1.farm.particle.cz:8150#default /etc/grid-security/hostcert.pem /etc/grid-security/hostkey.pem /etc/grid-security/certificates" % sys.argv[0])
        print("  # use multiple Argus servers:")
        print("  %s 'argus1.farm.particle.cz|argus2.farm.particle.cz'" % sys.argv[0])
        print("  %s 'argus1.farm.particle.cz:8150|argus2.farm.particle.cz:8150'" % sys.argv[0])
        print("  # ...")
        sys.exit(0)

    hosts = sys.argv[1]
    cert = '/etc/grid-security/hostcert.pem' if len(sys.argv) <= 2 else sys.argv[2]
    key = '/etc/grid-security/hostkey.pem' if len(sys.argv) <= 3 else sys.argv[3]
    capath = '/etc/grid-security/certificates' if len(sys.argv) <= 4 else sys.argv[4]

    for host in hosts.split('|'):
        port = 8150
        aliases = None
        if host.find('#') != -1:
            host, alias = host.split('#')
            aliases = [alias]
        if host.find(':') != -1:
            host, port = host.split(':')
            port = int(port)

        try:
            aban = ArgusBan(host, port, cert, key, capath)
            if not aliases:
                aliases = aban.pap_aliases()
            for alias in aliases:
                print("%s: %s" % (alias, aban.pap_simple_banlist(alias)))
            break
        except Exception as e:
            print("failed to get data from %s: %s" % (host, str(e)))

    sys.exit(0)
