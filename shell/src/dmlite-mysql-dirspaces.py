#!/usr/bin/python2
#
# A script that calculates the space occupied by files in every directory
# and can set the metadata filesize field with it, for the first N levels
#
# This script can directly update information in the database and not all
# operation leads to consistent results if you don't stop all DPM daemons
# before executing this script (see help screen for more details and examples)
#
# March 2015 - Fabrizio Furano - CERN IT/SDC - furano@cern.ch
# January 2019 - Petr Vokac - petr.vokac@cern.ch
#
#
# Usage:
# * get the help screen with all available options
#   dmlite-mysql-dirspaces [-h] [--help]
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys, os
import optparse
import re
import stat
import datetime
import time
import socket
import threading
import logging, logging.handlers

try: import simplejson as json
except ImportError: import json

# compatibility for existing SLC6, CentOS7, CentOS8 packages
try:
    import pymysql
    import pymysql.cursors as pymysql_cursors
except ImportError:
    import MySQLdb as pymysql
    import MySQLdb.cursors as pymysql_cursors

__version__ = '1.2.0'
__author__ = 'Fabrizio Furano'

_log = logging.getLogger('DPMDIRSPACES')

DEFAULT_CNS_DB = 'cns_db'
DEFAULT_DPM_DB = 'dpm_db'
DEFAULT_DOME_CONFIG = '/etc/domehead.conf'
DEFAULT_UPDATELEVELS = 6



def guess_config_files():
    """ Guesses the location of DPM namespace configuration file """
    try:
        possible_nsconfigs = ['/opt/lcg/etc/NSCONFIG', '/usr/etc/NSCONFIG']
        if 'LCG_LOCATION' in os.environ:
            possible_nsconfigs.insert(0, os.environ['LCG_LOCATION'].rstrip('/') + '/etc/NSCONFIG')

        for f in possible_nsconfigs:
            if os.path.exists(f):
                return f

    except Exception as e:
        _log.warn("failed to guess DB config file location: %s", str(e))

    return None



def get_conn_data(nsconfig):
    """ Returns connection data from NSCONFIG"""
    retval = {}

    _log.debug("getting connection info from %s", nsconfig)
    try:
        nsconfig_line = open(nsconfig).readline().strip()
    except Exception as e:
        _log.error("Cannot open DPM config file %s: %s", nsconfig, str(e))
        sys.exit(-1)

    nsre = re.compile(r"(.*)/(.*)@([^/]*)(?:/(.*))?")
    m = nsre.match(nsconfig_line)
    if m == None:
        _log.error("Bad line in DPM config '%s', doesn't match re '%s'", nsconfig, nsre)
        sys.exit(-1)
    retval['user'] = m.group(1)
    retval['pass'] = m.group(2)
    retval['host'] = m.group(3)
    if m.group(4):
        retval['cns_db'] = m.group(4)
    else:
        retval['cns_db'] = DEFAULT_CNS_DB
    retval['dpm_db'] = DEFAULT_DPM_DB

    _log.debug("database connection: host=%s, user=%s, cns_db=%s, dpm_db=%s", retval['host'], retval['user'], retval['cns_db'], retval['dpm_db'])

    return retval



def get_connection(conn_data, db, cclass=pymysql.cursors.Cursor):
    return pymysql.connect(
            host=conn_data['host'], user=conn_data['user'],
            passwd=conn_data['pass'], db=conn_data[db],
            cursorclass=cclass)



def get_updatelivels(filename):
    """ Guesses the location of DPM namespace configuration file """
    ret = DEFAULT_UPDATELEVELS

    try:
        if os.path.exists(filename):
            with open(filename) as f:
                for line in f:
                    res = re.match(r'^head.dirspacereportdepth:\s*(\d+).*', line)
                    if res != None:
                        ret = int(res.group(1))
                        _log.debug("found updatelevel %i configuration in %s", ret, filename)
        else:
            _log.debug('DPM DOME head config %s not found', filename)

    except Exception as e:
        _log.warn("failed to guess updatelvels from %s: %s", filename, str(e))

    return ret



class CachedFullPath(object):
    """DPM file metadata stored in database have pointer just
    to parent directory and to build whole path it is necessary
    to recursively query all parent directories to the root "/".

    Althought these queries are pretty cheap they are done for
    each file and even with small latencies (especially with
    database on dedicated server) they can take quite a time.

    This class not only caches past queries, but also limit
    size of cached data not to exhaust whole memory while
    dumping big DPM database with a lot of files.
    """

    def __init__(self, conn, maxsize=1000000, table='Cns_file_metadata', fileid_only=False):
        self._cache = {}
        self._cache_path = {}
        self._conn = conn
        self._maxsize = maxsize
        self._table = table
        self._fileid_only = fileid_only
        self._ntotal = 0
        self._ncached = 0
        self._nqueries = 0
        self._ncleanup = 0

    def __del__(self):
        if _log:
            _log.info("path lookup cache usage: total %i, cached %i, queries %i, cleanup %i", self._ntotal, self._ncached, self._nqueries, self._ncleanup)

    def _fill_cache(self, fileid):
        """Recursively get full path for given fileid (simple - could be used to validate _fill_cache_multi)"""
        self._ntotal += 1

        if fileid in self._cache:
            self._ncached += 1
            return

        if len(self._cache) >= self._maxsize:
            _log.debug("fullpath cache too big (%i), dropping cached records...", len(self._cache))
            self._ncleanup += 1
            del(self._cache)
            del(self._cache_path)
            self._cache = {}
            self._cache_path = {}

        sql = "SELECT parent_fileid, name FROM %s WHERE fileid=%%s" % self._table
        if self._fileid_only:
            sql = "SELECT parent_fileid FROM %s WHERE fileid=%%s" % self._table

        cursor = self._conn.cursor()
        cursor.execute(sql, (fileid, ))
        res = cursor.fetchone()
        cursor.close()

        self._nqueries += 1

        if _log.getEffectiveLevel() < logging.DEBUG:
            _log.debug("query parent directory '%s': %s", sql, res)

        if res == None:
            if fileid != 0:
                _log.info("db inconsistency: could not find path for fileid %i (most likely the entry is orphan)", fileid)
            else:
                _log.debug("no parent for top level directory 0")
            self._cache[fileid] = None
            return

        parentid = res[0]
        name = str(fileid) if self._fileid_only else res[1]

        if parentid == 0: # top level directory
            self._cache[fileid] = [fileid]
            self._cache_path[fileid] = ''
            return

        if name.find('/') != -1: # this script doesn't support '/' characters in metadata name
            raise Exception("fileid {0} contains slash character in its name '{1}'".format(fileid, name))

        self._fill_cache(parentid)
        if parentid not in self._cache or self._cache[parentid] == None:
            # db inconsistency already detected and logged
            self._cache[fileid] = None
            return

        self._cache[fileid] = self._cache[parentid] + [fileid]
        if not self._fileid_only:
            fullpath = "{0}/{1}".format(self._cache_path[parentid], name)
            self._cache_path[fileid] = fullpath

    def _fill_cache_multi(self, fileids):
        """Reduce impact of query latency by resolving paths for multiple
        fileids. Max number of queries still corresponds to the max path
        depth, but all fileids are resolved at the same time."""
        self._ntotal += len(fileids)

        if len(self._cache) + len(fileids) >= self._maxsize:
            _log.debug("fullpath cache too big (%i+%i), dropping cached records...", len(self._cache), len(fileids))
            self._ncleanup += 1
            del(self._cache)
            del(self._cache_path)
            self._cache = {}
            self._cache_path = {}

        tores = set()
        id2name = {}
        id2parent = {}
        for fileid in fileids:
            if fileid in self._cache:
                if self._cache[fileid] != None:
                    self._ncached += len(self._cache[fileid])
                else:
                    self._ncached += 1
            else:
                tores.add(fileid)

        if len(tores) > 0:
            cursor = self._conn.cursor()

            while len(tores) > 0:

                sql = "SELECT fileid, parent_fileid, name FROM Cns_file_metadata WHERE fileid IN ({0})".format(','.join([ str(x) for x in tores ]))
                if self._fileid_only:
                    sql = "SELECT fileid, parent_fileid FROM Cns_file_metadata WHERE fileid IN ({0})".format(','.join([ str(x) for x in tores ]))

                cursor.execute(sql)

                self._nqueries += 1
                tores = set()

                for row in cursor:
                    fileid = row[0]
                    parentid = row[1]
                    name = str(row[0]) if self._fileid_only else row[2]

                    if _log.getEffectiveLevel() < logging.DEBUG:
                        _log.debug("query parent directory '%s': %s", sql, row)

                    if parentid == 0:
                        name = ''

                    if name.find('/') != -1: # this script doesn't support '/' characters in metadata name
                        raise Exception("fileid {0} contains slash character in its name '{1}'".format(fileid, name))

                    id2name[fileid] = name
                    id2parent[fileid] = parentid

                    if parentid == 0:
                        pass
                    elif parentid in self._cache:
                        if self._cache[parentid] != None:
                            self._ncached += len(self._cache[parentid])
                        else:
                            self._ncached += 1
                    elif parentid not in id2parent:
                        tores.add(parentid)

            cursor.close()

        for fileid in fileids:
            if fileid in self._cache: continue

            currid = fileid
            revids = []

            while True:
                if currid in self._cache:
                    if self._cache[currid] != None:
                        self._ncached += len(self._cache[currid])
                    else:
                        self._ncached += 1
                    break
                elif currid in id2parent:
                    if currid in revids:
                        revids.reverse()
                        fullpath = '/'.join(map(lambda x: id2name[x], revids))
                        _log.info("db inconsistency: detected directory loop for fileid %i parent %i %s", fileid, currid, fullpath)
                        for revid in revids: self._cache[revid] = None
                        revids = []
                        break
                    revids.append(currid)
                    currid = id2parent[currid]
                    if currid == 0: # root directory
                        break
                else:
                    if fileid != 0:
                        if fileid != currid:
                            revids.reverse()
                            fullpath = '/'.join(map(lambda x: id2name[x], revids))
                            _log.info("db inconsistency: could not find path for fileid %i parent %i (most likely the entry is orphan, path %s)", fileid, currid, fullpath)
                        else:
                            _log.info("db inconsistency: could not find path for fileid %i (most likely the entry is orphan)", fileid)
                    else:
                        _log.debug("no parent for top level directory 0")
                    for revid in revids: self._cache[revid] = None
                    revids = []
                    break

            if len(revids) > 0:
                revids.reverse()
                for i, revid in enumerate(revids):
                    if currid == 0:
                        self._cache[revid] = revids[:i+1]
                    else:
                        self._cache[revid] = self._cache[currid] + revids[:i+1]
                    if not self._fileid_only:
                        if revid in self._cache_path: continue
                        pathsuffix = '/'.join(map(lambda x: id2name[x], revids[:i+1]))
                        if currid == 0:
                            self._cache_path[revid] = pathsuffix
                        elif currid in self._cache_path:
                            self._cache_path[revid] = "{0}/{1}".format(self._cache_path[currid], pathsuffix)


    def get_ids(self, fileid):
        self._fill_cache_multi(fileid)
        #self._fill_cache_multi([fileid])

        return self._cache.get(fileid)


    def get_path(self, fileid):
        if self._fileid_only:
            raise Exception("can't get directory path with {0} instance in fileid_only mode".format(self.__class__.__name__))

        self._fill_cache(fileid)
        #self._fill_cache_multi([fileid])

        return self._cache_path.get(fileid)


    def get_ids_multi(self, fileids):
        ret = {}

        self._fill_cache_multi(fileids)

        for fileid in fileids:
            if fileid not in self._cache: continue # db inconsistency already reported
            ret[fileid] = self._cache[fileid]

        return ret


    def get_path_multi(self, fileids):
        if self._fileid_only:
            raise Exception("can't get directory path with {0} instance in fileid_only mode".format(self.__class__.__name__))

        ret = {}

        self._fill_cache_multi(fileids)

        for fileid in fileids:
            if fileid not in self._cache_path: continue # db inconsistency already reported
            ret[fileid] = self._cache_path[fileid]

        return ret



def get_path_id(conn, path):
    """Get fileid for given path"""
    fileid = 0
    cursor = conn.cursor()
    try:
        done = []
        for name in path.split('/'):
            if name == '': name = '/' # root directory
            cursor.execute('SELECT fileid from Cns_file_metadata WHERE parent_fileid = %s AND name = %s', (fileid, name))
            res = cursor.fetchone()
            if res == None:
                _log.debug("path {0} not found (failed with {1} directory with fileid {2} has no name '{3}')".format(path, '/'.join(done), fileid, name))
                return None
            fileid = res[0]

    finally:
        cursor.close()

    return fileid



def get_path_size(conn, path):
    """Get size for given path"""
    size = -1
    cursor = conn.cursor()
    try:
        done = []
        parentid = 0
        for name in path.split('/'):
            if name == '': name = '/' # root directory
            cursor.execute('SELECT fileid, filesize from Cns_file_metadata WHERE parent_fileid = %s AND name = %s', (parentid, name))
            res = cursor.fetchone()
            if res == None:
                _log.warn("for path {0} could not find metadata for {1} (parentid: {2}, name: {3}), assuming 0 size".format(path, '/'.join(done), parentid, name))
                size = 0
                break
            parentid, size = res

    finally:
        cursor.close()

    return size



def get_quotatoken_data(conn):
    cursor = conn.cursor()

    ret = []
    try:
        cursor.execute("SELECT path, poolname, u_token, s_token, t_space, g_space, u_space FROM dpm_space_reserv")
        rows = cursor.fetchall()

        for path, poolname, u_token, s_token, t_space, g_space, u_space in rows:
            if path == None:
                _log.error("missing directory path for quotatoken %s", u_token)

        # sort quotatokens by path length with longest first
        # (this ordering is assumed by code calling this function)
        for row in sorted(filter(lambda x: x[0] != None, rows), key=lambda x: len(x[0]), reverse=True):
            path, poolname, u_token, s_token, t_space, g_space, u_space = row
            ret.append((path, poolname, u_token, s_token, t_space, g_space, u_space))

    finally:
        cursor.close()

    return ret



def get_accounted_dirs(conn, updatelevels):
    """find all directories up to updatelevels"""
    cursor = conn.cursor()
    try:

        cursor.execute('SELECT fileid FROM Cns_file_metadata WHERE parent_fileid = 0 AND (filemode & {0}) = {0}'.format(stat.S_IFDIR))
        fileid = cursor.fetchone()[0]

        adirids = [] # skip first level "/"
        currfileids = [str(fileid)]

        for level in range(updatelevels):
            cursor.execute('SELECT fileid FROM Cns_file_metadata WHERE parent_fileid IN ({0}) AND (filemode & {1}) = {1}'.format(','.join(currfileids), stat.S_IFDIR))
            currfileids = []
            for row in cursor:
                fileid = row[0]
                if level > 0: # skip second level "/dpm"
                    adirids.append(fileid)
                currfileids.append(str(fileid))

        _log.info("found %i directories within %i levels", len(adirids), updatelevels)

    finally:
        cursor.close()

    return adirids



def fix_zero_dirs(conn_data, updatelevels, updatedb=False):
    """Set filesize to zero for directories below update level"""
    _log.debug("fix_zero_dirs(updatelevels=%i, updatedb=%s)", updatelevels, updatedb)

    conn = get_connection(conn_data, 'cns_db')
    cursor = conn.cursor()
    try:
        currfileids = get_accounted_dirs(conn, updatelevels)
        currfileids_list = ','.join([ str(x) for x in currfileids ])

        if updatedb:
            cursor.execute('UPDATE Cns_file_metadata SET filesize = 0 WHERE fileid NOT IN ({0}) AND (filemode & {1}) = {1} AND filesize != 0'.format(currfileids_list, stat.S_IFDIR))
            conn.commit()
        else:
            pathname = CachedFullPath(conn)
            cursor.execute('SELECT fileid, filesize FROM Cns_file_metadata WHERE fileid NOT IN ({0}) AND (filemode & {1}) = {1} AND filesize != 0'.format(currfileids_list, stat.S_IFDIR))
            for row in cursor:
                fileid, filesize = row
                _log.info("dry-run: update directory %s with fileid %i (below %i levels) with size %i to zero", pathname.get_path(fileid), fileid, updatelevels, filesize)

        _log.info("updated %i non-accounted directories to filesize zero (updatelevels=%i)", cursor.rowcount, updatelevels)

    finally:
        cursor.close()



def fix_spacetokens_by_path(conn_data, skip=[], updatedb=False):
    # NOTE: this function doesn't check if storage pool associated with replica
    # use right host:/fs defined in 'dpm_db'.'dpm_fs' table
    _log.debug("fix_spacetokens_by_path(skip=%s, updatedb=%s)", skip, updatedb)

    dry_run = '' if updatedb else 'dry-run: '

    conn_dpm = get_connection(conn_data, 'dpm_db')
    qt = get_quotatoken_data(conn_dpm)
    s2name = {}
    for path, poolname, u_token, s_token, t_space, g_space, u_space in qt:
        s2name[s_token] = u_token
    conn_dpm.close()

    conn = get_connection(conn_data, 'cns_db')
    conn_ss = get_connection(conn_data, 'cns_db', cclass=pymysql.cursors.SSCursor)
    conn_path = get_connection(conn_data, 'cns_db')
    pathname = CachedFullPath(conn_path)

    try:
        # join metadata table with replicas to account size of all file replicas
        cursor = conn_ss.cursor()
        ucursor = conn.cursor()

        sql = 'SELECT replica.rowid, replica.fileid, metadata.parent_fileid, metadata.name, replica.poolname, replica.setname, replica.xattr FROM Cns_file_metadata AS metadata JOIN Cns_file_replica AS replica ON metadata.fileid = replica.fileid'
        cursor.execute(sql)

        updated = 0
        cnt_rows = 0
        while True:
            # retreiving data in chunks dramatically increase performance
            # mostly because of latency associated with each DB query
            rows = cursor.fetchmany(1000)
            if len(rows) == 0: break

            if _log.getEffectiveLevel() < logging.DEBUG:
                _log.debug("fetched %i rows", len(rows))

            fileids = [ row[2] for row in rows ]
            pathnames = pathname.get_path_multi(fileids)

            for row in rows:
                cnt_rows += 1

                if cnt_rows % 1000000 == 0:
                    _log.debug("processed %i records, updated %i", cnt_rows, updated)

                if _log.getEffectiveLevel() < logging.DEBUG:
                    _log.debug("row %i: %s", cnt_rows, str(row))

                rowid, fileid, pfileid, name, poolname, setname, xattr = row
                path = pathnames.get(pfileid)
                if path == None:
                    _log.warn("skipping fileid %i, unable to reconstruct its parent %i path (dpm-dbck can correct this problem)", fileid, pfileid)
                    continue

                qt_selected = filter(lambda x: path.startswith(x[0]), qt)
                if len(qt_selected) == 0:
                    _log.warn("skipping %s/%s with fileid %i, because it doen't match any quotatoken path", path, name, fileid)
                    continue

                # first selected QT is the right one because "qt" array is sorted by path length
                spath, spool, u_token, s_token, t_space, g_space, u_space = qt_selected[0]
                # skip replicas with matching pool and spacetokens
                if poolname == spool and setname == s_token: continue
                # update only spacetokens not explicetely excluded
                if setname in skip and s_token in skip: continue

                # deal with JSON in xattr
                xattr_dict = {}
                if xattr != None and xattr not in [ None, '', '{}' ]:
                    try:
                        xattr_dict = json.loads(xattr)
                    except Exception:
                        _log.error("unable to parse replica xattr for %s/%s (fileid %i), xattr not updated: %s", path, name, fileid, str(xattr))

                loginfo = []
                updates = []
                values = []
                xattr_update = False

                if poolname != spool:
                    if _log.getEffectiveLevel() <= logging.INFO:
                        loginfo.append("{0} -> {1}".format(poolname, spool))
                        _log.debug("UPDATE_POOL[%s@%s/%s]: %s -> %s", fileid, path, name, poolname, spool)
                    updates.append('poolname = %s')
                    values.append(spool)
                    if 'pool' in xattr_dict and xattr_dict['pool'] != spool:
                        xattr_dict['pool'] = spool
                        xattr_update = True

                if setname != s_token:
                    if _log.getEffectiveLevel() <= logging.INFO:
                        loginfo.append("{0} -> {1}".format(setname, s_token))
                        _log.debug("UPDATE_TOKEN[%s@%s/%s]: %s -> %s", fileid, path, name, setname, s_token)
                    updates.append('setname = %s')
                    values.append(s_token)
                    if 'accountedspacetokenname' in xattr_dict and xattr_dict['accountedspacetokenname'] != u_token:
                        xattr_dict['accountedspacetokenname'] = u_token
                        xattr_update = True

                if xattr_update:
                    if _log.getEffectiveLevel() <= logging.INFO:
                        loginfo.append("{0} -> {1}".format(json.loads(xattr), xattr_dict))
                        _log.debug("UPDATE_XATTR[%s@%s/%s]: %s -> %s", fileid, path, name, json.loads(xattr), xattr_dict)
                    updates.append('xattr = %s')
                    values.append(json.dumps(xattr_dict))

                _log.info("%supdate replica rowid %i for %s/%s with fileid %i (%s)", dry_run, rowid, path, name, fileid, ', '.join(loginfo))
                if updatedb:
                    values.append(rowid)
                    sql = "UPDATE Cns_file_replica SET {0} WHERE rowid = %s".format(', '.join(updates))
                    ucursor.execute(sql, values)
                    conn.commit()

                updated += 1

        ucursor.close()
        cursor.close()

        _log.info("processed %i records, updated %i", cnt_rows, updated)

    except Exception:
        # query in progress that use SSCursor can be killed only by terminating DB connection
        # (closing / deleting cursor lead to retreival of all selected entries from DB)
        del(pathname)
        conn_path.close()
        conn_ss.close()
        conn.close()
        raise

    del(pathname)
    conn_path.close()
    conn_ss.close()
    conn.close()



def fix_dir_size_offline(conn_data, updatelevels, updatedb=False):
    _log.debug("fix_dir_size_offline(updatelevels=%i, updatedb=%s", updatelevels, updatedb)

    dry_run = '' if updatedb else 'dry-run: '

    conn_ss = get_connection(conn_data, 'cns_db', cclass=pymysql.cursors.SSCursor)
    conn_path = get_connection(conn_data, 'cns_db')
    pathname = CachedFullPath(conn_path)

    psize = {}
    try:
        # join metadata table with replicas to account size of all file replicas
        sql = 'SELECT metadata.fileid, metadata.parent_fileid, metadata.filesize FROM Cns_file_metadata AS metadata JOIN Cns_file_replica AS replica ON metadata.fileid = replica.fileid WHERE replica.status = "-"'
        cursor = conn_ss.cursor()
        cursor.execute(sql)

        cnt_rows = 0
        while True:
            # retreiving data in chunks dramatically increase performance
            # mostly because of latency associated with each DB query
            rows = cursor.fetchmany(1000)
            if len(rows) == 0: break

            if _log.getEffectiveLevel() < logging.DEBUG:
                _log.debug("fetched %i rows", len(rows))

            fileids = [ row[1] for row in rows ]
            pathnames = pathname.get_ids_multi(fileids)

            for row in rows:
                cnt_rows += 1
                if _log.getEffectiveLevel() < logging.DEBUG:
                    _log.debug("row %i: %s", cnt_rows, str(row))

                fileid, pfileid, filesize = row
                prefix_fileids = pathnames.get(pfileid)
                if prefix_fileids == None:
                    _log.warn("skipping fileid %i, unable to reconstruct its parent %i path (dpm-dbck can correct this problem)", fileid, pfileid)
                    continue

                # DPM doesn't update directory size for top two level "/dpm"
                for prefix_fileid in prefix_fileids[2:updatelevels+1]:
                    psize[prefix_fileid] = psize.get(prefix_fileid, 0) + filesize

                if cnt_rows % 1000000 == 0:
                    _log.debug("processed %i records", cnt_rows)

        _log.info("processed %i records", cnt_rows)

        cursor.close()

    except Exception:
        # query in progress that use SSCursor can be killed only by terminating DB connection
        # (closing / deleting cursor lead to retreival of all selected entries from DB)
        del(pathname)
        conn_path.close()
        conn_ss.close()
        raise

    conn_ss.close()

    # list of directories with updated "filesize"
    conn = get_connection(conn_data, 'cns_db')
    dirids = get_accounted_dirs(conn, updatelevels)

    updated = 0
    if len(dirids) > 0:
        curr_psize = {}
        cursor = conn.cursor()
        cursor.execute("SELECT fileid, filesize FROM Cns_file_metadata WHERE fileid IN ({0})".format(','.join([ str(x) for x in dirids ])))
        for row in cursor:
            fileid, filesize = row
            curr_psize[fileid] = filesize
        cursor.close()

        pathnames = pathname.get_path_multi(dirids)

        cursor = conn.cursor()

        for fileid, filesize in sorted(curr_psize.items()):
            filesize_new = psize.get(fileid, 0)
            if filesize == filesize_new: continue
            _log.info("%supdate directory %s (id %i) with current size %i to %i", dry_run, pathnames.get(fileid), fileid, filesize, filesize_new)
            if updatedb:
                cursor.execute("UPDATE Cns_file_metadata SET filesize = {0} WHERE fileid = {1}".format(filesize_new, fileid))
            updated += 1

        conn.commit()
        cursor.close()

    conn.close()

    del(pathname)
    conn_path.close()

    _log.info("updated filesize for %i records", updated)



def fix_dir_size(conn_data, updatelevels, updatedb=False):
    _log.debug("fix_dir_size(updatelevels=%i, updatedb=%s", updatelevels, updatedb)

    dry_run = '' if updatedb else 'dry-run: '

    # create snapshot of database data necessary to recalculate
    # relative directory size differences with respect to stored files
    #timestr = time.strftime("%Y%m%d%H%M%S", time.localtime())
    tmptable = 'temptable_fix_dir_size'
    #filename = "{0}_{1}".format(tmptable, timestr)
    updated = 0

    try:

        conn = get_connection(conn_data, 'cns_db')
        cursor = conn.cursor()

        # create temporary table
        cursor.execute("SHOW TABLES")
        for row in cursor:
            if row[0] != tmptable: continue
            _log.debug("removing existing temporary table %s", tmptable)
            cursor.execute("DROP TABLE IF EXISTS {0}".format(tmptable))

        _log.debug("create temporary table %s", tmptable)
        cursor.execute("""
                CREATE TABLE {0} (
                    fileid BIGINT UNSIGNED,
                    parent_fileid BIGINT UNSIGNED,
                    filesize BIGINT UNSIGNED,
                    fileflags TINYINT UNSIGNED,
                    INDEX (fileid)
                ) ENGINE = InnoDB
        """.format(tmptable))

        _log.debug("create snapshot of required data from metadata and replica tables in %s", tmptable)
        conn_ss = get_connection(conn_data, 'cns_db', cclass=pymysql.cursors.SSCursor)
        try:
            sql = """
                SELECT metadata.fileid as fileid, parent_fileid, filesize,
                       IF(filemode & {0} = {0}, 1, 0) |
                       IF(filemode & {1} = {1}, 2, 0) |
                       IF(filemode & {2} = {2}, 4, 0) |
                       IF(NOT ISNULL(replica.rowid), 8, 0) AS fileflags
                FROM Cns_file_metadata AS metadata
                LEFT JOIN Cns_file_replica AS replica
                ON metadata.fileid = replica.fileid
                WHERE metadata.filemode & {1} = {1} OR replica.status = "-"
            """.format(stat.S_IFREG, stat.S_IFDIR, stat.S_IFLNK)

            cursor_ss = conn_ss.cursor()
            cursor_ss.execute(sql)

            cnt_rows = 0
            cnt_rows_info = 0
            while True:
                rows = cursor_ss.fetchmany(1000)
                if len(rows) == 0: break

                cursor.executemany("INSERT INTO {0} (fileid, parent_fileid, filesize, fileflags) values (%s, %s, %s, %s)".format(tmptable), rows)
                conn.commit()

                cnt_rows += len(rows)
                if cnt_rows - 1000000 >= cnt_rows_info:
                    _log.debug("processed %i records", cnt_rows)
                    cnt_rows_info = cnt_rows

            cursor_ss.close()
            conn_ss.close()

        except Exception:
            conn_ss.close()
            raise

        cursor.close()
        conn.close()

        _log.debug("read and caclulate directory size for %s", tmptable)
        conn_ss = get_connection(conn_data, 'cns_db', cclass=pymysql.cursors.SSCursor)
        conn_path = get_connection(conn_data, 'cns_db')
        pathname = CachedFullPath(conn_path, table=tmptable, fileid_only=True)

        psize = {}
        curr_psize = {}
        try:
            # join metadata table with replicas to account size of all file replicas
            sql = "SELECT fileid, parent_fileid, filesize, fileflags FROM {0}".format(tmptable)
            cursor = conn_ss.cursor()
            cursor.execute(sql)

            cnt_rows = 0
            while True:
                # retreiving data in chunks dramatically increase performance
                # mostly because of latency associated with each DB query
                rows = cursor.fetchmany(1000)
                if len(rows) == 0: break

                if _log.getEffectiveLevel() < logging.DEBUG:
                    _log.debug("fetched %i rows", len(rows))

                fileids = [ row[1] for row in rows ]
                pathnames = pathname.get_ids_multi(fileids)

                for row in rows:
                    cnt_rows += 1
                    if _log.getEffectiveLevel() < logging.DEBUG:
                        _log.debug("row %i: %s", cnt_rows, str(row))

                    fileid, pfileid, filesize, fileflags = row

                    prefix_fileids = pathnames.get(pfileid)
                    if prefix_fileids == None:
                        if pfileid != 0: # not top directory
                            _log.warn("skipping fileid %i, unable to reconstruct its parent %i path (dpm-dbck can correct this problem)", fileid, pfileid)
                        continue

                    if fileflags & 1 == 1: # filemode stat.S_IFREG mapped to fileflags 1
                        # replica file
                        if fileflags & 8 == 8: # non-null replicas mapped to fileflags 8
                            if filesize < 0:
                                _log.error("negative size %i for fileid %i", filesize, fileid)

                            # DPM doesn't update directory size for top two level "/dpm"
                            for prefix_fileid in prefix_fileids[2:updatelevels+1]:
                                psize[prefix_fileid] = psize.get(prefix_fileid, 0) + filesize

                    elif fileflags & 2 == 2: # filemode stat.S_IFDIR mapped to fileflags 2
                        # directory
                        if len(prefix_fileids) <= updatelevels:
                            #_log.error("FIXME_PREFIX: prefix %s, level %i, fileid %i, filesize %i", [str(x) for x in prefix_fileids], len(prefix_fileids), fileid, filesize)
                            curr_psize[fileid] = filesize

                    if cnt_rows % 1000000 == 0:
                        _log.debug("processed %i records", cnt_rows)

            _log.info("processed %i records", cnt_rows)

            cursor.close()

        except Exception:
            # query in progress that use SSCursor can be killed only by terminating DB connection
            # (closing / deleting cursor lead to retreival of all selected entries from DB)
            del(pathname)
            conn_path.close()
            conn_ss.close()
            raise

        conn_ss.close()

        if len(curr_psize) > 0:
            # try to get real paths for updated directories
            pathnames = pathname.get_ids_multi(curr_psize.keys())
            pathrealname = CachedFullPath(conn_path)
            pathrealnames = pathrealname.get_path_multi(curr_psize.keys())
            for fileid in pathnames.keys():
                if fileid in pathrealnames:
                    pathnames[fileid] = pathrealnames[fileid]
                else:
                    pathnames[fileid] = "fileidpath[{0}]".format(','.join([ str(x) for x in pathnames[fileid] ]))

            conn = get_connection(conn_data, 'cns_db')
            conn.autocommit(False)
            cursor = conn.cursor()

            for fileid, filesize in sorted(curr_psize.items()):
                filesize_new = psize.get(fileid, 0)
                if filesize == filesize_new: continue
                pdiff = 0. if filesize_new == 0 else float(filesize_new-filesize) / filesize_new
                _log.info("%supdate directory %s (id %i) %.02f%% change from %i to %i, relative difference %i",
                          dry_run, pathnames[fileid], fileid, pdiff,
                          filesize, filesize_new, filesize_new-filesize)
                if updatedb:
                    cursor.execute("BEGIN") # be explicit
                    cursor.execute("UPDATE Cns_file_metadata SET filesize = filesize + {0} WHERE fileid = {1}".format(filesize_new-filesize, fileid))
                    conn.commit()
                updated += 1

            cursor.close()
            conn.close()

            del(pathrealname)

        del(pathname)
        conn_path.close()

    finally:
        conn = get_connection(conn_data, 'cns_db')
        cursor = conn.cursor()
        cursor.execute("SHOW TABLES")
        for row in cursor:
            if row[0] != tmptable: continue
            _log.debug("removing existing temporary table %s", tmptable)
            cursor.execute("DROP TABLE IF EXISTS {0}".format(tmptable))
        cursor.close()
        conn.close()

    _log.info("updated filesize for %i records", updated)



def fix_spacetokens_size(conn_data, updatedb=False):
    """Update spacetoken usable space according associated directory size.
    Consistent results are not completely guaranteed with running DPM/dmlite
    daemons, but this update is very fast and discrepancies should be very
    small (directory size updates and spacetoken updates are not executed
    in one transaction in the DPM/dmlite)."""
    _log.debug("fix_spacetokens_size(updatedb=%s)", updatedb)

    dry_run = '' if updatedb else 'dry-run: '

    conn = get_connection(conn_data, 'cns_db')
    conn_dpm = get_connection(conn_data, 'dpm_db')

    qt = get_quotatoken_data(conn_dpm)

    id2path = {}
    path2id = {}
    path2st = {}
    for path, poolname, u_token, s_token, t_space, g_space, u_space in qt:
        if path.endswith('/'):
            raise Exception("unsupported quotatoken path %s (\"/\" can't be at the end)", path)
            continue
        if path.find('//') != -1:
            raise Exception("unsupported quotatoken path %s (\"//\" used in the path)", path)
            continue
        pathid = get_path_id(conn, path)
        if pathid == None:
            _log.error("unable to find directory associated with %s quotatoken path %s", s_token, path)
            continue

        id2path[pathid] = path
        path2id[path] = pathid
        path2st[path] = u_token

    # for each quotatoken directory find all (closest) subdirectories
    # with assigned quotatoken that must be substracted from spacetoken
    path2subid = {}
    for path, poolname, u_token, s_token, t_space, g_space, u_space in qt:
        accounted_dirs = []
        for longer_path, pathid in sorted(path2id.items(), key=lambda x: len(x[0])):
            if not longer_path.startswith("%s/" % path): continue
            parent_already_accounted = False
            for accounted_dir in accounted_dirs:
                if longer_path.startswith("%s/" % accounted_dir):
                    parent_already_accounted = True
            if parent_already_accounted: continue
            accounted_dirs.append(longer_path)
        path2subid[path] = map(lambda x: path2id[x], accounted_dirs)

    updated = 0
    conn.autocommit(False)
    conn_dpm.autocommit(False)
    cursor = cursor_dpm = None
    for path, pathid in path2id.items():
        try:
            # lock for updates directories that corresponds to the quotatoken path
            # to prevent changes while updating spacetoken data in dpm_space_reserv table
            id2size = {}
            qtsubdirids = path2subid[path]
            cursor = conn.cursor()
            cursor.execute("SELECT fileid, filesize FROM Cns_file_metadata WHERE fileid IN ({0}) FOR UPDATE".format(','.join(map(lambda x: str(x), [pathid] + qtsubdirids))))
            for row in cursor:
                fileid, filesize = row
                id2size[fileid] = filesize
            cursor_dpm = conn_dpm.cursor()
            cursor_dpm.execute("SELECT t_space, u_space FROM dpm_space_reserv WHERE path = %s FOR UPDATE", (path, ))
            t_space, u_space = cursor_dpm.fetchone()
            pathfreespace = t_space - (id2size[pathid] - sum(map(lambda x: id2size[x], qtsubdirids)))
            if u_space != pathfreespace:
                _log.info("%supdate spacetoken %s[%s] %.02f%% relative change %i from %i to %i = t_space(%i) - (dirsize(%i) - sum(%s))",
                          dry_run, path2st[path], path, 100. * (u_space - pathfreespace) / pathfreespace, u_space - pathfreespace, u_space,
                          pathfreespace, t_space, id2size[pathid], ','.join(map(lambda x: "{0}({1})".format(id2path[x], id2size[x]), qtsubdirids)))
                if updatedb:
                    cursor_dpm.execute("BEGIN") # be explicit
                    cursor_dpm.execute("UPDATE dpm_space_reserv SET u_space = %s WHERE path = %s", (pathfreespace, path))
                    conn_dpm.commit()
                updated += 1
        except Exception as e:
            _log.error("failed to update spacetoken %s[%s]: %s", path2st[path], path, str(e))
        finally:
            if cursor_dpm != None: cursor_dpm.close()
            if cursor != None: cursor.close()

    conn_dpm.close()
    conn.close()

    _log.info("updated size for %i spacetokens", updated)




#=====================================================================
# main
#=====================================================================
if __name__ == '__main__':
    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    # parse options from command line
    def opt_set_loglevel(option, opt, value, parser):
        loglevel = option.default
        if value != None:
            loglevel = int({
                'CRITICAL': logging.CRITICAL,
                'DEBUG': logging.DEBUG,
                'ERROR': logging.ERROR,
                'FATAL': logging.FATAL,
                'INFO': logging.INFO,
                'NOTSET': logging.NOTSET,
                'WARN': logging.WARN,
                'WARNING': logging.WARNING,
            }.get(value, value))
        if loglevel < 0:
            # relative log level for multiple -vvv argument
            loglevel += _log.getEffectiveLevel()
            if loglevel < 1: loglevel = 1
        _log.setLevel(loglevel)
        setattr(parser.values, option.dest, loglevel)

    class IndentedHelpFormatterWithEpilogExamples(optparse.IndentedHelpFormatter):
        def format_epilog(self, epilog):
            import textwrap
            if not epilog:
                return ""
            text_width = self.width - self.current_indent
            indent = " "*self.current_indent
            sections = []
            if type(epilog) == str:
                sections.append(textwrap.fill(epilog, text_width, initial_indent=indent, subsequent_indent=indent))
            else:
                example_section = False
                for epilog_section in epilog:
                    if not epilog_section.startswith('EXAMPLE '):
                        sections.append(textwrap.fill(epilog_section, text_width, initial_indent=indent, subsequent_indent=indent))
                        sections.append('')
                        example_section = False
                    else:
                        if not example_section:
                            sections.append('Examples:')
                            example_section = True
                        sections.append("  {0}{1}".format(indent, epilog_section[len('EXAMPLE '):].replace("\n", "\n{0}".format(indent))))
            return "\n{0}\n".format("\n".join(sections))

    # default config values - log level is not yet set while calling these functions
    guess_nsconfig = guess_config_files()
    guess_updatelevels = get_updatelivels(DEFAULT_DOME_CONFIG)

    # command line arguments
    usage = "usage: %prog [options]"
    epilog = []
    epilog.append(
        "By default this script runs in dry mode without any updates in DPM database. Dry-run "
        "is safe to execute with running DPM services (althought results can be a bit inaccurate "
        "due to updates in database). In case you want to apply updates it is first necessary "
        "to stop all DPM services on your DPM headnode.")
    epilog.append(
        "Description of available DB fixes:"
    )
    epilog.append(
        "* zero-dir -- set to 0 the size of all the directories below first N levels. "
        "Time to apply these updates should be less than a minute and it is reasonable "
        "safe to run this fix even with running DPM services."
    )
    epilog.append(
        "* spacetoken -- update spacetoken information according quotatoken paths "
        "(file uploads with non-SRM protocols and legacy DPM were usually done "
        "without proper association with the spacetoken. If you want to use SRM "
        "protocol with DOME DPM these inconsistencies together with out of sync "
        "spacetoken usage can cause troubles enfocing storage space limits. For "
        "pure DOME DPM without legacy adapter you can skip these updates. This "
        "update examine all records in replica table and it can take several "
        "minutes for DPM with tens of millions files. It should be reasonable "
        "safe to apply this fix even with running DPM services."
    )
    epilog.append(
        "* dir-size -- update directory size for first N levels. If you ran dir-size "
        "for first time with running DPM services it might be necessary to run it"
        "twice to reach consistent directory size. This update can take quite a lot of time"
        "and it requires additional privileges to store/read database dump in a file."
    )
    epilog.append(
        "* dir-size-offline -- same updates as in \"dir-size\", but to get "
        "consistent results it is necessary to shutdown all DPM daemons"
        "before executing update script with this option."
    )
    epilog.append(
        "* spacetoken-size -- synchronize spacetoken used space with quotatoken. "
        "This script rely on correct directory size (could be achieved by dir-size "
        "or dir-size-offline) and DPM services must be stopped before executing this "
        "fix in \"updatedb\" mode. This update is very fast and should not take "
        "more than a second."
    )
    epilog.append("EXAMPLE # dry-run with default configuration (no DB updates)")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=zero-dir")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=spacetoken")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=dir-size")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=dir-size-offline")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=spacetoken-size")
    epilog.append("EXAMPLE ")
    epilog.append("EXAMPLE # dry-run multiple DB checks")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --fix=zero-dir,spacetoken,dir-size,spacetoken-size")
    epilog.append("EXAMPLE ")
    epilog.append("EXAMPLE # database updates with --updatedb option must be executed")
    epilog.append("EXAMPLE # while DPM is offline for fixes with \"offline\" suffix")
    epilog.append("EXAMPLE ")
    epilog.append("EXAMPLE # run all consistency checks and updates, recommended for DOME")
    epilog.append("EXAMPLE # migration if you can afford a bit longer downtime, see next")
    epilog.append("EXAMPLE # section if you would like to update running DPM.")
    epilog.append("EXAMPLE # stop DPM services on headnode with init scripts (e.g. SL6)")
    epilog.append("EXAMPLE service httpd stop; service rfiod stop; service srmv2.2 stop; service dpnsdaemon stop; service dpm stop; service dpm-gsiftp stop; service xrootd stop")
    epilog.append("EXAMPLE # stop DPM services on headnode with systemd (e.g. CentOS7)")
    epilog.append("EXAMPLE systemctl stop httpd rfiod srmv2.2 dpnsdaemon dpm dpm-gsiftp xrootd@dpmredir")
    epilog.append("EXAMPLE # execute DB directory and spacetoken size update (can take long time)")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --log-file=/var/log/dmlite-mysql-dirspaces.log --fix=zero-dir,spacetoken,dir-size-offline,spacetoken-size --updatedb")
    epilog.append("EXAMPLE # stop DPM services on headnode with init scripts (e.g. SL6)")
    epilog.append("EXAMPLE service httpd start; service rfiod start; service srmv2.2 start; service dpnsdaemon start; service dpm start; service dpm-gsiftp start; service xrootd start")
    epilog.append("EXAMPLE # stop DPM services on headnode with systemd (e.g. CentOS7)")
    epilog.append("EXAMPLE systemctl start httpd rfiod srmv2.2 dpnsdaemon dpm dpm-gsiftp xrootd@dpmredir")
    epilog.append("EXAMPLE ")
    epilog.append("EXAMPLE # standard consistency checks and updates can be executed with running DPM")
    epilog.append("EXAMPLE # this can be also used with online legacy DPM before migration to DPM DOME")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --log-file=/var/log/dmlite-mysql-dirspaces.log --updatedb")
    epilog.append("EXAMPLE ")
    epilog.append("EXAMPLE # assigning quotatoken to existing directory with stored files")
    epilog.append("EXAMPLE # must be followed by fixing spacetoken data in the database")
    epilog.append("EXAMPLE python /usr/bin/dmlite-mysql-dirspaces.py --log-file=/var/log/dmlite-mysql-dirspaces.log --fix=spacetoken --updatedb")
    description = "Update and synchronize space and spacetoken data for DOME DPM."
    parser = optparse.OptionParser(usage=usage, description=description, version="%prog", epilog=epilog, formatter=IndentedHelpFormatterWithEpilogExamples())
    parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=-10, help="each \"v\" increases the verbosity level")
    parser.add_option("--debug", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    #parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    parser.add_option("-q", "--quiet", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.ERROR, help="set log level to ERROR")
    parser.add_option("--log-level", dest="loglevel", action="callback", callback=opt_set_loglevel, type="string", help="set log level, default: %default")
    parser.add_option("--log-file", dest="logfile", metavar="FILE", help="set log file, default: %default")
    parser.add_option("--log-size", dest="logsize", type="int", default=10*1024*1024, help="maximum size of log file, default: %default")
    parser.add_option("--log-backup", dest="logbackup", type="int", default=4, help="number of log backup files, default: %default")
    parser.add_option("-c", "--nsconfig", dest="nsconfig", default=guess_nsconfig, help="NSCONFIG file with sql connection info, default: %default")
    parser.add_option('--headnode', dest='headnode', default=socket.getfqdn(), help="DPM headnode - verify offline for unsafe DB updates (default: %default)")
    parser.add_option('--dbhost', dest='dbhost', default=None, help="database host, if no NSCONFIG given")
    parser.add_option('--dbuser', dest='dbuser', default=None, help="database user, if no NSCONFIG given")
    parser.add_option('--dbpwd', dest='dbpwd', default=None, help="database password, if no NSCONFIG given")
    parser.add_option('--dbname', dest='dbname', default=DEFAULT_CNS_DB, help="database name, if no NSCONFIG given (default: %default)")
    parser.add_option('--dbdpm', dest='dbdpm', default=DEFAULT_DPM_DB, help="DPM database name, if no NSCONFIG given (default: %default)")
    parser.add_option('--force', dest='force', action='store_true', default=False, help="force unsafe DB updates even tough DPM is still online: %default")
    parser.add_option('--updatedb', dest='updatedb', action='store_true', default=False, help="update original DB with the results of the directory sizes, default: %default")
    parser.add_option('--updatelevels', dest='updatelevels', type='int', default=guess_updatelevels, help="Allows setting the directory size only for the first N levels, default: %default")
    parser.add_option('--fix', dest='fix', default='zero-dir,spacetoken,dir-size,spacetoken-size', help="Apply comma separated DB fixes, default: %default")
    parser.add_option('--skip-spacetokens', dest='skip_spacetokens', action='append', default=[], help="skip fixes for spacetoken, default: %default")


    (options, args) = parser.parse_args()

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    import getpass
    import inspect
    import hashlib
    _log.info("command: %s", " ".join(sys.argv))
    _log.info("script: %s", os.path.abspath(inspect.getfile(inspect.currentframe())))
    _log.info("version: %s", __version__)
    _log.info("sha256: %s", hashlib.sha256(open(__file__).read().encode('utf-8')).hexdigest())
    _log.info("python: %s", str(sys.version_info))
    _log.info("user: %s@%s", getpass.getuser(), socket.gethostname())
    _log.info("system load: %s", str(os.getloadavg()))

    # validate command line options
    fix_names = options.fix.split(',')
    if len(fix_names) == 0:
        _log.error("no fix specified, please --help command line option to get more informations")
        sys.exit(1)
    for fix in fix_names:
        if fix not in [ 'zero-dir', 'spacetoken', 'dir-size', 'dir-size-offline', 'spacetoken-size' ]:
            _log.error("unknown fix \"%s\", use --help command line option to get more informations", fix)
            sys.exit(1)

    if not options.force and len(filter(lambda x: x.endswith('offline'), fix_names)):
        _log.info('DPM must be offline for requested safe DB updates')

        # try to open TCP connections to the DPM headnode service ports
        def test_connection(host, port, results, lock):
            _log.debug("test connection to %s:%i", host, port)
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.settimeout(1.0) # short timeout to local headnode services
                sock.connect((host, port))
                with lock:
                    results.add(port)
            except Exception as e:
                _log.debug("unable to connect to %s:%i: %s", options.headnode, port, str(e))

        cthreads = []
        results = set()
        lock = threading.Lock()

        # NOTE: we should use configured ports
        for port in [ 443, 1094, 2811, 8446 ]:
            cthread = threading.Thread(target=test_connection, args=(options.headnode, port, results, lock))
            cthread.start()
            cthreads.append((cthread, port))

        maxtime = time.time() + 5.0
        for cthread, port in cthreads:
            _log.debug("joining connection thread for %s:%i", options.headnode, port)
            timeout = maxtime - time.time()
            if timeout < 0: break
            cthread.join(timeout) # maximum time in case connect gets stuck

        if len(results) > 0:
            _log.error("DPM must be offline for requested DB updates %s (headnode %s open ports %s) or you can add --force which doesn't guarantee correct directory/spacetoken size updates", ','.join(fix_names), options.headnode, ','.join([ str(x) for x in sorted(results) ]))
            sys.exit(1)

    conn_data = {}
    if options.dbhost == None:
        if options.nsconfig == None:
            _log.error("no database configuration specified, use either nsconfig or db* command line options")
            sys.exit(1)

        conn_data = get_conn_data(options.nsconfig)

    else:
        if options.dbuser == None or options.dbpwd == None:
            _log.error("no database user or password defined as command line options")
            sys.exit(1)

        conn_data['host'] = options.dbhost
        conn_data['user'] = options.dbuser
        conn_data['pass'] = options.dbpwd
        conn_data['cns_db'] = options.dbname
        conn_data['dpm_db'] = options.dbdpm

    _log.info("%s %s for %i levels of the DPM database (host=%s, cns_db=%s, dpm_db=%s)", 'fix' if options.updatedb else 'dry-run', ', '.join(fix_names), options.updatelevels, conn_data['host'], conn_data['cns_db'], conn_data['dpm_db'])

    starttime = datetime.datetime.now()
    if 'zero-dir' in fix_names: fix_zero_dirs(conn_data, options.updatelevels, updatedb=options.updatedb)
    if 'spacetoken' in fix_names: fix_spacetokens_by_path(conn_data, skip=options.skip_spacetokens, updatedb=options.updatedb)
    if 'dir-size-offline' in fix_names: fix_dir_size_offline(conn_data, options.updatelevels, updatedb=options.updatedb)
    if 'dir-size' in fix_names: fix_dir_size(conn_data, options.updatelevels, updatedb=options.updatedb)
    if 'spacetoken-size' in fix_names: fix_spacetokens_size(conn_data, updatedb=options.updatedb)
    endtime = datetime.datetime.now()

    _log.info("done (time: %ss)", (endtime-starttime).seconds)
