#!/usr/bin/python2
# CGI wrapper for dpm-storage-summary.py
#
# Parameters read from environment:
#   DPM_SRR_HOST    fqdn of DPM headnode (default: local machine)
#   DPM_SRR_PORT    DPM DOME port (default: 1094)
#   DPM_SRR_CERT    DPM DOME auth certificate (default: /etc/grid-security/dpmmgr/dpmcert.pem)
#   DPM_SRR_KEY     DPM DOME auth key (default: /etc/grid-security/dpmmgr/dpmkey.pem)
#   DPM_LOG_LEVEL   script log level for syslog logging (default: WARN)
#   DPM_SRR_CACHE   use cached data in case DPM DOME interface is unavailable (default: 0)
#                   -1 .. infinite cache time
#                   0 ... cache disable
#                   n ... cache expiration time in seconds
#   DPM_SRR_FILE    file for cached data (default: /tmp/storagesummary.json)
#
# Simple apache configuration:
#   ScriptAlias /static/srr "/usr/bin/dpm-storage-summary.cgi"
# Apache configuration with all available SRR options passed as environment:
#   SetEnvIf Request_URI /static/srr DPM_SRR_HOST=fqdn DPM_SRR_PORT=1094 DPM_SRR_CERT=/etc/grid-security/dpmmgr/dpmcert.pem DPM_SRR_KEY=/etc/grid-security/dpmmgr/dpmkey.pem DPM_SRR_LOG_LEVEL=WARN DPM_SRR_CACHE=0 DPM_SRR_FILE=/tmp/storagesummary.json
#   ScriptAlias /static/srr "/usr/bin/dpm-storage-summary.cgi"
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os, sys, time
import socket
import StringIO
import json
import logging
import logging.handlers


class Config(object):
    pass


try:
    handler = logging.handlers.SysLogHandler(address='/dev/log')
    _log = logging.getLogger('DPMSRR')
    _log.setLevel(logging.WARN)
    _log.addHandler(handler)

    srr = __import__('dpm-storage-summary')

    options = Config()
    options.host = os.getenv('DPM_SRR_HOST', socket.getfqdn())
    options.port = os.getenv('DPM_SRR_PORT', 1094)
    options.cert = os.getenv('DPM_SRR_CERT', '/etc/grid-security/dpmmgr/dpmcert.pem')
    options.key = os.getenv('DPM_SRR_KEY', '/etc/grid-security/dpmmgr/dpmkey.pem')

    cache_time = 0
    cache_file = os.getenv('DPM_SRR_FILE', '/tmp/storagesummary.json')
    try:
        cache_time = int(os.getenv('DPM_SRR_CACHE', 0))
    except:
        pass

    if os.getenv('DPM_SRR_LOG_LEVEL') != None:
        loglevel = int({
            'CRITICAL': logging.CRITICAL,
            'DEBUG': logging.DEBUG,
            'ERROR': logging.ERROR,
            'FATAL': logging.FATAL,
            'INFO': logging.INFO,
            'NOTSET': logging.NOTSET,
            'WARN': logging.WARN,
            'WARNING': logging.WARNING,
        }.get(os.getenv('DPM_SRR_LOG_LEVEL').upper(), os.getenv('DPM_SRR_LOG_LEVEL')))
        _log.setLevel(loglevel)

    try:
        # read new SRR info using DPM DOME interface
        dpm = srr.storageService(options)
        dpm.addshares()
        dpm.addendpoints()
    except Exception as e:
        _log.error("unable to get fresh SRR info from DPM DOME: %s", str(e))
        if cache_time != 0 and os.path.exists(cache_file):
            # failed to access DPM DOME => use cached data
            try:
                with open(cache_file) as f:
                    srr_data = json.load(f)
                    latestupdate = srr_data['storageservice']['latestupdate']
                    if cache_time < 0 or time.time() < latestupdate + cache_time:
                        _log.error("using cached data from %s: %s", cache_file, time.ctime(latestupdate))
                        sys.stdout.write("Content-type: application/json\r\n\r\n")
                        sys.stdout.write(json.dumps(srr_data, indent=4))
                        sys.exit(0)
                    else:
                        _log.info("cached data too old %s: %i + %i >= %i", cache_file, latestupdate, cache_time, time.time())
            except Exception as e:
                _log.warn("unable to read SRR from %s: %s", cache_file, str(e))
        sys.stdout.write("Status: 503 DPM DOME Service Unavailable\r\n")
        sys.stdout.write("Retry-After: 600\r\n")
        sys.stdout.write("Content-Type: text/html\r\n\r\n")
        sys.stdout.write("<h1>Unable to access DPM DOME interface</h1>\r\n")
        sys.stdout.write("<pre>{0}</pre>\r\n".format(str(e)))
        sys.exit(1)

    _log.debug("publish fresh SRR data")
    out = StringIO.StringIO()
    out.write("Content-type: application/json\r\n\r\n")
    dpm.printjson(out)
    sys.stdout.write(out.getvalue())

    if cache_time != 0:
        # update cache file
        try:
            with open(cache_file, "w") as f:
                dpm.printjson(f)
        except Exception as e:
            _log.warn("unable to save SRR data in cache file %s: %s", cache_file, str(e))

except Exception as e:
    sys.stdout.write("Status: 500 Internal Server Error\r\n")
    sys.stdout.write("Content-Type: text/html\r\n\r\n")
    sys.stdout.write("<h1>Unexpected exception occured</h1>\r\n")
    sys.stdout.write("<pre>{0}</pre>\r\n".format(str(e)))
    sys.exit(1)
