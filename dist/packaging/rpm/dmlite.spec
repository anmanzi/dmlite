%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib(1))")}

%{!?dmlite_test: %global dmlite_tests 0}

# By default we build without AddressSanitizer. To enable it,
# pass the "--with asan" flag to the rpmbuild command
%bcond_without asan

# additional macros based on environment or existing definitions
%if %{?_with_asan:1}%{!?_with_asan:0}
  %define devtoolset devtoolset-8
%endif

# systemd definition, to do the right thing if we need to restart daemons
%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
%global systemd 1
%else
%global systemd 0
%endif


%{!?_httpd_mmn: %{expand: %%global _httpd_mmn %%(cat %{_includedir}/httpd/.mmn || echo 0-0)}}


Name:					dmlite
Version:				1.15.0
Release:				1%{?dist}
Summary:				Lcgdm grid data management and storage framework
Group:					Applications/Internet
License:				ASL 2.0
URL:					https://svnweb.cern.ch/trac/lcgdm/wiki/Dpm/Dev/Dmlite
# The source of this package was pulled from upstream's vcs. Use the
# following commands to generate the tarball:
# svn export http://svn.cern.ch/guest/lcgdm/dmlite/tags/dmlite_0_7_0_d dmlite-0.7.0
# tar -czvf dmlite-0.7.0.tar.gz dmlite-0.7.0
Source0:				%{name}-%{version}.tar.gz
Buildroot:				%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
BuildRequires:                  boost-devel >= 1.48.0
%else
BuildRequires:                  boost148-devel >= 1.48.0
%endif


BuildRequires:			cmake
BuildRequires:			cmake3
BuildRequires:			cppunit-devel
BuildRequires:			doxygen
BuildRequires:			graphviz
BuildRequires:			openssl-devel
BuildRequires:			python-devel
BuildRequires:			zlib-devel

%if %{?fedora}%{!?fedora:0} >= 29 || %{?rhel}%{!?rhel:0} >= 7
BuildRequires:      python%{python3_pkgversion}-devel
BuildRequires:      boost-python%{python3_pkgversion}-devel
%endif

# Plugins require
BuildRequires:			dpm-devel
BuildRequires:			lcgdm-devel
BuildRequires:			libmemcached-devel
BuildRequires:			mysql-devel
BuildRequires:			protobuf-devel
BuildRequires:			davix-devel >= 0.6.7
BuildRequires:			globus-gridftp-server-devel >= 11.8
BuildRequires:			voms-devel
%if %systemd
BuildRequires:			systemd
%endif
BuildRequires:			gridsite-devel
BuildRequires:			gsoap-devel
BuildRequires:			httpd-devel
BuildRequires:			jansson-devel
BuildRequires:  		libbsd-devel

# Address sanitizer builds (mock libasan5 sclo config)
%if %{?_with_asan:1}%{!?_with_asan:0}
%if %{?rhel}%{!?rhel:0} == 6
BuildRequires: sl-release-scl
BuildRequires: %{devtoolset} %{devtoolset}-libasan-devel
%else
%if %{?rhel}%{!?rhel:0} >= 7
BuildRequires: epel-release
BuildRequires: %{devtoolset} %{devtoolset}-libasan-devel
%else
BuildRequires: libasan
%endif
%endif
%endif

%description
This package provides the dmlite framework that implements common
logic for data management and storage for the Lcg grid.





%package apache-httpd
Summary:	Apache HTTPD frontend for dmlite
Requires:	httpd%{?_isa}
Requires:	httpd-mmn = %{_httpd_mmn}
Requires:	dmlite-libs = %{version}-%{release}

Provides:	lcgdm-dav-server = %{version}-%{release}
Provides:	lcgdm-dav-libs = %{version}-%{release}
Provides:	mod-lcgdm-dav = %{version}-%{release}
Provides:	lcgdm-dav = %{version}-%{release}

Obsoletes:	lcgdm-dav-server < %{version}-%{release}
Obsoletes:	lcgdm-dav-libs < %{version}-%{release}
Obsoletes:	mod_lcgdm_dav < %{version}-%{release}
Obsoletes:	lcgdm-dav < %{version}-%{release}
Obsoletes:	lcgdm-dav-debuginfo < %{version}-%{release}

Requires:	gridsite%{?_isa} >= 1.7
Requires:	httpd%{?_isa}
Requires:	mod_ssl%{?_isa}

%description apache-httpd
This package provides the HTTP/WebDAV frontend to DMLite. It's used for DPM and Dynafed



%files apache-httpd
%defattr(-,root,root,-)
%{_bindir}/htcopy
%{_mandir}/man1/htcopy.1*
%doc src/plugins/apache-httpd/src/client/README LICENSE
%{_libdir}/liblcgdmhtext.so.*
%{_libdir}/libdmlitemacaroons.so*
%doc README LICENSE
%{_libdir}/httpd/modules/mod_lcgdm_ns.so
%{_libdir}/httpd/modules/mod_lcgdm_disk.so
%{_libdir}/httpd/modules/mod_lcgdm_dav.so
%{_datadir}/lcgdm-dav/*
%config(noreplace) %{_sysconfdir}/httpd/conf.d/*
%config(noreplace) %{_sysconfdir}/cron.d/*




%package dpmhead-domeonly
Summary:  DPM Head Node (MySQL) in DOME-only configuration
Group:    Applications/Internet
Requires: bdii

Requires: dmlite-dpm-dsi = %{version}
Requires: dmlite-plugins-domeadapter = %{version}
Requires: dmlite-dome = %{version}
Requires: dmlite-shell = %{version}
Requires: edg-mkgridmap
Requires: fetch-crl
Requires: dmlite-apache-httpd = %{version}
Requires: davix >= 0.6.7
Requires: xrootd >= 1:5.0.2

Obsoletes: emi-dpm_mysql
Obsoletes: dpmhead
Obsoletes: dpmhead-dome
Conflicts: dpm%{?_isa}
Conflicts: dpm-devel%{?_isa}
Conflicts: dpm-name-server-mysql%{?_isa}
Conflicts: dpm-perl%{?_isa}
#Conflicts: dpm-python%{?_isa}
Conflicts: dpm-rfio-server%{?_isa}
Conflicts: dpm-server-mysql%{?_isa}
Conflicts: dpm-srm-server-mysql%{?_isa}
Conflicts: dmlite-plugins-adapter

%description dpmhead-domeonly
The Disk Pool Manager (DPM) creates a Grid storage element from a set
of disk servers. It provides several interfaces for storing and retrieving
data such as HTTP, Xrootd, GridFTP
This is a metapackage providing all required daemons for a DPM Head Node that
does not contain the legacy servers.

%package dpmdisk-domeonly
Summary:  DPM Disk Node
Group:    Applications/Internet

Requires: dmlite-dpm-dsi = %{version}

Requires: dmlite-plugins-domeadapter = %{version}
Requires: dmlite-dome = %{version}
Requires: edg-mkgridmap
Requires: fetch-crl
Requires: dmlite-apache-httpd = %{version}

Requires: davix >= 0.6.7

Obsoletes: emi-dpm_disk
Obsoletes: dpmdisk
Obsoletes: dpmdisk-dome
Conflicts: dpm%{?_isa}
Conflicts: dpm-devel%{?_isa}
Conflicts: dpm-perl%{?_isa}
Conflicts: dpm-python%{?_isa}
Conflicts: dpm-rfio-server%{?_isa}
Conflicts: dmlite-plugins-adapter = %{version}

%description dpmdisk-domeonly
The Disk Pool Manager (DPM) creates a Grid storage element from a set
of disk servers. It provides several interfaces for storing and retrieving
data such as HTTP, Xrootd, GridFTP
This is a metapackage providing all required daemons for a DPM
Disk Node that does not contain the legacy servers.










%package dpmhead
Summary:  EMI DPM Head Node (MySQL)
Group:    Applications/Internet
Requires: bdii
Requires: dpm%{?_isa} >= 1.10
Requires: dpm-devel%{?_isa} >= 1.10
Requires: dmlite-dpm-dsi = %{version}
Requires: dpm-name-server-mysql%{?_isa} >= 1.10
Requires: dpm-perl%{?_isa} >= 1.10
Requires: dpm-python%{?_isa} >= 1.10
Requires: dpm-rfio-server%{?_isa} >= 1.10
Requires: dpm-server-mysql%{?_isa} >= 1.10
Requires: dpm-srm-server-mysql%{?_isa} >= 1.10

Requires: dmlite-plugins-adapter = %{version}
Requires: dmlite-plugins-domeadapter = %{version}
Requires: dmlite-dome = %{version}
Requires: dmlite-shell = %{version}
Requires: dmlite-plugins-mysql = %{version}
Requires: edg-mkgridmap
Requires: fetch-crl
Requires: dmlite-apache-httpd = %{version}
Requires: davix >= 0.6.7

Obsoletes: emi-dpm_mysql


%description dpmhead
The Disk Pool Manager (DPM) creates a Grid storage element from a set
of disk servers. It provides several interfaces for storing and retrieving
data such as HTTP, Xrootd, GridFTP
This is a metapackage providing all required daemons for a DPM Head Node, including
legacy components like SRM, RFIO, libshift, CSec, dpm, dpnsdaemon.

%package dpmdisk
Summary:  EMI DPM Disk Node
Group:    Applications/Internet
Requires: dpm%{?_isa} >= 1.10
Requires: dpm-devel%{?_isa} >= 1.10
Requires: dpm-perl%{?_isa} >= 1.10
Requires: dpm-python%{?_isa} >= 1.10
Requires: dpm-rfio-server%{?_isa} >= 1.10

Requires: dmlite-dpm-dsi = %{version}
Requires: dmlite-plugins-adapter = %{version}
Requires: dmlite-plugins-domeadapter = %{version}
Requires: dmlite-dome = %{version}
Requires: dmlite-shell = %{version}
Requires: edg-mkgridmap

Requires: fetch-crl
Requires: dmlite-apache-httpd = %{version}
Requires: davix >= 0.6.7

Obsoletes: emi-dpm_disk

%description dpmdisk
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This is a virtual package providing all required daemons for a DPM
Disk Node, including legacy components like RFIO, libshift, CSec.












%package libs
Summary:			Libraries for dmlite packages
Group:				Applications/Internet

# transition fix for package merge dmlite-*.src.rpm to dmlite.src.rpm
Obsoletes:			dmlite-plugins-adapter-debuginfo < 0.7.0-1
Obsoletes:			dmlite-plugins-mysql-debuginfo < 0.7.0-1
Obsoletes:			dmlite-plugins-memcache-debuginfo < 0.7.0-1
Obsoletes:			dmlite-plugins-profiler-debuginfo < 0.7.0-1
Obsoletes:			dmlite-plugins-librarian-debuginfo < 0.7.0-1
Obsoletes:			dmlite-shell-debuginfo < 0.7.0-1

# dpm-xrootd was the standalone project/package. Now dmlite-dpm-xrootd is part of dmlite
Conflicts:      dpm-xrootd

# # dpm-dsi was the standalone project/package. Now it's one of the dmlite plugins
Conflicts:      dpm-dsi < %{version}

# lcgdm-dav was the standalone project/package. Now it's built with the dmlite plugins, and we don't want 
Conflicts:      lcgdm-dav-server < %{version}
Conflicts:      lcgdm-dav < %{version}
Conflicts:      lcgdm-dav-libs < %{version}
Conflicts:      mod_lcgdm_dav < %{version}

# Versions prior to this one do not properly do accounting on directories
Conflicts:      lcgdm-libs < 1.10

# Versions prior to this one do not have the PoolManager::fileCopyPush/Pull and
# the C calls dmlite_copypush/pull
Conflicts:      dynafed < 1.5.0

%description libs
This package provides the core libraries of dmlite.

%package dome
Summary:			The dome daemon
Group:				Applications/Internet
Requires:     httpd
Requires:     xrootd >= 1:5.0.2
BuildRequires:     xrootd-devel >= 1:5.0.2
BuildRequires:     xrootd-server-devel >= 1:5.0.2
BuildRequires:     xrootd-private-devel >= 1:5.0.2

%description dome
This package provides the binaries necessary to run the dome daemon.

%package devel
Summary:			Development libraries and headers for dmlite
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}

%description devel
This package provides C headers and development libraries for dmlite.

%package docs
Summary:			Documentation files for dmlite
Group:				Applications/Internet


%description docs
This package contains the man pages and HTML documentation for dmlite.


%package private-devel
Summary:			Private development libraries and headers for dmlite
Group:				Applications/Internet
Requires:			%{name}-devel%{?_isa} = %{version}-%{release}
%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
BuildRequires:                  boost-devel >= 1.48.0
%else
BuildRequires:                  boost148-devel >= 1.48.0
%endif
Obsoletes:			dpm-xrootd-devel


%description private-devel
Private development headers for dmlite. Provided for the development of
dmlite plugins only, no API compatibility is guaranteed on these headers.



%package dpm-tester
Summary:      The dpm tester tool
Group:        Applications/Internet
Requires:     python
Requires:     gfal2-python
Requires:     python-argparse
Requires:     gfal2-plugin-http
Requires:     gfal2-plugin-xrootd
Requires:     gfal2-plugin-srm
Requires:     gfal2-plugin-gridftp
Requires:     gfal2-plugin-file

%description dpm-tester
Tool that is useful to test the main features of a DPM setup

%package -n python-dmlite
Summary:			Python wrapper for dmlite
Group:				Development/Libraries

%description -n python-dmlite
This package provides a python wrapper for dmlite.

%if %{?fedora}%{!?fedora:0} >= 29 || %{?rhel}%{!?rhel:0} >= 7
%package -n python%{python3_pkgversion}-dmlite
Summary:                        Python wrapper for dmlite
Group:                          Development/Libraries
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-dmlite
This package provides a python wrapper for dmlite.

%files -n python%{python3_pkgversion}-dmlite
%defattr(-,root,root,-)
%{python3_sitearch}/pydmlite.so
%endif

%package test
Summary:			All sorts of tests for dmlite interfaces
Group:				Applications/Internet

%description test
Set of C,CPP and Python tests for dmlite interfaces and plug-ins.

%package plugins-memcache
Summary:			Memcached plugin for dmlite
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}

# Merge migration
Obsoletes:			dmlite-plugins-memcache < 0.7.0-1

%description plugins-memcache
This package provides the memcached plug-in for dmlite. It provides a
memcached based layer for the Lcgdm nameserver.

%package plugins-profiler
Summary:			Monitoring plugin for dmlite
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}

# Merge migration
Obsoletes:			dmlite-plugins-profiler < 0.7.0-1

%description plugins-profiler
This package provides the profiler plug-in for dmlite. This plug-in
provides multiple performance measurement tools for dmlite.


%package plugins-librarian
Summary:                        Libarian plugin for dmlite
Group:                          Applications/Internet
Requires:                       %{name}-libs%{?_isa} = %{version}-%{release}

# Merge migration
Obsoletes:                      dmlite-plugins-librarian < 0.7.0-1

%description plugins-librarian
This package provides the librarian plug-in for dmlite.




%package shell
Summary:			Shell environment for dmlite
Group:				Applications/Internet
#%if %{?fedora}%{!?fedora:0} >= 10 || %{?rhel}%{!?rhel:0} >= 6
#BuildArch:			noarch
#%endif

Requires:			python-dateutil
Requires:			python-dmlite = %{version}
Requires:     MySQL-python
Requires:     python-pycurl
Requires:     python-ldap
Requires:     python-libs
Requires:     rpm-python
Requires:     davix >= 0.6.7
Requires:     m2crypto

Obsoletes:			dmlite-shell < %{version}-%{release}

%description shell
This package provides a shell environment for dmlite. It includes useful
commands for system administration, testers and power users.








%package dpm-xrootd
Summary:			XROOT interface to the Disk Pool Manager (DPM)
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(post):		chkconfig
Requires(postun):	initscripts
Requires:		xrootd >= 1:5.0.2
Requires:		xrootd-client >= 1:5.0.2
Requires:		xrootd-selinux >= 1:5.0.2
Conflicts:		vomsxrd <= 1:0.2.0
Conflicts:		xrootd-server-atlas-n2n-plugin <= 2.1
Conflicts:		xrootd-alicetokenacc <= 1.2.2
BuildRequires:	openssl-devel
BuildRequires:	xrootd-server-devel >= 1:4.12.3
%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
BuildRequires:                  boost-devel >= 1.48.0
%else
BuildRequires:                  boost148-devel >= 1.48.0
%endif
Obsoletes:              dpm-xrootd

%description dpm-xrootd
This package contains plugins for XROOTD to allow it to provide
access to DPM managed storage via the XROOT protocol.


%preun dpm-xrootd
if [ "$1" = "0" ]; then
    /sbin/service xrootd stop >/dev/null 2>&1 || :
    /sbin/service cmsd stop >/dev/null 2>&1 || :
fi

%postun dpm-xrootd
/sbin/ldconfig
if [ "$1" -ge "1" ] ; then
    /sbin/service xrootd condrestart >/dev/null 2>&1 || :
    /sbin/service cmsd condrestart >/dev/null 2>&1 || :
fi

%files dpm-xrootd
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/xrootd/xrootd-dpmdisk.cfg
%config(noreplace) %{_sysconfdir}/xrootd/xrootd-dpmfedredir_atlas.cfg
%config(noreplace) %{_sysconfdir}/xrootd/xrootd-dpmredir.cfg
%{_libdir}/libXrdDPMDiskAcc-5.so
%{_libdir}/libXrdDPMDiskAcc.so-5.3
%{_libdir}/libXrdDPMFinder-5.so
%{_libdir}/libXrdDPMFinder.so-5.3
%{_libdir}/libXrdDPMOss-5.so
%{_libdir}/libXrdDPMOss.so-5.3
%{_libdir}/libXrdDPMRedirAcc-5.so
%{_libdir}/libXrdDPMRedirAcc.so-5.3
%{_libdir}/libXrdDPMStatInfo-5.so
%{_libdir}/libXrdDPMStatInfo.so-5.3
%{_libdir}/libXrdDPMCks-5.so
%{_libdir}/libXrdDPMCks.so-5.3










%package dpm-dsi
Summary:	Disk Pool Manager (DPM) plugin for the Globus GridFTP server
Group:		Applications/Internet
Requires:	globus-gridftp-server-progs >= 11.8
Requires:	dmlite-libs = %{version}-%{release}

%if %systemd
Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd
%else
Requires(post):     	chkconfig
Requires(preun):    	chkconfig
Requires(preun):    	initscripts
Requires(postun):   	initscripts
%endif

Provides:			DPM-gridftp-server = %{version}-%{release}
Obsoletes:		DPM-gridftp-server < %{version}-%{release}
Provides:			DPM-DSI = %{version}-%{release}
Obsoletes:		DPM-DSI < %{version}-%{release}
Obsoletes:		dpm-dsi < %{version}-%{release}

%description dpm-dsi
The dpm-dsi package provides a Disk Pool Manager (DPM) plugin for the 
Globus GridFTP server, following the Globus Data Storage Interface (DSI).

The Disk Pool Manager (DPM) is a lightweight storage solution for grid sites.
It offers a simple way to create a disk-based grid storage element and 
supports relevant protocols (SRM, gridFTP, RFIO) for file 
management and access.

Globus provides open source grid software, including a server implementation
of the GridFTP protocol. This plugin implements the DPM backend specifics 
required to expose the data using this protocol.



%files dpm-dsi
%defattr(-,root,root)
%if %systemd
%attr(0644,root,root) %{_unitdir}/dpm-gsiftp.service
%else
%{_initrddir}/dpm-gsiftp
%endif
%config(noreplace) %{_sysconfdir}/logrotate.d/dpm-gsiftp
%config(noreplace) %{_sysconfdir}/sysconfig/dpm-gsiftp
%{_libdir}/libglobus_gridftp_server_dmlite*.so*
%{_localstatedir}/log/dpm-gsiftp
%doc LICENSE RELEASE-NOTES

%post dpm-dsi
%if %systemd
	/bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
	/sbin/chkconfig --add dpm-gsiftp
%endif
/sbin/ldconfig

%preun dpm-dsi
if [ $1 -eq 0 ] ; then
%if %systemd
	/bin/systemctl stop dpm-gsiftp.service > /dev/null 2>&1 || :
	/bin/systemctl --no-reload disable dpm-gsiftp.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm-gsiftp stop > /dev/null 2>&1
	/sbin/chkconfig --del dpm-gsiftp
%endif
fi

%postun dpm-dsi
/sbin/ldconfig 
if [ $1 -ge 1 ]; then
%if %systemd
        /bin/systemctl try-restart dpm-gsiftp.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm-gsiftp condrestart > /dev/null 2>&1 || :
%endif
fi


















%package plugins-mysql
Summary:			MySQL plugin for dmlite
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}

Obsoletes:			dmlite-plugins-mysql < 0.7.0-1

%description plugins-mysql
This package provides the MySQL plug-in for dmlite.

%package plugins-adapter
Summary:			Adapter plugin for dmlite
Group:				Applications/Internet
Requires:			%{name}-libs%{?_isa} = %{version}-%{release}
Requires:			dpm-libs >= 1.8.8
Requires:			lcgdm-libs >= 1.8.8

Obsoletes:			dmlite-plugins-adapter < 0.7.0-1

%description plugins-adapter
This package provides the adapter plug-in for dmlite. This plug-in provides both
a name-space and pool management implementation which fallback to forwarding
calls to the old LcgDM DPNS and DPM daemons.

%package plugins-domeadapter
Summary:      Adapter plugin for dmlite
Group:        Applications/Internet
Requires:     %{name}-libs%{?_isa} = %{version}-%{release}

%description plugins-domeadapter
This package provides the next-generation adapter plug-in for dmlite, which uses
dome and does not depend on the old LcgDM DPNS and DPM daemons.


%package puppet-dpm
Summary:                        Puppet modules for DPM configuration
Group:                          Applications/Internet
BuildArch:                      noarch

Obsoletes:                      dmlite-puppet-dpm < %{version}-%{release}

%description puppet-dpm
This package provides the modules for the DPM configuration via puppet



















%prep
%setup -q -n %{name}-%{version}

%global build_flags -DCMAKE_INSTALL_PREFIX=/ -DRUN_ONLY_STANDALONE_TESTS=ON -DOVERWRITE_CONFIGFILES=ON
%if %systemd
%global build_flags %{build_flags} -DSYSTEMD_INSTALL_DIR=%{_unitdir}
%endif
%if %{?_with_asan:1}%{!?_with_asan:0}
%global build_flags %{build_flags} -DASAN=1
%endif

%build
./src/plugins/apache-httpd/buildcurl.sh
%if %{?_with_asan:1}%{!?_with_asan:0} && %{?rhel}%{!?rhel:0} >= 6
#  . scl_source enable %{devtoolset} 2> /dev/null > /dev/null
  . /opt/rh/%{devtoolset}/enable
%endif
%cmake3 . %{build_flags}

make %{?_smp_mflags}
make doc

%check
pushd tests
LD_LIBRARY_PATH=~+/../src/ ctest -V
if [ $? -ne 0 ]; then
    exit 1
fi
popd

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
mkdir -p %{buildroot}%{_libdir}

make install DESTDIR=%{buildroot}
# clean up the startup scripts we don't need - otherwise rpmbuild will fail
# due to unpackaged files
%if %systemd
  rm -rf %{buildroot}/%{_sysconfdir}/rc.d
%else
  rm -rf %{buildroot}/usr/lib/systemd
%endif

## remote tests if not needed
%if %{?dmlite_tests} == 0
rm -rf %{buildroot}/%{_libdir}/dmlite/test
%endif

%define basefolder %{buildroot}/%{_prefix}/share/dmlite/puppet/modules
mkdir -p %{basefolder}
cp -R src/puppet/dmlite %{basefolder}
cp -R src/puppet/dpm %{basefolder}
cp -R src/puppet/gridftp %{basefolder}
cp -R src/puppet/lcgdm %{basefolder}
cp -R src/puppet/xrootd %{basefolder}
mkdir -p %{basefolder}/bdii
tar zxvf src/puppet/CERNOps-bdii-*.tar.gz -C %{basefolder}/bdii/ --strip-components 1
mkdir -p %{basefolder}/fetchcrl
tar zxvf src/puppet/puppet-fetchcrl-*.tar.gz -C %{basefolder}/fetchcrl/ --strip-components 1
mkdir -p %{basefolder}/firewall
tar zxvf src/puppet/puppetlabs-firewall-*.tar.gz -C %{basefolder}/firewall/ --strip-components 1
mkdir -p %{basefolder}/memcached
tar zxvf src/puppet/saz-memcached-*.tar.gz -C %{basefolder}/memcached/ --strip-components 1
mkdir -p %{basefolder}/mysql
tar zxvf src/puppet/puppetlabs-mysql-*.tar.gz -C %{basefolder}/mysql/ --strip-components 1
mkdir -p %{basefolder}/stdlib
tar zxvf src/puppet/puppetlabs-stdlib-*.tar.gz -C %{basefolder}/stdlib --strip-components 1
mkdir -p %{basefolder}/concat
tar zxvf src/puppet/puppetlabs-concat-*.tar.gz  -C %{basefolder}/concat/ --strip-components 1
mkdir -p %{basefolder}/translate
tar zxvf src/puppet/puppetlabs-translate-*.tar.gz  -C %{basefolder}/translate/ --strip-components 1
mkdir -p %{basefolder}/voms
tar zxvf src/puppet/lcgdm-voms-*.tar.gz  -C %{basefolder}/voms/ --strip-components 1

## for dpm-xrootd
ln -s libXrdDPMFinder-5.so %{buildroot}%{_libdir}/libXrdDPMFinder.so-5.3
ln -s libXrdDPMDiskAcc-5.so %{buildroot}%{_libdir}/libXrdDPMDiskAcc.so-5.3
ln -s libXrdDPMOss-5.so %{buildroot}%{_libdir}/libXrdDPMOss.so-5.3
ln -s libXrdDPMRedirAcc-5.so %{buildroot}%{_libdir}/libXrdDPMRedirAcc.so-5.3
ln -s libXrdDPMStatInfo-5.so %{buildroot}%{_libdir}/libXrdDPMStatInfo.so-5.3
ln -s libXrdDPMCks-5.so %{buildroot}%{_libdir}/libXrdDPMCks.so-5.3

## for dpm-dsi
install -p -d -m 755 %{buildroot}%{_localstatedir}/log/dpm-gsiftp

%clean
rm -rf %{buildroot}

%post libs
/sbin/ldconfig
/sbin/service rsyslog condrestart || true
%if %systemd
        /bin/systemctl try-restart dpm.service || true
        /bin/systemctl try-restart dpnsdaemon.service || true
        /bin/systemctl try-restart httpd.service || true
        /bin/systemctl try-restart dpm-gsiftp.service || true
%else
        /sbin/service dpm condrestart  || true
        /sbin/service dpnsdaemon condrestart ||true
        /sbin/service httpd condrestart || true
        /sbin/service dpm-gsiftp condrestart || true
%endif


%postun libs -p /sbin/ldconfig


%files dpmhead-domeonly
%defattr(-,root,root,-)
%{_prefix}/share/dmlite/dbscripts
%{_prefix}/share/dmlite/filepull
%{_prefix}/share/dmlite/StAR-accounting

%files dpmdisk-domeonly
%defattr(-,root,root,-)
%{_prefix}/share/dmlite/filepull



%files dpmhead
%defattr(-,root,root,-)
%{_prefix}/share/dmlite/dbscripts
%{_prefix}/share/dmlite/filepull
%{_prefix}/share/dmlite/StAR-accounting

%files dpmdisk
%defattr(-,root,root,-)
%{_prefix}/share/dmlite/filepull

%files libs
%defattr(-,root,root,-)
%dir %{_sysconfdir}/dmlite.conf.d
%dir %{_libdir}/dmlite
%config(noreplace) %{_sysconfdir}/dmlite.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/dmlite
%config(noreplace) %{_sysconfdir}/rsyslog.d/20-log-dmlite.conf
%{_libdir}/libdmlite.so.*
%{_libdir}/dmlite/plugin_config.so
%doc README LICENSE RELEASE-NOTES

%files dome
%defattr(-,root,root,-)
%{_bindir}/dome-checksum
%{_libdir}/libdome*.so
%{_sysconfdir}/domehead.conf.example
%{_sysconfdir}/domedisk.conf.example

%files devel
%defattr(-,root,root,-)
%{_includedir}/dmlite/c
%{_includedir}/dmlite/common
%{_libdir}/libdmlite.so
%{_includedir}/lcgdm-dav
%{_libdir}/liblcgdmhtext.so

%files private-devel
%defattr(-,root,root,-)
%{_includedir}/dmlite/cpp
## for dpm-xrootd
%defattr(-,root,root) 
%dir %{_includedir}/XrdDPM
%{_includedir}/XrdDPM/XrdCompileVersion.hh

%files dpm-tester
%defattr(-,root,root,-)
%{_bindir}/dpm-tester.py


%files docs
%defattr(-,root,root,-)
%{_mandir}/man3/*
%{_defaultdocdir}/%{name}-%{version}

%files -n python-dmlite
%defattr(-,root,root,-)
%{python_sitearch}/pydmlite.so

%if %{?dmlite_tests}

%files test
%defattr(-,root,root,-)
%{_libdir}/dmlite/test

%endif %{?dmlite_tests}

%files plugins-memcache
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_memcache.so
%doc LICENSE README RELEASE-NOTES
%config(noreplace) %{_sysconfdir}/dmlite.conf.d/zmemcache.conf

%files plugins-profiler
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_profiler.so
%doc LICENSE README RELEASE-NOTES
%config(noreplace) %{_sysconfdir}/dmlite.conf.d/profiler.conf

%files plugins-librarian
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_librarian.so
%doc LICENSE README RELEASE-NOTES


%files shell
%defattr(-,root,root,-)
%{_bindir}/dmlite-shell
%{_bindir}/dpm-storage-summary.py 
%{_bindir}/dpm-storage-summary.cgi
%{_bindir}/dmlite-mysql-dirspaces.py
%{_bindir}/dome-info-provider.py
%{_sharedstatedir}/bdii/gip/provider/dome-info-exec
%config(noreplace) %{_sysconfdir}/sysconfig/dpminfo
%if %{?rhel}%{!?rhel:99} <= 5
%{_bindir}/dmlite-mysql-dirspaces.pyc
%{_bindir}/dmlite-mysql-dirspaces.pyo
%endif
%{python_sitelib}/dmliteshell
%doc LICENSE README RELEASE-NOTES

%files plugins-mysql
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_mysql.so
%doc LICENSE README RELEASE-NOTES
%config(noreplace) %{_sysconfdir}/dmlite.conf.d/mysql.conf

%files plugins-adapter
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_adapter.so
%doc LICENSE README RELEASE-NOTES
%config(noreplace) %{_sysconfdir}/dmlite.conf.d/adapter.conf

%files plugins-domeadapter
%defattr(-,root,root,-)
%{_libdir}/dmlite/plugin_domeadapter.so
%doc LICENSE README RELEASE-NOTES
%config(noreplace) %{_sysconfdir}/dmlite.conf.d/domeadapter.conf

%files puppet-dpm
%defattr(-,root,root,-)
%{_prefix}/share/dmlite/puppet/modules

%changelog
* Tue Jul 21 2020  Fabrizio Furano <furano@cern.ch> - 1.14
- Require xrootd >= 4.12.3

* Thu Dec 20 2018  Fabrizio Furano <furano@cern.ch> - 1.11.1
- Require xrootd >= 4.9

* Thu Apr 19 2018  Andrea Manzi <amanzi@cern.ch>  1.10.2
- fix schema upgrade script
- add deps to lcgdm >=1.10

* Wed Mar 7 2018  Fabrizio Furano <furano@cern.ch> - 1.10.1
- Build with xrootd 4.8.2

* Thu Feb 8 2018  Fabrizio Furano <furano@cern.ch> - 1.10.0
- Build with xrootd 4.8.0

* Thu Oct 13 2016  Fabrizio Furano <furano@cern.ch> - 0.8.1
- Add metapackages for dome flavour setup

* Mon Feb 15 2016  Andrea Manzi <amanzi@cern.ch> - 0.7.6-1
- Added move replicat to dmlite-shell
- fix crash in dmlite-plugins-mysql
- some fixes in dmlite-shell drain


* Mon Nov 02 2015  Andrea Manzi <amanzi@cern.ch> - 0.7.5-1
- added xattr to Memcache plugin
- fix for checksums store

* Wed Jul 08 2015  Fabrizio Furano <furano@cern.ch> - 0.7.3-1
- Add librarian to the core plugins

* Mon Nov 17 2014  Fabrizio Furano <furano@cern.ch> - 0.7.2-1
- Fix logname on RFIO.cpp
- Fix logging issue in adapter

* Fri Oct 03 2014 Andrea Manzi <amanzi@cern.ch> - 0.7.1-1
- Fix for wrong file size stored in Memcache
- Fix for xroot third party copy when Memcache enabled

* Mon Jun 16 2014 Fabrizio Furano <furano@cern.ch> - 0.7.0-2
- Push on Fedora/EPEL for 0.7.0
- Fix ppc EPEL5 compilation issue

* Mon Jun 16 2014 Fabrizio Furano <furano@cern.ch> - 0.7.0-1
- Introduced the private devel headers
- Merged shell, profiler, memcache, mysql, adapter

* Fri Nov 29 2013 Alejandro Alvarez <aalvarez@cern.ch> - 0.6.1-2
- Enabled Python bindings

* Wed Jul 10 2013 Alejandro Alvarez <aalvarez@cern.ch> - 0.6.1-1
- Update for new upstream release

* Wed Dec 19 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.6.0-1
- Update for new upstream release

* Thu Oct 25 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.5.0-1
- Update for new upstream release

* Wed Oct 24 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.4.2-2
- Fedora #869568 - dmlite-libs should own /usr/lib(64)/dmlite

* Mon Sep 24 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.4.2-1
- update for new upstream release
- dropped plugin packages (moved to separate individual packages)

* Sat Sep 22 2012  Remi Collet <remi@fedoraproject.org> - 0.3.0-2
- rebuild against libmemcached.so.11 without SASL

* Thu Jul 19 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.3.0-1
- Update for new upstream release

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jun 05 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-3
- Removed subversion build dep
- Added patches for proper tests compilation (missing include, wrong cmake dep)

* Tue Feb 28 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-2
- Split plugins into multiple packages, added dependencies
- Updated package descriptions

* Tue Jan 31 2012 Alejandro Alvarez <alejandro.alvarez.ayllon@cern.ch> - 0.2.0-1
- Added documentation to the build process

* Mon Jan 23 2012 Alejandro Alvarez <alejandro.alvarez.ayllon@cern.ch> - 0.1.0-1
- Added cppunit-devel as a build dependency

* Fri Jan 20 2012 Alejandro Alvarez <alejandro.alvarez.ayllon@cern.ch> - 0.1.0-1
- Created spec file
