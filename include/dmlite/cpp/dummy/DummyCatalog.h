/// @file    include/dmlite/cpp/dummy/DummyCatalog.h
/// @brief   A dummy plugin that just delegates calls to a decorated one.
/// @details It makes sense as a base for other decorator plug-ins.
/// @author  Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef DMLITE_CPP_DUMMY_CATALOG_H
#define DMLITE_CPP_DUMMY_CATALOG_H

#include "../catalog.h"

namespace dmlite {

  // Dummmy catalog implementation
  class DummyCatalog: public Catalog
  {
   public:

    /// Constructor
    /// @param decorated The underlying decorated catalog.
    DummyCatalog(Catalog* decorated)  ;

    /// Destructor
    virtual ~DummyCatalog();

    // Overloading
    virtual void setStackInstance(StackInstance*)  ;
    virtual void setSecurityContext(const SecurityContext*)  ;

    virtual void        changeDir     (const std::string&)  ;
    virtual std::string getWorkingDir (void)                ;

    virtual ExtendedStat extendedStat(const std::string&, bool)  ;
    virtual DmStatus extendedStat(ExtendedStat &xstat, const std::string&, bool)  ;
    virtual ExtendedStat extendedStatByRFN(const std::string& rfn)  ;

    virtual bool access(const std::string& path, int mode)  ;
    virtual bool accessReplica(const std::string& replica, int mode)  ;

    virtual void addReplica   (const Replica&)  ;
    virtual void deleteReplica(const Replica&)  ;
    virtual std::vector<Replica> getReplicas(const std::string&)  ;

    virtual void symlink (const std::string&, const std::string&)  ;
    std::string  readLink(const std::string& path)  ;

    virtual void unlink(const std::string&)                      ;

    virtual void create(const std::string&, mode_t)  ;

    virtual mode_t umask       (mode_t)                           throw ();
    virtual void   setMode     (const std::string&, mode_t)        ;
    virtual void   setOwner    (const std::string&, uid_t, gid_t, bool)  ;

    virtual void setSize    (const std::string&, size_t)  ;
    virtual void setChecksum(const std::string&, const std::string&, const std::string&)  ;
    virtual void getChecksum(const std::string& path,
                             const std::string& csumtype,
                             std::string& csumvalue,
                             const std::string& pfn, const bool forcerecalc = false, const int waitsecs = 0)  ;
                             
                             
    virtual void setAcl(const std::string&, const Acl&)  ;

    virtual void utime(const std::string&, const struct utimbuf*)  ;

    virtual std::string getComment(const std::string&)                      ;
    virtual void        setComment(const std::string&,
                                   const std::string&)  ;

    virtual void setGuid(const std::string&,
                         const std::string&)  ;

    virtual void updateExtendedAttributes(const std::string&,
                                          const Extensible&)  ;


    virtual Directory* openDir (const std::string&)  ;
    virtual void       closeDir(Directory*)          ;

    virtual struct dirent* readDir (Directory*)  ;
    virtual ExtendedStat*  readDirx(Directory*)  ;

    virtual void makeDir(const std::string&, mode_t)  ;

    virtual void rename     (const std::string&, const std::string&)  ;
    virtual void removeDir  (const std::string&)                      ;

    virtual Replica getReplicaByRFN(const std::string& rfn)  ;
    virtual void    updateReplica(const Replica& replica)  ;

   protected:
    Catalog* decorated_;
  };

};

#endif // DMLITE_DUMMY_CATALOG_H
