/// @file    core/builtin/Catalog.h
/// @brief   Implementation of a Catalog using other plugins, as INode.
/// @details Intended to ease the development of database backends.
/// @author  Alejandro Álvarez Ayllon <aalvarez@cern.ch>
#ifndef BUILTIN_CATALOG_H
#define	BUILTIN_CATALOG_H

#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/inode.h>
#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/pooldriver.h>

namespace dmlite {

  struct BuiltInDir: public Directory {
    virtual ~BuiltInDir() {};
    IDirectory*  idir;
    ExtendedStat dir;
  };

  class BuiltInCatalog: public Catalog {
   public:
    BuiltInCatalog(bool updateATime, unsigned symLinkLimit)  ;
    ~BuiltInCatalog();

    std::string getImplId(void) const throw();

    void setStackInstance(StackInstance* si)  ;

    void setSecurityContext(const SecurityContext*)  ;

    void        changeDir     (const std::string&)  ;
    std::string getWorkingDir (void)  ;

    DmStatus extendedStat(ExtendedStat &xstat,
                          const std::string& path,
                          bool followSym)  ;

    ExtendedStat extendedStat(const std::string& path,
                              bool followSym = true)  ;
    ExtendedStat extendedStatByRFN(const std::string& rfn)  ;


                                     
    bool access(const std::string& path, int mode)  ;
    bool accessReplica(const std::string& replica, int mode)  ;

    void addReplica   (const Replica& replica)  ;
    void deleteReplica(const Replica& replica)  ;

    std::vector<Replica> getReplicas(const std::string& path)  ;
    Location get(const std::string& path)  ;

    Location put(const std::string& path)  ;
    void     putDone(const std::string& host, const std::string& rfn,
                    const std::map<std::string, std::string>& params)  ;

    void symlink(const std::string& oldpath,
                 const std::string& newpath)  ;
    std::string readLink(const std::string& path)  ;

    void unlink(const std::string& path)  ;

    void create(const std::string& path, mode_t mode)  ;

    void makeDir  (const std::string& path, mode_t mode)  ;
    void removeDir(const std::string& path)  ;

    void rename(const std::string& oldPath,
                const std::string& newPath)  ;

    mode_t umask(mode_t mask) throw ();

    void setMode     (const std::string& path, mode_t mode)  ;
    void setOwner    (const std::string& path, uid_t newUid, gid_t newGid,
                      bool followSymLink = true)  ;

    void setSize    (const std::string& path, size_t newSize)  ;

    void setAcl(const std::string& path, const Acl& acls)  ;

    void utime(const std::string& path, const struct utimbuf* buf)  ;

    std::string getComment(const std::string& path)  ;
    void        setComment(const std::string& path,
                           const std::string& comment)  ;

    void setGuid(const std::string& path,
                 const std::string &guid)  ;

    void updateExtendedAttributes(const std::string& path,
                                  const Extensible& attr)  ;

    Directory*     openDir (const std::string& path)  ;
    void           closeDir(Directory* dir)  ;
    struct dirent* readDir (Directory* dir)  ;
    ExtendedStat*  readDirx(Directory* dir)  ;

    Replica getReplicaByRFN(const std::string& rfn)  ;
    void    updateReplica(const Replica& replica)    ;

   protected:
    /// Get the parent of a directory.
    /// @param path       The path to split.
    /// @param parentPath Where to put the parent path.
    /// @param name       Where to put the file name (stripping last /).
    /// @return           The parent metadata.
    ExtendedStat getParent(const std::string& path, std::string* parentPath,
                          std::string* name)  ;

    /// Update access time (if updateATime is true)
    void updateAccessTime(const ExtendedStat& meta)  ;

    /// Traverse backwards to check permissions.
    /// @param meta The file at the end
    /// @note       Throws an exception if it is not possible.
    void traverseBackwards(const ExtendedStat& meta)  ;

    /// addFileSizeToParents
    /// Add (or subtract) the size of the given file from
    /// all its parent directories
    /// @param fname The logical file name (SFN) of the file in question
    /// @param subtract If true then subtract instead of adding
    void addFileSizeToParents(const std::string &fname, bool subtract)  ;

    /// addFileSizeToParents
    /// Add (or subtract) the size of the given file from
    /// all its parent directories
    /// @param st The stat information about the file in question
    /// @param subtract If true then subtract instead of adding
    void addFileSizeToParents(const ExtendedStat &statinfo, bool subtract)  ;

   private:
    StackInstance*   si_;

    const SecurityContext* secCtx_;

    std::string cwdPath_;
    ino_t       cwd_;

    mode_t   umask_;
    bool     updateATime_;
    unsigned symLinkLimit_;
  };

  /// Plug-ins must implement a concrete factory to be instantiated.
  class BuiltInCatalogFactory: public CatalogFactory {
   public:
    BuiltInCatalogFactory();
    ~BuiltInCatalogFactory();

    void configure(const std::string&, const std::string&)  ;

    Catalog* createCatalog(PluginManager*)  ;

   private:
    bool     updateATime_;
    unsigned symLinkLimit_;
  };

};

#endif	// BUILTIN_CATALOG_H
