#include <dmlite/cpp/pooldriver.h>
#include "NotImplemented.h"


using namespace dmlite;



PoolDriverFactory::~PoolDriverFactory()
{
  // Nothing
}



std::string PoolDriverFactory::implementedPool() throw ()
{
  return std::string();
}



FACTORY_NOT_IMPLEMENTED(PoolDriver* PoolDriverFactory::createPoolDriver(void)  );



PoolDriver::~PoolDriver()
{
  // Nothing
}



NOT_IMPLEMENTED(PoolHandler* PoolDriver::createPoolHandler(const std::string&)  );
NOT_IMPLEMENTED(void PoolDriver::toBeCreated(const Pool&)  );
NOT_IMPLEMENTED(void PoolDriver::justCreated(const Pool&)  );
NOT_IMPLEMENTED(void PoolDriver::update(const Pool&)  );
NOT_IMPLEMENTED(void PoolDriver::toBeDeleted(const Pool&)  );



PoolHandler::~PoolHandler()
{
  // Nothing
}



NOT_IMPLEMENTED_WITHOUT_ID(std::string PoolHandler::getPoolType(void)  );
NOT_IMPLEMENTED_WITHOUT_ID(std::string PoolHandler::getPoolName(void)  );
NOT_IMPLEMENTED_WITHOUT_ID(uint64_t PoolHandler::getTotalSpace(void)  );
NOT_IMPLEMENTED_WITHOUT_ID(uint64_t PoolHandler::getFreeSpace(void)  );
NOT_IMPLEMENTED_WITHOUT_ID(bool PoolHandler::poolIsAvailable(bool)  );
NOT_IMPLEMENTED_WITHOUT_ID(bool PoolHandler::replicaIsAvailable(const Replica&)  );
NOT_IMPLEMENTED_WITHOUT_ID(Location PoolHandler::whereToRead(const Replica&)  );
NOT_IMPLEMENTED_WITHOUT_ID(void PoolHandler::removeReplica(const Replica&)  );
NOT_IMPLEMENTED_WITHOUT_ID(Location PoolHandler::whereToWrite(const std::string&)  );
NOT_IMPLEMENTED_WITHOUT_ID(void PoolHandler::cancelWrite(const Location& loc)  );
