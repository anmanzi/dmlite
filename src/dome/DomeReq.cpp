/*
 * Copyright 2015 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/** @file   DomeReq.cpp
 * @brief  A helper class that describes a request to dpmrest, with helpers to parse it
 * @author Fabrizio Furano
 * @date   Aug 2015
 */

#include "DomeReq.h"
#include "DomeLog.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "cpp/utils/urls.h"
#include "utils/DomeUtils.h"
#include "utils/HorribleMutex.h"


std::string disksrvurl(const char *protopfx, const char *srv) {
  std::string durl(protopfx);
  durl += srv;
  if (!strchr(srv, ':')) {
    durl += ":";
    durl += CFG->GetString("head.defaultdiskport", (char *)"1095");
  }
  durl += CFG->GetString("head.diskdomemgmtsuffix", (char *)"/domedisk/"); 
  
  return durl;
} 


int DomeReq::takeJSONbodyfields(char *body) {

  // We assume that the body that we received is in JSON format
  std::istringstream s(body);
  Log(Logger::Lvl4, domelogmask, domelogname, "Entering: '" << body << "'");
  if ( strlen(body) > 2 ) {

    try {
      //boost::lock_guard<boost::mutex> l(horribleboostmtx);
      boost::property_tree::read_json(s, bodyfields);
    } catch (boost::property_tree::json_parser_error &e) {
      Err("takeJSONbodyfields", "Could not process JSON: " << e.what() << " '" << body << "'");
      return -1;
    }

  }

  Log(Logger::Lvl3, domelogmask, domelogname, "Exiting: '" << body << "'");
  return 0;
}

int DomeReq::SendSimpleResp(int httpcode, const std::ostringstream &body, const char *logwhereiam) {
  return SendSimpleResp(httpcode, body.str(), logwhereiam);
}

int DomeReq::SendSimpleResp(int httpcode, const boost::property_tree::ptree &body, const char *logwhereiam) {
  std::ostringstream os;
  boost::property_tree::write_json(os, body);
  return SendSimpleResp(httpcode, os.str(), logwhereiam);
}




int DomeReq::TakeRequest() {
  Log(Logger::Lvl4, domelogmask, domelogname, "Ctor");
  
  verb = xrequest.verb;
  object = xrequest.resource;
  
  Log(Logger::Lvl4, domelogmask, domelogname, "Acquiring request: '" << verb << "' '" << object << "'");
  
  std::vector<std::string> vecurl = dmlite::Url::splitPath(object);
  if (vecurl.size() > 1)
    domecmd = vecurl[vecurl.size()-1];
  
  // Extract the authz info about the client that dared to attempt a connection
  // This is the remote user, typically a grid identity
  creds.clientName = GetHeaderParam("remoteclientdn", "<unknownuser>");
  creds.remoteAddress = GetHeaderParam("remoteclienthost", "<unknownhost>");
  std::string s = GetHeaderParam("remoteclientgroups", "");
  creds.groups = DomeUtils::split(s, ",");
  
  creds.oidc_audience = GetHeaderParam("remoteclientoidcaudience", "");
  creds.oidc_issuer = GetHeaderParam("remoteclientoidcissuer", "");
  creds.oidc_scope = GetHeaderParam("remoteclientoidcscope", "");
  
  
  clientdn = xrequest.clientdn;
  clienthost = xrequest.clienthost;
  //this->clientfqans = request.clientgroups;
  
  //   // We also must know the info on the client that is contacting us
  //   // This is typically a server of the cluster that is forwarding a request (e.g. putdone)
  //   if ( (s = FCGX_GetParam("SSL_CLIENT_S_DN", request.envp)) )
  //     this->clientdn = s;
  //   if ( (s = FCGX_GetParam("REMOTE_HOST", request.envp)) )
  //     this->clienthost = s;
  
  
  // We assume that the body fits in 4K, otherwise we ignore it ?!?
  char buf[4096];
  char *buf2;
  int nb = 0;
  if (xrequest.length >= 4096-1) {
    nb = SendSimpleResp(500, (char *) "Request body too large");
    return -1;
  }
  Log(Logger::Lvl4, domelogmask, domelogname, "Going to read body: " << xrequest.length << " bytes.");
  if ( (nb = xrequest.BuffgetData(xrequest.length, &buf2, true)) < xrequest.length ) {
    nb = SendSimpleResp(501, SSTR("Error in getting the request body. len: " << xrequest.length << " nb: " << nb));
    return -1;
  }
  Log(Logger::Lvl4, domelogmask, domelogname, "Got body: " << nb << " bytes.");
  if (nb < (int)sizeof(buf)) {
    memcpy(buf, buf2, nb);
    buf[nb] = '\0';
    Log(Logger::Lvl4, domelogmask, domelogname, "Body: '" << buf << "'");
    takeJSONbodyfields( buf );
    return 0;
  }
  
  return -1;
}



int DomeReq::SendSimpleResp(int httpcode, const char *body, const char *logwhereiam) {
  Log(Logger::Lvl4, domelogmask, domelogname, "Entering: code: " << httpcode << " body: '" << body << "'");
  
  xrequest.SendSimpleResp(httpcode, NULL, NULL, (char *)body, strlen(body));
  
  // We prefer to log all the responses
  if (logwhereiam) {
    
    if ((Logger::get()->getLevel() >= 2) || (httpcode >= 400))
      Log(Logger::Lvl2, domelogmask, logwhereiam, "Exiting: code: " << httpcode << " body: '" << body << "'");
    else
      Log(Logger::Lvl1, domelogmask, logwhereiam, "Exiting: code: " << httpcode);
    
  } else if ((Logger::get()->getLevel() >= 2) || (httpcode >= 400))
      Log(Logger::Lvl2, domelogmask, domelogname, "Exiting: code: " << httpcode << " body: '" << body << "'");
    else
      Log(Logger::Lvl1, domelogmask, domelogname, "Exiting: code: " << httpcode);
    
    return 1;
}

int DomeReq::SendSimpleResp(int httpcode, const std::string &body, const char *logwhereiam) {
  std::string fixed_body = DomeUtils::unescape_forward_slashes(body);
  Log(Logger::Lvl4, domelogmask, domelogname, "Entering: code: " << httpcode << " body: '" << fixed_body << "'");

  xrequest.SendSimpleResp(httpcode, NULL, NULL, (char *)fixed_body.c_str(), fixed_body.length());
  
  // We prefer to log all the responses
  if (logwhereiam) {

    if ((Logger::get()->getLevel() >= 2) || (httpcode >= 400))
       Log(Logger::Lvl2, domelogmask, logwhereiam, "Exiting: code: " << httpcode << " body: '" << fixed_body << "'");
    else
       Log(Logger::Lvl1, domelogmask, logwhereiam, "Exiting: code: " << httpcode);

  } else

    if ((Logger::get()->getLevel() >= 2) || (httpcode >= 400))
       Log(Logger::Lvl2, domelogmask, domelogname, "Exiting: code: " << httpcode << " body: '" << fixed_body << "'");
    else
       Log(Logger::Lvl1, domelogmask, domelogname, "Exiting: code: " << httpcode);

  return 1;
}


std::string DomeReq::GetHeaderParam(const char *key, const char *dflt = "") {
  std::map<std::string, std::string>::iterator i;
  i = xrequest.headers.find(key);
  
  if (i != xrequest.headers.end())
    return i->second;
  
  return dflt;
}

void DomeReq::fillSecurityContext(dmlite::SecurityContext &ctx) {
  // Fill the information we have about the client machine that is
  // submitting the request
  ctx.credentials.clientName = clientdn;
  ctx.credentials.remoteAddress = clienthost;
  
  // Fill the information we have about the user that
  // sent the original request
  ctx.user.name = creds.clientName;
  
  for(size_t i = 0; i < creds.groups.size(); i++) {
    dmlite::GroupInfo g;
    g.name = creds.groups[i];
    ctx.groups.push_back(g);
  }
  
}
