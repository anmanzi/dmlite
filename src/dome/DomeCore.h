/*
 * Copyright 2015 CERN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/** @file   DomeCore.h
 * @brief  The main class where operation requests are applied to the status
 * @author Fabrizio Furano
 * @date   Dec 2015
 */

#ifndef DOMECORE_H
#define DOMECORE_H


#include "DomeStatus.h"
#include "utils/TaskExec.h"
#include "DomeReq.h"
#include "DomeLog.h"

#include "utils/mysqlpools.h"
#include "utils/DavixPool.h"

#include "utils/Config.hh"
#include "DomeMysql.h"
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <bitset>
#include <boost/thread.hpp>
#include "cpp/catalog.h"
#include "cpp/authn.h"
#include <sys/stat.h>
#include <unistd.h>

/// Data associated to a running checksum calculation
struct PendingChecksum {
  std::string lfn;
  std::string server;
  std::string pfn;
  dmlite::DomeCredentials creds;
  std::string chksumtype;
  bool updateLfnChecksum;

  PendingChecksum(std::string _lfn, std::string _server, std::string _pfn, dmlite::DomeCredentials _creds, std::string _chksumtype, bool _updateLfnChecksum)
  : lfn(_lfn), server(_server), pfn(_pfn), creds(_creds), chksumtype(_chksumtype), updateLfnChecksum(_updateLfnChecksum) {}

  PendingChecksum() {}
};

/// Data associated to a pending file pull
struct PendingPull {
  std::string lfn;
  std::string server;
  std::string pfn;
  dmlite::DomeCredentials creds;
  std::string chksumtype;

  PendingPull(std::string _lfn, std::string _server, std::string _pfn, dmlite::DomeCredentials _creds, std::string _chksumtype)
  : lfn(_lfn), server(_server), pfn(_pfn), creds(_creds), chksumtype(_chksumtype) {}

  PendingPull() {}
};
// Remember: A DMLite connection pool to the db is just a singleton, always accessible

/// Data associated to a pending file put
struct PendingPut {
  int64_t fileid;
  std::string lfn;
  std::string server;
  std::string pfn;
  std::string rfn;
  time_t puttime;
  
  PendingPut(std::string _lfn, int64_t _fid, std::string _server, std::string _pfn, std::string _rfn): fileid(_fid), lfn(_lfn), server(_server), pfn(_pfn), rfn(_rfn) {
    puttime = time(0);
  };
 
  PendingPut() {};
};


class DomeCore: public dmlite::dmTaskExec {

public:

  DomeCore();
  virtual ~DomeCore();

  /// Reads the config file and initializes all the subsystems
  /// After this call everything has to be operative
  /// To be called after the ctor to initialize the object.
  /// @param cfgfile Path to the config file to be loaded
  int init(const char *cfgfile = 0);

  /// The app status, plus functions that modify it
  DomeStatus status;


  /// Ticking this gives life to the objects belonging to this class
  /// This is useful for managing things that expire, pings or periodic checks
  virtual void tick(int parm);
  virtual void queueTick(int parm);
  virtual void pendingPutsTick(int parm);
  int sendInformerstring(std::ostringstream &urlquery);
  int getInformerstring(std::ostringstream &str);
  void informerTick(int parm);
  
  int processreq(DomeReq &dreq);

  /// Requests calls. These parse the request, do actions and send the response, using the original fastcgi func
  int dome_put(DomeReq &req, bool &success, struct DomeFsInfo *destfs = 0, std::string *destrfn = 0, bool dontsendok = false, int64_t *spacetomake = NULL);
  int dome_putdone_head(DomeReq &req);
  int dome_putdone_disk(DomeReq &req);
  int dome_getspaceinfo(DomeReq &req);
  int dome_getquotatoken(DomeReq &req);
  int dome_setquotatoken(DomeReq &req);
  int dome_modquotatoken(DomeReq &req);
  int dome_delquotatoken(DomeReq &req);
  int dome_chksum(DomeReq &req);
  int dome_chksumstatus(DomeReq &req);

  int dome_get(DomeReq &req);
  int dome_pullstatus(DomeReq &req);
  int dome_statpool(DomeReq &req);
  int dome_ping(DomeReq &req) {
    
    int r = req.SendSimpleResp(200,
                               "Content-type: text/html\r\n"
                               "\r\n"
                               "Have a nice day.\r\n");
    
    return (r);
  };

  int dome_pull(DomeReq &req);
  int dome_dochksum(DomeReq &req);
  int dome_statpfn(DomeReq &req);

  int dome_getdirspaces(DomeReq &req);

  /// Disk server only. Removes a physical file
  int dome_pfnrm(DomeReq &req);
  /// Disk server only. Removes a replica, both from the catalog and the disk
  int dome_delreplica(DomeReq &req);
  /// Updates the fields of a replica
  int dome_updatereplica(DomeReq &req);
  /// Adds a new pool
  int dome_addpool(DomeReq &req);
  /// Modify an existing pool
  int dome_modifypool(DomeReq &req);
  /// Removes a pool and all the related filesystems
  int dome_rmpool(DomeReq &req);
  /// Adds a filesystem to an existing pool. This implicitly creates a pool
  int dome_addfstopool(DomeReq &req);
  /// Removes a filesystem, no matter to which pool it was attached
  int dome_rmfs(DomeReq &req);
  /// Modifies an existing filesystem
  int dome_modifyfs(DomeReq &req);


  /// Creates a directory
  int dome_makedir(DomeReq &req);
  
  /// Fecthes logical stat information for an LFN or file ID or a pfn
  int dome_getstatinfo(DomeReq &req);
  /// Fecthes replica info from a rfn
  int dome_getreplicainfo(DomeReq &req);
  /// Get all the replicas of a given file
  int dome_getreplicavec(DomeReq &req);
  /// Like an HTTP GET on a directory, gets all the content
  int dome_getdir(DomeReq &req);
  /// Get user information
  int dome_getuser(DomeReq &req);
  /// Get all the uses in one shot
  int dome_getusersvec(DomeReq &req);
  /// Adds an user
  int dome_newuser(DomeReq &req);
  /// Delete an user
  int dome_deleteuser(DomeReq &req);
  /// Update an user
  int dome_updateuser(DomeReq &req);
  /// Get group information
  int dome_getgroup(DomeReq &req);
  /// Update a group
  int dome_updategroup(DomeReq &req);
  /// Get all the groups in one shot
  int dome_getgroupsvec(DomeReq &req);
  /// Delete a group
  int dome_deletegroup(DomeReq &req);
  /// Create a new group
  int dome_newgroup(DomeReq &req);
  /// Get id mapping
  int dome_getidmap(DomeReq &req);
  /// Update extended attributes
  int dome_updatexattr(DomeReq &req);
  /// Make space in volatile filesystems
  int dome_makespace(DomeReq &req);

  /// Send a simple info message
  int dome_info(DomeReq &req, int myidx, bool authorized);
		
		/// Chooses a random server or filesystem where to do side things like a tunnel
		int dome_chooseserver(DomeReq &req);
  /// Tells if a n user can access a file
  int dome_access(DomeReq &req);
  /// Tells if an user can access a replica
  int dome_accessreplica(DomeReq &req);
  /// Add a replica to a file
  int dome_addreplica(DomeReq &req);
  /// Create a new file
  int dome_create(DomeReq &req);
  /// Get the comment associated to a file
  int dome_getcomment(DomeReq &req);
  /// Get the destination of a symlink
  int dome_readlink(DomeReq &req);
  /// Remove a directory
  int dome_removedir(DomeReq &req);
  /// Like the unix command 'mv'
  int dome_rename(DomeReq &req);
  /// Oh no! Setting an Acl can perturb the Universe!
  int dome_setacl(DomeReq &req);
  /// Set a comment
  int dome_setcomment(DomeReq &req);
  /// Sets various file metadata fields
  int dome_setmode(DomeReq &req);
  /// Set the uid/gid
  int dome_setowner(DomeReq &req);
  /// Set the size of a file
  int dome_setsize(DomeReq &req);
  /// Set the checksum  of a file
  int dome_setchecksum(DomeReq &req);
  /// Create a symlink
  int dome_symlink(DomeReq &req);
  /// Delete a file and all its replicas
  int dome_unlink(DomeReq &req);
  
  
  int makespace(std::string fsplusvo, int64_t size);
  bool addFilesizeToDirs(DomeMySql &sql, dmlite::ExtendedStat file, int64_t size);
  /// Utility: fill a dmlite security context with ALL the information we have
  /// about the client that is sending the request and the user that originated it
  /// NOTE: This is a relevant part of the authorization policy for users
  void fillSecurityContext(dmlite::SecurityContext &ctx, DomeReq &req);
  
  /// Utility: perform a permission  check that is broader than the one of dmlite,
  /// e.g. taking into account the bearer tokens too
  int checkPermissionsFullpath(const std::string &fullpath, const dmlite::SecurityContext* context, const dmlite::Acl& acl, const struct stat& stat, __mode_t mode );
  
  virtual void onLoggingRequest(Logger::Level lvl, std::string const & msg) {
    Log(lvl, domelogmask, domelogname, msg);   
  };
  
  /// Event invoked internally to log stuff
  virtual void onErrLoggingRequest(std::string const & msg) {
    Err((Logger::Level)0, msg);   
  };
  
private:
  
  
  bool initdone, terminationrequested;
  boost::recursive_mutex mtx;
  boost::mutex accept_mutex;
  int fcgi_listenSocket;

  
  
  dmlite::DavixCtxFactory *davixFactory;
  dmlite::DavixCtxPool *davixPool;


  /// The thread that ticks
  boost::thread *ticker;
  boost::thread *queueTicker;
  boost::thread *informerTicker;
  boost::thread *pendingPutsTicker;
  
  Davix::Context informerdavixctx;
  Davix::RequestParams informerdavixparams;
  
  // monitor pull and checksum queues
  void TickQueuesFast();
  boost::condition_variable tickqueue_cond;
  boost::mutex tickqueue_mtx;

  /// Atomically increment and returns the number of put requests that this server saw since the last restart
  long getGlobalputcount();

  /// Pending disknode checksums
  std::map<int, PendingChecksum> diskPendingChecksums;

    /// Pending disknode file pulls
  std::map<int, PendingPull> diskPendingPulls;

protected:

  // given some hints, pick a list of filesystems for writing
  std::vector<DomeFsInfo> pickFilesystems(const std::string &pool, const std::string &host, const std::string &fs);

  // Put tasks that have not yet received a putdone. The key is the rfio-style url
  boost::mutex putqueue_mtx;
  std::map<std::string, PendingPut> pendingPuts;
  
  // In the case of a disk server, checks the free/used space in the mountpoints
  void checkDiskSpaces();

  /// Send a notification to the head node about the completion of this task
  virtual void onTaskCompleted(dmlite::dmTask &task);

  /// Send a notification to the head node about a task that is running
  virtual void onTaskRunning(dmlite::dmTask &task);

  // helper functions for checksums
  int calculateChecksum(DomeReq &req, std::string lfn, dmlite::Replica replica, std::string checksumtype, bool updateLfnChecksum, bool forcerecalc);
  void sendChecksumStatus(const PendingChecksum &pending, const dmlite::dmTask &task, bool completed);

  // Helper for file pulls
  void touch_pull_queue(DomeReq &req, const std::string &lfn, const std::string &server, const std::string &fs,
                        const std::string &rfn, int64_t spacetomake);
  int enqfilepull(DomeReq &req, std::string lfn);
  void sendFilepullStatus(const PendingPull &pending, const dmlite::dmTask &task, bool completed);


  
};




#define SSTR(message) static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str()



#endif
