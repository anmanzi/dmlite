/*
 * Copyright 2016 CERN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/// @file   DomeTalker.cpp
/// @brief  Utility class, used to send requests to Dome
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
/// @date   Mar 2016

#include "DomeTalker.h"
#include "utils/DomeUtils.h"
#include <boost/property_tree/json_parser.hpp>
#include "status.h"

#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

#include <time.h>

#include "HorribleMutex.h"

using namespace dmlite;


// This has to be in sync with XrdHttpReq.hh
const char *DomeTalker::reqTypes[12] = {
  "none", "malformed",
  "GET", "HEAD", "PUT", "OPTIONS", "PATCH", "DELETE", "PROPFIND", "MKCOL", "MOVE", "POST"
};

DomeTalker::DomeTalker(DavixCtxPool &pool, const DomeCredentials &creds, std::string uri,
                       std::string verb, std::string cmd)
    : pool_(pool), creds_(creds), uri_(DomeUtils::trim_trailing_slashes(uri)), verb_(verb), cmd_(cmd),
    grabber_(pool_), ds_(grabber_) {

  err_ = NULL;
  parsedJson_ = false;
  target_ = uri_ + "/command/" + cmd_;
}


DomeTalker::DomeTalker(DavixCtxPool &pool, std::string uri,
                       std::string verb, std::string cmd)
: pool_(pool), creds_(0), uri_(DomeUtils::trim_trailing_slashes(uri)), verb_(verb), cmd_(cmd),
grabber_(pool_), ds_(grabber_) {
  
  err_ = NULL;
  parsedJson_ = false;
  target_ = uri_ + "/command/" + cmd_;
}


void DomeTalker::setcommand( const DomeCredentials &creds, const char *verb, const char *cmd) {
  creds_ = creds;
  verb_ = verb;
  cmd_ = cmd;
  err_ = NULL;
  parsedJson_ = false;
  target_ = uri_ + "/command/" + cmd_;
}

DomeTalker::~DomeTalker() {
  Davix::DavixError::clearError(&err_);
}

bool DomeTalker::execute() {
  return this->execute("");
}

bool DomeTalker::execute(const std::ostringstream &ss) {
  return this->execute(ss.str());
}

bool DomeTalker::execute(const std::string &str) {
  
  
  Davix::DavixError::clearError(&err_);

  // Fill the info that XrdHttp on the server side
  // will use to allow the connection from this client
  Davix::Uri target(target_);
  size_t hashl = CFG->GetString("glb.restclient.xrdhttpkey", (char *)"").length();
  
  if (hashl >= 32) {
    char tk[1024];
    char tmbuf[128];
    time_t tim = time(0);
    struct tm tms;
    localtime_r(&tim, &tms);
    strftime(tmbuf, sizeof (tmbuf), "%s", &tms);
    

    calcXrdHttpHashes(
      tk,
      target.getPath().c_str(),
      getXrdHttpReqIndex(verb_.c_str()),
      "headnode",
      "no_vorg",
      "no_addr",
      CFG->GetString("glb.restclient.present-as", (char *)"--").c_str(),
      time(0),
      CFG->GetString("glb.restclient.xrdhttpkey", (char *)"--").c_str());
    
    target.addQueryParam("xrdhttpname", "headnode");
    target.addQueryParam("xrdhttpvorg", "no_vorg");
    target.addQueryParam("xrdhttphost", "no_addr");
    target.addQueryParam("xrdhttpdn", CFG->GetString("glb.restclient.present-as", (char *)"--"));
    target.addQueryParam("xrdhttptime", tmbuf);
    target.addQueryParam("xrdhttptk", tk);
  
  }
  else if (hashl > 0) {
    Err("dometalker", " Invalid xrdhttpkey (must be longer or equal than 32 characters)");
    return false;
  }
  
  
  
  Davix::HttpRequest req(*ds_->ctx, target, &err_);
  if(err_) return false;

  req.setRequestMethod(verb_);

  if(!creds_.clientName.empty()) {
    req.addHeaderField("remoteclientdn", creds_.clientName);
  }

  if(!creds_.remoteAddress.empty()) {
    req.addHeaderField("remoteclienthost", creds_.remoteAddress);
  }

  if(!creds_.groups.empty()) {
    req.addHeaderField("remoteclientgroups", DomeUtils::join(",", creds_.groups));
  }

  if(!creds_.oidc_audience.empty()) {
    req.addHeaderField("remoteclientoidcaudience", creds_.oidc_audience);
  }
  
  if(!creds_.oidc_issuer.empty()) {
    req.addHeaderField("remoteclientoidcissuer", creds_.oidc_issuer);
  }
  
  if(!creds_.oidc_scope.empty()) {
    req.addHeaderField("remoteclientoidcscope", creds_.oidc_scope);
  }
  
  req.setParameters(*ds_->parms);
  
  std::string bdy = DomeUtils::unescape_forward_slashes(str);
  req.setRequestBody(bdy);

  Log(Logger::Lvl2, Logger::unregistered, "dometalker", " Sending dome RPC as '" << creds_.clientName <<
    "' to " << target_ << ": " << bdy);
  int rc = req.executeRequest(&err_);
  response_ = std::string(req.getAnswerContentVec().begin(), req.getAnswerContentVec().end());
  status_ = req.getRequestCode();
  Log(Logger::Lvl2, Logger::unregistered, "dometalker", " RPC response - status: " << status_ << ", contents: " << response_);

  if(rc || err_) return false;
  return true;
}

const std::string& DomeTalker::response() {
  return response_;
}

const boost::property_tree::ptree& DomeTalker::jresp() {
  if(parsedJson_) return json_;

  std::istringstream iss(&response_[0]);
  //boost::lock_guard<boost::mutex> l(horribleboostmtx);
  boost::property_tree::read_json(iss, json_);
  parsedJson_ = true;
  return json_;
}

bool DomeTalker::execute(const boost::property_tree::ptree &params) {
  std::ostringstream ss;
  boost::property_tree::write_json(ss, params);
  return this->execute(ss.str());
}

bool DomeTalker::execute(const std::string &key, const std::string &value) {
  boost::property_tree::ptree params;
  params.put(key, value);
  return this->execute(params);
}

bool DomeTalker::execute(const std::string &key1, const std::string &value1,
                         const std::string &key2, const std::string &value2) {
  boost::property_tree::ptree params;
  params.put(key1, value1);
  params.put(key2, value2);
  return this->execute(params);
}

bool DomeTalker::execute(const std::string &key1, const std::string &value1,
                         const std::string &key2, const std::string &value2,
                         const std::string &key3, const std::string &value3) {
  boost::property_tree::ptree params;
  params.put(key1, value1);
  params.put(key2, value2);
  params.put(key3, value3);
  return this->execute(params);
                         }

std::string DomeTalker::err() {
  if(err_) {
    std::ostringstream os;
    os << "Error when issuing request to '" << target_ << "'. Status " << status_ << ". ";
    os << "DavixError: '" << err_->getErrMsg() << "'. ";

    if(response_.size() != 0) {
      os << "Response (" << response_.size() << " bytes): '" << response_ << "'.";
    }
    else {
      os << "No response to show.";
    }

    return os.str();
  }
  return "";
}

int DomeTalker::status() {
  return status_;
}

struct StatusCodePair {
  int status;
  int code;
};

static struct StatusCodePair pairs[] = {
  { DOME_HTTP_OK,                   DMLITE_SUCCESS  },
  { DOME_HTTP_BAD_REQUEST,          EINVAL          },
  { DOME_HTTP_NOT_FOUND,            ENOENT          },
  { DOME_HTTP_CONFLICT,             EEXIST          },
  { DOME_HTTP_INSUFFICIENT_STORAGE, ENOSPC          },
  { DOME_HTTP_DENIED,               EACCES          }
};

int dmlite::http_status(const dmlite::DmException &e) {
  for(size_t i = 0; i < sizeof(pairs) / sizeof(pairs[0]); i++) {
    if(pairs[i].code == DMLITE_ERRNO(e.code())) {
      return pairs[i].status;
    }
  }
  return DOME_HTTP_INTERNAL_SERVER_ERROR;
}

int dmlite::http_status(const dmlite::DmStatus &e) {
  for(size_t i = 0; i < sizeof(pairs) / sizeof(pairs[0]); i++) {
    if(pairs[i].code == DMLITE_ERRNO(e.code())) {
      return pairs[i].status;
    }
  }
  return DOME_HTTP_INTERNAL_SERVER_ERROR;
}
int DomeTalker::dmlite_code() {
  for(size_t i = 0; i < sizeof(pairs); i++) {
    if(pairs[i].status == status_) {
      return pairs[i].code;
    }
  }
  return EINVAL;
}





// Encode an array of bytes to base64

void Tobase64(const unsigned char *input, int length, char *out) {
  BIO *bmem, *b64;
  BUF_MEM *bptr;
  
  if (!out) return;
  
  out[0] = '\0';
  
  b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  bmem = BIO_new(BIO_s_mem());
  BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  
  if (BIO_flush(b64) <= 0) {
    BIO_free_all(b64);
    return;
  }
  
  BIO_get_mem_ptr(b64, &bptr);
  
  
  memcpy(out, bptr->data, bptr->length);
  out[bptr->length] = '\0';
  
  BIO_free_all(b64);
  
  return;
}




#if OPENSSL_VERSION_NUMBER < 0x10100000L
static HMAC_CTX* HMAC_CTX_new() {
  HMAC_CTX *ctx = (HMAC_CTX *)OPENSSL_malloc(sizeof(HMAC_CTX));
  if (ctx) HMAC_CTX_init(ctx);
  return ctx;
}

static void HMAC_CTX_free(HMAC_CTX *ctx) {
  if (ctx) {
    HMAC_CTX_cleanup(ctx);
    OPENSSL_free(ctx);
  }
}
#endif

void DomeTalker::calcXrdHttpHashes(
  char *hash,
  const char *fn,
  int16_t request,
  const char *sslclientshortname,
  const char *sslclientvorg,
  const char *sslclienthost,
  const char *sslclientdn,
  time_t tim,
  const char *key) {
  
  HMAC_CTX *ctx;
  unsigned int len;
  unsigned char mdbuf[EVP_MAX_MD_SIZE];
  char buf[64];
  struct tm tms;
  
  
  if (!hash) {
    return;
  }
  
  if (!key) {
    return;
  }
  
  hash[0] = '\0';
  
  if (!fn) {
    return;
  }
  
  ctx = HMAC_CTX_new();
  
  if (!ctx) {
    return;
  }
  
  
  
  HMAC_Init_ex(ctx, (const void *) key, strlen(key), EVP_sha256(), 0);
  
  
  if (fn)
    HMAC_Update(ctx, (const unsigned char *) fn,
                strlen(fn) + 1);
    
  HMAC_Update(ctx, (const unsigned char *) &request,
                sizeof (request));
    
  if (sslclientshortname)
      HMAC_Update(ctx, (const unsigned char *) sslclientshortname,
                  strlen(sslclientshortname) + 1);
      
  if (sslclientvorg)
      HMAC_Update(ctx, (const unsigned char *) sslclientvorg,
                  strlen(sslclientvorg) + 1);
        
  if (sslclienthost)
      HMAC_Update(ctx, (const unsigned char *) sslclienthost,
                  strlen(sslclienthost) + 1);
          
  if (sslclientdn)
      HMAC_Update(ctx, (const unsigned char *) sslclientdn,
                  strlen(sslclientdn) + 1);
            
  localtime_r(&tim, &tms);
  strftime(buf, sizeof (buf), "%s", &tms);
  HMAC_Update(ctx, (const unsigned char *) buf,
                strlen(buf) + 1);
        
  HMAC_Final(ctx, mdbuf, &len);
        
  Tobase64(mdbuf, len / 2, hash);
        
  HMAC_CTX_free(ctx);

    
}
  
  int DomeTalker::compareXrdHttpHashes(
    const char *h1,
    const char *h2) {
    
    if (h1 == h2) return 0;
    
    if (!h1 || !h2)
      return 1;
    
    return strcmp(h1, h2);
    
    }
    
