
/*
 * Copyright 2015 CERN
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */



/** @file   dmTaskExec.cpp
 * @brief  A class that spawns commands that perform actions
 * @author Fabrizio Furano
 * @author Andrea Manzi
 * @date   Dec 2015
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "utils/logger.h"
#include "utils/Config.hh"
#include "TaskExec.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <queue>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/poll.h>
#include <fcntl.h>

using namespace boost;
using namespace std;



#define dmTaskLog(inst, lvl, where, what)                      \
{                                                              \
  if (Logger::get()->getLevel() >= lvl) {                      \
    std::ostringstream outs;                                   \
    outs << where << " " << __func__ << " : " << what;         \
    inst->onLoggingRequest((Logger::Level)lvl, outs.str());    \
  }                                                            \
}                                                              \
\

#define dmTaskErr(inst, where, what)                           \
{                                                              \
  std::ostringstream outs;                                     \
  outs << where << " " << __func__ << " : " << what;           \
  inst->onErrLoggingRequest(outs.str());                       \
}                                                              \
\



namespace dmlite {
  
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  
  dmTask::dmTask(dmTaskExec *wheretolog): finished(false), loggerinst(wheretolog) {
    starttime = time(0);
    endtime = 0;
    pid = -1;
    
    // Initialize placeholders for individual parms for execv
    for (int i =0; i < 64; i++)
      parms[i] = NULL;
  }
  
  dmTask::~dmTask() {
    //boost::unique_lock <boost::mutex> lck (*this);
    for (int i =0; i < 64; i++)
      // Free the execv parms, if we need...
      if (parms[i]) free((void *)parms[i]);
      else break;
  }
  
  
  void dmTask::splitCmd()
  {
    const char *tok;
    char *saveptr, *str = (char *)cmd.c_str();
    int i = 0;
    
    while ( (tok = strtok_r(str, " ", &saveptr)) ) {
      parms[i++] = strdup(tok);
      str = 0;
    }
    
    return;
    
  }
  
  
  int dmTask::waitFinished(int sectmout) {
    const char *fname = "dmTaskExec::waitFinished";
    
    dmTaskLog(loggerinst, Logger::Lvl4, fname, "Checking task termination. Key: " << key << " cmd: " << cmd << " finished: " << finished);
    
    system_time const timelimit = get_system_time() + posix_time::seconds(sectmout);
    dmTaskLog(loggerinst, Logger::Lvl4, fname, "Time: " << get_system_time());
    dmTaskLog(loggerinst, Logger::Lvl4, fname, "TimeLimit: " << timelimit);
    
    { 
      boost::unique_lock <boost::mutex>  lck (*this);
      while(!finished && get_system_time() < timelimit) {
        dmTaskLog(loggerinst, Logger::Lvl4, fname, "Task not finished at time " << get_system_time() << ": '" << cmd << "'");
        
        boost::system_time const tm = boost::get_system_time() + boost::posix_time::seconds(1);
        condvar.timed_wait(lck, tm);
      }
    }
    // We are here either if timeout or something happened
    
    if (finished) {
      dmTaskLog(loggerinst, Logger::Lvl3, fname, "Finished task. Key: " << key << " res: " << resultcode << " cmd: " << cmd);
      return 0;
    }
    
    dmTaskLog(loggerinst, Logger::Lvl3, fname, "Still running task. Key: " << key << " cmd: " << cmd);
    
    return 1;
    
  }
  
  
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  dmTaskExec::dmTaskExec() {
    taskcnt = 1;
  }
  
  dmTaskExec::~dmTaskExec() {}
  
  /// Taken from here and slightly modified.
  /// https://sites.google.com/site/williamedwardscoder/popen3
  int dmTaskExec::popen3(int fd[3], pid_t *pid,  const  char ** argv) {
    int i, e;
    int p[3][2];
    //
    // set all the FDs to invalid
    for(i=0; i<3; i++)
      p[i][0] = p[i][1] = -1;
    
    // create the pipes
    for(int i=0; i<3; i++) 
      if(pipe(p[i]))
        goto error;
      
    // and fork
    *pid = fork();
    
    if(-1 == *pid)
      goto error;
    
    // in the parent?
    if(*pid) {
      // parent
      fd[STDIN_FILENO] = p[STDIN_FILENO][1];
      close(p[STDIN_FILENO][0]);
      
      fd[STDOUT_FILENO] = p[STDOUT_FILENO][0];
      close(p[STDOUT_FILENO][1]);
      
      fd[STDERR_FILENO] = p[STDERR_FILENO][0];
      close(p[STDERR_FILENO][1]);
      
      // success
      return 0;
      
    } else {
      // child
      while ( (dup2(p[STDIN_FILENO][0],STDIN_FILENO) == -1) && (errno == EINTR) ) {};
      close(p[STDIN_FILENO][1]);
      
      while ( (dup2(p[STDOUT_FILENO][1],STDOUT_FILENO) == -1) && (errno == EINTR) ) {};
      close(p[STDOUT_FILENO][0]);
      
      while ( (dup2(p[STDERR_FILENO][1],STDERR_FILENO) == -1) && (errno == EINTR) ) {};
      close(p[STDERR_FILENO][0]);
      
      int execret = execv(argv[0],(char **)argv);
      char errbuf[128];
      dpm_strerror_r(errno, errbuf, sizeof(errbuf));
      // if we are here, then we failed to launch our program
      dmTaskErr(this, "popen3", "Cannot launch cmd: " << argv[0] << " res: " << execret << " errno: " << errno << " err: '" << errbuf << "'");
      fprintf(stderr," \"%s\"\n",argv[0]);
      _exit(EXIT_FAILURE);
    }
    
    
    error:
    
    // preserve original error
    e = errno;
    for(i=0; i<3; i++) {
      close(p[i][0]);
      close(p[i][1]);
    }
    errno = e;
    return -1;
  }
  
  
  void dmTaskExec::run(int key) {
    
    dmTaskLog(this, Logger::Lvl3, "taskrun", "Starting task " << key << " on instance " << instance);

    dmTask *dt = NULL;
    {
      scoped_lock lck (*this);

      map <int, dmTask *>::iterator i = tasks.find(key);
      if ( i == tasks.end() ) {
        dmTaskErr(this, "taskrun", "Task " << key << " not found on instance " << instance);
        return;
      }

      dt = i->second;
    }

    dmTaskLog(this, Logger::Lvl3, "taskrun", "Found task " << key << " on instance " << instance << ", starting command: '" << dt->cmd << "'") ;
    //start time
    {
      boost::unique_lock <boost::mutex>  l(*dt);
      time(&dt->starttime);
    }
    
    dt->resultcode = popen3((int *)dt->fd, &dt->pid, dt->parms);
    
    /// It is then possible for the parent process to read the output of the child process from file descriptor
    char buffer[256];
    struct pollfd fds[2];
    int ret;
    int timeout = 1000;
    siginfo_t info;

    // watch stdout and stderr from child process
    fds[0].fd = dt->fd[STDOUT_FILENO];
    fds[0].events = POLLIN;
    fds[1].fd = dt->fd[STDERR_FILENO];
    fds[1].events = POLLIN;

    while (1) {

      ret = poll(fds, 2, timeout);

      if (ret == -1) { // poll error
        dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " poll failed with errno " << ret);
        
        if ( (errno == EAGAIN) || (errno == EINTR) ) continue;
        
        break;

      } else if (!ret) { // poll timeout
        //memset(&info, 0, sizeof(siginfo_t));
        info.si_pid = 0;
        if (waitid(P_PID, dt->pid, &info, WEXITED | WNOHANG | WNOWAIT) == -1) {
          dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " poll timeout, unable to get child status (errno=" << errno << ")");
          break;
        }

        if (info.si_pid == dt->pid) {
          if (timeout == 0) {
            dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " poll timeout, child exited, no more data to read");
            break;
          }
          dmTaskLog(this, Logger::Lvl4, "taskrun", "Read task " << key << " poll timeout, child exited, check for latest child stdout data");
          timeout = 0;
        }
        continue;

      }

      // process stdout
      if (fds[0].revents & (POLLERR | POLLNVAL)) {
        dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " stdout in invalid state " << fds[0].revents);
        break;
      }

      // read child process stdout
      if (fds[0].revents & POLLIN) {
        ssize_t count = read(fds[0].fd, buffer, sizeof(buffer));
        dmTaskLog(this, Logger::Lvl4, "taskrun", "Read task " << key << " stdout count " << count);

        if (count == -1) {
          if (errno == EINTR) continue;
          dmTaskErr(this, "taskrun", "Read task " << key << " cant get output, errno " << errno);
          break;
        }

        if (count == 0) {
          dmTaskLog(this, Logger::Lvl4, "taskrun", "End Stdout") ;
          break;
        }
        else {
          boost::unique_lock <boost::mutex>   l(*dt);
          dt->stdout.append(buffer, count);

          if (Logger::get()->getLevel() >= Logger::Lvl4) {
            size_t pos = 0, pp = 0;
            int i = 0;
            // Break the large string into individual lines for logging
            while (1) {
              pp = dt->stdout.find_first_of('\n', pos);

              if (pp == string::npos) {
                dmTaskLog(this, Logger::Lvl4, "taskrun",
                          "Pid " << dt->pid << " stdout(" << i << "): '" <<
                          dt->stdout.substr(pos, pp) << "'" );
                break;
              }
              else {
                dmTaskLog(this, Logger::Lvl4, "taskrun",
                          "Pid " << dt->pid << " stdout(" << i << "): '" <<
                          dt->stdout.substr(pos, (pp-pos)) << "'" );
              }

              pos = pp+1;
              i++;
            }
          }
        }
      }

      // process stderr
      if (fds[1].revents & (POLLERR | POLLNVAL)) {
        dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " stderr in invalid state " << fds[1].revents);

      } else if (fds[1].revents & POLLIN) {
        ssize_t count = read(fds[1].fd, buffer, sizeof(buffer));
        if (count > 0) {
          dmTaskLog(this, Logger::Lvl4, "taskrun", "Read task " << key << " stderr: " << buffer);
        } else {
          dmTaskLog(this, Logger::Lvl4, "taskrun", "Read task " << key << " stderr failed: count = " << count << ", errno = " << errno);
        }
      }

      if (fds[0].revents & POLLHUP) {
        dmTaskLog(this, Logger::Lvl3, "taskrun", "Read task " << key << " finished");
        break;
      }
    }
    
    dmTaskLog(this, Logger::Lvl4, "taskrun", "Closing fds. key: " << key);
    for(int i=0; i<3; i++) 
      close(dt->fd[i]);
    
    dmTaskLog(this, Logger::Lvl4, "taskrun", "Finalizing key: " << key) ;
    
    
    int rescode;
    waitpid(dt->pid, &rescode, 0);
    
    {
      boost::unique_lock <boost::mutex> l(*dt);
      dt->finished = true;
      
      if WIFSIGNALED(rescode) {
        dmTaskErr(this, "taskrun", "Task killed by signal " << WTERMSIG(rescode) << " command: '" << dt->cmd << "'" );
        dt->resultcode = -1;
      }
      else
        dt->resultcode = WEXITSTATUS(rescode);
      
      //endtime
      time(&dt->endtime);
    }
    dt->notifyAll();
    
    dmTaskLog(this, Logger::Lvl4, "taskrun", "Dispatching onTaskCompleted key: " << key << " command: '" << dt->cmd << "'" );
    onTaskCompleted(*dt);
    
    
    
    dmTaskLog(this, Logger::Lvl4, "taskrun", "Finished task " << key << " on instance " << instance);
    
    
    
  }
  
  
  int dmTaskExec::submitCmd(std::string cmd) {
    int taskkey;
    dmTask * task = new dmTask(this);
    task->cmd= cmd;
    task->splitCmd();
    {  
      scoped_lock lck (*this);
      
      task->key = ++taskcnt; 
      tasks.insert( std::pair<int,dmTask*>(task->key,task));
      taskkey = task->key;
    }
    
    return taskkey;
  }
  
  int dmTaskExec::submitCmd(std::vector<std::string> &args) {
    dmTask * task = NULL;
    int taskkey;
    std::ostringstream oss;
    
    if (!args.empty())
    {
      task =  new dmTask(this);
      // Convert all but the last element to avoid a trailing ","
      std::copy(args.begin(), args.end()-1,
                std::ostream_iterator<string>(oss, " "));
      oss << args.back();
      task->cmd= oss.str();
    } else return -1;
    
    assignCmd(task,args);
    {
      scoped_lock lck (*this);
      
      task->key = ++taskcnt;
      tasks.insert( std::pair<int,dmTask*>(task->key,task) );
      taskkey = task->key;
    }
    
    return taskkey;
    
    
    
  }
  
  void dmTaskExec::goCmd(int id) {
    try {
      boost::thread workerThread( &dmTaskExec::run, this, id);

      // Explicit detach to work around different boost implementations of the boost::thread destructor
      workerThread.detach();
    }
    catch ( ... ) {
      dmTaskErr(this, "goCmd", "Exception when starting thread for task " << id << " on instance '" << instance << "'");
    }
    
  }
  
  
  
  /// Split che command string into the single parms
  void dmTaskExec::assignCmd(dmTask *task, std::vector<std::string> &args) {
    int i = 0;
    for(std::vector<string>::iterator it = args.begin() ; it != args.end(); ++it) {
      task->parms[i++] = strdup((*it).c_str());
    }
  }
  
  int dmTaskExec::waitResult(int taskID, int tmout) {
    
    dmTask *dt = NULL;
    
    {
      scoped_lock lck (*this);
      
      map <int, dmTask *>::iterator i = tasks.find(taskID);
      if ( i != tasks.end() ) {
        dmTaskLog(this, Logger::Lvl4, "waitResult", "Found task " << taskID);
        dt = i->second;
      }
    }
    
    if (dt) {
      dt->waitFinished(tmout);
      if (dt->finished) return 0;
      dmTaskLog(this, Logger::Lvl4, "waitResult", "Task with ID " << taskID << " has not finished in " << tmout << " seconds.");
      return 1;
    }
    else {
      dmTaskLog(this, Logger::Lvl4, "waitResult", "Task with ID " << taskID << " not found");
      return 1;
    }
    
  }
  
  int dmTaskExec::killTask(int taskID){
    
    dmTask *dt = NULL;
    {
      scoped_lock lck (*this);
      
      map <int, dmTask *>::iterator i = tasks.find(taskID);
      
      if ( i != tasks.end() ) {
        dmTaskLog(this, Logger::Lvl4, "killTask", "Found task " << taskID);
        dt = i->second;
      }
    }
    
    if (dt) {
      killTask(dt);
      return 0;
    }
    else {
      dmTaskLog(this, Logger::Lvl4, "waitTask", "Task with ID " << taskID << " not found");
      return 1;
    }
    
    return 0;
  }
  
  
  dmTask* dmTaskExec::getTask(int taskID) {
    
    scoped_lock lck (*this);
    
    map <int, dmTask *>::iterator i = tasks.find(taskID);
    if ( i != tasks.end() ) 
      return i->second;
    else 
      return NULL;
    
  }
  
  int dmTaskExec::getTaskStdout(int taskID, std::string &stdout) {
    
    scoped_lock lck (*this);
    
    map <int, dmTask *>::iterator i = tasks.find(taskID);
    if ( i != tasks.end() ) {
      boost::unique_lock <boost::mutex>  lck2 (*i->second);
      stdout = i->second->stdout;
      return 0;
    }
    else 
      return -1;
    
  }
  
  
  int dmTaskExec::killTask(dmTask *task){
    
    boost::unique_lock< boost::mutex > l(*task);
    
    
    if (task->finished){
      dmTaskLog(this, Logger::Lvl4, "killTask", "Task " << task->key << " already finished");
      return 0;
    } else if ( task->pid == -1) {
      dmTaskLog(this, Logger::Lvl4, "killTask", "Task " << task->key << " not yet started");
      return 0;
    } else if ( task->pid == 0) {
      dmTaskLog(this, Logger::Lvl4, "killTask", "Task " << task->key << " already killed");
      return 0;
    } else {
      kill(task->pid, SIGKILL);
      task->pid = 0;
      //close fd related to the task
      for(int k=0; k<3; k++) 
        close(task->fd[k]);
      dmTaskLog(this, Logger::Lvl4, "killedTask", "Task " << task->key);
      return 0;
    }
    
  }
  
  int dmTaskExec::getTaskCounters(int &tot, int &running) {
    scoped_lock lck (*this);
    
    tot = tasks.size();
    running = 0;
    for( map<int, dmTask *>::iterator i = tasks.begin(); i != tasks.end(); ++i ) {
      if (!i->second->finished)
        running++;
    }
    
    return 0;
  };
  
  
  void dmTaskExec::tick() {
    std::deque<dmTask *> notifq_running;
    std::deque<dmTask *> delq;
    std::deque<dmTask *> killq;
    
    int maxruntime = CFG->GetLong("glb.task.maxrunningtime", 3600);
    int purgetime = CFG->GetLong("glb.task.purgetime", 3600);
    
    dmTaskLog(this, Logger::Lvl4, "tick", "tick start");
    
    {
      scoped_lock lck (*this);

        for( map<int, dmTask *>::iterator i = tasks.begin(); i != tasks.end();  ) {
          dmTaskLog(this, Logger::Lvl4, "tick", "Found task " << i->first << " with command " << i->second->cmd);
          dmTaskLog(this, Logger::Lvl4, "tick", "The status of the task is " << i->second->finished);
          dmTaskLog(this, Logger::Lvl4, "tick", "StartTime " << i->second->starttime << " EndTime " << i->second->endtime);
          dmTaskLog(this, Logger::Lvl4, "tick", "Pid " << i->second->pid << " resultcode " << i->second->resultcode);
          time_t timenow;
          time(&timenow);
          
          // Treat the task object, in locked state, and accumulate events to be sent later
          {
            boost::unique_lock< boost::mutex > lck (*(i->second));
            
            if (!i->second->finished) {

              if (i->second->starttime< (timenow - maxruntime)) {
                dmTaskErr(this, "tick", "Task with id  " << i->first << " exceed maxrunnngtime " << (timenow - maxruntime));
                killq.push_back(i->second);
              } else {
                notifq_running.push_back( i->second );
              }

              ++i;

            } else { // finished
            
              //check if purgetime has exceeded and clean
              if (i->second->endtime < (timenow - purgetime)) {
                int taskkey = i->first;
                dmTaskLog(this, Logger::Lvl4, "tick", "Task with id  " << taskkey << " to purge");
              
                //delete the task
                delq.push_back(i->second);
                dmTaskLog(this, Logger::Lvl4, "tick", "Task with id  " << taskkey << " to be deleted");
                //remove from map
                tasks.erase(i++);
              
                dmTaskLog(this, Logger::Lvl3, "tick", "Task with id  " << taskkey << " purged");
              } else {

                ++i;

              }

            }

          }
          
        }

    } // lock
    
    // Now send the accumulated notifications. We send them here in order not to keep the whole
    // object locked. Of course we are assuming that even in case of network hiccups
    // this phase ends well quicker than the purgetime
    for( std::deque<dmTask *>::iterator q = notifq_running.begin(); q != notifq_running.end(); ++q ) {
      boost::unique_lock< boost::mutex > lck (**q);
      onTaskRunning(**q);
    }
    
    int killed = killq.size();
    int deleted = delq.size();
    {
      scoped_lock lck (*this);
      for( std::deque<dmTask *>::iterator q = killq.begin(); q != killq.end(); ++q ) {
        dmTaskLog(this, Logger::Lvl4, "tick", "Killing task with id  " << (*q)->key);
        killTask(*q);
      }
      for( std::deque<dmTask *>::iterator q = delq.begin(); q != delq.end(); ++q ) {
        dmTaskLog(this, Logger::Lvl4, "tick", "Task with id  " << (*q)->key << " deleted");
        delete *q;
      }
    }
    
    dmTaskLog(this, Logger::Lvl4, "tick", "tick end (deleted " << deleted << ", killed " << killed << ")");
  }
  
  
  
  void dmTaskExec::onTaskRunning(dmTask &task) {
    dmTaskLog(this, Logger::Lvl3, "onTaskRunning", "task " << task.key << " with command " << task.cmd);    
  }
  void dmTaskExec::onTaskCompleted(dmTask &task) {
    dmTaskLog(this, Logger::Lvl3, "onTaskCompleted", "task " << task.key << " res: " << task.resultcode << " with command " << task.cmd);
  }
  
  
}
