#ifndef HorribleMutex_HH
#define HorribleMutex_HH

#include <boost/thread.hpp>

// This is a horrible workaround in order to have a global mutex that serializes
// certain questionable boost functions, e.g. read_json
extern boost::mutex horribleboostmtx;


#endif
