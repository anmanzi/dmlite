/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: Fabrizio Furano, David Smith <David.Smith@cern.ch>
*/

#ifndef __DPM_FINDER__
#define __DPM_FINDER__

#include <vector>
#include <memory>
#include <utility>

#include <XrdDPMCommon.hh>
#include <XrdCms/XrdCmsClient.hh>
#include <XrdOuc/XrdOucString.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdOuc/XrdOucErrInfo.hh>
#include <XrdAcc/XrdAccAuthorize.hh>
#include <XrdNet/XrdNetAddr.hh>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <dmlite/cpp/dmlite.h>

/**********************************/
/* classes internal to DPM Finder */
/**********************************/

class DpmFinderConfigOptions {
public:
   DpmFinderConfigOptions() :
      xrd_server_port(0), mkpath_bool(true), reqput_lifetime(0),
      reqput_ftype('P'), reqput_reqsize(0), reqget_lifetime(0),
      reqget_ftype('\0'), gracetime(0), authorize_bool(false),
      list_voms(0), no_hv1(0) {}

   unsigned short             xrd_server_port;
   bool                       mkpath_bool;
   std::vector<XrdNetAddr>    mmReqHosts;

   time_t                     reqput_lifetime;
   char                       reqput_ftype;
   XrdOucString               reqput_stoken;
   size_t                     reqput_reqsize;
   time_t                     reqget_lifetime;
   char                       reqget_ftype;
   XrdOucString               reqget_stoken;

   DpmCommonConfigOptions     CommonConfig;
   DpmRedirConfigOptions      RedirConfig;
   std::vector<unsigned char> key;
   int                        gracetime;

   XrdOucString               authlib;
   XrdOucString               authparm;
   bool                       authorize_bool;
   bool                       list_voms;
   bool                       no_hv1;
};

class DpmFileRequestOptions {
public:
   DpmFileRequestOptions(bool isPut, XrdOucEnv *env,
      const DpmFinderConfigOptions &defaults);

   bool         isPut;
   time_t       lifetime;
   char         ftype;
   XrdOucString stoken;
   XrdOucString utoken;
   size_t       reqsize;
   bool         isMkpath;
};

class DpmFileRequest {
public:
   DpmFileRequest(dmlite::StackInstance &si, const XrdOucString &path,
         int flags, DpmFileRequestOptions &ReqOpts);

   void                         DoQuery() ;
   void                         Abort();
   int                          BackoffWaitTime();
   const XrdOucString&          Host() const {return r_host;}
   const dmlite::Location&      Location() const {return r_Location;}
   const DpmFileRequestOptions& ROpts() const {return ReqOpts;}

private:
   DpmFileRequest(const DpmFileRequest&);
   DpmFileRequest& operator=(const DpmFileRequest&);
   void init();
   void dmput() ;
   void dmget() ;

   dmlite::StackInstance  &si;
   bool                    withOverwrite;
   XrdOucString            spath;
   int                     flags;
   DpmFileRequestOptions   ReqOpts;
   dmlite::Location        r_Location;
   XrdOucString            r_host;
   int                     MkpathState;
};


/******************************************************************************/
/*                             D P M   F i n d e r                            */
/******************************************************************************/

class XrdDPMFinder : public XrdCmsClient
{
public:
   int   Configure(const char *cfn, char *Args, XrdOucEnv *EnvInfo);

   int   Forward(XrdOucErrInfo &Resp, const char *cmd,
           const char *arg1=0,  const char *arg2=0,
           XrdOucEnv  *Env1=0,  XrdOucEnv  *Env2=0);

   int   Locate(XrdOucErrInfo &Resp, const char *path, int flags,
           XrdOucEnv *Info=0);

   int   Space(XrdOucErrInfo &Resp, const char *path, XrdOucEnv *Info=0);

   XrdDPMFinder(XrdCmsClient *cmsClient, XrdSysLogger *lp,
                int whoami=0, int Port=0);

   DpmRedirConfigOptions* getRedirConfigObj() { return &Opts.RedirConfig; }

private:
   int    setupAuth(XrdSysLogger *lp, const char *cfn);
   int    DoFileAccessRequest(XrdOucErrInfo &Resp, const char *xrd_fn,
                         int flags, XrdOucEnv *Info, dmlite::StackInstance &si,
                         const XrdOucString &FullPath, bool isPut,
                         const DpmIdentity &ident, XrdOucString &tried) ;
   bool   IsMetaManagerDiscover(XrdOucEnv *Info, int flags,
                         const char *path, const char *user);

   void   DpmAdded(const char *path, int Pend=0);

   void   DpmRemoved(const char *path);

   DpmFinderConfigOptions Opts;
   XrdAccAuthorize       *Authorization;
   bool                   AuthSecondary;
   XrdCmsClient          *defaultCmsClient;
};
#endif
