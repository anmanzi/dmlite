/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMDiskAcc.cc
 * Author: Fabrizio Furano, David Smith
 * 
 * An Oss plugin that interacts with a DPM
 *
 * Created on Jan 13, 2012, 10:25 AM
 */

#include <XrdDPMDiskAcc.hh>
#include <XrdDPMTrace.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdSec/XrdSecEntity.hh>
#include <XrdVersion.hh>

#include <vector>
#include <exception>

XrdVERSIONINFO(XrdAccAuthorizeObject,XrdDPMDiskAcc)

/******************************************************************************/
/*                  E r r o r   R o u t i n g   O b j e c t                   */
/******************************************************************************/

namespace DpmDiskAcc {
   XrdSysError Say(0, "dpmdiskacc_");
   XrdOucTrace Trace(&Say);
}

using namespace DpmDiskAcc;


XrdAccPrivs XrdDPMDiskAcc::Access(const XrdSecEntity    *Entity,
   const char            *path,
   const Access_Operation oper,
   XrdOucEnv             *env) {
   
   EPNAME("Access");
   
   XrdOucString hv1_dpmtk, hv1_pfn, hv1_sfn;
   XrdOucString dpmnonce;
   char *isput_s = 0;
   unsigned int flags = 0;
   XrdOucString dhost;
   const char *tim_s = 0;
   time_t tim = 0;
   int reqgracetime = 0;
   int effgracetime = 0;
   XrdOucString dn, vomsnfo;
   XrdOucString locstr;
   std::vector<XrdOucString> chunkstr;
   struct tm tms;

   if (!env) {
      Say.Emsg("Access","No environment parameters passed.");
      return XrdAccPriv_None;
   }

   if (oper != AOP_Create &&
       oper != AOP_Update &&
       oper != AOP_Read &&
       oper != AOP_Stat) {
      DEBUG("Operation " << oper << " not permitted.");
      return XrdAccPriv_None;
   }

   // Ugly fix. The xrootd TPC+checksum wants to contact directly
   // the disk server to stat the file without passing through the
   // headnode to get a token
   // Here we just check for this case, the user DN must be not empty
   if ((oper == AOP_Stat) &&
     Entity && Entity->name &&
     strlen(Entity->name) > 0)
     return XrdAccPriv_Lookup;
   
   // Then, get all the other parameters to verify

   try {
      hv1_pfn = DecodeString(env->Get("dpm.sfn"));
      hv1_sfn = DecodeString(env->Get("dpm.surl"));
   } catch (dmlite::DmException &e) {
      DEBUG("Error unencoding cgi argument: " << e.what());
      return XrdAccPriv_None;
   } catch (const std::exception &) {
      Say.Emsg("Access","Unexpected exception while unencoding cgi "
         "argument");
      return XrdAccPriv_None;
   }

   if ((isput_s = env->Get("dpm.put")) && atoi(isput_s))
      flags |= XRDDPM_IS_PUT;
   
   hv1_dpmtk = env->Get("dpm.tk");
   dpmnonce = env->Get("dpm.nonce");

   try {
      EnvToLocstr(env, locstr, chunkstr);
   } catch (dmlite::DmException &e) {
      DEBUG("Error unencoding location arguments: " << e.what());
      return XrdAccPriv_None;
   } catch (const std::exception &) {
      Say.Emsg("Access","Unexpected exception while unencoding "
         "location arguments");
      return XrdAccPriv_None;
   }

   dhost = env->Get("dpm.dhost");
   if (!dhost.length()) {   
      DEBUG("Token is missing target disk server host name.");
      return XrdAccPriv_None;
   }

   // Get the req time, do some checks
   tim_s = env->Get("dpm.time");
   if (!tim_s ||
       !(tim_s = strptime(tim_s, "%s", &tms)) || (*tim_s && *tim_s!=','))  {
      DEBUG("Invalid request time.");
      return XrdAccPriv_None;
   }
   tim = mktime(&tms);
   if (*tim_s)
      reqgracetime = atoi(++tim_s);
   if (reqgracetime<0) {
      DEBUG("Negative validity time in request.");
      return XrdAccPriv_None;
   }
   effgracetime = reqgracetime;
   if (maxgracetime) {
      if (!effgracetime || effgracetime > maxgracetime) {
         effgracetime = maxgracetime;
      }
   }
   if (!effgracetime) {
      XrdOucString err = 
         "No maximum validity period configured or supplied in request.";
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }
   if (tim < time(0)-effgracetime) {
      Say.Emsg("Access", "Request time expired.");
      return XrdAccPriv_None;
   }
   if (tim > time(0)+effgracetime) {
      XrdOucString err = "Request time is in the future.";
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }

   try {
      DpmIdentity dpmid(env);
      dn = dpmid.Dn();
      vomsnfo = dpmid.Groups();
      if (!dpmid.IsSecEntID()) {
         flags |= XRDDPM_ID_URL;
      }
   } catch(dmlite::DmException &e) {
      DEBUG("Could not get identity: " << e.what());
      return XrdAccPriv_None;
   } catch(const std::exception &) {
      Say.Emsg("Access","Could not get identity: Unexpected "
         "exception");
      return XrdAccPriv_None;
   }

   bool hv1_compat = 0;
   if (!locstr.length()) hv1_compat = 1;

   const char *hash_opaque;
   if (hv1_compat) {
      hash_opaque = env->Get("dpm.hv1");
   } else {
      hash_opaque = env->Get("dpm.hv2");
   }

   if (!hash_opaque) {
      DEBUG("Hash not found in opaque data.");
      return XrdAccPriv_None;
   }
   
   // Calculate the expected hash
   char *hash;
   if (key.size()) {
      char *calchash[2];
      calc2Hashes(calchash, hv1_compat ? 1 : 2, path, SafeCStr(hv1_sfn),
         SafeCStr(dhost), SafeCStr(hv1_pfn), SafeCStr(hv1_dpmtk),
         flags, SafeCStr(dn), SafeCStr(vomsnfo), tim, reqgracetime, 
         SafeCStr(dpmnonce), locstr, chunkstr, &key[0],
         key.size());
      if (hv1_compat) {
         hash = calchash[0];
      } else {
         hash = calchash[1];
      }
   }
   else {
      XrdOucString err = "No shared secret --> no hash.";
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }

   if (!hash) {
      XrdOucString err = "Failed to calculate hash.";
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }
   
   if (compareHash(hash, hash_opaque)) {
      XrdOucString err = "Invalid request signature.";
      free(hash);
      if (Entity && Entity->host && *Entity->host) {
         err += " Client host " + XrdOucString(Entity->host);
      }
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }
   free(hash);

   std::vector<XrdOucString>::const_iterator itr;
   bool dfound=0;
   for(itr = LocalHostNames.begin();
         itr != LocalHostNames.end(); ++itr) {
      if (!strcasecmp(SafeCStr(*itr),SafeCStr(dhost))) {
         dfound=1;
         break;
      }
   }
   if (!dfound) {
      XrdOucString err = "Disk server hostname "
         + dhost
         + " not matched to this host.";
      Say.Emsg("Access", SafeCStr(err));
      return XrdAccPriv_None;
   }

   return (flags & XRDDPM_IS_PUT) ? XrdAccPriv_Create : XrdAccPriv_Read;
}

int XrdDPMDiskAcc::Test(const XrdAccPrivs priv,const Access_Operation oper)
{

// Warning! This table must be in 1-to-1 correspondence with Access_Operation
//
   static XrdAccPrivs need[] = {XrdAccPriv_None,                 // 0
                                XrdAccPriv_Chmod,                // 1
                                XrdAccPriv_Chown,                // 2
                                XrdAccPriv_Create,               // 3
                                XrdAccPriv_Delete,               // 4
                                XrdAccPriv_Insert,               // 5
                                XrdAccPriv_Lock,                 // 6
                                XrdAccPriv_Mkdir,                // 7
                                XrdAccPriv_Read,                 // 8
                                XrdAccPriv_Readdir,              // 9
                                XrdAccPriv_Rename,               // 10
                                XrdAccPriv_Lookup,               // 11
                                XrdAccPriv_Update                // 12
                               };
   if (oper < 0 || oper > AOP_LastOp) return 0;
   return (int)(need[oper] & priv) == need[oper];
}

XrdDPMDiskAcc::~XrdDPMDiskAcc() { }

XrdDPMDiskAcc::XrdDPMDiskAcc(const char *cfn, const char *parm) :
      maxgracetime(300)
{
   if (DpmCommonConfigProc(Say, cfn, CommonConfig)) {
      throw dmlite::DmException(DMLITE_CFGERR(EINVAL),
         "problem with (common) configuration");
   }
   Trace.What = CommonConfig.OssTraceLevel;

   InitLocalHostNameList(LocalHostNames);

   // Parse the parameters:
   // - grace period
   XrdOucString item,prms(parm);
   int tkcnt = 0;
   int from = 0;
   while ((from = prms.tokenize(item, from, ' ')) != STR_NPOS) {
      const char *buf = SafeCStr(item);
      switch(tkcnt) {
         case 0:
            Say.Say("NewObject", "setting maxgracetime:", buf);
            maxgracetime = atoi(buf);
            if (maxgracetime < 0) {
               throw dmlite::DmException( DMLITE_CFGERR(EINVAL),
                  "Negative maximum token lifetime");
            }
            break;
      }
      tkcnt++;
   }

   unsigned char *dat;
   const char *msg;
   size_t dlen;
   if ((msg = LoadKeyFromFile(&dat, &dlen))) {
      throw dmlite::DmException(DMLITE_CFGERR(EINVAL),
         "Error while reading key from file: %s", msg);
   } else {
      for(size_t idx=0;idx<dlen;++idx)
         key.push_back(dat[idx]);
      free(dat);
   }
}

extern "C" XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
      const char   *cfn, const char   *parm)
{
   EPNAME("XrdAccAuthorizeObject");
   DpmDiskAcc::Say.logger(lp);

   XrdSysError_Table *ETab = XrdDmliteError_Table();
   DpmDiskAcc::Say.addTable(ETab);

   XrdDmCommonInit(lp);

   try {
      return new XrdDPMDiskAcc(cfn,parm);
   } catch(dmlite::DmException &e) {
      DpmDiskAcc::Say.Emsg("NewObject","cannot start the access control layer", e.what());
   } catch(const std::exception &) {
      DpmDiskAcc::Say.Emsg("NewObject","unexpected exception");
   }
   return 0;
}
