/*                         
 * Copyright (C) 2012 by CERN/IT/GT/DMS. All rights reserved.
 *
 * Author: David Smith <David.Smith@cern.ch>
*/

#include <XrdDPMFinder.hh>
#include <XrdDPMTrace.hh>
#include <XrdOuc/XrdOucStream.hh>
#include <XrdOuc/XrdOuca2x.hh>
#include <XrdNet/XrdNetAddr.hh>
#include <XrdNet/XrdNetUtils.hh>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <exception>

#define MIN_KEYLEN 32

// globals
namespace DpmFinder {
   extern XrdSysError Say;
   extern XrdOucTrace Trace;
   extern XrdDmStackStore dpm_ss;
}

using namespace DpmFinder;


int XrdDPMFinder::Configure(const char *cfn, char *Args, XrdOucEnv *EnvInfo) {
   EPNAME("Configure");
   int cfgFD;
   int NoGo = 0;
   int retc,v;
   const char *var, *val;
   XrdOucEnv myEnv;
   XrdOucStream CFile(&Say, getenv("XRDINSTANCE"), &myEnv, "=====> ");
   bool enableCms = 0;
   std::vector<XrdOucString> reqhost;

   if (!cfn || !*cfn) {
      Say.Emsg("Configure","Configuration file not specified.");
      return 0;
   }

   if (DpmCommonConfigProc(Say, cfn, Opts.CommonConfig, &Opts.RedirConfig)) {
      Say.Emsg("Configure","cannot read (common) configuration");
      return 0;
   }
   Trace.What = Opts.CommonConfig.OfsTraceLevel;
   try {
      dpm_ss.SetDmConfFile(Opts.CommonConfig.DmliteConfig);
      dpm_ss.SetDmStackPoolSize(Opts.CommonConfig.DmliteStackPoolSize);
      // get a dmlite stack to trigger the loading of the dmlite
      // configuration. This may do things like setting variables
      // in the enviornment, which is not thread safe and might cause
      // problems later.
      DpmIdentity empty_ident;
      XrdDmStackWrap sw(dpm_ss, empty_ident);
   } catch(const std::exception &) {
      Say.Emsg("Configure","problem setting up the dmlite stack");
      return 0;
   }

   // Try to open the configuration file.
   //
   if ((cfgFD = open(cfn, O_RDONLY, 0))<0) {
      Say.Emsg("Config", errno, "open config file", cfn);
      return 0;
   }
   CFile.Attach(cfgFD);
   // Start reading records until eof.
   //
   while((var = CFile.GetMyFirstWord())) {
      if (!strncmp(var, "dpm.", 4)) {
         var += 4;

         if (!strcmp("putlifetime",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config",
               "'put' lifetime value not specified.");
               NoGo |= 1;
            } else {
               if(XrdOuca2x::a2tm(Say,"putlifetime value", val, &v))
                  NoGo |= 1;
               else
                  Opts.reqput_lifetime = v;
            }
         } else
         if (!strcmp("putfiletype",var)) {
            if (!(val = CFile.GetWord()) || strlen(val)!=1) {
               Say.Emsg("Config",
                        "'put' file type value not specified or invalid.");
               NoGo |= 1;
            } else {
               Opts.reqput_ftype = (*val == '-') ? '\0' : *val;
            }
         } else if (!strcmp("putstoken",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config",
                        "'put' space token not specified or invalid.");
               NoGo |= 1;
            } else { Opts.reqput_stoken = val; }
         } else if (!strcmp("fixedidrestrict",var)) {
            while((val = CFile.GetWord())) {
               Opts.RedirConfig.AuthLibRestrict.
                  push_back(CanonicalisePath(val,1));
            }
         } else if (!strcmp("enablecmsclient",var)) {
            enableCms = 1;
         } else if (!strcmp("allowvo",var)) {
            while((val = CFile.GetWord())) {
               Opts.RedirConfig.IdentConfig.
                  validvo.push_back(val);
            }
         } else if (!strcmp("putsize",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config","defult 'put' size not specified.");
               NoGo |= 1;
            } else {
               long long vSz;
               if (XrdOuca2x::a2sz(Say, "putsize value", val, &vSz, 0))
                  NoGo |= 1;
               else
                  Opts.reqput_reqsize = vSz;
            }
         } else if (!strcmp("getlifetime",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config", "'get' lifetime value not specified.");
               NoGo |= 1;
            } else {
               if (XrdOuca2x::a2tm(Say,"getlifetime value", val, &v))
                  NoGo |= 1;
               else
                  Opts.reqget_lifetime = v;
            }
         } else if (!strcmp("getfiletype",var)) {
            if (!(val = CFile.GetWord()) || strlen(val)!=1) {
               Say.Emsg("Config", 
                        "'get' file type value not specified or invalid.");
               NoGo |= 1;
            } else {
               Opts.reqget_ftype = (*val == '-') ? '\0' : *val;
            }
         } else if (!strcmp("getstoken",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config", 
                        "'get' space token not specified or invalid.");
               NoGo |= 1;
            } else { Opts.reqget_stoken = val; }
         } else if (!strcmp("xrdserverport",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config", 
                        "'xrdserverport' is not specified.");
               NoGo |= 1;
            } else {
               if (XrdOuca2x::a2i(Say,"xrdserverport value", val, &v))
                  NoGo |= 1;
               else
                  Opts.xrd_server_port = v;
            }
         } else if (!strcmp("mkpath",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config",
                        "'mkpath' is not specified.");
               NoGo |= 1;
            } else {
               if (XrdOuca2x::a2i(Say,"mkpath value", val, &v))
                  NoGo |= 1;
               else
                  Opts.mkpath_bool = v;
            }
         } else if (!strcmp("mmreqhost",var)) {
            while((val = CFile.GetWord())) {
               reqhost.push_back(val);
            }
         } else if (!strcmp("gracetime", var)) {
            if (!(val = CFile.GetWord())) {
               // ok leave the default
               //
            } else {
               if (XrdOuca2x::a2i(Say,"gracetime value", val, &v))
                  NoGo |= 1;
               else
                  Opts.gracetime = v;
            }
         } else if (!strcmp("principal",var)) {
            // handled below; need to preserve spaces
         } else if (!strcmp("fqan",var)) {
            while((val = CFile.GetWord())) {
               XrdOucString vat, vals(val);
               int from = 0;
               while ((from = vals.tokenize(vat, from, ',')) != STR_NPOS) {
                  if (!vat.length()) continue;
                  Opts.RedirConfig.IdentConfig.fqans.push_back(vat);
               }
            }
         } else if (!strcmp("listvoms",var)) {
            Opts.list_voms = 1;
         } else if (!strcmp("nohv1",var)) {
            Opts.no_hv1 = 1;
         } else if (!strcmp("dmconf",var)) {
            // this is processed by the common configuration
         } else if (!strcmp("dmstackpoolsize", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("defaultprefix", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("namecheck", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("localroot", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("namelib", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("replacementprefix", var)) {
            // this is processed by the common configuration
         } else if (!strcmp("dmio",var)) {
            // this is processed by the dpmoss configuration
         } else {
            Say.Say("Config warning: ignoring unknown "
                    "directive '",var,"'.");
            CFile.Echo();
         }

      } else if (!strncmp(var, "ofs.", 4)) {
         var += 4;
         if (!strcmp("authlib",var)) {
            if (!(val = CFile.GetWord())) {
               Say.Emsg("Config","authlib not specified.");
               NoGo |= 1;
            } else {
               Opts.authlib = val;
               char parms[1024];
               if (!CFile.GetRest(parms,sizeof(parms))) {
                  Say.Emsg("Config","authlib parameters too long");
                  NoGo |= 1;
               } else {Opts.authparm = parms;}
            }
         } else if (!strcmp("authorize",var)) {
            Opts.authorize_bool = 1;
         }
      }
   }
   // Check if any errors occured during file i/o
   //
   if ((retc = CFile.LastError()))
      NoGo = Say.Emsg("Config", retc, "read config file", cfn);
   CFile.Close();

   // Try to open the configuration file again for principal
   //
   if ( (cfgFD = open(cfn, O_RDONLY, 0)) < 0) {
      Say.Emsg("Config", errno, "open config file", cfn);
      return 0;
   }
   CFile.Attach(cfgFD);
   while((var = CFile.GetLine())) {
      while(*var==' ' || *var=='\t') var++;
      if (*var=='#') continue;
      if (!strncmp("dpm.principal",var,13)) {
         var += 13;
         if (!(*var==' ' || *var=='\t')) continue;
         while(*var==' ' || *var=='\t') var++;
         const char *tmp = var;
         while(*tmp) tmp++;
         while(tmp!=var && (*(tmp-1)==' '||*(tmp-1)=='\t')) tmp--;
         size_t s = tmp-var;
         if (s>0) {
            Opts.RedirConfig.IdentConfig.principal = XrdOucString(var, s);
         }
      }
   }
   // Now check if any errors occured during file i/o
   //
   if ((retc = CFile.LastError()))
      NoGo = Say.Emsg("Config", retc, "read config file", cfn);
   CFile.Close();

   Opts.RedirConfig.ss = &dpm_ss;

   // if setting up a cmsclient we expect that the cms will
   // send us stat requests, so prepopulate the allowed host
   // list with this host.
   if (enableCms) {
      std::vector<XrdOucString> lh;
      InitLocalHostNameList(lh);
      reqhost.insert(reqhost.end(),lh.begin(),lh.end());
      reqhost.push_back("localhost");
   }

   // host names to IP addresses
   std::vector<XrdOucString>::const_iterator itr;
   for(itr=reqhost.begin();itr!=reqhost.end();++itr) {
      const char *host = SafeCStr(*itr);
      XrdNetAddr *nap = 0;
      int num = 0;
      XrdNetUtils::GetAddrs(host, &nap, num, XrdNetUtils::allIPMap,
         XrdNetUtils::NoPortRaw);
      for(int i=0;i<num;++i) {
         Opts.mmReqHosts.push_back(nap[i]);
      }
      delete[] nap;
   }

   if (Opts.key.size() == 0) {
      unsigned char *dat;
      size_t dlen;
      const char *msg;
      if ((msg = LoadKeyFromFile(&dat, &dlen))) {
         Say.Emsg("Config", "Error while reading key from file:", msg);
      } else {
         for(size_t idx=0;idx<dlen;++idx)
            Opts.key.push_back(dat[idx]);
         free(dat);
      }
   }
   if (Opts.key.size() < MIN_KEYLEN) {
      char buf[32];
      snprintf(buf, sizeof(buf), "%d", MIN_KEYLEN);
      Say.Emsg("Config", "The shared key is undefined or too short, "
         "the minimum length required is", buf);
      NoGo |= 1;
   }
   XrdOucString err;
   if (!NoGo && Opts.RedirConfig.IdentConfig.principal.length() &&
       DpmIdentity::badPresetID(Opts.RedirConfig.IdentConfig, err)) {
      Say.Emsg("Config","The preset identity has a problem:", SafeCStr(err));
      NoGo |= 1;
   }
   if (!NoGo && Opts.RedirConfig.IdentConfig.principal == "root" &&
         Opts.RedirConfig.IdentConfig.fqans.size() != 0) {
      Say.Emsg("Config","dpm.fqan must not be set if dpm.principal "
         "is 'root'");
      NoGo |= 1;
   }
   if (!NoGo && !Opts.RedirConfig.IdentConfig.principal.length() &&
         Opts.RedirConfig.IdentConfig.fqans.size() != 0) {
      Say.Emsg("Config","Setting dpm.fqan without dpm.principal "
         "has no effect");
      NoGo |= 1;
   }
   // this will set AuthSecondary
   if (!NoGo && Opts.authorize_bool) {
      if (setupAuth(Say.logger(), cfn)) {
         Say.Emsg("Config", "Problem configuring authorization library");
         NoGo |= 1;
      }
   }
   if (!NoGo &&
      !(Opts.RedirConfig.IdentConfig.principal.length() == 0 &&
         !AuthSecondary &&
         Opts.RedirConfig.AuthLibRestrict.size() == 0) &&
      !(Opts.RedirConfig.IdentConfig.principal.length() != 0 &&
         AuthSecondary &&
         Opts.RedirConfig.AuthLibRestrict.size() != 0)) {
      Say.Emsg("Config", "dpm.principal, dpm.fixedidrestrict and a "
         "secondary authlib are either all required or should "
         "all be unset.");
      NoGo |= 1;
   }
   if (!NoGo && !Authorization) {
      NoGo |= 1;
      Say.Emsg("Config","DPMFinder requires authorization to be enabled");
   }
   if (enableCms) {
      // start standard cms client
      //
      if (!defaultCmsClient ||
          !defaultCmsClient->Configure(cfn, Args, EnvInfo)) {
         Say.Emsg("Configure","Could not configure cms client");
         NoGo |= 1;
      }
   } else {
      // not needed
      //
      delete defaultCmsClient;
      defaultCmsClient = 0;
   }

   if (NoGo) {
      Say.Say("Error while processing configuration for DPM Finder plugin");
   } else {
      for(std::vector<XrdOucString>::const_iterator itr=
            Opts.RedirConfig.IdentConfig.validvo.begin();
            itr!=Opts.RedirConfig.IdentConfig.validvo.end();
            ++itr) {
         TRACEX("Allowed vo       = " << *itr);
      }
      if (!Opts.RedirConfig.IdentConfig.validvo.size()) {
         TRACEX("Allowed vo       = <any>");
      }
      if (Opts.xrd_server_port) {
         TRACEX("xrd_server_port  = " << Opts.xrd_server_port);
      } else {
         TRACEX("xrd_server_port  = <xroot default>");
      }
      TRACEX("mkpath_bool      = " << (Opts.mkpath_bool ? "on" : "off"));
      if (Opts.reqput_lifetime) {
         TRACEX("reqput_lifetime  = " << Opts.reqput_lifetime);
      } else {
         TRACEX("reqput_lifetime  = <dpm default>");
      }
      if (Opts.reqput_ftype) {
         TRACEX("reqput_ftype     = '" << Opts.reqput_ftype << "'");
      } else {
         TRACEX("reqput_ftype     = <dpm default>");
      }
      if (Opts.reqput_stoken.length()) {
         TRACEX("reqput_stoken    = '" << Opts.reqput_stoken << "'");
      } else {
         TRACEX("reqput_stoken    = <none>");
      }
      if (Opts.reqput_reqsize) {
         TRACEX("reqput_reqsize   = " << Opts.reqput_reqsize);
      } else {
         TRACEX("reqput_reqsize   = <dpm default>");
      }
      if (Opts.reqget_lifetime) {
         TRACEX("reqget_lifetime  = " << Opts.reqget_lifetime);
      } else {
         TRACEX("reqget_lifetime  = <dpm default>");
      }
      if (Opts.reqget_ftype) {
         TRACEX("reqget_ftype     = '" << Opts.reqget_ftype << "'");
      } else {
         TRACEX("reqget_ftype     = <dpm default>");
      }
      if (Opts.reqget_stoken.length()) {
         TRACEX("reqget_stoken    = '" << Opts.reqget_stoken << "'");
      } else {
         TRACEX("reqget_stoken    = <none>");
      }
      if (Opts.RedirConfig.IdentConfig.principal.length()) {
         TRACEX("principal        = '" <<
            Opts.RedirConfig.IdentConfig.principal << "'");
      } else {
         TRACEX("principal        = <none>");
      }
      for(std::vector<XrdOucString>::const_iterator itr=
            Opts.RedirConfig.IdentConfig.fqans.begin();
            itr != Opts.RedirConfig.IdentConfig.fqans.end();
            ++itr) {
         TRACEX("fqan             = " << *itr);
      }
      if (Opts.gracetime) {
         TRACEX("max token life   = " << Opts.gracetime
            << " seconds");
      } else {
         TRACEX("max token life   = <none specified, disk "
            "server discretion>");
      }
      if (AuthSecondary) {
         TRACEX("fixed ID authz   = Yes");
      } else {
         TRACEX("fixed ID authz   = No");
      }
      for(std::vector<XrdNetAddr>::iterator itr=
            Opts.mmReqHosts.begin();
            itr!=Opts.mmReqHosts.end();++itr) {
         char ipbuff[512];
         itr->Format(ipbuff, sizeof(ipbuff), XrdNetAddr::fmtAddr,
            XrdNetAddr::noPort);
         TRACEX("mmReqHost        = " << XrdOucString(ipbuff));
      }
      if (enableCms) {
         TRACEX("CmsClient (TRG)  = Yes");
      } else {
         TRACEX("CmsClient (TRG)  = No");
      }
      for(std::vector<XrdOucString>::const_iterator itr=
            Opts.RedirConfig.N2NCheckPrefixes.begin();
            itr!=Opts.RedirConfig.N2NCheckPrefixes.end();
            ++itr) {
         TRACEX("Allowed name pfx = " << *itr);
      }
      if (Opts.RedirConfig.lroot_param.length()) {
         TRACEX("N2N LocalRoot    = " << Opts.RedirConfig.lroot_param);
      } else if (Opts.RedirConfig.theN2N) {
         TRACEX("N2N LocalRoot    = <none>");
      }
      for(std::vector<std::pair<XrdOucString, XrdOucString > >::const_iterator
            itr=Opts.RedirConfig.pathPrefixes.begin();
            itr!=Opts.RedirConfig.pathPrefixes.end();
            ++itr) {
         TRACEX("Replace prefix   = " << itr->first << " by " << itr->second);
      }
      if (!Opts.RedirConfig.theN2N &&
            !Opts.RedirConfig.pathPrefixes.size()) {
         TRACEX("Replace prefix   = <none>");
      }
      if (Opts.RedirConfig.defaultPrefix.length()) {
         TRACEX("Default prefix   = " << Opts.RedirConfig.defaultPrefix);
      } else if (!Opts.RedirConfig.theN2N) {
         TRACEX("Default prefix   = <none>");
      }
      TRACEX("dmlite config    = " << Opts.CommonConfig.DmliteConfig);
      Say.Say("DPM Finder plugin initialisation completed.");
   }

   // Return final return code
   //
   return NoGo ? 0 : 1;
}
