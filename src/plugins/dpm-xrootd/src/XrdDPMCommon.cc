/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMCommon.cc
 * Author: Fabrizio Furano, David Smith
 * 
 * Common functions
 *
 * Created on February 2, 2012, 1:57 PM
 */

#include <XrdDPMCommon.hh>
#include <XrdSys/XrdSysError.hh>
#include <XrdSys/XrdSysPlugin.hh>
#include <XrdOuc/XrdOucPinPath.hh>
#include <XrdNet/XrdNetUtils.hh>
#include <XrdSys/XrdSysPthread.hh>
#include <XrdDPMTrace.hh>
#include <XrdOuc/XrdOucStream.hh>

#include <pthread.h>
#include <string.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

#include <memory>
#include <vector>
#include <algorithm>
#include <exception>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <errno.h>

#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/authn.h>

static int cm_key_valid = 0;
static pthread_key_t cm_key;
static XrdSysLogger *theLp = 0;

#if OPENSSL_VERSION_NUMBER < 0x10100000L
static HMAC_CTX* HMAC_CTX_new() {
  HMAC_CTX *ctx = (HMAC_CTX *)OPENSSL_malloc(sizeof(HMAC_CTX));
  if (ctx) HMAC_CTX_init(ctx);
  return ctx;
}

static void HMAC_CTX_free(HMAC_CTX *ctx) {
  if (ctx) {
    HMAC_CTX_cleanup(ctx);
    OPENSSL_free(ctx);
  }
}
#endif

/******************************************************************************/
/*                                 classes                                    */
/******************************************************************************/

/******************************************************************************/
/*                       X r d D m S t a c k F a c t o r y                    */
/******************************************************************************/

dmlite::StackInstance *XrdDmStackFactory::create() {
  
   dmlite::PluginManager *mg;
   {
      XrdSysMutexHelper mh(&ManagerMtx);
      if (!(mg=managerP.get())) {
        std::unique_ptr<dmlite::PluginManager> mP;
        mP.reset((mg = new dmlite::PluginManager()));
        mP->loadConfiguration(SafeCStr(DmConfFile));
        managerP = std::move(mP);
      }
   }
   
   return new dmlite::StackInstance(mg);
}

/******************************************************************************/
/*                         X r d D m S t a c k S t o r e                      */
/******************************************************************************/

dmlite::StackInstance *
      XrdDmStackStore::getStack(DpmIdentity &ident, bool &ViaPool) {
   dmlite::StackInstance *si=0;
   if (depth) {
      si = pool.acquire();
      ViaPool = true;
   } else {
      si = factory.create();
      ViaPool = false;
   }
   if (!si) {
      throw dmlite::DmException(DMLITE_SYSERR(EINVAL), "No stack");
   }
   try {
      resetStackDpmParams(*si);
      ident.CopyToStack(*si);
   } catch(...) {
      RetireStack(si, ViaPool);
      throw;
   }
   return si;
}

/******************************************************************************/
/*                            D p m I d e n t i t y                           */
/******************************************************************************/

void DpmIdentity::parse_secent(const XrdSecEntity *secEntity)
{
   m_name.erase();
   m_endors_raw.erase();

   if (!secEntity || !secEntity->name) {
      throw dmlite::DmException(DMLITE_SYSERR(EACCES),
         "Insufficient authentication data");
   }
   if (!strcmp(secEntity->prot,"sss")) {
      if (strcmp(secEntity->name,"nobody")) {
         m_name = DecodeString(secEntity->name);
      }
   } else if (!strcmp(secEntity->prot,"pwd")) {
      m_name = DecodeString(secEntity->name);
   } else {
      m_name = secEntity->name;
   }

   if (m_name.length() == 0) {
      throw dmlite::DmException(DMLITE_SYSERR(EACCES),
         "No identity provided by the authentication library");
   }

   if (!strcmp(secEntity->prot,"gsi")) {
      m_endors_raw = secEntity->endorsements;
   } else if (!strcmp(secEntity->prot,"sss")) {
      if (secEntity->grps && strcmp(secEntity->grps,"nogroup")) {
         m_endors_raw = secEntity->grps;
      }
   } else {
      m_endors_raw = secEntity->grps;
   }
}

void DpmIdentity::check_validvo(DpmIdentityConfigOptions &config)
{
   if (m_name == "root") {
      return;
   }
   if (!config.validvo.size()) {
      return;
   }
   if (m_vorgs.size() == 0) {
      throw dmlite::DmException(EACCES,
         "User identity includes no vo to "
         "check against allowed list");
   }
   std::vector<XrdOucString>::const_iterator itr;
   bool ok=1;
   for(itr = m_vorgs.begin(); itr != m_vorgs.end(); ++itr) {
      if (std::find(config.validvo.begin(),
            config.validvo.end(), *itr) == 
               config.validvo.end()) {
         ok=0;
         break;
      }
   }
   if (!ok) {
      throw dmlite::DmException(EACCES,
         "User belongs to a vo which is not accepted");
   }
}

void DpmIdentity::parse_grps()
{
   XrdOucString vat;
   vs.clear();
   m_vorgs.clear();
   int from = 0;
   while ((from = m_endors_raw.tokenize(vat, from, ',')) != STR_NPOS) {
      if (!vat.length())
         continue;
      if (vat.length() <= 1)
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
            "Group is too short");
      if (vat[0] != '/')
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
            "Group does not start with /");
      XrdOucString vo;
      int isl = vat.find('/', 1);
      if (isl != STR_NPOS) {
         if (isl > 1) vo.assign(vat, 1, isl - 1);
      } else {
         vo.assign(vat, 1, vat.length() - 1);
      }
      if (!vo.length())
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
            "Group includes no vo name");
      if (std::find(m_vorgs.begin(),m_vorgs.end(),vo)
            == m_vorgs.end()) {
         m_vorgs.push_back(vo);
      }
      isl = vat.find("/Role=NULL");
      if (isl != STR_NPOS)
         vat.erase(isl);
      isl = vat.find("/Capability=NULL");
      if (isl != STR_NPOS)
         vat.erase(isl);
      vs.push_back(vat);
   }
}

DpmIdentity::DpmIdentity() :
   m_name("root"), UsesSecEntForID(0) {}

DpmIdentity::DpmIdentity(XrdOucEnv *Env)
{
   UsesSecEntForID = 1;
   if (Env && Env->Get("dpm.dn"))
      UsesSecEntForID = 0;
   if (UsesSecEntForID) {
      const XrdSecEntity *secEntity = 0;
      if (Env)
         secEntity = Env->secEnv();
      parse_secent(secEntity);
   } else {
      m_name = DecodeString(Env->Get("dpm.dn"));
      if (m_name.length() == 0) {
         throw dmlite::DmException(DMLITE_SYSERR(EACCES),
            "No identity passed in the environment");
      }
   }

   XrdOucString vms;
   if (Env) {
      vms = DecodeString(Env->Get("dpm.voms"));
   }
   if (vms == ".") {
      m_endors_raw.erase();
   } else if (vms.length()) {
      m_endors_raw = vms;
   }

   parse_grps();
}

DpmIdentity::DpmIdentity(XrdOucEnv *Env, DpmIdentityConfigOptions &config)
{
   UsesSecEntForID = 1;
   if (usesPresetID(Env))
      UsesSecEntForID = 0;
   if (UsesSecEntForID) {
      const XrdSecEntity *secEntity = 0;
      if (Env)
         secEntity = Env->secEnv();
      parse_secent(secEntity);
   } else {
      m_name = config.principal;
      if (m_name.length() == 0) {
         throw dmlite::DmException(EACCES,
            "No useable identity provided");
      }
      if (m_name != "root") {
         std::vector<XrdOucString>::iterator itr;
         for(itr = config.fqans.begin();
               itr != config.fqans.end(); ++itr) {
            if (m_endors_raw.length())
               m_endors_raw += ",";
            m_endors_raw += *itr;
         }
      }
   }
   parse_grps();
   check_validvo(config);
}

/* static member function */
bool DpmIdentity::badPresetID(DpmIdentityConfigOptions &config,
      XrdOucString &err) {
   try {
      DpmIdentity id((XrdOucEnv*)0, config);
   } catch (dmlite::DmException &e) {
      err = e.what();
      return 1;
   }
   return 0;
}

void DpmIdentity::CopyToStack(dmlite::StackInstance &si) const
{
   if (strcmp(SafeCStr(m_name), "root")) {
      dmlite::SecurityCredentials creds;
      for (std::vector<XrdOucString>::const_iterator itr =
            vs.begin(); itr != vs.end(); ++itr) {
         creds.fqans.push_back(SafeCStr(*itr));
      }
      creds.clientName = SafeCStr(m_name);
      si.setSecurityCredentials(creds);
   } else {
      std::unique_ptr<dmlite::SecurityContext> scP(si.getAuthn()->createSecurityContext());
      si.setSecurityContext(*scP);
   }
}

/******************************************************************************/
/*                static member function: usesPresetID                        */
/******************************************************************************/

bool DpmIdentity::usesPresetID(XrdOucEnv *Env, const XrdSecEntity *Entity)
/*
  Function: check if to use the preset identity given in the configuration
            dpm.principal and optional dpm.fqan options.
            If the preset is to be used a secondary authorization library
            must also authorize an action.
*/
{
   if (!Entity && Env)
      Entity = Env->secEnv();

// if no authentication data, must use preset
//
   if (!Entity)
      return 1;

// if authentication type is "unix" then only consider the fixed identity
//
   if (!strcmp(Entity->prot, "unix"))
      return 1;

// if it's the shared secret then check if a username is specified,
// if not use the preset
//
   if (!strcmp(Entity->prot, "sss")) {
      if (!Entity->name || !strcmp(Entity->name,"nobody")) {
         return 1;
      }
   }

// if the environment has one of the alice token specifiers then use the preset
//
   if (Env && (Env->Get("signature") || Env->Get("authz")))
      return 1;

// use the identity information in secEnt: the DpmIdentity constructor
// has to be able to extract and parse the identity
//
   return 0;
}

/******************************************************************************/
/*                                functions                                   */
/******************************************************************************/

static void cm_destructor(void *p) {
#if OPENSSL_VERSION_NUMBER >= 0x10000001L
   ERR_remove_thread_state(0);
#else
   ERR_remove_state(0);
#endif
}

static void cm_init() {
   umask(0);
   SSL_library_init();
   SSL_load_error_strings();
   if (!pthread_key_create(&cm_key, cm_destructor))
      cm_key_valid = 1;
}

XrdOucString DecodeString(XrdOucString in) {
   XrdOucString tmp,out;
   int isl,from=0;

   while(1) {
      isl = in.find('%',from);
      if (isl == STR_NPOS)
         break;
      if (isl+3 > in.length())
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
            "DecodeString: not enough characters after "
            "percent");
      tmp.assign(in, isl+1, isl+2);
      unsigned int i;
      if (sscanf(SafeCStr(tmp), "%2x", &i) != 1 || i == 0)
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
            "DecodeString: unexpected character encoding");
      if (isl>0) {
         tmp.assign(in, from, isl-1);
         out.append(tmp);
      }
      out.append((char)i);
      from = isl + 3;
   }

   tmp.assign(in,from);
   out.append(tmp);

   return out;
}


// Encode an array of bytes to base64

char *Tobase64(const unsigned char *input, int length) {
   BIO *bmem, *b64;
   BUF_MEM *bptr;

   b64 = BIO_new(BIO_f_base64());
   if (!b64) {
      return NULL;
   }
   BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
   bmem = BIO_new(BIO_s_mem());
   if (!bmem) {
      BIO_free_all(b64);
      return NULL;
   }
   BIO_push(b64, bmem);
   if (BIO_write(b64, input, length) != length) {
      BIO_free_all(b64);
      return NULL;
   }
   if (BIO_flush(b64) <= 0) {
      BIO_free_all(b64);
      return NULL;
   }

   BIO_get_mem_ptr(b64, &bptr);

   char *buff = (char *) malloc(bptr->length+1);

   if (!buff) {
      BIO_free_all(b64);
      return NULL;
   }
      
   memcpy(buff, bptr->data, bptr->length);
   buff[bptr->length] = '\0';

   BIO_free_all(b64);

   return buff;
}

// Calculates the opaque arguments hash, v1 and/or v2
//
// - hashes points to a 2 element array of char* which will be
//   filled with the location of the resulting hash strings.
//   The caller is responsible for freeing the strings.
//
// - versions: <3>=all, <1>=v1 only, <2>=v2 only
//
// - xrd_fn: the filename in the xroot request
// - sfn: sfn of the file (needed elsewhere to close it)
// - dpmdhost: target disk hostname
// - pfn: file name on disk
// - dpmtk: dpm request token
// - flags: set of bit mapped flags
// - dn: DN of the client
// - voms: string voms attribs in XrdSecgsi comma separed style
// - tim: creation time of the url
// - tim_grace: validity time before and after creation time
// - nonce: string (possibly) to neable the hash to be made unique
// - loc: serialised Location (not including chunk data)
// - chunks: array of serialised chunk data for Location
//
// Input for the key
// - key
// - key length
//

void calc2Hashes(
   char **hashes,
   unsigned int versions,
   const char *xrd_fn,
   const char *sfn,
   const char *dpmdhost,
   const char *pfn,
   const char *dpmtk,
   unsigned int flags,
   const char *dn,
   const char *vomsnfo,
   time_t tim,
   int tim_grace,
   const char *nonce,
   const XrdOucString &locstr,
   const std::vector<XrdOucString>   &chunkstr,
   unsigned const char *key,
   size_t keylen) {

   unsigned int  iver, istart, istop;
   HMAC_CTX      *ctx;
   unsigned int  len;
   unsigned char mdbuf[EVP_MAX_MD_SIZE];
   char buf[64];
   struct tm tms;
   int ret;
   size_t n;

   // set so key destructor can trigger removal of
   // libcrypto error state when the thread finishes
   if (cm_key_valid)
      pthread_setspecific(cm_key, &cm_key);

   if (!hashes) {
      return;
   }

   hashes[0] = hashes[1] = 0;
   
   if (!xrd_fn || !sfn || !dpmdhost || !pfn ||
         !dpmtk || !dn || !vomsnfo || !nonce) {
      return;
   }
   
   ctx = HMAC_CTX_new();

   if (!ctx) {
      return;
   }

   class lguard {
      public:
         lguard(HMAC_CTX *ctx, char **hashes) :
            ctx(ctx), hashes(hashes) { }

         ~lguard() {
            HMAC_CTX_free(ctx);
            if (hashes) {
               free(hashes[0]);
               free(hashes[1]);
               hashes[0] = hashes[1] = 0;
            }
         }

         void disarmHashDelete() { hashes = 0; }
      private:
         HMAC_CTX *ctx;
         char **hashes;
   } guard(ctx,hashes);

   bool first=1;
   if (versions == 1 || versions == 2) {
      istart = istop = versions;
   } else {
      istart = 1;
      istop = 2;
   }
   for(iver=istart;iver<=istop;++iver) {
      if (first) {
         HMAC_Init_ex(ctx, key, keylen, EVP_sha256(), 0);
      } else {
         HMAC_Init_ex(ctx, 0, 0, 0, 0);
      }
      first=0;

      if (iver >= 2) {
         memset(buf, 0, 8);
         buf[7] = iver;
         HMAC_Update(ctx, (unsigned char *)buf, 8);
      }
   
      HMAC_Update(ctx, (const unsigned char *)xrd_fn,
         strlen(xrd_fn)+1);
      if (iver == 1) {
         HMAC_Update(ctx, (const unsigned char *)sfn,
            strlen(sfn)+1);
      }
      HMAC_Update(ctx, (const unsigned char *)dpmdhost,
         strlen(dpmdhost)+1);
      if (iver == 1) {
         HMAC_Update(ctx, (const unsigned char *)pfn,
            strlen(pfn)+1);
         HMAC_Update(ctx, (const unsigned char *)dpmtk,
            strlen(dpmtk)+1);
      }
   
      ret = snprintf(buf, sizeof(buf), "%u", flags);
      if (ret < 0 || (size_t)ret >= sizeof(buf)) {
         return;
      }
      HMAC_Update(ctx, (unsigned char *)buf, strlen(buf)+1);
   
      HMAC_Update(ctx, (const unsigned char *)dn, strlen(dn)+1);
      HMAC_Update(ctx, (const unsigned char *)vomsnfo,
         strlen(vomsnfo)+1);

      if (!localtime_r(&tim, &tms)) {
         return;
      }
      n = strftime(buf, sizeof(buf), "%s", &tms);
      if (n == 0 || n >= sizeof(buf)) {
         return;
      }
      n = sizeof(buf)-strlen(buf);
      ret = snprintf(&buf[strlen(buf)],n,",%d",tim_grace);
      if (ret < 0 || (size_t)ret >= n) {
         return;
      }
      HMAC_Update(ctx, (unsigned char *)buf, strlen(buf)+1);
      HMAC_Update(ctx, (unsigned char *)nonce, strlen(nonce)+1);

      if (iver >= 2) {
         HMAC_Update(ctx, (unsigned char *)SafeCStr(locstr),
            locstr.length()+1);
         n = chunkstr.size();
         ret = snprintf(buf, sizeof(buf), "%u", (unsigned int)n);
         if (ret < 0 || (size_t)ret >= sizeof(buf)) {
            return;
         }
         HMAC_Update(ctx, (unsigned char *)buf, strlen(buf)+1);
         for(size_t idx=0;idx<n;++idx) {
            HMAC_Update(ctx,
               (unsigned char*)SafeCStr(chunkstr[idx]),
               chunkstr[idx].length()+1);
         }
      }
      len = 0;
      HMAC_Final(ctx, mdbuf, &len);
      if (len < SHA256_DIGEST_LENGTH) {
         return;
      }
      hashes[iver-1] = Tobase64(mdbuf, len/2);
      if (!hashes[iver-1]) {
         return;
      }
   }

   guard.disarmHashDelete();
}

int compareHash(
   const char *h1,
   const char *h2)
{
   if (!h1 || !h2)
      return 1;

   size_t l1 = strlen(h1), l2 = strlen(h2);
   if (l1 != l2)
      return 1;

   char result = 0;
   for(size_t i = 0; i < l1; ++i) {
      result |= h1[i] ^ h2[i];
   }
   return result != 0;
}

void InitLocalHostNameList(std::vector<XrdOucString> &names) {
   char *localhostname;
   const char *errtxt=0;

   names.clear();
   localhostname = XrdNetUtils::MyHostName((char*)0, &errtxt);

   if (localhostname && !errtxt && strlen(localhostname))
      names.push_back(localhostname);

   free(localhostname);

   const char *p = getenv("DPMXRD_ALTERNATE_HOSTNAMES");
   if (p) {
      char *p3,*p2 = strdup(p);
      char *curr = p2;
      while((p3=strsep(&curr, " ,\t"))) {
         names.push_back(p3);
      }
      free(p2);
   }
}

const char *LoadKeyFromFile(unsigned char **dat, size_t *dsize) {
   int fd;
   struct stat sbuf;
   const static char fn[] = "/etc/xrootd/dpmxrd-sharedkey.dat";
   class lkh_c {
   public:
      lkh_c(int fd) : fd(fd) { };
      ~lkh_c() { close(fd); }
   private:
      int fd;
   };

   if (!dat || !dsize) {
      return "Invalid argument";
   }

   *dat = 0;
   *dsize = 0;

   fd = open(fn, O_RDONLY);

   if (fd < 0) {
      return fn;
   }
   lkh_c hlp(fd);

   if (fstat(fd, &sbuf)<0) {
      return "Could not stat";
   }

   if (!S_ISREG(sbuf.st_mode)) {
      return "Not a regular file";
   }

   if (sbuf.st_mode & (S_IROTH | S_IWOTH | S_IXOTH)) {
      return "'Other' user has some permissions set";
   }

   if (sbuf.st_size == 0) {
      return "Empty";
   }

   if (sbuf.st_size > 64) {
      return "Too long";
   }

   size_t remain = sbuf.st_size;
   size_t nread;
   ssize_t nret;

   *dat = (unsigned char *)malloc(remain);
   if (! *dat) {
      return "Could not allocate memory";
   }
   nread = 0;
   while(remain) {
      nret = read(fd, (*dat + nread), remain);
      if (nret < 0) {
         if (errno == EINTR) { continue; }
         free(*dat);
         *dat = 0;
         return "Could not read the shared key";
      } else if (nret == 0) {
         free(*dat);
         *dat = 0;
         return "Unexpected end of file";
      } else {
         nread += nret;
         remain -= nret;
      }
   }

   *dsize = nread;
   return 0;
}

XrdOucString CanonicalisePath(const char *s, int trailing_slash)
{
   XrdOucString out;
   out = s;

   while(out.find("//") != STR_NPOS)   
      out.replace("//","/");
   if (!out.beginswith('/'))
      out = "/" + out;
   if (trailing_slash && !out.endswith('/'))
      out += "/";
   else if (!trailing_slash && out.length()>1 && out.endswith('/'))
      out.erasefromend(1);

   return out;
}

XrdOucString TranslatePath(DpmRedirConfigOptions &config,const char *in)
{
   char Apath[MAXPATHLEN*2+1];

   if (config.theN2N) {
      int ret;
      Apath[MAXPATHLEN*2] = '\0';
      XrdOucString envstr;

      if (!(ret=config.theN2N->lfn2pfn(in, Apath, sizeof(Apath)))) {
         in = Apath;
      }

      if (ret || Apath[MAXPATHLEN*2]) {
         if (ret) {
            throw dmlite::DmException(DMLITE_SYSERR(-ret),
               "N2N error");
         }
         throw dmlite::DmException(DMLITE_SYSERR(ENAMETOOLONG),
            "N2N result too long");
      }
      XrdOucString check = CanonicalisePath(in,1);
      bool ok=0;
      std::vector<XrdOucString>::const_iterator itr;
      for(itr=config.N2NCheckPrefixes.begin();
            itr!=config.N2NCheckPrefixes.end();++itr) {
         if (check.find(*itr) == 0) {
            ok=1;
            break;
         }
      }
      if (!ok) {
         throw dmlite::DmException(DMLITE_SYSERR(EACCES),
            "Prefix of N2N result not listed in "
            "dpm.namecheck "
            "lfn: %s", check.c_str() );
      }
   }

   bool endsWithSlash = (strlen(in) && in[strlen(in)-1] == '/');
   XrdOucString out = CanonicalisePath(in, 1);

   if (config.theN2N) {
      if (!endsWithSlash)
         out.erasefromend(1);
      return(out);
   }

   std::vector<std::pair<XrdOucString, XrdOucString > >::const_iterator itr;

   for(itr=config.pathPrefixes.begin(); itr != config.pathPrefixes.end();
         ++itr) {
      const XrdOucString &pfx = itr->first;
      const XrdOucString &repl = itr->second;
      if (out.find(pfx) == 0) {
         out.erasefromstart(pfx.length());
         out = repl + out;
         if (!endsWithSlash)
            out.erasefromend(1);
         return out;
      }
   }

   if (config.defaultPrefix.length() == 0 ||
       out.find(config.defaultPrefix) == 0) {
      if (!endsWithSlash)
         out.erasefromend(1);
      return(out);
   }

   out.erasefromstart(1);
   out = config.defaultPrefix + out;
   if (!endsWithSlash)
           out.erasefromend(1);

   return out;
}

std::vector<XrdOucString> TranslatePathVec(DpmRedirConfigOptions &config,
            const char *in)
{
   std::vector<XrdOucString> res;
   if (!config.theN2NVec) {
      XrdOucString s = TranslatePath(config, in);
      res.push_back(s);
      return res;
   }
   std::vector<std::string *> *fnVec;
   fnVec = config.theN2NVec->n2nVec(in);
   if (fnVec == 0) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "N2N returned no list of translated names");
   }
   for (size_t i = 0; i < fnVec->size(); ++i) {
      in = (*fnVec)[i]->c_str();
      XrdOucString out = CanonicalisePath(in,1);
      bool ok=0;
      std::vector<XrdOucString>::const_iterator itr;
      for(itr=config.N2NCheckPrefixes.begin();
          itr!=config.N2NCheckPrefixes.end();++itr) {
         if (out.find(*itr) == 0) {
            ok=1;
            break;
         }
      }
      if (!ok) continue;
      bool endsWithSlash = (strlen(in) && in[strlen(in)-1] == '/');
      if (!endsWithSlash)
         out.erasefromend(1);
      res.push_back(out);
   }

   // res is either the found name or the last name in the list

   bool vecempty = fnVec->empty();
   config.theN2NVec->Recycle(fnVec);

   if (vecempty) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "N2N returned empty list of translated names");
   }

   if (res.size() == 0) {
      throw dmlite::DmException(DMLITE_SYSERR(EACCES),
         "None of the prefixes of the N2N results were listed "
         "in dpm.namecheck");
   }

   return res;
}

XrdOucString TranslatePath(DpmRedirConfigOptions &config,
            const char *in, XrdDmStackWrap &sw, bool ensure /* = false */)
{
   std::vector<XrdOucString> names = TranslatePathVec(config, in);
   if (names.size() == 1 && !ensure) {
      return names[0];
   }
   bool found = false;
   XrdOucString res;
   for (size_t i = 0; i < names.size(); ++i) {
      try {
         res = names[i];
         dmlite::ExtendedStat xstat;
         dmlite::DmStatus st = sw->getCatalog()->extendedStat(xstat, SafeCStr(res));
         if ( !st.ok() ) continue;
         
         found = true;
         break;
      } catch(const std::exception &) {
         // nothing
      }
   }
   if (!found && ensure) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "None of the translated file names exist");
   }
   return res;
}

void LocationToOpaque(const dmlite::Location &loc, XrdOucString &locstr,
      std::vector<XrdOucString> &chunkstr) {
   locstr = (int)loc.size();
   chunkstr.clear();
   std::vector<dmlite::Chunk>::const_iterator itr;
   for(itr=loc.begin();itr!=loc.end();++itr) {
      std::string s = itr->url.toString();
      XrdOucString ss(s.c_str());
      char buf[128];
      snprintf(buf,sizeof(buf),"%lld,%llu,",
         (long long)itr->offset,
         (unsigned long long)itr->size);
      ss = buf + ss;
      chunkstr.push_back(ss);
   }
}

void EnvToLocstr(XrdOucEnv *env, XrdOucString &locstr,
      std::vector<XrdOucString> &chunkstr) {
   locstr.erase();
   chunkstr.clear();
   if (!env) return;
   locstr = DecodeString(env->Get("dpm.loc"));
   if (locstr.length()) {
      int isl = locstr.find(',');
      if (isl == 0 || (isl != STR_NPOS && isl >= locstr.length()-1)) {
         throw dmlite::DmException(EINVAL,
            "Malformed loc string");
      }
      XrdOucString ss;
      if (isl == STR_NPOS) {
         ss = locstr;
      } else {
         ss.assign(locstr, 0, isl-1);
      }
      size_t nchunks = atoi(SafeCStr(ss));
      for(size_t idx=0;idx<nchunks;++idx) {
         XrdOucString ca = "dpm.chunk";
         ca += (int)idx;
         XrdOucString chk = DecodeString(env->Get(SafeCStr(ca)));
         if (!chk.length()) {
            throw dmlite::DmException(EINVAL,
               "Empty chunk string");
         }
         chunkstr.push_back(chk);
      }
   }
}

void EnvToLocation(dmlite::Location &loc,
      XrdOucEnv *env,
      const char *fn) {

   loc.clear();
   if (!env) return;
   XrdOucString locstr;
   std::vector<XrdOucString> chunkstr;
   EnvToLocstr(env, locstr, chunkstr);
   if (!locstr.length()) {
      dmlite::Chunk chnk;
      XrdOucString sfn = DecodeString(env->Get("dpm.surl"));
      const char *p;
      bool isput = false;
      if ((p=env->Get("dpm.put")) && atoi(p)) { isput=true; }
      if (!sfn.length() && isput) { sfn = fn; }
      if (sfn.length()) {
         std::string s(SafeCStr(sfn));
         chnk.url.query["sfn"] = s;
      }
      XrdOucString rtok = env->Get("dpm.tk");
      if (rtok.length()) {
         std::string s(SafeCStr(rtok));
         chnk.url.query["dpmtoken"] = s;
      }
      XrdOucString dhost,pfn;
      pfn = DecodeString(env->Get("dpm.sfn"));
      dhost = env->Get("dpm.dhost");
      chnk.url.domain = SafeCStr(dhost);
      chnk.url.path = SafeCStr(pfn);
      chnk.offset = chnk.size = 0;
      loc.push_back(chnk);
      return;
   }
   size_t nchunks = chunkstr.size();
   if (nchunks == 0) {
      throw dmlite::DmException(EINVAL,"No chunks");
   }
   for(size_t idx=0;idx<nchunks;++idx) {
      XrdOucString ss = chunkstr[idx];
      if (!ss.length()) {
         throw dmlite::DmException(EINVAL,"Invalid chunk");
      }
      XrdOucString ss2;
      int isl = ss.find(',');
      if (isl == STR_NPOS || isl == 0 || isl >= ss.length()-1) {
         throw dmlite::DmException(EINVAL,
            "Bad chunk offset field");
      }
      ss2.assign(ss,0,isl-1);
      long long the_offset;
      if (sscanf(SafeCStr(ss2),"%lld",&the_offset) != 1) {
         throw dmlite::DmException(EINVAL,"Bad chunk offset");
      }
      int isl2 = ss.find(',',isl+1);
      if (isl2 == STR_NPOS || isl2 <= isl+1 ||
            isl2 >= ss.length()-1) {
         throw dmlite::DmException(EINVAL,"Bad chunk size field");
      }
      ss2.assign(ss,isl+1,isl2-1);
      unsigned long long the_size;
      if (sscanf(SafeCStr(ss2),"%llu",&the_size) != 1) {
         throw dmlite::DmException(EINVAL,"Bad chunk size");
      }
      ss2 = ss;
      ss.assign(ss2,isl2+1);
      
      dmlite::Chunk chnk;
      chnk.url = dmlite::Url(SafeCStr(ss));
      chnk.offset = the_offset;
      chnk.size = the_size;
      loc.push_back(chnk);
   }
}

int DmExInt2Errno(int err) {
  // translate dmlite exception error code into a code suitable for use in the
  // setErrInfo in a response object. In particular replace no replicas error
  // with enoent, as xroot will do some specal handling in that case which is
  // also applicable for no replcias
  int e = DMLITE_ERRNO(err);
  if (e < 0)
    return DMLITE_INTERNAL_ERROR;
  switch(e) {
    case DMLITE_NO_REPLICAS:
      return ENOENT;
    case 0:
      return DMLITE_UNKNOWN_ERROR;
    default:
      return e;
  }
  return DMLITE_INTERNAL_ERROR;
}

int DmExErrno(const dmlite::DmException &e) {
   // translate dmlite exception error code into a code suitable for use in the
   // setErrInfo in a response object. In particular replace no replicas error
   // with enoent, as xroot will do some specal handling in that case which is
   // also applicable for no replcias
  
  return (DmExInt2Errno(e.code()));

}

XrdOucString DmExStrerror(const dmlite::DmException &e,
      const char *action /* = 0 */, const char *path /* = 0 */)
{
   // translate the error code from the exception into a text message
   // suitable for returning to the user. The full text contained in the
   // exception what() is not used here (to avoid sending it remotely, but
   // that text may be logged locally, at the point where the expcetion is
   // caught)
   int err = DMLITE_ERRNO(e.code());
   if (err < 0) {
      err = DMLITE_INTERNAL_ERROR;
   } else if (err == 0) {
      err = DMLITE_UNKNOWN_ERROR;
   }
   const char *buf = XrdSysError::ec2text(err);

   // add in the dmlite system/configuration/database localisation of the error
   XrdOucString s(buf);
   if (DMLITE_ETYPE(e.code()) == DMLITE_SYSTEM_ERROR) {
      s = "Server error: " + s;
   } else if (DMLITE_ETYPE(e.code()) == DMLITE_CONFIGURATION_ERROR) {
      s = "Server configuration error: " + s;
   } else if (DMLITE_ETYPE(e.code()) == DMLITE_DATABASE_ERROR) {
      s = "Server database error: " + s;
   }

   // add in the optional action type
   if (action && *action) {
      XrdOucString r = XrdOucString("Unable to ") + action;
      if (path && *path) {
         r += XrdOucString(" ") + path;
      }
      s = r + "; " + s;
   }
   return s;
}

XrdSysError_Table *XrdDmliteError_Table() {
   static struct texts {
      int code;
      const char *text;
   } errortexts[] = {
      {DMLITE_UNKNOWN_ERROR, "Unknown error"},
      {DMLITE_UNEXPECTED_EXCEPTION, "Unexpected exception"},
      {DMLITE_INTERNAL_ERROR, "Internal error"},
      {DMLITE_NO_SUCH_SYMBOL, "No such symbol"},
      {DMLITE_API_VERSION_MISMATCH, "API version mismatch"},
      {DMLITE_NO_POOL_MANAGER, "No dmlite pool manager"},
      {DMLITE_NO_CATALOG, "No dmlite catalog plugin"},
      {DMLITE_NO_INODE, "No dmlite inode plugin"},
      {DMLITE_NO_AUTHN, "No dmlite authn plugin"},
      {DMLITE_NO_IO, "No dmlite IO plugin"},
      {DMLITE_NO_SECURITY_CONTEXT, "No security context"},
      {DMLITE_EMPTY_SECURITY_CONTEXT, "Empty security context"},
      {DMLITE_MALFORMED, "Malformed"},
      {DMLITE_UNKNOWN_KEY, "Unknown key"},
      {DMLITE_NO_COMMENT, "No comment"},
      {DMLITE_NO_REPLICAS, "No replicas"},
      {DMLITE_NO_SUCH_REPLICA, "No such replica"},
      {DMLITE_NO_USER_MAPPING, "No user mapping"},
      {DMLITE_NO_SUCH_USER, "No such user"},
      {DMLITE_NO_SUCH_GROUP, "No such group"},
      {DMLITE_INVALID_ACL, "Invalid ACL"},
      {DMLITE_UNKNOWN_POOL_TYPE, "Unknown dmlite pool type"},
      {DMLITE_NO_SUCH_POOL, "No such pool"},
      {0,0}
   };
   static int ebase = 0, elast = 0;
   if (!ebase || !elast) {
      for(int i=0; ;++i) {
         struct texts *t = &errortexts[i];
         if (!t->text) break;
         if (ebase == 0 || t->code < ebase) ebase = t->code;
         if (elast == 0 || t->code > elast) elast = t->code;
      }
   }
   static const char **tbl = 0;
   if (!tbl) {
      int len = elast-ebase+1;
      tbl = new const char*[len];
      for (int i=0; i<len; ++i) {
         tbl[i] = "Reserved error code";
      }
      for(int i=0; ;++i) {
         struct texts *t = &errortexts[i];
         if (!t->text) break;
         tbl[t->code-ebase] = t->text;
      }
   }
   return new XrdSysError_Table(ebase, elast, tbl);
}

static int xtrace(XrdOucStream &Config, XrdSysError &Eroute, int &trval)
{
   char *val;
   static struct traceopts {
      const char *opname;
      int opval;
   } tropts[] =
      {
         {"aio",      TRACE_aio},
         {"all",      TRACE_ALL},
         {"chmod",    TRACE_chmod},
         {"close",    TRACE_close},
         {"closedir", TRACE_closedir},
         {"debug",    TRACE_debug},
         {"delay",    TRACE_delay},
         {"dir",      TRACE_dir},
         {"exists",   TRACE_exists},
         {"getstats", TRACE_getstats},
         {"fsctl",    TRACE_fsctl},
         {"io",       TRACE_IO},
         {"mkdir",    TRACE_mkdir},
         {"most",     TRACE_MOST},
         {"open",     TRACE_open},
         {"opendir",  TRACE_opendir},
         {"qscan",    TRACE_qscan},
         {"read",     TRACE_read},
         {"readdir",  TRACE_readdir},
         {"redirect", TRACE_redirect},
         {"remove",   TRACE_remove},
         {"rename",   TRACE_rename},
         {"sync",     TRACE_sync},
         {"truncate", TRACE_truncate},
         {"write",    TRACE_write}
      };
   int i, neg,numopts = sizeof(tropts)/sizeof(struct traceopts);
   trval = 0;

   if (!(val = Config.GetWord())) {
      Eroute.Emsg("Config", "trace option not specified");
      return 1;
   }
   while (val) {
      if (!strcmp(val, "off")) trval = 0;
      else {
         if ((neg = (val[0] == '-' && val[1]))) val++;
         for (i = 0; i < numopts; i++) {
            if (!strcmp(val, tropts[i].opname)) {
               if (neg) trval &= ~tropts[i].opval;
               else  trval |=  tropts[i].opval;
               break;
                           
            }
         }
         if (i >= numopts) {
            Eroute.Say("Config warning: ignoring invalid "
               "trace option '",val,"'.");
         }
      }
      val = Config.GetWord();
   }
   return 0;
}

// based on XrdOssSys::ConfigN2N
//
static int DpmCommonSetupN2N(XrdSysError &Eroute, const char *cfn,
   XrdOucString &N2N_Lib, XrdOucString &N2N_Parms, DpmRedirConfigOptions &rconf)
{
   XrdSysPlugin    *myLib;
   typedef XrdOucName2Name *(*ept)(XrdOucgetName2NameArgs);

   char libBuf[2048];
   bool noFallBack;
   char *theLib,*altLib;
   if (!XrdOucPinPath(SafeCStr(N2N_Lib), noFallBack, libBuf,
       sizeof(libBuf))) {
      theLib = strdup(SafeCStr(N2N_Lib));
      altLib = 0;
   } else {
      theLib = strdup(libBuf);
      altLib = (noFallBack ? 0 : strdup(SafeCStr(N2N_Lib)));
   }
   ept ep = 0;
   myLib = new XrdSysPlugin(&Eroute, theLib);
   if (myLib) {
      ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
                      (myLib->getPlugin("XrdOucgetName2Name")));
   }
   if (!ep && altLib) {
      delete myLib;
      myLib = new XrdSysPlugin(&Eroute, altLib);
      if (myLib) {
         ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
                         (myLib->getPlugin("XrdOucgetName2Name")));
      }
   }
   free(theLib);
   free(altLib);

   rconf.theN2N = 0;
   if (ep) {
      rconf.theN2N = ep(&Eroute, cfn, SafeCStr(N2N_Parms),
                   rconf.lroot_param.length() ? SafeCStr(rconf.lroot_param) : 0,
                   0);
   }

   rconf.theN2NVec = 0;
   if (rconf.theN2N) {
      // the second argument to getPlugin shows this is an
      // optional symbol; don't log message if it's not found
      XrdOucName2NameVec **handle = static_cast<XrdOucName2NameVec**>
         (myLib->getPlugin("Name2NameVec",2));
      if (handle) {
         rconf.theN2NVec = *handle;
      }
      // can not delete myLib
   } else {
      delete myLib;
   }

   return rconf.theN2N == 0;
}


static int DpmCommonRedirConfigProc(XrdSysError &Eroute, const char *configfn,
      DpmRedirConfigOptions &rconf) {
   char *var,*val;
   int cfgFD, retc, NoGo = 0;
   XrdOucEnv myEnv;
   XrdOucStream Config(&Eroute, getenv("XRDINSTANCE"), &myEnv, "=====> ");

   XrdOucString    N2N_Lib;
   XrdOucString    N2N_Parms;

   if (!configfn || !*configfn) {
      Eroute.Say("Config warning: config file not specified; "
            "defaults assumed.");
      return 0;
   }

   if ((cfgFD = open(configfn, O_RDONLY, 0)) < 0) {
      Eroute.Emsg("Config", errno, "open config file", configfn);
      return 1;
   }
   Config.Attach(cfgFD);

   while((var = Config.GetMyFirstWord())) {
      if (!strncmp(var, "dpm.", 4) || !strncmp(var, "oss.", 4)) {
         if (!strcmp(var+4,"localroot")) {
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("Config",
               "'localroot' not specified.");
               NoGo |= 1;
            } else {
               rconf.lroot_param = val;
            }
         }
         if (!strcmp(var+4,"namelib")) {
            if (!(val = Config.GetWord()) || !val[0]) {
               Eroute.Emsg("Config",
               "namelib not specified.");
               NoGo |= 1;
            } else {
               char parms[1040];
               N2N_Lib = val;
               if (!Config.GetRest(parms,sizeof(parms))) {
                  Eroute.Emsg("Config","namelib "
                  "parameters too long.");
                  NoGo |= 1;
               }
               N2N_Parms = parms;
            }
         }
      }
      if (!strncmp(var, "dpm.", 4)) {
         if (!strcmp(var+4,"defaultprefix")) {
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("Config",
               "'defaultprefix' not specified.");
               NoGo |= 1;
            } else {
               rconf.defaultPrefix =
                  CanonicalisePath(val, 1);
            }
         }
         if (!strcmp(var+4,"namecheck")) {
            while((val = Config.GetWord())) {
               XrdOucString cs;
               cs = CanonicalisePath(val, 1);
               rconf.N2NCheckPrefixes.push_back(cs);
            }
         }
         if (!strcmp(var+4,"replacementprefix")) {
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("Config",
               "'replacementprefix' initial path "
               "not specified.");
               NoGo |= 1;
            } else {
               XrdOucString pfx1 =
                  CanonicalisePath(val, 1);
               if (!(val = Config.GetWord())) {
                  Eroute.Emsg("Config","'pathprefix'"
                  " replacement path not specified.");
                  NoGo |= 1;
               } else {
                  XrdOucString pfx2 = CanonicalisePath(val, 1);
                  rconf.pathPrefixes.push_back(make_pair(pfx1, pfx2));
               }
            }
         }
      }
   }
   if ((retc = Config.LastError())) {
      NoGo = Eroute.Emsg("Config", retc, "read config file",
         configfn);
   }
   Config.Close();

   if (!NoGo && N2N_Lib.length() &&
       (rconf.pathPrefixes.size()>0 || rconf.defaultPrefix.length())) {
      Eroute.Emsg("Config",
                  "Can not configure defaultprefix or replacementprefix "
                  "with a namelib");
      NoGo |= 1;
   }
   if (!NoGo && !N2N_Lib.length() && rconf.lroot_param.length()) {
      Eroute.Emsg("Config", "The localroot parameter has no effect "
                            "without a namelib");
      NoGo |= 1;
   }
   if (!NoGo && !N2N_Lib.length() && rconf.N2NCheckPrefixes.size()) {
      Eroute.Emsg("Config", "Can not set namecheck without a namelib");
      NoGo |= 1;
   }
   if (!NoGo && N2N_Lib.length() && !rconf.N2NCheckPrefixes.size()) {
      Eroute.Emsg("Config", "Must set namecheck when using a namelib");
      NoGo |= 1;
   }

   // load the n2n if it's not already available
   if (!NoGo && N2N_Lib.length() && !rconf.theN2NVec && !rconf.theN2N) {
      if (DpmCommonSetupN2N(Eroute, configfn, N2N_Lib, N2N_Parms, rconf)) {
         Eroute.Emsg("Config", "Problem configuring namelib");
         NoGo |= 1;
      }  else {
         Eroute.Say("DpmCommon loaded N2N = ", SafeCStr(N2N_Lib));
      }
   }

   return NoGo;
}

int DpmCommonConfigProc(XrdSysError &Eroute, const char *configfn,
      DpmCommonConfigOptions &conf, DpmRedirConfigOptions *rconf /* = 0 */) {
   char *var;
   int cfgFD, retc, NoGo = 0;
   XrdOucEnv myEnv;
   XrdOucStream Config(&Eroute, getenv("XRDINSTANCE"), &myEnv, "=====> ");

   XrdOucString    N2N_Lib;
   XrdOucString    N2N_Parms;

   if (getenv("XRDDEBUG")) {
      conf.OssTraceLevel = conf.OfsTraceLevel = TRACE_MOST | TRACE_debug;
   }

   if (!configfn || !*configfn) {
      Eroute.Say("Config warning: config file not specified; "
                 "defaults assumed.");
      return 0;
   }

   if ((cfgFD = open(configfn, O_RDONLY, 0)) < 0) {
      Eroute.Emsg("Config", errno, "open config file", configfn);
      return 1;
   }
   Config.Attach(cfgFD);

   while((var = Config.GetMyFirstWord())) {
      if (!strncmp(var, "oss.", 4)) {
         if (!strcmp(var+4, "trace")) {
            if (xtrace(Config, Eroute,
                  conf.OssTraceLevel)) {
               NoGo = 1;
               Config.Echo();
            }
         }
      } else if (!strncmp(var, "ofs.", 4)) {
         if (!strcmp(var+4, "trace")) {
            if (xtrace(Config, Eroute,
                  conf.OfsTraceLevel)) {
               NoGo = 1;
               Config.Echo();
            }
         } else
         if (!strcmp(var+4, "cmslib")) {
            const char *val;
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("CommonConfig",
                           "'cmslib' filename missing.");
               NoGo = 1;
               Config.Echo();
            } else {
               conf.cmslib = val;
            }
         }
      } else if (!strncmp(var, "dpm.", 4)) {
         if (!strcmp(var+4, "dmconf")) {
            const char *val;
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("CommonConfig",
                           "'dmconf' filename missing.");
               NoGo = 1;
               Config.Echo();
            } else {
               conf.DmliteConfig = val;
            }
         }
         if (!strcmp(var+4, "dmstackpoolsize")) {
            const char *val;
            if (!(val = Config.GetWord())) {
               Eroute.Emsg("CommonConfig",
                           "'dmstackpoolsize' size missing.");
               NoGo = 1;
               Config.Echo();
            } else {
               conf.DmliteStackPoolSize = atoi(val);
            }
         }
      }
   }
   if ((retc = Config.LastError())) {
      NoGo = Eroute.Emsg("Config", retc, "read config file", configfn);
   }
   Config.Close();

   if (!NoGo && rconf) {
      NoGo = DpmCommonRedirConfigProc(Eroute, configfn, *rconf);
   }

   return NoGo;
}

void XrdDmCommonInit(XrdSysLogger *lp) {
   static XrdSysMutex mtx;
   static int inited = 0;

   XrdSysMutexHelper mh(&mtx);

   if (!inited) {
      cm_init();
      theLp = lp;
      ++inited;
   }
}

DpmRedirConfigOptions *GetDpmRedirConfig(XrdOucString &cmslib) {
   static DpmRedirConfigOptions *inst = 0;
   static XrdSysMutex mtx;

   XrdSysMutexHelper mh(&mtx);
   if (inst) return inst;

   if (!cmslib.length()) return 0;

   typedef DpmRedirConfigOptions *(*ept)();
   XrdSysError Eroute(theLp, "GetDpmRedirConfig");

   char libBuf[2048];
   bool noFallBack;
   char *theLib, *altLib;
   if (!XrdOucPinPath(SafeCStr(cmslib), noFallBack, libBuf,
         sizeof(libBuf))) {
      theLib = strdup(SafeCStr(cmslib));
      altLib = 0;
   } else {
      theLib = strdup(libBuf);
      altLib = (noFallBack ? 0 : strdup(SafeCStr(cmslib)));
   }
   ept ep=0;
   {
      XrdSysPlugin myLib(&Eroute, theLib);
      ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
         (myLib.getPlugin("DpmXrdCmsGetConfig")));
   }
   if (!ep && altLib) {
      XrdSysPlugin myLib(&Eroute, altLib);
      ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
         (myLib.getPlugin("DpmXrdCmsGetConfig")));
   }
   free(theLib);
   free(altLib);

   if (!ep) return 0;
   return (inst = ep());
}
