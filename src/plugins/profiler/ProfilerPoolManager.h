/// @file   ProfilerPoolManager.h
/// @brief  Profiler plugin.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef PROFILERPOOLMANAGER_H
#define	PROFILERPOOLMANAGER_H

#include "ProfilerXrdMon.h"

#include <dmlite/cpp/poolmanager.h>

namespace dmlite {

  class ProfilerPoolManager: public PoolManager, private ProfilerXrdMon {
   public:
    ProfilerPoolManager(PoolManager* decorated)  ;
    ~ProfilerPoolManager();

    std::string getImplId(void) const throw();

    void setStackInstance(StackInstance* si)  ;
    void setSecurityContext(const SecurityContext*)  ;

    std::vector<Pool> getPools(PoolAvailability)  ;
    Pool getPool(const std::string& poolname)  ;
    
    void newPool(const Pool& pool)  ;
    void updatePool(const Pool& pool)  ;
    void deletePool(const Pool& pool)  ;

    Location whereToRead (const std::string& path)  ;
    Location whereToRead (ino_t inode)              ;
    Location whereToWrite(const std::string& path)  ;
    
    
    void getDirSpaces(const std::string& path, int64_t &totalfree, int64_t &used)  ;
    
   protected:
    PoolManager* decorated_;
    char*        decoratedId_;
  };

};

#endif	// PROFILERPOOLMANAGER_H
