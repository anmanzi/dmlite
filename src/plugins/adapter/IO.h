/// @file   IO.h
/// @brief  Filesystem IO.
/// @author Alejandro Álvarez Ayllon <aalvarez@cern.ch>
#ifndef IO_H
#define	IO_H

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/io.h>
#include <fstream>

namespace dmlite {

  class StdIOFactory: public IODriverFactory {
   public:
    StdIOFactory();
    virtual ~StdIOFactory();

    void configure(const std::string& key, const std::string& value)  ;
    IODriver* createIODriver(PluginManager* pm)  ;

   private:
    std::string passwd_;
    bool        useIp_;
  };

  class StdIODriver: public IODriver {
   public:
    StdIODriver(std::string passwd, bool useIp);
    virtual ~StdIODriver();

    std::string getImplId() const throw();

    virtual void setSecurityContext(const SecurityContext* ctx)  ;
    virtual void setStackInstance(StackInstance* si)  ;

    IOHandler* createIOHandler(const std::string& pfn, int flags,
                               const Extensible& extras, mode_t mode)  ;

    void doneWriting(const Location& loc)  ;

   private:
    const SecurityContext* secCtx_;


    StackInstance* si_;



    std::string passwd_;
    bool        useIp_;
  };

  class StdIOHandler: public IOHandler {
  public:
    StdIOHandler(const std::string& path,
                 int flags, mode_t mode)  ;
    virtual ~StdIOHandler();

    void   close(void)  ;
    int    fileno()  ;
    struct stat fstat(void)  ;

    size_t read (char* buffer, size_t count)  ;
    size_t write(const char* buffer, size_t count)  ;
    size_t readv(const struct iovec* vector, size_t count)  ;
    size_t writev(const struct iovec* vector, size_t count)  ;

    size_t pread(void* buffer, size_t count, off_t offset)  ;
    size_t pwrite(const void* buffer, size_t count, off_t offset)  ;

    void   seek (off_t offset, Whence whence)  ;
    off_t  tell (void)  ;
    void   flush(void)  ;
    bool   eof  (void)  ;

  protected:
    int  fd_;
    bool eof_;
  };

};

#endif	// IO_H
