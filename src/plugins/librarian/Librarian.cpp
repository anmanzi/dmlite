/// @file    Librarian.cpp
/// @brief   Filter replicas.
/// @author  Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#include "Librarian.h"

using namespace dmlite;


LibrarianFactory::LibrarianFactory(CatalogFactory* catalogFactory)  :
  nestedFactory_(catalogFactory)
{
  // Nothing
}



LibrarianFactory::~LibrarianFactory()
{
  // Nothing
}



void LibrarianFactory::configure(const std::string& key, const std::string& value)  
{
  throw DmException(DMLITE_CFGERR(DMLITE_UNKNOWN_KEY),
                    std::string("Unknown option ") + key);
}



Catalog* LibrarianFactory::createCatalog(PluginManager* pm)  
{
  return new LibrarianCatalog(CatalogFactory::createCatalog(this->nestedFactory_, pm));
}



static void registerPluginLibrarian(PluginManager* pm)  
{
  CatalogFactory* nested = pm->getCatalogFactory();
  if (nested == NULL)
    throw DmException(DMLITE_SYSERR(DMLITE_NO_CATALOG),
                      std::string("Librarian can not be loaded first"));
    
  pm->registerCatalogFactory(new LibrarianFactory(pm->getCatalogFactory()));
}



/// This is what the PluginManager looks for
PluginIdCard plugin_librarian = {
  PLUGIN_ID_HEADER,
  registerPluginLibrarian
};
