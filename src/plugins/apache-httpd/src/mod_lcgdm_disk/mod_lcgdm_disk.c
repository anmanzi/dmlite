/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <httpd.h>
#include <http_connection.h>
#include <http_log.h>
#include <pthread.h>
#include "../client/htext.h"

#include "mod_lcgdm_disk.h"

/** Provider definition */
static const dav_provider dav_disk_provider = {
    &dav_disk_hooks_repository, /* Repository handlers  */
    &dav_disk_hooks_db, /* DB Handlers          */
    NULL, /* Lock                 */
    NULL, /* Versioning           */
    NULL, /* Binding              */
    NULL, /* Search               */
    NULL /* Context              */
};

/** Correspondence string->flag */
typedef struct
{
    const char *str;
    dir_flags flag;
} dav_disk_dir_flags;

/** Mapping from string to integer (flags) */
static dav_disk_dir_flags dav_disk_dir_flags_known[] = {
    {"write", DAV_DISK_WRITE },
    {"remotecopy", DAV_DISK_REMOTE_COPY },
    {"noauthn", DAV_DISK_NOAUTHN },
    {NULL, 0 }
};

/**
 * Creates a structure for the server configuration
 * @param p The pool used to allocate memory
 * @param s Information about the virtual server being configured
 * @return A clean dav_disk_server_conf instance
 */
static void *dav_disk_create_server_config(apr_pool_t *p, server_rec *s)
{
    (void) s;
    dav_disk_server_conf *conf = apr_pcalloc(p, sizeof(dav_disk_server_conf));

    return conf;
}

/**
 * Creates a structure for the directory configuration
 * @param p   The pool used to allocate memoryu
 * @param dir The directory being cofigured
 * @return A clean dav_dpm_dir_conf instance
 */
static void *dav_disk_create_dir_config(apr_pool_t *p, char *dir)
{
    (void) dir;

    dav_disk_dir_conf *conf = apr_pcalloc(p, sizeof(dav_disk_dir_conf));

    /* Set defaults */
    conf->flags = 0x00;
    conf->proxy_cache = "/var/proxycache";
    conf->capath = "/etc/grid-security/certificates";
    conf->cafile = NULL;
    conf->crlfile = NULL;
    conf->low_speed_time = 2*60;
    conf->low_speed_limit = 10*1024;

    /* Create a info key for each directory */
    conf->info_key = apr_pstrcat(p, "diskinfo_", dir, NULL);

    return conf;
}

/**
 * Function called for the configuration parameter NSHost
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config Configuration
 * @param arg    The value of the parameter
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_dmlite(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) config;

    if (cmd->path == NULL) {
        dav_disk_server_conf *conf;
        conf = ap_get_module_config(cmd->server->module_config, &lcgdm_disk_module) ;

        conf->manager = dmlite_manager_new();
        apr_pool_pre_cleanup_register(cmd->pool, conf->manager,
                (apr_status_t (*)(void*)) dmlite_manager_free);

        if (dmlite_manager_load_configuration(conf->manager, arg) != 0)
            return apr_psprintf(cmd->pool, "Could not load %s (%s)", arg,
                    dmlite_manager_error(conf->manager));
    }
    else {
        dav_disk_dir_conf *conf = config;

        conf->manager = dmlite_manager_new();
        apr_pool_pre_cleanup_register(cmd->pool, conf->manager,
                (apr_status_t (*)(void*)) dmlite_manager_free);

        if (dmlite_manager_load_configuration(conf->manager, arg) != 0)
            return apr_psprintf(cmd->pool, "Could not load %s (%s)", arg,
                    dmlite_manager_error(conf->manager));
    }

    return NULL ;
}

/**
 * Set the flags for a directory/location. Called once per flag.
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param w      The flag
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_flags(cmd_parms *cmd, void *config,
        const char *w)
{
    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    dav_disk_dir_flags *iter;

    for (iter = dav_disk_dir_flags_known; iter->str; ++iter) {
        if (strcasecmp(w, iter->str) == 0) {
            conf->flags |= iter->flag;

            if ((conf->flags & DAV_DISK_NOAUTHN)
                    && (conf->flags & DAV_DISK_WRITE)) {
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "You are disabling authentication and allowing write mode!");
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "This is probably not what you want");
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "If that's the case, please, check DiskFlags value");
            }

            return NULL ;
        }
    }

    return apr_psprintf(cmd->pool, "%s is not a recognised flag", w);
}

/**
 * Set the "anonymous" user
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The user
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_anon(cmd_parms *cmd, void *config,
        const char *arg)
{
    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    const char *colon = strchr(arg, ':');

    if (!colon) {
        conf->anon_group = conf->anon_user = apr_pstrdup(cmd->pool, arg);
    }
    else {
        size_t ulen = colon - arg;
        conf->anon_user = apr_pcalloc(cmd->pool, ulen + 1); // Remember \0!
        memcpy(conf->anon_user, arg, ulen);

        conf->anon_group = apr_pstrdup(cmd->pool, colon + 1);
    }
    return NULL ;
}

/**
 * Set the proxy cache location
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The user
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_proxy_cache(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    conf->proxy_cache = arg;
    return NULL ;
}

/**
 * Sets the delegation service URL.
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The URL or path.
 * @return
 */
static const char *dav_disk_cmd_delegation(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    conf->delegation_service = (!arg || !strlen(arg)) ? NULL : arg;
    return NULL ;
}

/**
 * Set CAPath with trusted certificates for TPC
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The CAPath
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_capath(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    conf->capath = (!arg || !strlen(arg)) ? NULL : arg;
    return NULL ;
}

/**
 * Set CAFile with trusted certificates for TPC
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The CAFile
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_cafile(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    conf->cafile = (!arg || !strlen(arg)) ? NULL : arg;
    return NULL ;
}

/**
 * Set CRLFile with revocated certificates for TPC
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The CRLFile
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_disk_cmd_crlfile(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    dav_disk_dir_conf *conf = (dav_disk_dir_conf*) config;
    conf->crlfile = (!arg || !strlen(arg)) ? NULL : arg;
    return NULL ;
}

/** Command list (configuration parameters) */
static const command_rec dav_disk_cmds[] = {
    AP_INIT_TAKE1 ("DiskDMLite", dav_disk_cmd_dmlite, NULL, ACCESS_CONF | RSRC_CONF,
            "DMLite configuration file"),
    AP_INIT_ITERATE("DiskFlags", dav_disk_cmd_flags, NULL, ACCESS_CONF,
            "Disk flags"),
    AP_INIT_TAKE1 ("DiskAnon", dav_disk_cmd_anon, NULL, ACCESS_CONF,
            "Anonymous user for fallback"),
    AP_INIT_TAKE1 ("DiskProxyCache", dav_disk_cmd_proxy_cache, NULL, ACCESS_CONF,
            "Proxy cache location"),
    AP_INIT_TAKE1 ("DiskProxyDelegationService", dav_disk_cmd_delegation, NULL, ACCESS_CONF,
            "Delegation service path or URL"), 
    AP_INIT_TAKE1 ("DiskSSLCACertificatePath", dav_disk_cmd_capath, NULL, ACCESS_CONF,
            "SSL CA Certificate path ('/path/to/dir' - contains PEM encoded files)"),
    AP_INIT_TAKE1 ("DiskSSLCACertificateFile", dav_disk_cmd_cafile, NULL, ACCESS_CONF,
            "SSL CA Certificate file ('/path/to/file' - PEM encoded)"),
    //AP_INIT_TAKE1 ("DiskSSLCARevocationPath", dav_disk_cmd_crlpath, NULL, ACCESS_CONF,
    //        "SSL CA Certificate Revocation List (CRL) path ('/path/to/dir' - contains PEM encoded files)"),
    AP_INIT_TAKE1 ("DiskSSLCARevocationFile", dav_disk_cmd_crlfile, NULL, ACCESS_CONF,
            "SSL CA Certificate Revocation List (CRL) file ('/path/to/file' - PEM encoded)"),
    AP_INIT_TAKE1 ("DiskLowSpeedTime", ap_set_int_slot,
            (void *)APR_OFFSETOF(dav_disk_dir_conf, low_speed_time), ACCESS_CONF,
            "Low speed limit time period in seconds"),
    AP_INIT_TAKE1 ("DiskLowSpeedLimit", ap_set_int_slot,
            (void *)APR_OFFSETOF(dav_disk_dir_conf, low_speed_limit), ACCESS_CONF,
            "Low speed limit in bytes per low speed time period"),
    { NULL, {NULL}, NULL, 0, 0, NULL}
};

/**
 * Callback used to register the hooks.
 * This is the entry point of the module, where all hooks are registered in
 * mod_dav so the requests can be passed.
 * @param p A pool to use for allocations
 */
static void mod_lcgdm_disk_register_hooks(apr_pool_t *p)
{
    //dav_hook_find_liveprop(dav_disk_find_liveprop, NULL, NULL, APR_HOOK_MIDDLE);
    //dav_hook_insert_all_liveprops(dav_disk_insert_all_liveprops, NULL, NULL, APR_HOOK_MIDDLE);
    //dav_register_liveprop_group(p, &dav_disk_liveprop_group);

    dav_register_provider(p, "disk", &dav_disk_provider);

#ifdef BUILD_HTCOPY
    /* This function has to be called, but it is not thread-safe, so this looks
     * like the best place */
    htext_global_init();
#endif
}

/**
 * Module's data structure.
 * This is the joint between the module and Apache (callbacks)
 * 'module' is defined in httpd.h
 */
module AP_MODULE_DECLARE_DATA lcgdm_disk_module = {
        STANDARD20_MODULE_STUFF, dav_disk_create_dir_config, /* Per-directory config creator */
        NULL,                          /* Dir config merger            */
        dav_disk_create_server_config, /* Server config creator        */
        NULL,                          /* Server config merger         */
        dav_disk_cmds,                 /* Command table                */
        mod_lcgdm_disk_register_hooks  /* Register hooks               */
};
