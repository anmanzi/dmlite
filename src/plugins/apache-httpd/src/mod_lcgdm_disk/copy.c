/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_buckets.h>
#include <apr_strings.h>
#include <ctype.h>
#include <dmlite/c/io.h>
#include <httpd.h>
#include <http_log.h>
#include <http_protocol.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <util_filter.h>

#include "../shared/delegation.h"
#include "mod_lcgdm_disk.h"
#include "../client/htext.h"
#include "../shared/utils.h"

/** Structure used to pass data along callbacks. */
typedef struct
{
    apr_bucket_brigade *brigade;
    ap_filter_t *output;
    request_rec *request;
    const char *source;
    const char *destination;
} dav_disk_copy_data;

/* Custom I/O handler */
typedef struct
{
    off_t fsize;
    dmlite_context* context;
    const dmlite_location* location;
} dav_disk_open_params;

/* Custom file handler */
typedef struct
{
    int open_flags;
    dmlite_fd* dmlite_fd;
    dmlite_context* context;
    const dmlite_location* location;
} dav_disk_fd;


off_t dav_disk_fsize(const char *path, void *udata)
{
    (void) path;
    dav_disk_open_params* params = udata;
    return params->fsize;
}

void* dav_disk_fopen(const char *path, const char *mode, void *udata)
{
    dav_disk_open_params* params = udata;

    int open_flags = O_RDONLY;
    if (strchr(mode, 'w') != NULL || strchr(mode, '+') != NULL )
        open_flags = O_WRONLY | O_CREAT;

    dav_disk_fd* ddfd = calloc(1, sizeof(dav_disk_fd));
    ddfd->dmlite_fd = dmlite_fopen(params->context, path, open_flags,
            params->location->chunks[0].url.query, 0640);
    if (ddfd->dmlite_fd == NULL) {
        free(ddfd);
        return NULL;
    }

    ddfd->context = params->context;
    ddfd->location = params->location;
    ddfd->open_flags = open_flags;
    return ddfd;
}

int dav_disk_fclose(void *fd)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;

    int ret = dmlite_fclose(ddfd->dmlite_fd);
    if (ret >= 0 && (O_WRONLY & ddfd->open_flags)) {
        ret = dmlite_donewriting(ddfd->context, ddfd->location);
        if (ret) {
          dmlite_put_abort(ddfd->context, ddfd->location);
        }
    }

    
    free(ddfd);
    
    // FIXME: this is experimental. I have the impression that
    // Apache does not free resources if this fclose call fails
    return 0;
    //return ret;
}

size_t dav_disk_fread(void *buffer, size_t size, size_t n, void *fd)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;

    ssize_t nbytes = dmlite_fread(ddfd->dmlite_fd, buffer, size * n);
    if (nbytes <= 0)
        return 0;
    else
        return nbytes;
}

size_t dav_disk_fwrite(const void* buffer, size_t size, size_t n, void *fd)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;

    ssize_t nbytes = dmlite_fwrite(ddfd->dmlite_fd, buffer, size * n);
    if (nbytes <= 0)
        return 0;
    else
        return nbytes;
}

int dav_disk_fseek(void *fd, long offset, int whence)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;
    return dmlite_fseek(ddfd->dmlite_fd, offset, whence);
}

long dav_disk_ftell(void* fd)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;
    return dmlite_ftell(ddfd->dmlite_fd);
}

int dav_disk_feof(void* fd)
{
    dav_disk_fd* ddfd = (dav_disk_fd*)fd;
    return dmlite_feof(ddfd->dmlite_fd);
}

struct htext_io_handler dav_disk_io_handler = {
        dav_disk_fsize, dav_disk_fopen, dav_disk_fclose, dav_disk_fread,
        dav_disk_fwrite, dav_disk_fseek, dav_disk_ftell, dav_disk_feof };

/**
 * Called by htext when there is logging output
 * @param handle The handle that triggers the call
 * @param type   Type of logging data
 * @param msg    The message itself
 * @param size   The size of the message (it might not be NULL terminated)
 * @param ud     User defined data
 */
static void dav_disk_copy_log(htext_handle *handle, HTEXT_LOG_TYPE type,
        const char *msg, size_t size, void *ud)
{
    (void) handle;
    (void) size;

    dav_disk_copy_data *ddc = (dav_disk_copy_data*) ud;

    switch (type) {
        case HTEXT_LOG:
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request, "%s", msg);
            break;
        case HTEXT_HEADER_IN:
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request, "> %s", msg);
            break;
        case HTEXT_HEADER_OUT:
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request, "< %s", msg);
            break;
        case HTEXT_TEXT:
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request, "| %s", msg);
            break;
        default:
            break;
    }
}

/**
 * Generates and sends the performance clients following GridFTP format
 * http://www.ogf.org/documents/GFD.20.pdf (Appendix II), skipping the
 * code.
 */
static void dav_disk_send_performance_markers(dav_disk_copy_data *ddc, size_t n,
        size_t total[], size_t done[], char *rconn[])
{
    (void) total;

    char buf[80];
    time_t timestamp = time(NULL );
    size_t i;

    for (i = 0; i < n; ++i) {
        buf[0] = '\0';
        if (rconn && rconn[i]) {
            snprintf(buf, 78, "\tRemoteConnections: %s", rconn[i]);
            strcat(buf, "\n"); // we reserved space for endl
        }
        apr_brigade_printf(ddc->brigade, ap_filter_flush, ddc->output,
                "Perf Marker\n"
                "\tTimestamp: %ld\n"
                "\tStripe Index: %u\n"
                "\tStripe Bytes Transferred: %ld\n"
                "\tTotal Stripe Count: %ld\n"
                "%sEnd\n", timestamp, (unsigned) i, (long) done[i],
                (long) n, buf);
    }
}

/**
 * Polls the handler, and sends feedback to the client
 */
static dav_error *dav_disk_pool_and_feedback(htext_handle *handle,
        dav_disk_copy_data *ddc)
{
    int wait, status;
    dav_error *error = NULL;
    const char *error_string;

    do {
        size_t *total, *done, n, i;
        size_t globalDone, globalTotal;
        char **rconn;

        status = htext_status(handle);
        switch (status) {
            case HTEXTS_SUCCEEDED:
            case HTEXTS_FAILED:
            case HTEXTS_ABORTED:
                wait = 0;
                break;
            default:
                /* In the first go we need to set the reply */
                if (ddc->request->status == 0) {
                    ddc->request->status = HTTP_ACCEPTED;
                    ap_set_content_type(ddc->request, "text/plain");
                }
                /* Print progress */
                htext_progress(handle, &n, &total, &done, &rconn);
                globalDone = globalTotal = 0;
                for (i = 0; i < n; ++i) {
                    globalDone += done[i];
                    globalTotal += total[i];
                }

                dav_disk_send_performance_markers(ddc, n, total, done, rconn);

                if (ap_fflush(ddc->output, ddc->brigade) == APR_SUCCESS) {
                    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request,
                            "COPY '%s' %lu/%lu", ddc->request->uri,
                            (unsigned long) globalDone,
                            (unsigned long) globalTotal);
                    wait = 1;
                    sleep(1);
                }
                else {
                    wait = 0;
                    htext_abort(handle);
                }
        }
    } while (wait);

    error_string = htext_error_string(handle);
    switch (status) {
        case HTEXTS_SUCCEEDED:
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ddc->request,
                    "Remote copy finished successfully (%d): %s => %s",
                    htext_http_code(handle), ddc->source, ddc->destination);
            /* 200 is OK, but 201 fits better */
            if (ddc->request->status == 0)
                ddc->request->status = HTTP_CREATED;
            apr_brigade_printf(ddc->brigade, ap_filter_flush, ddc->output, "success: Created\n");
            break;
        case HTEXTS_FAILED:
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, ddc->request,
                    "Remote copy failed (%d): %s => %s %s",
                    htext_http_code(handle), ddc->source, ddc->destination,
                    error_string ? error_string : "");
            if (ddc->request->status == 0)
                error = dav_shared_new_error(ddc->request, NULL,
                        htext_http_code(handle),
                        "Failed: Remote copy failed with status code %d: %s\n",
                        htext_http_code(handle),
                        error_string ? error_string : "");
            else
                apr_brigade_printf(ddc->brigade, ap_filter_flush, ddc->output,
                        "failure: Remote copy failed with status code %d: %s\n",
                        htext_http_code(handle),
                        error_string ? error_string : "");
            break;
        case HTEXTS_ABORTED:
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, ddc->request,
                    "Remote copy aborted (%d): %s => %s %s",
                    htext_http_code(handle), ddc->source, ddc->destination,
                    error_string ? error_string : "");
            if (ddc->request->status == 0)
                error = dav_shared_new_error(ddc->request, NULL,
                        HTTP_INTERNAL_SERVER_ERROR, "Aborted");
            else
                apr_brigade_printf(ddc->brigade, ap_filter_flush, ddc->output,
                        "failure: Aborted\n");
            break;
    }

    return error;
}

/**
 * Process the flags header
 */
typedef struct {
    request_rec *r;
    htext_handle *handle;
} process_flags_data_t;

static void dav_disk_process_flags_callback(process_flags_data_t *data, const char *key, const char *value)
{
    // Copy flags
    if (strcasecmp(key, "Copy-Flags") == 0) {
        apr_table_t* flags = dav_lcgdm_parse_cookies(data->r->pool, value);
        if (apr_table_get(flags, "NoHead")) {
            htext_setopt(data->handle, HTEXTOP_NOHEAD, 1);
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, data->r, "COPY request disabling final HEAD");
        }
    }
    // Forward transfer headers
    else if (strncasecmp(key, "TransferHeader", 14) == 0) {
        key += 14;
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, data->r, "COPY header %s", key);
        htext_addheader(data->handle, key, value);
    }
}


static void dav_disk_process_flags(request_rec *r, htext_handle* handle)
{
    process_flags_data_t callback_data = {r, handle};

    const apr_array_header_t *headers = apr_table_elts(r->headers_in);
    apr_table_entry_t *entry = (apr_table_entry_t*)headers->elts;
    int i;

    for (i = 0; i < headers->nelts; ++i) {
        dav_disk_process_flags_callback(&callback_data, entry[i].key, entry[i].val);
    }
}


/* Validate the delegated credentials, ask for new if needed */
static dav_error *dav_disk_check_delegation(const dav_resource* res, char** uproxy)
{
  const char *client_name;
  
  client_name = dmlite_get_security_context(res->info->ctx)->credentials->client_name;
  
  /* Get delegation proxy */
  *uproxy = dav_deleg_get_proxy(res->info->request,
                                res->info->d_conf->proxy_cache, client_name);
  
  /* Proxy exists and it is valid */
  if (*uproxy) {
    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, res->info->request,
                  "Using delegated proxy '%s'", *uproxy);
    return NULL;
  }
  /* Proxy does not exist or it isn't valid, but requested no delegation */
  else {
    if (apr_table_get(res->info->request->headers_in, "X-No-Delegate") != NULL) {
      /* X-No-Delegate header is deprecated, instead use Credential header */
      ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, res->info->request,
                  "Not doing the delegation (deprecated X-No-Delegate header present)");
      return NULL;
    }
    if (apr_table_get(res->info->request->headers_in, "TransferHeaderAuthorization") != NULL) {
      ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, res->info->request,
                    "Not doing the delegation (TransferHeaderAuthorization header present)");
      return NULL;
    }
  }
  
  
  
  /* Already asked for a proxy and still didn't get one
   * Probably the client does not understand this semantics */
  if (res->info->copy_already_redirected) {
    return dav_shared_new_error(res->info->request, NULL,
                                419, // Need credentials
                                "Could not find a delegated proxy after an explicit request for delegation.\n"
                                "Probably your client do not support the header 'X-Delegate-To'?");
  }
  /* Delegation service configured
   * Ask for credentials to the client */
  else if (res->info->d_conf->delegation_service != NULL) {
    const char* redirect_with_delegation;
    redirect_with_delegation = apr_pstrcat(res->pool,
                                           res->info->request->unparsed_uri, "&copyRedirected=1", NULL );
    
    apr_table_setn(res->info->request->err_headers_out, "X-Delegate-To",
                   res->info->d_conf->delegation_service);
    apr_table_setn(res->info->request->err_headers_out, "Location",
                   redirect_with_delegation);
    
    return dav_shared_new_error(res->info->request, NULL,
                                HTTP_MOVED_TEMPORARILY, "Could not find a delegated proxy.");
  }
  /*  No credentials, and no delegation endpoint configured :( */
  else {
    return dav_shared_new_error(res->info->request, NULL,
                                HTTP_INTERNAL_SERVER_ERROR,
                                "Could not find a delegated proxy, and there is no delegation endpoint configured.");
  }
}

/*
 * Generic code for copying both from and to the storage
 */
static dav_error *dav_disk_generic_copy(const dav_resource* res, const char* uproxy,
        dav_disk_open_params* open_params,
        const char* src, const char* dst)
{
    dav_error *error = NULL;
    apr_bucket *bkt;
    htext_handle *handle;
    dav_disk_copy_data ddc;
    request_rec* req = res->info->request;
    dav_disk_dir_conf* d_conf = res->info->d_conf;

    int oldcancel;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldcancel);
    
    /* Setup the handle */
    if (!(handle = htext_init())) {
        return dav_shared_new_error(req, NULL,
                HTTP_INTERNAL_SERVER_ERROR, "Not enough memory");
    }

    htext_setopt(handle, HTEXTOP_SOURCEURL, src);
    htext_setopt(handle, HTEXTOP_IO_HANDLER, &dav_disk_io_handler);
    htext_setopt(handle, HTEXTOP_IO_HANDLER_DATA, open_params);
    htext_setopt(handle, HTEXTOP_DESTINATIONURL, dst);

    if (uproxy) {
        htext_setopt(handle, HTEXTOP_USERCERTIFICATE, uproxy);
        htext_setopt(handle, HTEXTOP_USERPRIVKEY, uproxy);
        htext_setopt(handle, HTEXTOP_CAFILE, uproxy);
    }
    else if (d_conf->cafile) {
        htext_setopt(handle, HTEXTOP_CAFILE, d_conf->cafile);
    }
    if (d_conf->capath) {
        htext_setopt(handle, HTEXTOP_CAPATH, d_conf->capath);
    }
    if (d_conf->crlfile) {
        htext_setopt(handle, HTEXTOP_CRLFILE, d_conf->crlfile);
    }
    if (uproxy || d_conf->capath || d_conf->cafile) {
        htext_setopt(handle, HTEXTOP_VERIFYPEER, 1);
    }

    htext_setopt(handle, HTEXTOP_LOW_SPEED_TIME, d_conf->low_speed_time);
    htext_setopt(handle, HTEXTOP_LOW_SPEED_LIMIT, d_conf->low_speed_limit);

    htext_setopt(handle, HTEXTOP_LOGCALLBACK, dav_disk_copy_log);
    htext_setopt(handle, HTEXTOP_LOGCALLBACK_DATA, &ddc);
    htext_setopt(handle, HTEXTOP_VERBOSITY, 2);

    dav_disk_process_flags(req, handle);

    ddc.output = req->output_filters;
    ddc.brigade = apr_brigade_create(req->pool, ddc.output->c->bucket_alloc);
    ddc.request = req;
    ddc.source = src;
    ddc.destination = dst;

    /* Run */
    if (htext_perform(handle) != 0) {
        error = dav_shared_new_error(req, NULL,
                HTTP_INTERNAL_SERVER_ERROR, "Could not perform the action: %s",
                htext_error_string(handle));
        htext_destroy(handle);
        return error;
    }

    req->status = 0;
    bkt = apr_bucket_flush_create(ddc.output->c->bucket_alloc);

    /* Poll and feedback */
    error = dav_disk_pool_and_feedback(handle, &ddc);

    /* Finish */
    htext_destroy(handle);
    
    if (!error) {
        bkt = apr_bucket_eos_create(ddc.output->c->bucket_alloc);
        APR_BRIGADE_INSERT_TAIL(ddc.brigade, bkt);
        if (ap_pass_brigade(ddc.output, ddc.brigade) != APR_SUCCESS)
            error = dav_shared_new_error(req, NULL,
                    HTTP_INTERNAL_SERVER_ERROR,
                    "Could not write EOS to filter.");
    }    
    
    pthread_setcancelstate(oldcancel, NULL);
    return error;
}

/* Copy from the storage to a remote endpoint */
dav_error *dav_disk_remote_copy(const dav_resource *src, const char *dst)
{
    dav_error *error = NULL;
    char *uproxy = NULL;

    /* Third party copies enabled? */
    if (!(src->info->d_conf->flags & DAV_DISK_REMOTE_COPY)) {
        return dav_shared_new_error(src->info->request, NULL,
                HTTP_METHOD_NOT_ALLOWED,
                "This server does not allow remote copies");
    }

    const char *credential = apr_table_get(src->info->request->headers_in, "Credential");
    if (!credential || strcmp(credential, "gridsite") == 0) {
        /* Double check credentials */
        error = dav_disk_check_delegation(src, &uproxy);
        if (error != NULL) {
            return error;
        }
    }

    /* Set open params */
    dav_disk_open_params open_params;
    memset(&open_params, 0x00, sizeof(open_params));
    open_params.context = src->info->ctx;
    open_params.location = &src->info->loc;
    open_params.fsize = src->info->fsize;

    /* And copy */
    return dav_disk_generic_copy(src, uproxy,
            &open_params, 
            src->info->loc.chunks[0].url.path, dst);
}

/* Copy from a remote endpoint to the storage */
dav_error *dav_disk_remote_fetch(const char *src, const dav_resource *dst)
{
    dav_error *error = NULL;
    char *uproxy = NULL;

    /* Third party copies enabled? */
    if (!(dst->info->d_conf->flags & DAV_DISK_REMOTE_COPY)) {
        return dav_shared_new_error(dst->info->request, NULL,
                HTTP_METHOD_NOT_ALLOWED,
                "This server does not allow remote copies");
    }

    const char *credential = apr_table_get(dst->info->request->headers_in, "Credential");
    if (!credential || strcmp(credential, "gridsite") == 0) {
        /* Double check credentials */
        error = dav_disk_check_delegation(dst, &uproxy);
        if (error != NULL) {
            return error;
        }
    }

    /* Set open params */
    dav_disk_open_params open_params;
    memset(&open_params, 0x00, sizeof(open_params));
    open_params.context = dst->info->ctx;
    open_params.location = &dst->info->loc;

    /* And copy */
    return dav_disk_generic_copy(dst, uproxy,
            &open_params,
            src, dst->info->loc.chunks[0].url.path);
}
