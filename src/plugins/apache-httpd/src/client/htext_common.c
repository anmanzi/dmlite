/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE
#include <ctype.h>
#include <curl/curl.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "htext.h"
#include "htext_private.h"

void htext_partial_init(htext_chunk *p)
{
    memset(p, 0, sizeof(htext_chunk));
    sem_init(&(p->final), 0, 0);
}

void htext_partial_clean(htext_chunk *p)
{
    if (p->curl)
        curl_easy_cleanup(p->curl);
    if (p->fd)
        GETIO(p->handle) ->close(p->fd);
    if (p->chunk_rconn && *(p->chunk_rconn))
        free(*(p->chunk_rconn));
    if (p->location)
        free(p->location);
    if (p->headers)
        curl_slist_free_all(p->headers);
    sem_destroy(&(p->final));
}

char *trim(char *str)
{
    int i, j;
    /* Remove at the beginning */
    for (i = 0; str[i] != '\0' && isspace(str[i]); ++i)
        ;
    for (j = i, i = 0; str[j] != '\0'; ++i, ++j)
        str[i] = str[j];
    /* Remove at the end */
    for (--j; j >= 0 && isspace(str[j]); --j)
        str[j] = '\0';
    return str;
}

char *get_host(const char *url, char *buffer)
{
    char *start, *end;
    int i;

    start = index(url, ':') + 3; /* Jump over ://     */
    end = index(start, '/'); /* If NULL, it is ok */

    for (i = 0; start[i] != '\0' && start + i != end; ++i)
        buffer[i] = start[i];
    buffer[i] = '\0';
    return buffer;
}

int htext_log(htext_handle *handle, const char *fmt, ...)
{
    char buffer[512];
    int n;
    va_list args;
    htext_log_callback f;

    va_start(args, fmt);
    n = vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    f = (htext_log_callback) GETPTR(handle, HTEXTOP_LOGCALLBACK);
    if (f) {
        f(handle, HTEXT_LOG, buffer, strlen(buffer),
                GETPTR(handle,HTEXTOP_LOGCALLBACK_DATA));
    }

    return n;
}

int htext_error(htext_handle *handle, const char *fmt, ...)
{
    char buffer[512];
    int n = 0;
    va_list args;

    if (fmt != NULL ) {
        va_start(args, fmt);
        n = vsnprintf(buffer, sizeof(buffer), fmt, args);
        va_end(args);

        handle->error_string = strdup(buffer);
    }
    handle->status = HTEXTS_FAILED;

    return n;
}

size_t htext_header_callback(void *buffer, size_t size, size_t nmemb, void *st)
{
    htext_chunk *partial = (htext_chunk*) st;
    size_t bsize = size * nmemb;
    char *line = (char*) buffer;
    char *p;

    /* Do nothing if partial not set */
    if (!partial)
        return bsize;

    /* HTTP status code */
    if (strncasecmp("HTTP/", line, 5) == 0) {
        p = index(line, ' ') + 1;
        partial->http_status = atoi(p);
        /* Check it is no redirect */
        if ((partial->http_status < 300 || partial->http_status >= 400)) {
            sem_post(&(partial->final));
        }
        /* It is, so reset some fields */
        else {
            switch (partial->http_status) {
                case 300:
                case 301:
                    partial->redirect_is_cacheable = 1;
                    break;
                default:
                    partial->redirect_is_cacheable = 0;
            }
        }
    }
    /* Actual location of the file */
    else if (strncasecmp("Location:", line, 9) == 0
            || strncasecmp("Content-Location:", line, 17) == 0) {
        p = index(line, ':') + 1;
        partial->location = trim(strdup(p));
    }
    /* File size */
    else if (strncasecmp("Content-Length:", line, 15) == 0) {
        p = index(line, ':') + 1;
        if (p && partial->chunk_total != NULL )
            *(partial->chunk_total) = atol(p);
    }
    /* Chunked transfer */
    else if (strncasecmp("Transfer-Encoding: chunked", line, 27) == 0) {
        if (partial->chunk_total != NULL )
            *(partial->chunk_total) = 0; /* Who knows? */
    }
    /* Delegation service */
    else if (strncasecmp("X-Delegate-To:", line, 14) == 0 || strncasecmp("Delegate-To:", line, 12) == 0) {
        p = index(line, ':') + 1;
        htext_delegate(partial, trim(p));
    }
    /* Caching */
    else if (strncasecmp("Cache-Control:", line, 14) == 0) {
        p = index(line, ':') + 1;
        p = trim(p);
        if (strcasecmp("no-cache", p) == 0)
            partial->redirect_is_cacheable = 0;
    }
    else if (strncasecmp("Expires:", line, 8) == 0) {
        struct tm st_time;
        p = index(line, ':') + 1;
        p = trim(p);
        /* Scan the specified date */
        if (strptime(p, "%a, %d %b %Y %H:%M:%S GMT", &st_time) == NULL )
            partial->redirect_is_cacheable = 0;
        else if (difftime(mktime(&st_time), time(NULL )) <= 0)
            partial->redirect_is_cacheable = 0;
        else
            partial->redirect_is_cacheable = 1;
    }

    return bsize;
}

int htext_progress_callback(void *pp, curl_off_t dltotal, curl_off_t dlnow,
        curl_off_t ultotal, curl_off_t ulnow)
{
    htext_chunk *partial = (htext_chunk*) pp;
    if (!partial)
        return 0;

    if (partial->handle->http_method == HTTP_PUT) {
        *(partial->chunk_total) = ultotal;
        *(partial->chunk_done) = ulnow;
    }
    else {
        *(partial->chunk_total) = dltotal;
        *(partial->chunk_done) = dlnow;
    }

    CURLcode res = CURLE_OK;
    char *ip = NULL;
    long port = 0;

    if (res == CURLE_OK)
        res = curl_easy_getinfo(partial->curl, CURLINFO_PRIMARY_IP, &ip);
    if (res == CURLE_OK)
        res = curl_easy_getinfo(partial->curl, CURLINFO_PRIMARY_PORT, &port);
    if (res == CURLE_OK && ip) {
        int len = strlen(ip)+15;
        char *rconn = malloc(len);
        if (strchr(ip, ':')) {
            snprintf(rconn, len, "tcp:[%s]:%ld", ip, port);
        } else {
            snprintf(rconn, len, "tcp:%s:%ld", ip, port);
        }
        htext_log(partial->handle, "connection %s, download %ld/%ld, upload %ld/%ld", rconn, dlnow, dltotal, ulnow, ultotal);
        char *tmp = *(partial->chunk_rconn);
        *(partial->chunk_rconn) = rconn;
        if (tmp) free(tmp);
    }

    return 0;
}

int htext_progres_old_callback(void *pp, double dltotal, double dlnow,
        double ultotal, double ulnow)
{
    return htext_progress_callback(pp, (curl_off_t)dltotal, (curl_off_t)dlnow,
                    (curl_off_t)ultotal, (curl_off_t)ulnow);
}

size_t htext_write_callback(char *buffer, size_t size, size_t nmemb, void *pp)
{
    htext_chunk *partial = (htext_chunk*) pp;
    size_t bsize = size * nmemb;

    if (GETINT(partial->handle, HTEXTOP_VERBOSITY)> 0 && bsize) {
        htext_log_callback f = (htext_log_callback)GETPTR(partial->handle, HTEXTOP_LOGCALLBACK);
        f(partial->handle, HTEXT_BODY, buffer, bsize, GETPTR(partial->handle, HTEXTOP_LOGCALLBACK_DATA));
    }

    return bsize;
}

size_t htext_dummy_write_callback(char *buffer, size_t size, size_t nmemb,
        void *pp)
{
    (void) buffer;
    (void) pp;
    /* We want to ignore this output */
    return size * nmemb;
}

int htext_debug_callback(CURL *curl, curl_infotype type, char *data,
        size_t size, void *pp)
{
    (void) curl;

    htext_chunk *partial = (htext_chunk*) pp;

    if (partial && GETINT(partial->handle, HTEXTOP_VERBOSITY)> 1) {
        htext_log_callback f = (htext_log_callback)GETPTR(partial->handle,HTEXTOP_LOGCALLBACK);

    switch (type) {
      case CURLINFO_HEADER_IN:
        f(partial->handle, HTEXT_HEADER_IN, data, size, GETPTR(partial->handle, HTEXTOP_LOGCALLBACK_DATA));
        break;
      case CURLINFO_HEADER_OUT:
        f(partial->handle, HTEXT_HEADER_OUT, data, size, GETPTR(partial->handle, HTEXTOP_LOGCALLBACK_DATA));
        break;
      case CURLINFO_TEXT:
        f(partial->handle, HTEXT_TEXT, data, size, GETPTR(partial->handle, HTEXTOP_LOGCALLBACK_DATA));
        break;
      default:
        break;
    }
  }

    return 0;
}

struct curl_slist *htext_copy_slist(struct curl_slist *list)
{
    struct curl_slist *copy = NULL;
    while (list) {
        copy = curl_slist_append(copy, list->data);
        list = list->next;
    }
    return copy;
}