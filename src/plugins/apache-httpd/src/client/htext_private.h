/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _HTEXT_PRIVATE_H
#define _HTEXT_PRIVATE_H

#include <curl/curl.h>
#include <pthread.h>
#include <semaphore.h>
#include "htext.h"

/* Methods */
enum
{
    HTTP_GET = 0, HTTP_PUT, HTTP_COPY, HTTP_UNSUPPORTED
};

/* Types */
typedef enum
{
    OT_STRING = 0, OT_INT, OT_POINTER
} option_type;

typedef union
{
    char *s;
    int i;
    void *p;
} option_value;

typedef struct
{
    option_type type;
    option_value value;
} option_entry;

struct htext_handle
{
    unsigned status; /* As defined in htext.h */
    option_entry *options; /* An array with the options */
    unsigned http_method; /* One of HTTP_* */
    unsigned http_status /* HTTP status code (e.g. 404, 200,...) */;
    char *error_string; /* When an error ocurred, the description is set here */
    unsigned partials; /* How many streams are being used */
    size_t *partial_done; /* An array with the bytes transfered per stream. malloc by performer */
    size_t *partial_total;/* An array with the bytes to transfer per stream. malloc by performer */
    char **partial_rconn; /* An array with the remote connection info per stream. malloc by performer */

    pthread_t thread; /* The associated thread, if any */

    CURLSH *curl_share; /* CURL share structure so ssl sessions are reused */
    struct curl_slist *headers; /* HTTP Headers */
};

#define GETSTR(h,o) (h->options[o].value.s)
#define GETINT(h,o) (h->options[o].value.i)
#define GETPTR(h,o) (h->options[o].value.p)
#define GETIO(h)    ((struct htext_io_handler*)h->options[HTEXTOP_IO_HANDLER].value.p)

/* Common data structure used by methods
 * Sub-threads must _not_ allocate or free any of these, except
 * those handle by common functions (as location) or CURL (as headers)
 */
struct htext_chunk
{
    htext_handle *handle; /* "Parent" handle */
    pthread_t thread; /* The associated thread, if any */
    int index; /* Position of this chunk */
    int nchunks; /* Total number of chunks */

    CURL *curl; /* The CURL handle to use */
    unsigned http_status; /* The HTTP status for this specific partial */

    short redirect_is_cacheable; /* Can cache the redirect location? */

    void *fd; /* Where to read/write */
    size_t start, end; /* First and last byte of the stream */
    size_t total;

    size_t *chunk_done; /* A pointer to an _external_ where to store the progress done */
    size_t *chunk_total;/* A pointer to an _external_ where to store the total bytes to transfer */
    char **chunk_rconn; /* A pointer to an _external_ where to store the connection info */

    char *location; /* The actual location where performing */

    struct curl_slist *headers; /* Used by subrequests for the HTTP headers */
    sem_t final; /* Used internally by performers to lock until the final location is known */

    void *extra; /* Extra data used internally */
};
typedef struct htext_chunk htext_chunk;

/* Data structure for Perf Marker
 * and unprocessed last line from input data
 */
#define HTEXT_PERF_MARKER_MAX_LINE_SIZE 1024
struct htext_perf_marker_parsed_data
{
    time_t latest; /* Timestamp for received data */
    int index; /* Stripe index */
    off_t transferred; /* Stripe bytes transferred */
    int count; /* Total stripe count */
    char rconn[HTEXT_PERF_MARKER_MAX_LINE_SIZE]; /* Remote connections */

    char remaining[HTEXT_PERF_MARKER_MAX_LINE_SIZE]; /* Remaining input data from unterminated last line */
};
typedef struct htext_perf_marker_parsed_data htext_perf_marker_parsed_data;

/**
 * Initializes a previously allocated partial
 * Only the main thread (htext_*_method function) should call this
 */
void htext_partial_init(htext_chunk *p);

/**
 * Cleans a partial, but does NOT free it
 * Only the main thread (htext_*_method function) should call this
 */
void htext_partial_clean(htext_chunk *p);

/**
 * Performs the GET
 * @param h The handle
 * @return NULL when done
 */
void *htext_get_method(void *h);

/**
 * Performs the PUT
 * @param h The handle
 * @return NULL when done
 */
void *htext_put_method(void *h);

/**
 * Performs the COPY (third party copies)
 * @param h The handle
 * @return NULL when done
 */
void *htext_copy_method(void *h);

/**
 * Removes all whitespaces at the beginning and end
 * @param The string to trim
 * @return str
 */
char *trim(char *str);

/**
 * Puts in the buffer the host extracted from url
 * @param url    The url to parse
 * @param buffer Where to put the host
 * @return buffer
 */
char *get_host(const char *url, char *buffer);

/**
 * Sends a log entry to the callback
 * @param handle The handle
 * @param fmt    A format string
 */
int htext_log(htext_handle *handle, const char *fmt, ...);

/**
 * Sets the error string
 * @param handle The handle
 * @param fmt    A format string
 */
int htext_error(htext_handle *handle, const char *fmt, ...);

/**
 * Called by Curl every time a line of the header needs to be processed
 * @param buffer   Where the data is
 * @param size     The size of the element
 * @param nmemb    The number of elements
 * @param pp       A pointer to a htext_partial structure
 * @return         The number of read bytes
 */
size_t htext_header_callback(void *buffer, size_t size, size_t nmemb, void *pp);

/**
 * Called to keep track of the progress
 * @param pp       A pointer to a htext_partial structure
 * @param dltotal  To be downloaded
 * @param dlnow    Already downloaded
 * @param ultotal  To be uploaded
 * @param ulnow    Already uploaded
 * @return         0 on success
 */
int htext_progress_callback(void *pp, curl_off_t dltotal, curl_off_t dlnow,
        curl_off_t ultotal, curl_off_t ulnow);

/**
 * Called to keep track of the progress (libcurl older than 7.32.0)
 * @param pp       A pointer to a htext_partial structure
 * @param dltotal  To be downloaded
 * @param dlnow    Already downloaded
 * @param ultotal  To be uploaded
 * @param ulnow    Already uploaded
 * @return         0 on success
 */
int htext_progress_old_callback(void *pp, double dltotal, double dlnow,
        double ultotal, double ulnow);

/**
 * Method called by Curl to write received data from the server
 * @param buffer The retrieved data
 * @param size   The size of an element
 * @param nmemb  The number of elements
 * @param pp     A pointer to a htext_partial structure
 * @return       The number of written bytes
 */
size_t htext_write_callback(char *buffer, size_t size, size_t nmemb, void *pp);

/**
 * Method called by Curl to write received data from the server, dummy implementation
 * @param buffer The retrieved data
 * @param size   The size of an element
 * @param nmemb  The number of elements
 * @param pp     A pointer to a htext_partial structure
 * @return       The number of written bytes
 */
size_t htext_dummy_write_callback(char *buffer, size_t size, size_t nmemb,
        void *pp);

/**
 * Method called by Curl on debug output
 * @param curl The CURL handle calling
 * @param type The type of data passed
 * @param data The debug information
 * @param size The size of the buffer passed in data
 * @param pp   A pointer to a htext_partial structure
 * @return     0 on success
 */
int htext_debug_callback(CURL *curl, curl_infotype type, char *data,
        size_t size, void *pp);

/**
 * Initializes locking for OpenSSL/GnuTLS
 * @return 0 on success.
 */
int htext_locking_setup(void);

/**
 * Do the delegation
 * @param partial    A partial handle
 * @param url        The delegation endpoint
 * @param err_buffer Used to build the error string
 * @return 0 on success.
 */
int htext_delegate(htext_chunk *partial, char *url);

/**
 * Creates a clone of list
 * @param list The list to be copied
 * @return A copy of list
 */
struct curl_slist *htext_copy_slist(struct curl_slist *list);

#ifdef _GNU_SOURCE
#define htext_strerror_r(errnum, buf, buflen) \
  if (buflen > 0) { \
    int old_errno = errno, cur_errno = errnum; \
    char buffer[128], *msg = NULL; \
    errno = 0; \
    buf[0] = '\0'; \
    msg = strerror_r(cur_errno, buffer, sizeof(buffer)); \
    if (msg) \
      strncpy(buf, msg, buflen); \
    else \
      snprintf(buf, buflen, "Unknown error %d", errnum); \
    buf[buflen-1] = '\0'; \
    errno = old_errno; \
  }
#else
#define htext_strerror_r(errnum, buf, buflen) \
  if (buflen > 0) { \
    int old_errno = errno, cur_errno = errnum, rc; \
    errno = 0; \
    rc = strerror_r(cur_errno, buf, buflen); \
    switch (rc) { \
      case 0: \
      case ERANGE: \
        break; \
      case EINVAL: \
        snprintf(buf, buflen, "Unknown error %d", errnum); \
        buf[buflen-1] = '\0'; \
        break; \
    } \
    errno = old_errno; \
  }
#endif

#endif
