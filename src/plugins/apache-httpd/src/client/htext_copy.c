/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "htext.h"
#include "htext_private.h"

/**
 * Used to parse the output from the server to know the progress
 */
static size_t htext_copy_write_callback(char *buffer, size_t size, size_t nmemb,
        void *pp)
{
    size_t received = size * nmemb;
    htext_chunk *partial = (htext_chunk*) pp;
    htext_handle *handle = (htext_handle*) partial->handle;
    htext_perf_marker_parsed_data *perf_data = (htext_perf_marker_parsed_data*) partial->extra;
    char line[HTEXT_PERF_MARKER_MAX_LINE_SIZE]; // ignore line characters beyond this size
    char *p1 = buffer, *p2 = NULL;

    htext_write_callback(buffer, size, nmemb, pp);

    while (p1 < buffer + received) {
        p2 = p1;
        if (perf_data->remaining[0] != '\0') {
            // find end of line
            while (p2 < buffer + received && *p2 != '\0' && *p2 != '\n')
                p2++;

            size_t remaining_size = strlen(perf_data->remaining);
            size_t append_size = p2-p1 < HTEXT_PERF_MARKER_MAX_LINE_SIZE-remaining_size-1 ? p2-p1 : HTEXT_PERF_MARKER_MAX_LINE_SIZE-remaining_size-1;
            memcpy(line, perf_data->remaining, remaining_size);
            memcpy(line + remaining_size, p1, append_size);
            line[remaining_size + append_size] = '\0';
            perf_data->remaining[0] = '\0';
        } else {
            // skip whitespaces and find new end of line
            while (p2 < buffer + received && isspace(*p2))
                p2++;
            p1 = p2;
            while (p2 < buffer + received && *p2 != '\0' && *p2 != '\n')
                p2++;

            size_t line_size = p2-p1 < HTEXT_PERF_MARKER_MAX_LINE_SIZE-1 ? p2-p1 : HTEXT_PERF_MARKER_MAX_LINE_SIZE-1;
            memcpy(line, p1, line_size);
            line[line_size] = '\0';
        }

        // end of buffer with '\0' or '\n'
        if (line[0] == '\0') break;

        p1 = p2;
        if (p2 == buffer + received) {
            // save remaining data not terminated by new line
            strcpy(perf_data->remaining, line);
            break;
        }

        // process line
        char *p = line;
        if (strncasecmp("Perf Marker", p, 11) == 0)
        {
            memset(perf_data, 0, sizeof(*perf_data));
            perf_data->index = -1;
            perf_data->transferred = -1;
        }
        else if (strncasecmp("Timestamp:", p, 10) == 0)
        {
            perf_data->latest = atol(p + 10);
        }
        else if (strncasecmp("Stripe Index:", p, 13) == 0)
        {
            perf_data->index = atoi(p + 13);
        }
        else if (strncasecmp("Stripe Bytes Transferred:", p, 25) == 0)
        {
            perf_data->transferred = atol(p + 26);
        }
        else if (strncasecmp("Total Stripe Count:", p, 19) == 0)
        {
            perf_data->count = atoi(p + 20);
        }
        else if (strncasecmp("RemoteConnections:", p, 18) == 0)
        {
            strcpy(perf_data->rconn, p + 19);
        }
        // skip unused dCache perf markers
        else if (strncasecmp("State:", p, 6) == 0) {}
        else if (strncasecmp("State description:", p, 18) == 0) {}
        else if (strncasecmp("Stripe Start Time:", p, 18) == 0) {}
        else if (strncasecmp("Stripe Transfer Time:", p, 21) == 0) {}
        else if (strncasecmp("Stripe Bytes Transferred:", p, 25) == 0) {}
        else if (strncasecmp("Stripe Status:", p, 14) == 0) {}
        // end of performance marker
        else if (strncasecmp("End", p, 3) == 0)
        {
            const int max_perf_marker_count = 64;

            if (perf_data->index < 0) {
                htext_log(handle, "Performance marker Stripe Index missing or invalid (%i)", perf_data->index);
            } else if (perf_data->index > perf_data->count) {
                htext_log(handle, "Performance marker Stripe Index out Stripe Count (%i >= %i)", perf_data->index, perf_data->count);
            } else if (perf_data->index > max_perf_marker_count) {
                htext_log(handle, "Performance marker Stripe Index too high (%i)", perf_data->index);
            } else if (perf_data->transferred < 0) {
                htext_log(handle, "Performance marker negative size of transferred data (%i)", perf_data->transferred);
            } else {

                // validated incomming data from perfmarker
                size_t count = max_perf_marker_count < perf_data->count ? max_perf_marker_count : perf_data->count;
                if (handle->partials < count) {
                    handle->partial_done = realloc(handle->partial_done, sizeof(size_t) * count);
                    handle->partial_total = realloc(handle->partial_total, sizeof(size_t) * count);
                    handle->partial_rconn = realloc(handle->partial_rconn, sizeof(char*) * count);
                    handle->partials = count;
                }

                handle->partial_done[perf_data->index] = perf_data->transferred;
                handle->partial_total[perf_data->index] = 0;
                handle->partial_rconn[perf_data->index] = perf_data->rconn[0] != '\0' ? strdup(perf_data->rconn) : NULL;

            }
            
        }
        else if (strncasecmp("success", p, 7) == 0)
        {
            /* Leave things going */
        }
        else if (strncasecmp("aborted", p, 7) == 0)
        {
            handle->status = HTEXTS_ABORTED;
        }
        else if (strncasecmp("failed", p, 6) == 0 || strncasecmp("failure", p, 7) == 0)
        {
            htext_error(handle, line);
        }
        else
        {
            htext_log(handle, "Unknown performance marker, ignoring: %s", line);
        }
    }

    return received;
}

void *htext_copy_method(void *h)
{
    htext_handle *handle = (htext_handle*) h;
    CURL *curl;
    htext_chunk control;
    htext_perf_marker_parsed_data perf_data;
    char buffer[CURL_MAX_WRITE_SIZE], err_buffer[CURL_ERROR_SIZE];
    int nstreams;

    /* Initialize control and buffer */
    htext_partial_init(&control);
    memset(&perf_data, 0, sizeof(perf_data));
    perf_data.index = -1;
    perf_data.transferred = -1;
    control.handle = handle;
    control.extra = &perf_data;

    handle->partial_done = NULL;
    handle->partial_total = NULL;
    handle->partial_rconn = NULL;
    handle->partials = 0;

    const char* source = GETSTR(handle, HTEXTOP_SOURCEURL);
    const char* destination = GETSTR(handle, HTEXTOP_DESTINATIONURL);

    /* Create and initialize CURL handle */
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "COPY");
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, htext_header_callback);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, htext_copy_write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &control);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &control);
    curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 64);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, GETSTR(handle, HTEXTOP_CLIENTID));
    curl_easy_setopt(curl, CURLOPT_CAPATH, GETSTR(handle, HTEXTOP_CAPATH));
    curl_easy_setopt(curl, CURLOPT_CAINFO, GETSTR(handle, HTEXTOP_CAFILE));
    curl_easy_setopt(curl, CURLOPT_CRLFILE, GETSTR(handle, HTEXTOP_CRLFILE));
    curl_easy_setopt(curl, CURLOPT_SSLCERT,
            GETSTR(handle, HTEXTOP_USERCERTIFICATE));
    curl_easy_setopt(curl, CURLOPT_SSLKEY, GETSTR(handle, HTEXTOP_USERPRIVKEY));
    curl_easy_setopt(curl, CURLOPT_SSLKEYPASSWD,
            GETSTR(handle, HTEXTOP_USERPRIVKEYPASS));
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER,
            GETINT(handle, HTEXTOP_VERIFYPEER));
    curl_easy_setopt(curl, CURLOPT_SHARE, handle->curl_share);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, handle->headers);

    // COPY destination Source: source
    if (GETINT(handle, HTEXTOP_USE_COPY_FROM_SOURCE)) {
        curl_easy_setopt(curl, CURLOPT_URL, destination);
        snprintf(buffer, sizeof(buffer), "Source: %s", source);
        control.headers = curl_slist_append(control.headers, buffer);
    }
    // COPY source Destination: destination
    else {
        curl_easy_setopt(curl, CURLOPT_URL, source);
        snprintf(buffer, sizeof(buffer), "Destination: %s", destination);
        control.headers = curl_slist_append(control.headers, buffer);
    }

    /* Number of streams */
    nstreams = GETINT(handle, HTEXTOP_NUMBEROFSTREAMS);
    if (nstreams > 1) {
        snprintf(buffer, sizeof(buffer), "X-Number-Of-Streams: %d", nstreams);
        control.headers = curl_slist_append(control.headers, buffer);
    }

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, control.headers);

    if (GETINT(handle, HTEXTOP_VERBOSITY)> 1) {
        curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, htext_debug_callback);
        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &control);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    }

    htext_log(handle, "Running the remote copy");
    handle->status = HTEXTS_RUNNING;

    if (curl_easy_perform(curl) != CURLE_OK) {
        htext_error(handle, err_buffer);
    }
    else if (control.http_status >= 400 || handle->status == HTEXTS_FAILED) {
        htext_error(handle, NULL );
    }
    else {
        handle->status = HTEXTS_SUCCEEDED;
    }

    handle->http_status = control.http_status;

    /* Clean up */
    htext_partial_clean(&control);
    curl_easy_cleanup(curl);
    return NULL ;
}
