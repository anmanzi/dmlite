cmake_minimum_required (VERSION 2.6)


configure_file (${CMAKE_CURRENT_SOURCE_DIR}/shared/config.h.cmake
                ${CMAKE_CURRENT_SOURCE_DIR}/shared/config.h)


if (BUILD_HTCOPY)
  add_subdirectory (client)
endif (BUILD_HTCOPY)

if (BUILD_DAV)
  # This builds a patched mod_dav (based on 2.2 Apache's one) that
  # honor the redirect code returned by the underlying dav provider.
  # This has been reported in
  # https://issues.apache.org/bugzilla/show_bug.cgi?id=35981
  # It has been fixed for trunk and 2.4, so once it propagates
  # to vanilla mod_dav, this should be dropped.
  include_directories (${CMAKE_CURRENT_SOURCE_DIR}/mod_dav/)

  add_subdirectory (mod_dav)
  add_subdirectory (mod_lcgdm_disk)
  add_subdirectory (mod_lcgdm_ns)
endif (BUILD_DAV)

if (BUILD_POST)
    add_subdirectory (mod_lcgdm_post)
endif (BUILD_POST)
