/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UTILS_H_
#define	UTILS_H_

#include <dmlite/c/dmlite.h>
#include <dmlite/c/utils.h>
#include <mod_lcgdm_dav.h>

#define DAV_DPM_RFC2068 0
#define DAV_DPM_RFC3339 1

#ifdef __GNUC__
#define WARN_UNUSED_RESULT __attribute__ ((__warn_unused_result__))
#else
#define WARN_UNUSED_RESULT
#endif

struct redirect_cfg
{
    const char *scheme;
    unsigned port_unsecure;
    unsigned port_secure;
};

/**
 * Create a dav error with proper error message and HTTP status.
 * @param r         The request thar originated the error.
 * @param ctx       If not NULL, the error string/code will be retrieved from it.
 * @param http_code If specified, this will be used instead of mapping err_no.
 * @param format    Additional string to print (format).
 * @return          A initialized dav_error instance. Must not be freed.
 */
__attribute__((warn_unused_result))
dav_error* WARN_UNUSED_RESULT dav_shared_new_error(request_rec *r, dmlite_context *ctx,
        int http_code, const char *format, ...);

/**
 * Return a string with the date-time formated.
 * @param buffer Where to put the formated date.
 * @param maxlen The size of the buffer.
 * @param tstamp The timestamp.
 * @param format Format flag (DAV_DPM_RFC2068 or DAV_DPM_RFC3339).
 * @return       The length of the printed string.
 */
int dav_shared_format_datetime(char *buffer, size_t maxlen, time_t tstamp,
        unsigned format);

/**
 * Add a response to the walk resource.
 * @param wres  The walk resource.
 * @param error The error to add.
 */
void dav_shared_add_response(dav_walk_resource *wres, dav_error *error);

/**
 * Parses a query (everything after a ?) and initializes a table with it.
 * @param pool  The pool to use.
 * @param query The query to parse.
 * @param size  The number of elements found.
 * @return      A hash table with key=value.
 */
apr_table_t *dav_shared_parse_query(apr_pool_t *pool, const char *query,
        unsigned *size);

/**
 * Generates a printable version of the filemode.
 * @param str  Where to put the string. Must have 9+1 bytes.
 * @param mode The mode.
 * @return     str.
 */
char *dav_shared_mode_str(char *str, mode_t mode);

/**
 * Generates a printable version of the size.
 * @param str  Where to put the string.
 * @param max  Max length.
 * @param size The size in bytes.
 * @return     str.
 */
char *dav_shared_size_str(char *str, size_t max, off_t size);

/**
 * Builds an URL from a url struct
 * @param pool           The pool for the memory
 * @param uri            The struct
 * @param redirect       The default redirect configuration
 * @param force_secure   If != 0, the redirection will be secure (https)
 *                       regardless of the configuration
 * @return               A http URL.
 */
char *dav_shared_build_url(apr_pool_t *pool, dmlite_url *url,
        const struct redirect_cfg* redirect, char force_secure);

/*
 * Return 1 if the requests specify that it accepts "content" mimetypes,
 * 0 otherwise.
 * (i.e. dav_shared_request_acceps(r, "application/metalink+xml")
 */
int dav_shared_request_accepts(request_rec* r, const char* content);

/* Utility functions to wrap dmlite free functions */
apr_status_t dav_shared_context_free(void*);
apr_status_t dav_shared_dict_free(void*);
apr_status_t dav_shared_fclose(void*);

/* Notify that the module+resource support being copied */
void dav_lcgdm_notify_support_external_copy(request_rec *req);

/**
 * Parse a cookie string and return a table
 */
apr_table_t* dav_lcgdm_parse_cookies(apr_pool_t* pool, const char* cookie_str);

/**
 * Parses *want_digest in order to find the next digest algorithm
 * We don't care much about priorities, so give it as we find them
 * @param want_digest Points initially to the beginning of the list, updated after each run
 * @param outout      The algorithm type is copied here
 * @param outsize     Size of the buffer pointed by output
 * @return 0 when there are no more digest available
 */
int dav_shared_next_digest(const char **want_digest, char *output, size_t outsize);

/**
* Decode a hex digest array to base64 if needed. Returns the
* length of the resulting string, 0 if error
**/
int dav_shared_hexdigesttobase64(const char *digest_name, const char *input, char *output);

#endif	/* UTILS_H_ */
