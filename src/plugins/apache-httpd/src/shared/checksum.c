/*
 * Copyright (c) CERN 2013-2017
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/checksums.h>
#include <httpd.h>
#include <regex.h>
#include "shared/utils.h"
#include <http_log.h>
#include <strings.h>

#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#define RFC3230_REGEXP "^([[:alnum:]]+)(;q=[[:digit:]]*)?([,]?)"

static char* dav_shared_strcpy_lower(char *out, const char *in, size_t max)
{
    size_t i;
    for (i = 0; in[i] && i < max; ++i)
        out[i] = tolower(in[i]);
    return out;
}


int dav_shared_next_digest(const char **want_digest, char *output, size_t outsize)
{
    regex_t regex;
    regmatch_t matches[3];

    if (regcomp(&regex, RFC3230_REGEXP, REG_EXTENDED) != 0)
        abort();

    while (isspace(**want_digest))
        ++(*want_digest);

    if (regexec(&regex, *want_digest, 3, matches, 0) == 0) {
        size_t digest_len = matches[1].rm_eo - matches[1].rm_so;
        const char *digest = (*want_digest) + matches[1].rm_so;

        if (outsize < digest_len) {
            digest_len = outsize;
        }

        dav_shared_strcpy_lower(output, digest, digest_len);
        output[digest_len] = '\0';

        *want_digest += matches[0].rm_eo;

        return 1;
    }

    return 0;
}


static int
char_to_int(char c)
{
  if (isdigit(c)) {
    return c - '0';
  } else {
    c = tolower(c);
    if (c >= 'a' && c <= 'f') {
      return c - 'a' + 10;
    }
    return -1;
  }
}

// Decode a hex digest array to base64 if needed. Returns the
// length of the resulting string, 0 if error
//
int dav_shared_hexdigesttobase64(const char *digest_name,
                                 const char *input, char *output) {
  int digest_length = strlen(input);
    
  // Check if we have to convert it or not
  if (!strcasecmp(digest_name, "adler32")) {
    strcpy(output, input);
    return digest_length;
  }
  
  if (strcasecmp(digest_name, "md5")) {
    // Let's check now the less probable algorithms
    if ( strcasecmp(digest_name, "SHA") &&
      strcasecmp(digest_name, "SHA") &&
      strcasecmp(digest_name, "SHA-256") &&
      strcasecmp(digest_name, "SHA-512") ) {
      
      if (strcasecmp(digest_name, "UNIXcksum")) {
        strcpy(output, input);
        return digest_length;
      }
    }
  }
  
  // We are here if the digest has to be converted, e.g. md5, sha
  char tmpout[1024];
  
  // Decode a hex digest array to raw bytes.
  int idx;
  for (idx=0; idx < digest_length; idx += 2) {
    int upper =  char_to_int(input[idx]);
    int lower =  char_to_int(input[idx+1]);
    if ((upper < 0) || (lower < 0) ||
      (upper > 15) || (lower > 15)) {
      return 0; // Error in the input data, not hex
    }
    tmpout[idx/2] = (upper << 4) + lower;
  }
  
  // Now the raw bytes into base64
  BIO *bmem, *b64;
  BUF_MEM *bptr;
       
  output[0] = '\0';
  
  b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  bmem = BIO_new(BIO_s_mem());
  BIO_push(b64, bmem);
  BIO_write(b64, tmpout, digest_length/2);
  
  if (BIO_flush(b64) <= 0) {
    BIO_free_all(b64);
    return 0;
  }
  
  BIO_get_mem_ptr(b64, &bptr);
  
  
  memcpy(output, bptr->data, bptr->length);
  output[bptr->length] = '\0';
  
  int ret = bptr->length;
  
  BIO_free_all(b64);
  
  return ret;
}

