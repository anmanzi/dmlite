/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <dmlite/c/dmlite.h>
#include <httpd.h>
#include <http_log.h>
#include "security.h"
#include "utils.h"
#include "use_module.h"

static char *unescape_dn(char *dn)
{
    /* ap_unescape_url does not replace + */
    char *p;
    for (p = dn; *p != '\0'; ++p) {
        if (*p == '+')
            *p = ' ';
    }

    ap_unescape_url(dn);
    return dn;
}

static int dav_shared_get_user_from_notes(apr_pool_t *pool, apr_table_t* notes,
        dmlite_credentials* creds)
{
    int i;
    char grst_key_name[20];
    const char *p, *s;

    i = 0;
    snprintf(grst_key_name, sizeof(grst_key_name), "GRST_CRED_AURI_%d", i);
    while ((s = apr_table_get(notes, grst_key_name))) {
        if (strncmp(s, "dn:", 3) == 0 && !creds->client_name
                && (p = index(s, '/'))) {
            creds->client_name = unescape_dn(apr_pstrdup(pool, p));
        }
        if (strncmp(s, "fqan:", 5) == 0 && creds->nfqans < DAV_SHARED_MAX_FQANS
                && (p = index(s, '/'))) {
            creds->fqans[creds->nfqans++] = unescape_dn(apr_pstrdup(pool, p));
        }

        ++i;
        snprintf(grst_key_name, sizeof(grst_key_name), "GRST_CRED_AURI_%d", i);
    }

    return creds->client_name != NULL ;
}

static int dav_shared_is_trusted(apr_array_header_t* trusted,
        dmlite_credentials* creds)
{
    int i, is_trusted = 0;
    if (trusted != NULL ) {
        for (i = 0, is_trusted = 0; i < trusted->nelts && !is_trusted; ++i) {
            const char *dn = ((const char**) trusted->elts)[i];
            is_trusted = (strcmp(dn, creds->client_name) == 0);
        }
    }
    return is_trusted;
}

static int dav_shared_get_user_from_trusted(apr_pool_t *pool,
        apr_table_t* headers, dmlite_credentials* creds)
{
    const char *dn_header = apr_table_get(headers, "X-Auth-Dn");
    if (dn_header != NULL ) {
        int i;
        const char *fqan;
        char x_auth_name[15];

        creds->client_name = apr_pstrdup(pool, dn_header);
        creds->nfqans = 0;

        i = 0;
        snprintf(x_auth_name, sizeof(x_auth_name), "X-Auth-Fqan%d", i);
        while ((fqan = apr_table_get(headers, x_auth_name))) {
            creds->fqans[creds->nfqans++] = apr_pstrdup(pool, fqan);

            ++i;
            snprintf(x_auth_name, sizeof(x_auth_name), "X-Auth-Fqan%d", i);
        }
    }

    const char *ip_header = apr_table_get(headers, "X-Auth-Ip");
    if (ip_header != NULL )
        creds->remote_address = ip_header;

    return 0;
}

typedef struct pass_ctx_t {
    dmlite_credentials *creds;
    apr_pool_t *pool;
} pass_ctx_t;

int dav_shared_pass_query(void *rec, const char *key, const char *value)
{
    pass_ctx_t *ctx = (pass_ctx_t*)rec;
    dmlite_any *any_value = dmlite_any_new_string(value);
    dmlite_any_dict_insert(ctx->creds->extra, key, any_value);
    dmlite_any_free(any_value);
    return 1;
}

void dav_shared_pass_query_args(apr_pool_t *pool, dmlite_credentials *creds, request_rec *r)
{
    unsigned nargs;
    apr_table_t *query = dav_shared_parse_query(pool, r->parsed_uri.query, &nargs);

    pass_ctx_t ctx = {creds, pool};
    apr_table_do(dav_shared_pass_query, &ctx, query, NULL);
}

int dav_shared_pass_header(void *rec, const char *key, const char *value)
{
    pass_ctx_t *ctx = (pass_ctx_t*)rec;

    const char *header_key = apr_pstrcat(ctx->pool, "http.", key, NULL);

    dmlite_any *any_value = dmlite_any_new_string(value);
    dmlite_any_dict_insert(ctx->creds->extra, header_key, any_value);
    dmlite_any_free(any_value);

    return 1;
}

void dav_shared_pass_headers(apr_pool_t *pool, dmlite_credentials *creds, request_rec *r)
{
    pass_ctx_t ctx = {creds, pool};
    apr_table_do(dav_shared_pass_header, &ctx, r->headers_in, NULL);
}

dmlite_credentials *dav_shared_get_user_credentials(apr_pool_t *pool,
        request_rec *r, const char *anon_usr, const char *anon_grp,
        apr_array_header_t* trusted)
{
    dmlite_credentials *creds;

    /* Create user information */
    creds = apr_pcalloc(pool, sizeof(dmlite_credentials));
    creds->fqans = apr_pcalloc(pool, sizeof(char*) * DAV_SHARED_MAX_FQANS);
    creds->extra = dmlite_any_dict_new();

    /* Client IP */
#if AP_SERVER_MAJORVERSION_NUMBER == 2 &&\
    AP_SERVER_MINORVERSION_NUMBER < 4
    creds->remote_address = r->connection->remote_ip;
#else
    creds->remote_address = r->useragent_ip;
#endif

    /* Pass query and headers */
    //dav_shared_pass_query_args(pool, creds, r);
    //dav_shared_pass_headers(pool, creds, r);
    

    /* Get user information from the connection or request notes */
    if (!dav_shared_get_user_from_notes(pool, r->connection->notes, creds)
            && !dav_shared_get_user_from_notes(pool, r->notes, creds)) {
        /* No luck, so try with mod_ssl directly (won't work with proxies) */
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "mod_gridsite didn't give us anything. Trying with mod_ssl!");
        creds->client_name = apr_pstrdup(pool,
                apr_table_get(r->subprocess_env, "SSL_CLIENT_S_DN"));

        if (creds->client_name)
            creds->mech = "X509";
    }

    /* If no credentials are available, then try with traditional Auth mechanisms */
    if (!creds->client_name && r->user && r->user[0]) {
        creds->client_name = apr_pstrdup(pool, r->user);
        creds->mech = apr_pstrdup(pool, r->ap_auth_type);
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "Using Apache's authentication %s:%s", creds->mech, creds->client_name);
    }

    /* Try to fallback to the anon, but keeping DN */
    if (!creds->client_name) {
        if (anon_usr) {
            creds->client_name = apr_pstrdup(pool, anon_usr);
            creds->nfqans = 1;
            creds->fqans = apr_pcalloc(pool, sizeof(const char*));
            creds->fqans[0] = apr_pstrdup(pool, anon_grp);
            creds->mech = "NONE";

            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "No certificate presented. Falling back to %s:%s", anon_usr,
                    anon_grp);
        }
        else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "No client name info available.");
            return NULL ;
        }
    }
    if (!creds->nfqans) {
      if (anon_grp) {
        
        creds->nfqans = 1;
        creds->fqans = apr_pcalloc(pool, sizeof(const char*));
        creds->fqans[0] = apr_pstrdup(pool, anon_grp);
        creds->mech = "NONE";
        
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                      "Falling back to GROUP %s",
                      anon_grp);
      }
      else {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                      "No group info available.");
        return NULL ;
      }
    }

    /* Maybe the DN could be a trusted one */
    if (dav_shared_is_trusted(trusted, creds)) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Found trusted DN (%s)",
                creds->client_name);

        dav_shared_get_user_from_trusted(pool, r->headers_in, creds);

        ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r,
                "Trusted DN acting on behalf of %s (IP %s)", creds->client_name,
                creds->remote_address);
    }

    /* Done here */
    unsigned i;
    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Using DN: %s", creds->client_name);
    for (i = 0; i < creds->nfqans; ++i)
        ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Using FQAN: %s", creds->fqans[i]);
    return creds;
}

char is_ssl_used(request_rec *r)
{
    return apr_table_get(r->subprocess_env, "HTTPS") != NULL ;
}
