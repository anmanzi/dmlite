/**
 * @file   acl.c
 * @brief  ACL functions (see RFC 3744).
 * @author Alejandro Álvarez Ayllón <aalvarez.cern.ch>
 */
#include <apr_strings.h>
#include <dmlite/c/utils.h>
#include <stdio.h>
#include "acl.h"
#include "../shared/security.h"

const char* dav_ns_supported_privilege_set(void)
{
    return "\
<D:supported-privilege>\n\
  <D:supported-privilege>\n\
    <D:privilege><D:all/></D:privilege>\n\
    <D:abstract/>\n\
    <D:supported-privilege>\n\
      <D:privilege><D:read/></D:privilege>\n\
    </D:supported-privilege>\n\
    <D:supported-privilege>\n\
      <D:privilege><D:write/></D:privilege>\n\
    </D:supported-privilege>\n\
    <D:supported-privilege>\n\
      <D:privilege><D:executable/></D:privilege>\n\
    </D:supported-privilege>\n\
  </D:supported-privilege>\n\
</D:supported-privilege>";
}

char* dav_ns_acl_format(request_rec *r, const char *acl_str)
{
    unsigned nacls, i;
    char *xml = "<D:acl xmlns:lcgdm=\"LCGDM:\">\n";
    char buffer[512];
    apr_pool_t *pool = r->pool;
    struct dmlite_aclentry *acl;

    dmlite_deserialize_acl(acl_str, &nacls, &acl);

    for (i = 0; i < nacls; ++i) {
        xml = apr_pstrcat(pool, xml, "<D:ace>\n", NULL );

        switch (acl[i].type) {
            case ACL_USER_OBJ:
                strcpy(buffer, "<D:property><D:owner/></D:property>");
                break;
            case ACL_USER:
                snprintf(buffer, sizeof(buffer), "<lcgdm:user>%u</lcgdm:user>",
                        acl[i].id);
                break;
            case ACL_GROUP_OBJ:
                strcpy(buffer, "<D:property><D:group/></D:property>");
                break;
            case ACL_GROUP:
                snprintf(buffer, sizeof(buffer),
                        "<lcgdm:group>%u</lcgdm:group>", acl[i].id);
                break;
            case ACL_OTHER:
                strcpy(buffer, "<D:all/>");
                break;
            case ACL_MASK:
                strcpy(buffer, "<lcgdm:mask/>");
                break;
            default:
                break;
        }

        xml = apr_pstrcat(pool, xml, "\t<D:principal>", buffer,
                "</D:principal>\n\t<D:grant>\n", NULL );

        if (acl[i].perm == (S_IROTH | S_IWOTH | S_IXOTH))
            xml = apr_pstrcat(pool, xml,
                    "\t\t<D:privilege><D:all/></D:privilege>\n", NULL );
        else {
            if (acl[i].perm & S_IROTH)
                xml = apr_pstrcat(pool, xml,
                        "\t\t<D:privilege><D:read/></D:privilege>\n", NULL );
            if (acl[i].perm & S_IWOTH)
                xml = apr_pstrcat(pool, xml,
                        "\t\t<D:privilege><D:write/></D:privilege>\n", NULL );
            if (acl[i].perm & S_IXOTH)
                xml = apr_pstrcat(pool, xml,
                        "\t\t<D:privilege><D:executable/></D:privilege>\n",
                        NULL );
        }

        xml = apr_pstrcat(pool, xml, "\t</D:grant>\n</D:ace>\n", NULL );
    }

    xml = apr_pstrcat(pool, xml, "</D:acl>\n", NULL );

    dmlite_acl_free(nacls, acl);

    return xml;
}
