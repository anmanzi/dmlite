/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MIME_H_
#define	MIME_H_

/**
 * Initializes the internal structures with the mapping.
 * @param pool     The pool.
 * @param map_path The path to the mime file.
 */
void dav_ns_mime_init(apr_pool_t *pool, const char *map_path);

/**
 * Returns the mime type associated with the filename's extension.
 * If no extension is found, or not correspondance, NULL will be returned.
 * @param buffer   Where to put the value.
 * @param maxlen   The size of the buffer.
 * @param filename The file name.
 * @return         buffer if found, NULL if not.
 */
const char *dav_ns_mime_get(char *buffer, size_t maxlen, const char *filename);

#endif	/* MIME_H_*/

