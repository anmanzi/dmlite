if(NOT EXISTS "/afs/cern.ch/user/f/furano/Park/dpmdev/dmlite/src/plugins/apache-httpd/external/curl/build_sl6/install_manifest.txt")
  message(FATAL_ERROR "Cannot find install manifest: /afs/cern.ch/user/f/furano/Park/dpmdev/dmlite/src/plugins/apache-httpd/external/curl/build_sl6/install_manifest.txt")
endif()

if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/tmp/curl/bogusinstall")
endif()
message(${CMAKE_INSTALL_PREFIX})

file(READ "/afs/cern.ch/user/f/furano/Park/dpmdev/dmlite/src/plugins/apache-httpd/external/curl/build_sl6/install_manifest.txt" files)
string(REGEX REPLACE "\n" ";" files "${files}")
foreach(file ${files})
  message(STATUS "Uninstalling $ENV{DESTDIR}${file}")
  if(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
    exec_program(
      "/usr/bin/cmake3" ARGS "-E remove \"$ENV{DESTDIR}${file}\""
      OUTPUT_VARIABLE rm_out
      RETURN_VALUE rm_retval
      )
    if(NOT "${rm_retval}" STREQUAL 0)
      message(FATAL_ERROR "Problem when removing $ENV{DESTDIR}${file}")
    endif()
  else()
    message(STATUS "File $ENV{DESTDIR}${file} does not exist.")
  endif()
endforeach()
