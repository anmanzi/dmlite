#ifndef DMLITE_DSI_GRIDFTP_H
#define DMLITE_DSI_GRIDFTP_H

#include "globus_gridftp_server.h"

#include <dmlite/c/pool.h>
#include <dmlite/c/io.h>
#include <dmlite/c/checksums.h>

#define IPC_RETRY 3

#define GlobusGFSErrorOpFinished(_op, _type, _result)                       \
do                                                                          \
{                                                                           \
    globus_gfs_finished_info_t          _finished_info;                     \
                                                                            \
     memset(&_finished_info, '\0', sizeof(globus_gfs_finished_info_t));     \
    _finished_info.type = _type;                                            \
    _finished_info.code = 0;                                                \
    _finished_info.msg =                                                    \
        globus_error_print_friendly(globus_error_peek(_result));            \
    _finished_info.result = _result;                                        \
                                                                            \
    globus_gridftp_server_operation_finished(                               \
        _op,                                                                \
        _result,                                                            \
        &_finished_info);                                                   \
} while(0)

typedef enum {
	DMLITE_FILEMODE_NONE = 0,
	DMLITE_FILEMODE_READING,
	DMLITE_FILEMODE_WRITING
} globus_l_gfs_dmlite_filemode_t;

/*
 * The handle structure (we store here what we need for our plugin).
 */
typedef struct globus_l_gfs_dmlite_handle_s {
	struct dmlite_manager *		manager;
	int				file_mode;
	int				dir_mode;
	int				pollint;
	int				pollmax;
	globus_bool_t			rfn_nocheck;
	globus_bool_t			dome_checksum;
	char				client_host[HOST_NAME_MAX];
	char				pfn[PATH_MAX];
	dmlite_fd *			fd;
	dmlite_location *		location;
	globus_bool_t			is_replica;
	globus_mutex_t			gfs_mutex;
	globus_mutex_t			rep_mutex;
	globus_gfs_session_info_t	session_info;
	globus_gfs_operation_t		gfs_op;
	int				optimal_count;
	globus_size_t			block_size; // Set using a globus call (amount of bytes to handle in each operation)
	// The two below are used for partial reads / writes (offset and length)
	globus_off_t			cur_length; // Keeps the total amount to read/write
	globus_off_t			cur_offset; // Keeps the current offset
	globus_result_t			cur_result;
	int				pending;
	globus_bool_t			done;
	globus_list_t *			nodelist;
	globus_l_gfs_dmlite_filemode_t	mode;
	globus_bool_t			active_delay;
	globus_gfs_data_info_t *	active_data_info;
	globus_gfs_transfer_info_t *	active_transfer_info;
	globus_gfs_operation_t		active_op;
	void *				active_user_arg;
	globus_gfs_storage_transfer_t	active_callback;
} globus_l_gfs_dmlite_handle_t;

typedef globus_l_gfs_dmlite_handle_t dmlite_handle_t;

struct globus_l_gfs_remote_node_info_s;

typedef struct globus_l_gfs_remote_ipc_bounce_s {
	globus_gfs_operation_t			op;
	void *					state;
	dmlite_handle_t *			my_handle;
	int					nodes_obtained;
	int					nodes_pending;
	int					begin_event_pending;
	int					event_pending;
	int *					eof_count;
	struct globus_l_gfs_remote_node_info_s * node_info;
	int					partial_eof_counts;
	int					nodes_requesting;
	int					node_ndx;
	int					node_count;
	int					finished;
	int					final_eof;
	int					cached_result;
	int					sending;
	int					events_enabled;
} globus_l_gfs_remote_ipc_bounce_t;

typedef void (*globus_l_gfs_remote_node_cb) (
	struct globus_l_gfs_remote_node_info_s * node_info,
	globus_result_t				result,
	void *					user_arg
);

typedef struct globus_l_gfs_remote_node_info_s {
	dmlite_handle_t *			my_handle;
	globus_gfs_ipc_handle_t			ipc_handle;
	struct globus_l_gfs_remote_ipc_bounce_s * bounce;
	char *					cs;
	void *					data_arg;
	void *					event_arg;
	int					event_mask;
	int					node_ndx;
	int					stripe_count;
	int					info_needs_free;
	void *					info;
	globus_l_gfs_remote_node_cb		callback;
	void *					user_arg;
	int					error_count;
	globus_result_t				cached_result;
} globus_l_gfs_remote_node_info_t;

typedef struct globus_l_gfs_remote_request_s {
	dmlite_handle_t *			my_handle;
	globus_l_gfs_remote_node_cb		callback;
	void *					user_arg;
	int					nodes_created;
	void *					state;
} globus_l_gfs_remote_request_t;

int globus_l_gfs_dmlite_activate();

int globus_l_gfs_dmlite_deactivate();

#endif /* DMLITE_DSI_GRIDFTP_H */
