#include "dmlite_internal.h"

/**
 * Functions/macros internal to the plugin.
 */
#define LOGMSG_SIZE 1024

static char * ckType[] = { "md5", "adler32", "crc32" };

void dmlite_gfs_log(dmlite_handle_t * dmlite_handle_obj, globus_gfs_log_type_t log_type, char * format, ...) {
	va_list args;
	char buf[LOGMSG_SIZE];

	va_start(args, format);
	vsnprintf(buf, LOGMSG_SIZE, format, args);
	va_end(args);
	if (dmlite_handle_obj && dmlite_handle_obj->session_info.subject && dmlite_handle_obj->client_host) {
		globus_gfs_log_message(log_type, "dmlite :: %s :: %s :: %s\n",
			buf, dmlite_handle_obj->session_info.subject, dmlite_handle_obj->client_host);
	} else if (dmlite_handle_obj && dmlite_handle_obj->session_info.subject) {
		globus_gfs_log_message(log_type, "dmlite :: %s :: %s\n",
			buf, dmlite_handle_obj->session_info.subject);
	} else {
		globus_gfs_log_message(log_type, "dmlite :: %s\n", buf);
	}
}

int get_voms_creds(gssapi_voms_ctx_t *ctx, dmlite_handle_t *dmlite_handle)
{
	X509 *px509_cred = NULL;
	STACK_OF(X509) *px509_chain = NULL;
	int error = 0;
	struct vomsdata *vd = NULL;
	struct voms **volist;
	gss_cred_id_desc *cred_desc;
	gss_cred_id_t cred;
	globus_gsi_cred_handle_t gsi_cred_handle;
	int ret = EACCES;

	cred = dmlite_handle->session_info.del_cred;

	if (cred == GSS_C_NO_CREDENTIAL) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "No credential");
		return EACCES;
	}

	/* cast to gss_cred_id_desc */
	cred_desc = (gss_cred_id_desc *)cred;

	if (globus_module_activate(GLOBUS_GSI_CREDENTIAL_MODULE) != GLOBUS_SUCCESS) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR,
		    "Could not activate GLOBUS_GSI_CREDENTIAL_MODULE");
		return EFAULT;
	}

	/* Getting the X509 certificate */
	gsi_cred_handle = cred_desc->cred_handle;
	if (globus_gsi_cred_get_cert(gsi_cred_handle, &px509_cred) != GLOBUS_SUCCESS) {
		globus_module_deactivate(GLOBUS_GSI_CREDENTIAL_MODULE);
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR,
		    "Could not get certificate from credential");
		goto leave;
	}

	/* Getting the certificate chain */
	if (globus_gsi_cred_get_cert_chain (gsi_cred_handle, &px509_chain) != GLOBUS_SUCCESS) {
		X509_free (px509_cred);
		(void)globus_module_deactivate (GLOBUS_GSI_CREDENTIAL_MODULE);
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "Could not get certificate chain");
		goto leave;
	}

	/* No need for the globus module anymore, the rest are calls to VOMS */
	(void)globus_module_deactivate (GLOBUS_GSI_CREDENTIAL_MODULE);

	if ((vd = VOMS_Init (NULL, NULL)) == NULL) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "VOMS_Init failed");
		ret = EFAULT;
		goto leave;
	}

	if (VOMS_Retrieve (px509_cred, px509_chain, RECURSE_CHAIN, vd, &error) == 0) {
		if (error == VERR_NOEXT) /* Non voms proxies will fall into this error */
		{
			/* Non voms proxies go here: set the number of fqans to zero. Not an error */
			ctx->nbfqan = 0;
			ret = 0;
		}
		else
		{
			char buffer[LOGMSG_SIZE];
			VOMS_ErrorMessage(vd, error, buffer, LOGMSG_SIZE);
			dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, buffer);
			/* TODO: convert VOMS error code to POSIX error */
		}
		VOMS_Destroy (vd);
		goto leave;
	}
	volist = vd->data;

	if (volist !=NULL) {
		int i = 0;
		int nbfqan;

		/* Copying the voname */
		if ((*volist)->voname != NULL) {
			ctx->voname = strdup((*volist)->voname);
		}

		/* Counting the fqans before allocating the array */
		while( volist[0]->fqan[i] != NULL) {
			i++;
		}
		nbfqan = i;

		if (nbfqan > 0) {
			ctx->fqan = malloc(sizeof(char *) * (i+1));
			if (ctx->fqan != NULL) {
				for (i=0; i<nbfqan; i++) {
					ctx->fqan[i] = strdup( volist[0]->fqan[i]);
				}
				ctx->fqan[nbfqan] = NULL;
				ctx->nbfqan = nbfqan;
			}
		} /* if (nbfqan > 0) */
	}
	VOMS_Destroy (vd);
        ret = 0;

leave:
	if (px509_cred) X509_free (px509_cred);
	if (px509_chain) sk_X509_pop_free(px509_chain,X509_free);

	return ret;
}

void dmlite_gfs_hostid2host(char * host_id, char * host) {
	char * p = strrchr(host_id, ':');
	size_t len = p ? p-host_id : strlen(host_id);

	if (len > HOST_NAME_MAX-1) len = HOST_NAME_MAX-1;

	strncpy(host, host_id, len);
	host[len] = '\0'; // strncpy_s
}

int dmlite_gfs_node_cmp(void * datum, void * arg) {
	char * hostid = datum;
	char * node = arg;
        size_t hostlen = strlen(hostid);

        if (strchr(hostid, ':')) hostlen = strchr(hostid, ':') - hostid;

	return (strncmp(hostid, node, hostlen) == 0);
}

const char * dmlite_gfs_fixpath(const char * path, globus_bool_t hostname) {
	char *p, *p2;

	if (!path)
		return NULL;

	while (*path == '/' && *(path + 1) == '/')
		path++;  /* reduce leading multiple slashes to one */

	/* first colon-slash */
	p = strstr(path, ":/");

	/* first slash after any leading slashes */
	p2 = strchr((*path == '/') ? (path + 1) : path, '/');

	/* treat it as a hostname:/path if there are no slashes before the colon-slash */
	if (p2 == p + 1) {
		if (hostname == GLOBUS_FALSE)
			path = p2;
		else if (*path == '/')
			path++;
	}

	return path;
}

char * dmlite_gfs_gethostname(const char * path) {
	char *p, *p2;

	if (!path)
		return NULL;

	while (*path == '/' && *(path + 1) == '/')
		path++;  /* reduce leading multiple slashes to one */

	/* first colon-slash */
	p = strstr(path, ":/");

	/* first slash after any leading slashes */
	p2 = strchr((*path == '/') ? (path + 1) : path, '/');

	/* treat it as a hostname:/path if there are no slashes before the colon-slash */
	if (p2 != p + 1)
		return NULL;

	if (*path == '/')
		path++;

	/* return the hostname part */
	return strndup(path, p - path);
}

globus_result_t dmlite_error2gfs_result(const char * name, dmlite_handle_t * handle, struct dmlite_context * context) {
	globus_result_t result = GLOBUS_SUCCESS;
	int posix_errno, error_type;

	GlobusGFSName(name);

	error_type = dmlite_errtype(context);
	posix_errno = dmlite_errno(context);
	if(error_type == DMLITE_USER_ERROR && (posix_errno == DMLITE_NO_SUCH_REPLICA || posix_errno == DMLITE_NO_REPLICAS))
		posix_errno = ENOENT;
	/* Workaround for LCGDM-1759 */
	if(error_type == DMLITE_DATABASE_ERROR && posix_errno == 1062) {
		error_type = DMLITE_USER_ERROR;
		posix_errno = EEXIST;
	}

	if (error_type == DMLITE_USER_ERROR) {
		dmlite_gfs_log(handle, GLOBUS_GFS_LOG_WARN, "user error :: %d :: %s",
			posix_errno, dmlite_error(context));
		result = GlobusGFSErrorSystemError(_gfs_name, posix_errno);
	} else {
		dmlite_gfs_log(handle, GLOBUS_GFS_LOG_ERR, "internal error :: %d.%d :: %s",
			error_type, posix_errno, dmlite_error(context));
		result = GlobusGFSErrorGeneric("Internal server error");
	}

	return result;
}

globus_result_t posix_error2gfs_result(const char * name, dmlite_handle_t * handle, int errnum, char * format, ...) {
	va_list args;
	globus_result_t result;
	char buf[LOGMSG_SIZE];

	GlobusGFSName(name);

	va_start(args, format);
	vsnprintf(buf, LOGMSG_SIZE, format, args);
	dmlite_gfs_log(handle, GLOBUS_GFS_LOG_ERR, "internal error :: %d :: %s", errnum, buf);
	va_end(args);

	if (errnum != EFAULT) {
		result = GlobusGFSErrorSystemError(_gfs_name, errnum);
	} else {
		result = GlobusGFSErrorGeneric("Internal server error");
	}

	return result;
}

void dmlite_stat2gfs(char * name, struct stat * stat, globus_gfs_stat_t * gfs_stat) {

	gfs_stat->name = globus_libc_strdup(name);
	gfs_stat->symlink_target = NULL;
	gfs_stat->error = GLOBUS_GRIDFTP_SERVER_CONTROL_STAT_SUCCESS;
	gfs_stat->dev = 0;
	gfs_stat->ino = stat->st_ino;
	gfs_stat->mode = stat->st_mode;
	gfs_stat->link_mode = stat->st_mode;
	gfs_stat->nlink = stat->st_nlink;
	gfs_stat->uid = stat->st_uid;
	gfs_stat->gid = stat->st_gid;
	gfs_stat->size = stat->st_size;
	gfs_stat->atime = stat->st_atime;
	gfs_stat->ctime = stat->st_ctime;
	gfs_stat->mtime = stat->st_mtime;
}

struct dmlite_context * dmlite_get_context(dmlite_handle_t * dmlite_handle, int * reason) {

	struct dmlite_context * dmlite_context_obj = NULL;
	struct dmlite_credentials dmlite_creds_obj;
	int rc, i;
	int retval = 0;
	gssapi_voms_ctx_t voms_ctx;

	*reason = 0;
	memset(&voms_ctx, 0, sizeof(voms_ctx));

	/* Get a dmlite_manager object first */
	if (!dmlite_handle->manager) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "no manager, impossibru!");
		*reason = EFAULT;
		goto errout;
	}

	/* Get VOMS credentials */
	if ((rc = get_voms_creds(&voms_ctx, dmlite_handle))) {
		*reason = rc;
		goto errout;
	}

	/* Now we can fetch a new context */
	dmlite_context_obj = dmlite_context_new(dmlite_handle->manager);
	if (!dmlite_context_obj) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "failed to create new context :: %s",
			dmlite_manager_error(dmlite_handle->manager));
		*reason = EFAULT;
		goto errout;
	}

	/* Set user credentials (only support GSI for the moment) */
	memset(&dmlite_creds_obj, 0, sizeof(dmlite_creds_obj));
	dmlite_creds_obj.client_name = dmlite_handle->session_info.subject;
	dmlite_creds_obj.remote_address = dmlite_handle->client_host;
	dmlite_creds_obj.nfqans = voms_ctx.nbfqan;
	dmlite_creds_obj.fqans = (const char **)voms_ctx.fqan;
	retval = dmlite_setcredentials(dmlite_context_obj, &dmlite_creds_obj);
	if (retval) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "failed to set credentials :: %s",
			dmlite_error(dmlite_context_obj));
		*reason = EFAULT;
		goto errout;
	}

	/* Set the protocol on the dmlite context */
	dmlite_any* protocol;
	protocol = dmlite_any_new_string("gsiftp");
	retval = dmlite_set(dmlite_context_obj, "protocol", protocol);
	dmlite_any_free(protocol);
	if (retval) {
		dmlite_gfs_log(dmlite_handle, GLOBUS_GFS_LOG_ERR, "failed to set the protocol identifier :: %s",
			dmlite_error(dmlite_context_obj));
		*reason = EFAULT;
		goto errout;
	}

errout:
	if (*reason) {
		dmlite_context_free(dmlite_context_obj);
		dmlite_context_obj = NULL;
	}
	for(i=0;i<voms_ctx.nbfqan;i++)
		free(voms_ctx.fqan[i]);
	free(voms_ctx.fqan);
	free(voms_ctx.voname);

	return dmlite_context_obj;
}

int dmlite_gfs_check_node(char **host, dmlite_handle_t * dmlite_handle_obj, const char * path, int flags) {
	struct dmlite_context * dmlite_context_obj;
	dmlite_location *	dmlite_location_obj = NULL;
	const char *		localpath = dmlite_gfs_fixpath(path, GLOBUS_FALSE);
	char *			node = dmlite_gfs_gethostname(path);
	int			ctx_err;
	int			pollcnt = 0;

	*host = NULL;
	
	/* Sanity check */
	if (!dmlite_handle_obj)
		return -1;

	/* Check if path is RFN */
	if (node) {
		dmlite_handle_obj->is_replica = GLOBUS_FALSE;
		*host = node;
		return 0;
	}

	/* Get a dmlite_context object */
	dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err);
	if (!dmlite_context_obj)
		return -1;

restart:
	if (!localpath) {
		dmlite_location_obj = dmlite_chooseserver(dmlite_context_obj, localpath);
		// If the dmlite call fails because the method is not implemented (e.g. in legacy mode)
		// then we fail the call immediately. This will likely trigger a random node selection
		// in the caller func
		int dmliterr = dmlite_errno(dmlite_context_obj);
		if (!dmlite_location_obj) {
			// If the call was not implemented we return failure
			if (dmliterr == DMLITE_SYSERR(ENOSYS)) {
				dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_WARN, "dmlite could not give a location :: %s :: %s",
				       localpath, dmlite_error(dmlite_context_obj));
			
				if (dmlite_context_obj)
					dmlite_context_free(dmlite_context_obj);
			
				return -2;
			}
			
		
			// Any other errno means that there are legitimately no choices
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "dmlite could not find a location :: %s :: %s",
				       localpath, dmlite_error(dmlite_context_obj));
			
			if (dmlite_context_obj)
				dmlite_context_free(dmlite_context_obj);
			
			// We return success with no host found. This means that there are no available hosts,
			// and possibly the client should receive an error
			return 0;
		} 
		
		
	
	} else if ((flags & O_ACCMODE) == O_RDONLY) {
		dmlite_location_obj = dmlite_get(dmlite_context_obj, localpath);
	} else {
		dmlite_location_obj = dmlite_put(dmlite_context_obj, localpath);
	}

	if (dmlite_location_obj) { /* Got location and PFN */
		snprintf(dmlite_handle_obj->pfn, PATH_MAX, "%s:%s", dmlite_location_obj->chunks[0].url.domain,
		    dmlite_location_obj->chunks[0].url.path);
		node = strdup(dmlite_location_obj->chunks[0].url.domain);
		dmlite_handle_obj->is_replica = localpath ? GLOBUS_TRUE : GLOBUS_FALSE;
		if (dmlite_handle_obj->location)
			dmlite_location_free(dmlite_handle_obj->location);
		dmlite_handle_obj->location = dmlite_location_obj;
		goto out;
	} else if ((dmlite_errno(dmlite_context_obj) == EINPROGRESS ||
		    dmlite_errno(dmlite_context_obj) == EAGAIN) &&
		    pollcnt++ < dmlite_handle_obj->pollmax) {
		/* With DOME we sometimes have to wait */
		sleep(dmlite_handle_obj->pollint);
		goto restart;
	}

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "failed to get location :: %s :: %s",
		localpath, dmlite_error(dmlite_context_obj));

out:
	if (dmlite_context_obj)
		dmlite_context_free(dmlite_context_obj);

	*host = node;
	return 0;
}

globus_bool_t dmlite_gfs_fstat(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, const char * path, struct stat* buf) {
	dmlite_location *	dmlite_location_obj = NULL;
	dmlite_any_dict *	dmlite_query = NULL;
	dmlite_fd *			fd = NULL;
	const char *		rfn = dmlite_gfs_fixpath(path, GLOBUS_TRUE);
	const char *		localpath = dmlite_gfs_fixpath(path, GLOBUS_FALSE);
	globus_bool_t		local;
	char		pfn[PATH_MAX];
	int			pollcnt = 0;

	/* Sanity check */
	if (!dmlite_handle_obj)
		return GLOBUS_FALSE;

	if (!dmlite_context_obj)
		return GLOBUS_FALSE;

	/* At first we try a virtual namespace, but only if path is not in RFN format */
	local = strcmp(rfn, localpath) != 0;

	if (local) {
		strncpy(pfn, rfn, PATH_MAX);
	} else {
restart:
		dmlite_location_obj = dmlite_get(dmlite_context_obj, localpath);

		if (dmlite_location_obj) { /* Got location and PFN */
			snprintf(pfn, PATH_MAX, "%s:%s", dmlite_location_obj->chunks[0].url.domain,
			    dmlite_location_obj->chunks[0].url.path);
			dmlite_query = dmlite_location_obj->chunks[0].url.query;
		} else if ((dmlite_errno(dmlite_context_obj) == EINPROGRESS ||
			    dmlite_errno(dmlite_context_obj) == EAGAIN) &&
			    pollcnt++ < dmlite_handle_obj->pollmax) {
			/* With DOME we sometimes have to wait */
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "fstat sleep :: %s :: cnt=%i, poolint=%i", localpath, pollcnt, dmlite_handle_obj->pollint);
			sleep(dmlite_handle_obj->pollint);
			goto restart;
		} else if (dmlite_errno(dmlite_context_obj) == ENOENT) {
			strncpy(pfn, rfn, PATH_MAX);
		} else {
			//dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "fstat failed to fetch replica :: %s :: %s",
			//		localpath, dmlite_error(dmlite_context_obj));
			goto errout;
		}
	}

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "fstat opening :: %s", pfn);
	fd = dmlite_fopen(dmlite_context_obj, pfn, O_INSECURE | O_LARGEFILE, dmlite_query);
	if (!fd) {
		dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "fstat no fd :: %s :: %s", pfn, dmlite_error(dmlite_context_obj));
		goto errout;
	}
	if (dmlite_fstat(fd, buf)) {
		dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "fstat failed :: %s :: %s", pfn, dmlite_error(dmlite_context_obj));
		dmlite_gfs_close(NULL, dmlite_handle_obj, 0);
		goto errout;
	}
	dmlite_gfs_close(NULL, dmlite_handle_obj, 0);

	return GLOBUS_TRUE;

errout:
	if (dmlite_location_obj)
		dmlite_location_free(dmlite_location_obj);
	return GLOBUS_FALSE;
}

dmlite_fd * dmlite_gfs_open(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj,
	const char * path, int flags) {

	dmlite_location *	dmlite_location_obj = NULL;
	dmlite_any_dict *	dmlite_query = NULL;
	const char *		rfn = dmlite_gfs_fixpath(path, GLOBUS_TRUE);
	const char *		localpath = dmlite_gfs_fixpath(path, GLOBUS_FALSE);
	globus_bool_t		local;
	int			mode;
	int			pollcnt = 0;

	/* Sanity check */
	if (!dmlite_handle_obj)
		return NULL;

	dmlite_handle_obj->fd = NULL;

	if (!dmlite_context_obj)
		return NULL;

	/* At first we try a virtual namespace, but only if path is not in RFN format */
	local = strcmp(rfn, localpath) != 0;

	/* Some backends like HDFS do not handle RFNs*/
	if (dmlite_handle_obj->rfn_nocheck)
		flags |= O_INSECURE;

restart:
	if ((flags & O_ACCMODE) == O_RDONLY) {
		mode = R_OK;
		if (!local)
			dmlite_location_obj = dmlite_get(dmlite_context_obj, localpath);
	} else {
		mode = W_OK;
		if (!local)
			dmlite_location_obj = dmlite_put(dmlite_context_obj, localpath);
	}

	if (dmlite_location_obj) { /* Got location and PFN */
		snprintf(dmlite_handle_obj->pfn, PATH_MAX, "%s:%s", dmlite_location_obj->chunks[0].url.domain,
		    dmlite_location_obj->chunks[0].url.path);
		dmlite_handle_obj->is_replica = GLOBUS_TRUE;
		dmlite_query = dmlite_location_obj->chunks[0].url.query;
	} else if ((dmlite_errno(dmlite_context_obj) == EINPROGRESS ||
		    dmlite_errno(dmlite_context_obj) == EAGAIN) &&
		    pollcnt++ < dmlite_handle_obj->pollmax) {
		/* With DOME we sometimes have to wait */
		sleep(dmlite_handle_obj->pollint);
		goto restart;
	} else if (local || dmlite_errno(dmlite_context_obj) == ENOENT) {
		if (!(flags & O_INSECURE) && dmlite_accessr(dmlite_context_obj, rfn, mode)) {
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "access to RFN denied :: %s :: %s",
					rfn, dmlite_error(dmlite_context_obj));
			goto errout;
		}
		dmlite_handle_obj->is_replica = GLOBUS_FALSE;
		strncpy(dmlite_handle_obj->pfn, rfn, PATH_MAX);
	} else {
		dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "failed to fetch replica :: %s :: %s",
				localpath, dmlite_error(dmlite_context_obj));
		goto errout;
	}
	if (dmlite_handle_obj->location)
		dmlite_location_free(dmlite_handle_obj->location);
	dmlite_handle_obj->location = dmlite_location_obj;
	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "opening :: %s", dmlite_handle_obj->pfn);
	dmlite_handle_obj->fd = dmlite_fopen(dmlite_context_obj, dmlite_handle_obj->pfn, flags | O_INSECURE | O_LARGEFILE,
	    dmlite_query, dmlite_handle_obj->file_mode);
	return dmlite_handle_obj->fd;

errout:
	if (dmlite_location_obj)
		dmlite_location_free(dmlite_location_obj);
	return NULL;
}

int dmlite_gfs_putdone(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, globus_bool_t ok) {
	int retval = 0, retval2 = 0;

	if (ok) {
		retval = dmlite_donewriting(dmlite_context_obj, dmlite_handle_obj->location);

	if (retval) {
		dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "Failed dmlite_donewriting (err: %d) for '%s'",
		  retval, dmlite_handle_obj->pfn);
		retval2 = dmlite_put_abort(dmlite_context_obj, dmlite_handle_obj->location);
		if (retval2)
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "Failed aborting file write after a failed donewriting (err: %d) for '%s'",
			  retval2, dmlite_handle_obj->pfn);
		}
	} else {
		retval2 = dmlite_put_abort(dmlite_context_obj, dmlite_handle_obj->location);
		if (retval2)
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_ERR, "Failed aborting file write (err: %d) for '%s'",
			  retval2, dmlite_handle_obj->pfn);
	}

	return retval;
}

int dmlite_gfs_close(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, globus_bool_t ok) {
	int retval = 0;

	/* Sanity check 1 */
	if (!dmlite_handle_obj || !dmlite_handle_obj->fd)
		return -1;

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "closing :: %s", dmlite_handle_obj->pfn);

	retval = dmlite_fclose(dmlite_handle_obj->fd);

	if (retval == 0)
		dmlite_handle_obj->fd = NULL;

	if (!dmlite_handle_obj->is_replica)
		return retval;

	if (!dmlite_context_obj || !dmlite_handle_obj->location)
		return -1;

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "replica put status :: %d", ok);
	return dmlite_gfs_putdone(dmlite_context_obj, dmlite_handle_obj, ok);
}

globus_result_t dmlite_gfs_compute_checksum(
		struct dmlite_context *	dmlite_context_obj,
		dmlite_handle_t *	dmlite_handle_obj,
		const char *		pathname,
		const char *		algorithm,
		globus_off_t		offset,
		globus_off_t		length,
		char *			checksum,
		size_t			checksummaxlen) {

	int			is_replica;
	globus_result_t		result = GLOBUS_SUCCESS;
	int			retval = 0;
	struct dmlite_xstat	xstat;
	struct dmlite_any *	sany;
	char			ckName[64];
	int			ckIdx;
	const char *		rfn = dmlite_gfs_fixpath(pathname, GLOBUS_TRUE);
	const char *		localpath = dmlite_gfs_fixpath(pathname, GLOBUS_FALSE);

	GlobusGFSName(dmlite_gfs_compute_checksum);
	memset(&xstat, 0, sizeof(xstat));

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "checksum :: %s :: %s", algorithm, pathname);

	for (ckIdx = 0; ckIdx < (sizeof(ckType) / sizeof(ckType[0])); ckIdx++)
		if (!strcasecmp(algorithm, ckType[ckIdx])) {
			snprintf(ckName, sizeof(ckName), "checksum.%s", ckType[ckIdx]);
			goto match;
		}
	result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ENOTSUP, "unsupported algorithm");
	return result;

match:
	xstat.extra = dmlite_any_dict_new();

	if (offset != 0 || length != -1) {
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "checksum :: calculating partly");
		goto force;
	}

  
  // Ugly workaround against Globus sometimes
  // triggering a checksum request from the client before the putdone has finished
  // This is so disgusting that it even seems to do its job
  //
  // if a stat request comes for a file that has (still) size=0
  // and only one replica in state P
  // then wait until these conditions change, likely
  // because putdone has finished executing on a new file
  //
  int cnt = 0;
  is_replica = 0;
  while (cnt < 30) {
    /* At first try to stat in virtual namespace */
    if (dmlite_statx(dmlite_context_obj, localpath, &xstat))
      break;
    
    is_replica = 1;
    
    
    if (!S_ISREG(xstat.stat.st_mode)) break;
    if (xstat.stat.st_size != 0) {
      // Uhm the size of the file is nonzero, this is promising
      
      // Loop on the replicas and find one that is in status '-'
      unsigned nReplicas = 0;
      dmlite_replica* replicas = NULL;
      int ii;
      
      if (dmlite_getreplicas(dmlite_context_obj, localpath, &nReplicas, &replicas) != 0)
        break;
      
      for (ii = 0; ii < nReplicas; ii++) {
        
        if (replicas[ii].status == '-') {
          dmlite_replicas_free(nReplicas, replicas);
          // The size iszero and the replica status is '-'... we can go, otherwise loop again
          goto finstat;
        }
      }
      
      dmlite_replicas_free(nReplicas, replicas);
      
    }
        
    usleep(1000000);
    cnt++;
  }
  
  // We are here if the filesize is still 0 in the metadata, after having waited some time
  if (cnt > 0) {
    char bufbuf[1024];
    snprintf(bufbuf, 1024, "checksum :: Filesize never became non-zero or no replica is available yet [Globus race condition?] Continuing anyway. (cnt: %d totsleep: %dms) fn:'%s'", cnt, cnt*500, localpath);
    bufbuf[1023] = '\0';
    dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, bufbuf);
    goto finstat;
  }

  

	if (dmlite_errno(dmlite_context_obj) != ENOENT) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
		goto out;
	}
	/* At second try to stat a local replica */
	if (!dmlite_rstatx(dmlite_context_obj, rfn, &xstat)) {
		is_replica = 0;
		goto finstat;
	}
	result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
	goto out;

finstat:
	/* See if requested type of checksum is already stored in the extended attributes */
	sany = dmlite_any_dict_get(xstat.extra, ckName);
	if(sany) {
		dmlite_any_to_string(sany, checksum, checksummaxlen);
		dmlite_any_free(sany);
		goto out;
	}
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "checksum :: calculating for the first time");

force:
	/* Calculate the checksum */
	if (dmlite_handle_obj->fd) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
		    EINVAL, "session already has a file open");
		goto out;
	}
	if (!dmlite_gfs_open(dmlite_context_obj, dmlite_handle_obj, pathname,
	    O_RDONLY)) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj,
		    dmlite_context_obj);
		goto out;
	}
	switch (ckIdx) {
		case 0:
			retval = dmlite_checksum_md5(dmlite_handle_obj->fd,
			    offset, length, checksum, checksummaxlen);
			break;
		case 1:
			retval = dmlite_checksum_adler32(dmlite_handle_obj->fd,
			    offset, length, checksum, checksummaxlen);
			break;
		case 2:
			retval = dmlite_checksum_crc32(dmlite_handle_obj->fd,
			    offset, length, checksum, checksummaxlen);
			break;
		default:
			retval = DMLITE_SYSTEM_ERROR | ENOTSUP;
	}
	dmlite_gfs_close(NULL, dmlite_handle_obj, 0);
	if (retval) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj,
		    dmlite_context_obj);
		goto out;
	}
	if (offset != 0 || length != -1)
		goto out;
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "checksum :: updating extended attributes");
	/* Add a new checksum entry */
	sany = dmlite_any_new_string(checksum);
	dmlite_any_dict_insert(xstat.extra, ckName, sany);
	dmlite_any_free(sany);
	/* Add a filesize hint */
	sany = dmlite_any_new_u64(xstat.stat.st_size);
	dmlite_any_dict_insert(xstat.extra, "filesize", sany);
	dmlite_any_free(sany);

	if (is_replica) {
		retval = dmlite_update_xattr(dmlite_context_obj, localpath, xstat.extra);
	} else {
		retval = dmlite_iupdate_xattr(dmlite_context_obj, xstat.stat.st_ino, xstat.extra);
	}
	if (retval) {
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_WARN, "checksum :: failed to update extended attributes");
		dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
	}

out:
	dmlite_any_dict_free(xstat.extra);
	return result;
}

globus_result_t dmlite_gfs_get_checksum(
		struct dmlite_context *	dmlite_context_obj,
		dmlite_handle_t *	dmlite_handle_obj,
		const char *		pathname,
		const char *		algorithm,
		globus_off_t		offset,
		globus_off_t		length,
		char *			checksum,
		size_t			checksummaxlen) {

	globus_result_t		result = GLOBUS_SUCCESS;
	int			retval = 0;
	char			ckName[64];
	int			ckIdx;
	int			pollcnt = 0;

	GlobusGFSName(dmlite_gfs_get_checksum);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "dome checksum :: %s :: %s", algorithm, pathname);

	if (offset != 0 || length != -1) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ENOTSUP, "partial checksums are not supported");
		return result;
	}

	for (ckIdx = 0; ckIdx < (sizeof(ckType) / sizeof(ckType[0])); ckIdx++)
		if (!strcasecmp(algorithm, ckType[ckIdx])) {
			snprintf(ckName, sizeof(ckName), "checksum.%s", ckType[ckIdx]);
			goto match;
		}
	result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ENOTSUP, "unsupported algorithm");
	return result;

match:
	retval = dmlite_getchecksum(dmlite_context_obj, pathname, ckName, checksum, checksummaxlen, NULL, 0, 0);
	if ((retval == EINPROGRESS || retval == EAGAIN) &&
	     pollcnt++ < dmlite_handle_obj->pollmax) {
		/* With DOME we sometimes have to wait */
		sleep(dmlite_handle_obj->pollint);
		goto match;
	} else if (retval) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
	}
	return result;
}
