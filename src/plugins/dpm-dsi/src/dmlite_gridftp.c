/* DMLite DSI module
   Based on DPM DSI and Globus remote DSI

   Author: Andrey Kiryanov <kiryanov@cern.ch>
*/

#include "dmlite_internal.h"
#include "dmlite_gridftp.h"

/*************************************************************************
 *  session_start
 *  -------------
 *  This function is called when a new session is initialized, ie a user
 *  connectes to the server.  This hook gives the dsi an oppertunity to
 *  set internal state that will be threaded through to all other
 *  function calls associated with this session.  And an oppertunity to
 *  reject the user.
 *
 *  finished_info.info.session.session_arg should be set to an DSI
 *  defined data structure.  This pointer will be passed as the void *
 *  user_arg parameter to all other interface functions.
 *
 *  NOTE: at nice wrapper function should exist that hides the details
 *        of the finished_info structure, but it currently does not.
 *        The DSI developer should jsut follow this template for now
 ************************************************************************/
static void globus_l_gfs_dmlite_session_start(
		globus_gfs_operation_t		op,
		globus_gfs_session_info_t *	session_info)
{
	globus_gfs_finished_info_t	finished_info;
	globus_result_t			result = GLOBUS_SUCCESS;
	dmlite_handle_t *		dmlite_handle_obj = NULL;
	char *				options;
	char *				nextop;
	char *				tok;
	char *				remote_list;
	char				config[PATH_MAX] = "/etc/dmlite.conf";

	GlobusGFSName(globus_l_gfs_dmlite_start);
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "starting new connection");
	memset(&finished_info, 0, sizeof(finished_info));

	/* Check for valid delegated credentials */
	if (!session_info->del_cred) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				EFAULT, "could not find delegated credentials");
		goto out;
	}

	/* Allocate memory and initialize structs */
	dmlite_handle_obj = (dmlite_handle_t *)globus_malloc(sizeof(dmlite_handle_t));
	if (!dmlite_handle_obj) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				EFAULT, "failed to allocate handle");
		goto out;
	}
	memset(dmlite_handle_obj, 0, sizeof(dmlite_handle_t));

	globus_mutex_init(&dmlite_handle_obj->gfs_mutex, GLOBUS_NULL);
	globus_mutex_init(&dmlite_handle_obj->rep_mutex, GLOBUS_NULL);
	dmlite_gfs_hostid2host(session_info->host_id, dmlite_handle_obj->client_host);
	if(session_info->username != NULL)
		dmlite_handle_obj->session_info.username = strdup(session_info->username);
	if(session_info->password != NULL)
		dmlite_handle_obj->session_info.password = strdup(session_info->password);
	if(session_info->subject != NULL)
		dmlite_handle_obj->session_info.subject = strdup(session_info->subject);
	dmlite_handle_obj->session_info.map_user = session_info->map_user;
	dmlite_handle_obj->session_info.del_cred = session_info->del_cred;
	dmlite_handle_obj->file_mode = 0664;
	dmlite_handle_obj->dir_mode = 0775;
	dmlite_handle_obj->pollint = 5;
	dmlite_handle_obj->pollmax = 100;

	globus_gridftp_server_get_config_string(op, &options);
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "DSI options :: %s", options);
	nextop = options;
	while((tok = strsep(&nextop, ","))) {
		if (!strcmp(tok, "rfn_nocheck")) {
			dmlite_handle_obj->rfn_nocheck = GLOBUS_TRUE;
		}
		if (!strcmp(tok, "dome_checksum")) {
			dmlite_handle_obj->dome_checksum = GLOBUS_TRUE;
		}
		if (strstr(tok, "pollint=") == tok) {
			sscanf(tok + 8, "%i", &dmlite_handle_obj->pollint);
		}
		if (strstr(tok, "pollmax=") == tok) {
			sscanf(tok + 8, "%i", &dmlite_handle_obj->pollmax);
		}
		if (strstr(tok, "dir_mode=") == tok) {
			sscanf(tok + 9, "%i", &dmlite_handle_obj->dir_mode);
		}
		if (strstr(tok, "file_mode=") == tok) {
			sscanf(tok + 10, "%i", &dmlite_handle_obj->file_mode);
		}
		if (strstr(tok, "dmlite_config=") == tok) {
			strncpy(config, tok + 14, PATH_MAX);
		}
	}
	globus_free(options);

	if((remote_list = globus_gfs_config_get_string("remote_nodes"))) {
		dmlite_handle_obj->nodelist = globus_list_from_string(remote_list, ',', " \t");
	} else {
		dmlite_handle_obj->nodelist = NULL;
	}

	/* Initialize a dmlite manager */
	dmlite_handle_obj->manager = dmlite_manager_new();
	if (dmlite_manager_load_configuration(dmlite_handle_obj->manager, config)) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				EFAULT, "failed to initialize manager :: %s",
				dmlite_manager_error(dmlite_handle_obj->manager));
	}
	srandom(time(NULL));

out:
	/* Some defaults for the globus return info */
	finished_info.type = GLOBUS_GFS_OP_SESSION_START;
	finished_info.info.session.session_arg = dmlite_handle_obj;
	finished_info.info.session.username = session_info->username;
	finished_info.info.session.home_dir = "/";
	finished_info.result = result;
	globus_gridftp_server_operation_finished(op, result, &finished_info);
}

/*************************************************************************
 *  session_end
 *  -----------
 *  This is called when a session ends, ie client quits or disconnects.
 *  The dsi should clean up all memory they associated wit the session
 *  here. 
 ************************************************************************/
static void globus_l_gfs_dmlite_session_end(void * user_arg)
{
	dmlite_handle_t *	dmlite_handle_obj = (dmlite_handle_t *)user_arg;

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "closing connection");

	/* Now destroy whatever is still there */
	if(dmlite_handle_obj) {
		globus_mutex_destroy(&dmlite_handle_obj->gfs_mutex);
		globus_mutex_destroy(&dmlite_handle_obj->rep_mutex);
		if (dmlite_handle_obj->nodelist)
			globus_list_free(dmlite_handle_obj->nodelist);
		if (dmlite_handle_obj->location)
			dmlite_location_free(dmlite_handle_obj->location);
		if (dmlite_handle_obj->manager)
			dmlite_manager_free(dmlite_handle_obj->manager);
		globus_free(dmlite_handle_obj);
	}
}

/*************************************************************************
 *  stat
 *  ----
 *  This interface function is called whenever the server needs 
 *  information about a given file or resource.  It is called then an
 *  LIST is sent by the client, when the server needs to verify that 
 *  a file exists and has the proper permissions, etc.
 ************************************************************************/
static void globus_l_gfs_dmlite_stat_free(
		globus_gfs_stat_t *	stat_array,
		int			stat_count)
{
	int i;

	for (i = 0; i < stat_count; i++) {
		if (stat_array[i].name) {
			globus_free(stat_array[i].name);
		}
		if (stat_array[i].symlink_target) {
			globus_free(stat_array[i].symlink_target);
		}
	}
	if (stat_array)
		globus_free(stat_array);
}

static void globus_l_gfs_dmlite_stat(
		globus_gfs_operation_t		op,
		globus_gfs_stat_info_t *	stat_info,
		void *				user_arg)
{
	globus_result_t		result = GLOBUS_SUCCESS;
	globus_gfs_stat_t *	gfs_stat_array = NULL;
	globus_gfs_stat_t *	gfs_stat_array_tmp = NULL;
	int			stat_count = 0;
	dmlite_handle_t *	dmlite_handle_obj = (dmlite_handle_t *)user_arg;
	struct dmlite_context *	dmlite_context_obj;
	struct dmlite_xstat	dmlite_xstat_obj;
	struct dmlite_dir *	dir_fd;
	struct dmlite_xstat *	dir_entry_xstat;
	struct stat		tmpstat;
	int			nfiles, ctx_err;
	const char *		localpath = dmlite_gfs_fixpath(stat_info->pathname, GLOBUS_FALSE);
	const char *		rfn = dmlite_gfs_fixpath(stat_info->pathname, GLOBUS_TRUE);

	GlobusGFSName(globus_l_gfs_dmlite_stat);

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_INFO, "stat :: %s", stat_info->pathname);

	/* This mutex is used as a semaphore */
	if(dmlite_handle_obj->is_replica) {
		globus_mutex_lock(&dmlite_handle_obj->rep_mutex);
		globus_mutex_unlock(&dmlite_handle_obj->rep_mutex);
	}

	/* Get a dmlite_context object */
	dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err);
	if (!dmlite_context_obj) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				ctx_err, "failed to get context");
		goto errout;
	}
	memset(&dmlite_xstat_obj, 0, sizeof(dmlite_xstat_obj));

	if (strcmp(rfn, localpath) == 0) {
		// Try to stat in virtual namespace
		if (dmlite_statx(dmlite_context_obj, localpath, &dmlite_xstat_obj)) {
			result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
			goto errout;
		}
	} else {
		// Try to stat a local replica
		if (dmlite_rstatx(dmlite_context_obj, rfn, &dmlite_xstat_obj)) {
			result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
			goto errout;
		}
	}

	/* File like info on the entry is easy to handle */
	if (!S_ISDIR(dmlite_xstat_obj.stat.st_mode) || stat_info->file_only) {
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "stat :: single entry");
		/* Workaround for partially written replicas */
		if (S_ISDIR(dmlite_xstat_obj.stat.st_mode) || dmlite_xstat_obj.stat.st_size != 0)
			goto giveup;
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "stat :: zero size replica, maybe pending");
		if (dmlite_handle_obj->fd) {
			/* Ignore this error */
			posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EINVAL, "session already has a file open");
			goto giveup;
		}
		if (!dmlite_gfs_fstat(dmlite_context_obj, dmlite_handle_obj, rfn, &tmpstat)) {
			/* Ignore this error */
			dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "stat :: unable to fstat replica :: %s :: %s", rfn, dmlite_error(dmlite_context_obj));
			goto giveup;
		}
		dmlite_xstat_obj.stat.st_size = tmpstat.st_size;
giveup:
		/* Allocate the stat array */
		gfs_stat_array = (globus_gfs_stat_t *)globus_malloc(sizeof(globus_gfs_stat_t));
		if (!gfs_stat_array) {
			result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EFAULT, "failed to allocate array");
			goto errout;
		}
		dmlite_stat2gfs(dmlite_xstat_obj.name, &dmlite_xstat_obj.stat, gfs_stat_array);
		stat_count = 1;
	} else { /* Going through directory is a bit more work */
		nfiles  = dmlite_xstat_obj.stat.st_nlink;
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "stat :: full directory with %d files", nfiles);

		/* Allocate the stat array */
		gfs_stat_array = (globus_gfs_stat_t *)globus_malloc(sizeof(globus_gfs_stat_t) * nfiles);
		if (!gfs_stat_array) {
			result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EFAULT, "failed to allocate array");
			goto errout;
		}

		/* Open the directory */
		dir_fd = dmlite_opendir(dmlite_context_obj, localpath);
		if (dir_fd == NULL) {
			result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
			goto errout;
		}

		while ((dir_entry_xstat = dmlite_readdirx(dmlite_context_obj, dir_fd))) {
			/* Double check that nlink value was correct and we don't overflow */
			if (stat_count >= nfiles) {
				/* This can happen in case of directory update between stat and opendir or in case of inconsistent nlink value */
				dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_INFO, "stat :: nlinks %d doesn't match number of objects %d", nfiles, stat_count + 1);
				/* Reallocate stat array size + 1 */
				gfs_stat_array_tmp = (globus_gfs_stat_t *)globus_malloc(sizeof(globus_gfs_stat_t) * (stat_count + 1));
				if (!gfs_stat_array_tmp) {
					result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EFAULT, "failed to reallocate array (%d -> %d)", nfiles, stat_count + 1);
					goto errout;
				}
				memcpy(gfs_stat_array_tmp, gfs_stat_array, sizeof(globus_gfs_stat_t) * stat_count);
				globus_free(gfs_stat_array);
				gfs_stat_array = gfs_stat_array_tmp;
			}
			dmlite_stat2gfs(dir_entry_xstat->name, &dir_entry_xstat->stat, gfs_stat_array + stat_count);
			stat_count++;
		}

		/* Check for error */
		if (dmlite_errno(dmlite_context_obj) != 0) {
			result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
			goto errout;
		}

		/* Close the directory */
		if (dmlite_closedir(dmlite_context_obj, dir_fd)) {
			result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
			goto errout;
		}
	}

	globus_gridftp_server_finished_stat(op, GLOBUS_SUCCESS, gfs_stat_array, stat_count);
	globus_l_gfs_dmlite_stat_free(gfs_stat_array, stat_count);
	dmlite_context_free(dmlite_context_obj);
	return;

errout:
	globus_l_gfs_dmlite_stat_free(gfs_stat_array, stat_count);
	if (dmlite_context_obj)
		dmlite_context_free(dmlite_context_obj);
	if (result == GLOBUS_SUCCESS)
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EFAULT, "unknown error");
	globus_gridftp_server_finished_stat(op, result, GLOBUS_NULL, 0);
}

/*************************************************************************
 *  command
 *  -------
 *  This interface function is called when the client sends a 'command'.
 *  commands are such things as mkdir, remdir, delete.  The complete
 *  enumeration is below.
 *
 *  To determine which command is being requested look at:
 *      cmd_info->command
 *
 *      GLOBUS_GFS_CMD_MKD = 1,
 *      GLOBUS_GFS_CMD_RMD,
 *      GLOBUS_GFS_CMD_DELE,
 *      GLOBUS_GFS_CMD_RNTO,
 *      GLOBUS_GFS_CMD_RNFR,
 *      GLOBUS_GFS_CMD_CKSM,
 *      GLOBUS_GFS_CMD_SITE_CHMOD,
 *      GLOBUS_GFS_CMD_SITE_DSI
 ************************************************************************/
static void globus_l_gfs_dmlite_command(
		globus_gfs_operation_t		op,
		globus_gfs_command_info_t *	cmd_info,
		void *				user_arg)
{
	globus_result_t			result = GLOBUS_SUCCESS;
	dmlite_handle_t *		dmlite_handle_obj = (dmlite_handle_t *)user_arg;
	struct dmlite_context *		dmlite_context_obj;
	int				retval = 0;
	int				ctx_err;
	gid_t				new_gid;
	struct utimbuf			ubuf;
	char				cksumstr[PATH_MAX];
	const char *			localpath = dmlite_gfs_fixpath(cmd_info->pathname, GLOBUS_FALSE);
	const char *			rfn = dmlite_gfs_fixpath(cmd_info->pathname, GLOBUS_TRUE);
	char *				retdata = GLOBUS_NULL;

	GlobusGFSName(globus_l_gfs_dmlite_command);

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_INFO, "command :: %d :: %s", cmd_info->command, localpath);

	/* This mutex is used as a semaphore */
	if(dmlite_handle_obj->is_replica) {
		globus_mutex_lock(&dmlite_handle_obj->rep_mutex);
		globus_mutex_unlock(&dmlite_handle_obj->rep_mutex);
	}

	/* Get a dmlite_context object */
	dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err);
	if (!dmlite_context_obj) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ctx_err, "failed to get context");
		goto errout;
	}

	switch(cmd_info->command)
	{
		case GLOBUS_GFS_CMD_MKD: // mkdir
			retval = dmlite_mkdir(dmlite_context_obj, localpath, dmlite_handle_obj->dir_mode);
			break;

		case GLOBUS_GFS_CMD_RMD: // rmdir
			retval = dmlite_rmdir(dmlite_context_obj, localpath);
			break;

		case GLOBUS_GFS_CMD_DELE: // unlink
			retval = dmlite_unlink(dmlite_context_obj, localpath);
			break;

		case GLOBUS_GFS_CMD_RNTO: // rename
			retval = dmlite_rename(dmlite_context_obj, dmlite_gfs_fixpath(cmd_info->from_pathname, GLOBUS_FALSE), localpath);
			break;

		case GLOBUS_GFS_CMD_CKSM: // checksum
			if(dmlite_handle_obj->dome_checksum && rfn == localpath) /* DOME checksum does not work with RFNs */
				result = dmlite_gfs_get_checksum(dmlite_context_obj, dmlite_handle_obj, localpath,
					 cmd_info->cksm_alg, cmd_info->cksm_offset, cmd_info->cksm_length, cksumstr, sizeof(cksumstr));
			else
				result = dmlite_gfs_compute_checksum(dmlite_context_obj, dmlite_handle_obj, rfn,
					 cmd_info->cksm_alg, cmd_info->cksm_offset, cmd_info->cksm_length, cksumstr, sizeof(cksumstr));
			if (result == GLOBUS_SUCCESS)
				retdata = cksumstr;
			goto errout;

		case GLOBUS_GFS_CMD_SITE_CHMOD: // chmod
			retval = dmlite_chmod(dmlite_context_obj, localpath, cmd_info->chmod_mode);
			break;

		case GLOBUS_GFS_CMD_SITE_CHGRP: // chgrp
			retval = dmlite_getgrpbynam(dmlite_context_obj, cmd_info->chgrp_group, &new_gid);
			if (retval) break;
			retval = dmlite_lchown(dmlite_context_obj, localpath, -1, new_gid);
			break;

		case GLOBUS_GFS_CMD_SITE_UTIME: // utime
			ubuf.actime = cmd_info->utime_time;
			ubuf.modtime = cmd_info->utime_time;
			retval = dmlite_utime(dmlite_context_obj, localpath, &ubuf);
            retval = 0; // Let's ignore the result if it's not implemented in dmlite
			break;

		default:
			result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ENOTSUP, "unsupported command");
			goto errout;
	}

	if (retval) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
		goto errout;
	}

errout:
	if (dmlite_context_obj)
		dmlite_context_free(dmlite_context_obj);
	globus_gridftp_server_finished_command(op, result, retdata);
}

/*************************************************************************
 *  recv
 *  ----
 *  This interface function is called when the client requests that a
 *  file be transfered to the server.
 *
 *  To receive a file the following functions will be used in roughly
 *  the presented order.  They are doced in more detail with the
 *  gridftp server documentation.
 *
 *      globus_gridftp_server_begin_transfer();
 *      globus_gridftp_server_register_read();
 *      globus_gridftp_server_finished_transfer();
 *
 ************************************************************************/
static globus_bool_t globus_l_gfs_dmlite_recv_next_block(dmlite_handle_t * dmlite_handle_obj);

static void globus_l_gfs_dmlite_read_cb(
		globus_gfs_operation_t	op,
		globus_result_t		result,
		globus_byte_t *		buffer,
		globus_size_t		nbytes,
		globus_off_t		offset,
		globus_bool_t		eof,
		void *			user_arg)
{
	dmlite_handle_t *	dmlite_handle_obj = (dmlite_handle_t *)user_arg;
	struct dmlite_context *	dmlite_context_obj;
	globus_off_t		bytes_write;
	int			retval = 0;
	int			ctx_err;

	GlobusGFSName(globus_l_gfs_dmlite_read_cb);

	/* Convert the given handle */
	globus_mutex_lock(&dmlite_handle_obj->gfs_mutex);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "read-cb :: pending %d", dmlite_handle_obj->pending);
	/* We're done with one more write */
	dmlite_handle_obj->pending--;

	if (result != GLOBUS_SUCCESS || dmlite_handle_obj->cur_length == 0 || nbytes == 0) { /* Nothing to write */
		if (dmlite_handle_obj->cur_result == GLOBUS_SUCCESS)
			dmlite_handle_obj->cur_result = result;
		dmlite_handle_obj->done = GLOBUS_TRUE;
		goto done;
	}

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "read-cb :: ofs/len = %d/%d",
			dmlite_handle_obj->cur_offset + offset, dmlite_handle_obj->cur_length);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "read-cb :: got %d bytes at offset %d", nbytes, offset);
	/* Seek as we might be writing out-of-sequence */
	retval = dmlite_fseek(dmlite_handle_obj->fd, dmlite_handle_obj->cur_offset + offset, SEEK_SET);
	if (retval) {
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, retval, "failed to seek");
		dmlite_handle_obj->done = GLOBUS_TRUE;
		goto done;
	}

	/* Perform the actual write operation */
	bytes_write = dmlite_fwrite(dmlite_handle_obj->fd, buffer, nbytes);
	if (bytes_write < (globus_off_t)nbytes) { /* Check for errors */
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, EFAULT, "failed to write");
		dmlite_handle_obj->done = GLOBUS_TRUE;
		goto done;
	}
	globus_gridftp_server_update_bytes_written(op, offset, nbytes);
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "read-cb :: wrote %d bytes", bytes_write);

	/* Update the remaining length to be written */
	if (dmlite_handle_obj->cur_length >= bytes_write) /* Never go below zero */
		dmlite_handle_obj->cur_length -= bytes_write;

	/* Have we reached the EOF? */
	if (eof)
		dmlite_handle_obj->done = GLOBUS_TRUE;

done:
	globus_free(buffer);
	if (!dmlite_handle_obj->done)
		globus_l_gfs_dmlite_recv_next_block(dmlite_handle_obj);
	if (dmlite_handle_obj->pending != 0) {
		globus_mutex_unlock(&dmlite_handle_obj->gfs_mutex);
		return;
	}
	if ((dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err))) {
		if (dmlite_gfs_close(dmlite_context_obj, dmlite_handle_obj, dmlite_handle_obj->cur_result == GLOBUS_SUCCESS) &&
				dmlite_handle_obj->cur_result == GLOBUS_SUCCESS)
			dmlite_handle_obj->cur_result = dmlite_error2gfs_result(_gfs_name,
					dmlite_handle_obj, dmlite_context_obj);
		dmlite_context_free(dmlite_context_obj);
	} else {
		if (dmlite_handle_obj->cur_result == GLOBUS_SUCCESS) /* Do not report error here if we already handling one */
			dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name,
					dmlite_handle_obj, ctx_err, "failed to get context");
	}
	globus_gridftp_server_finished_transfer(op, dmlite_handle_obj->cur_result);
	globus_mutex_unlock(&dmlite_handle_obj->gfs_mutex);
}

static globus_bool_t globus_l_gfs_dmlite_recv_next_block(dmlite_handle_t * dmlite_handle_obj)
{
	globus_size_t		bytes_length;
	globus_byte_t *		buffer = NULL;
	struct dmlite_context *	dmlite_context_obj;
	int			ctx_err;
	char *			errmsg;

	GlobusGFSName(globus_l_gfs_dmlite_recv_next_block);

	/* Check we actually have something left to write */
	if (dmlite_handle_obj->cur_length == 0) {
		dmlite_handle_obj->cur_result = GLOBUS_SUCCESS;
		goto done;
	}

	/* How much should we actually write? block_size or less if bigger than remaining length */
	bytes_length = dmlite_handle_obj->block_size;
	if (dmlite_handle_obj->cur_length > 0 && dmlite_handle_obj->cur_length < dmlite_handle_obj->block_size)
		bytes_length = dmlite_handle_obj->cur_length;

	/* Allocate the write buffer */
	buffer = globus_malloc(bytes_length);
	if (!buffer) {
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
			EFAULT, "failed to allocate buffer of %d bytes", bytes_length);
		goto done;
	}
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "recv-next :: buffer size is %d bytes", bytes_length);

	/* Register the read operation to pull data from the client */
	dmlite_handle_obj->cur_result = globus_gridftp_server_register_read(dmlite_handle_obj->gfs_op, buffer,
			bytes_length, globus_l_gfs_dmlite_read_cb, dmlite_handle_obj);

	if (dmlite_handle_obj->cur_result == GLOBUS_SUCCESS) {
		dmlite_handle_obj->pending++;
		return GLOBUS_FALSE;
	}
	/* Workaround for GT-296 */
	errmsg = globus_object_printable_to_string(globus_error_get_cause(globus_error_peek(dmlite_handle_obj->cur_result)));
	if (errmsg && !strcmp(errmsg, "globus_ftp_control_data_read(): Handle not in proper state EOF.")) {
		dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_WARN, "recv-next :: workaround for Globus Bug 1234 aka GT-296");
		globus_error_get(dmlite_handle_obj->cur_result); /* Remove upper error from stack */
		globus_error_get(dmlite_handle_obj->cur_result); /* Remove lower error from stack */
		dmlite_handle_obj->cur_result = GLOBUS_SUCCESS; /* Pretend like nothing happened */
	}
	globus_free(errmsg);
	globus_free(buffer); /* Only in error case */

done:
	dmlite_handle_obj->done = GLOBUS_TRUE;
	if (dmlite_handle_obj->pending != 0)
		return GLOBUS_TRUE;
	if ((dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err))) {
		if (dmlite_gfs_close(dmlite_context_obj, dmlite_handle_obj, dmlite_handle_obj->cur_result == GLOBUS_SUCCESS) &&
				dmlite_handle_obj->cur_result == GLOBUS_SUCCESS)
			dmlite_handle_obj->cur_result = dmlite_error2gfs_result(_gfs_name,
					dmlite_handle_obj, dmlite_context_obj);
		dmlite_context_free(dmlite_context_obj);
	} else {
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name,
				dmlite_handle_obj, ctx_err, "failed to get context");
	}
	globus_gridftp_server_finished_transfer(dmlite_handle_obj->gfs_op, dmlite_handle_obj->cur_result);
	return GLOBUS_TRUE;
}

static void globus_l_gfs_dmlite_recv(
		globus_gfs_operation_t		op,
		globus_gfs_transfer_info_t *	transfer_info,
		void *				user_arg)
{
	globus_bool_t		done = GLOBUS_FALSE;
	globus_result_t		result = GLOBUS_SUCCESS;
	dmlite_handle_t *	dmlite_handle_obj = (dmlite_handle_t *)user_arg;
	struct dmlite_context *	dmlite_context_obj;
	int			ctx_err, i;

	GlobusGFSName(globus_l_gfs_dmlite_recv);

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "recv :: started");

	/* Get a dmlite_context object */
	dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err);
	if (!dmlite_context_obj) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ctx_err,
			"failed to get context");
		goto errout;
	}

	/* These two are values coming from globus */
	globus_gridftp_server_get_block_size(op, &dmlite_handle_obj->block_size);
	globus_gridftp_server_get_optimal_concurrency(op, &dmlite_handle_obj->optimal_count);

	/* Fill in the starting handle values for the write operations */
	dmlite_handle_obj->gfs_op = op;
	/* The rest are values using during the write sequence */
	dmlite_handle_obj->cur_result = GLOBUS_SUCCESS;
	dmlite_handle_obj->done = GLOBUS_FALSE;
	dmlite_handle_obj->pending = 0;
	/* These two in particular (offset/length) are set here when there are partial writes */
	globus_gridftp_server_get_write_range(dmlite_handle_obj->gfs_op,
		 &dmlite_handle_obj->cur_offset, &dmlite_handle_obj->cur_length);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "recv :: ofs/len = %d/%d",
			dmlite_handle_obj->cur_offset, dmlite_handle_obj->cur_length);

	/* Open the file */
	if (!dmlite_gfs_open(dmlite_context_obj, dmlite_handle_obj,
			transfer_info->pathname, O_WRONLY | O_CREAT)) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
		goto errout;
	}

	/* Initiate the write operations */
	globus_gridftp_server_begin_transfer(op, 0, dmlite_handle_obj);
	globus_mutex_lock(&dmlite_handle_obj->gfs_mutex);
	for(i = 0; i < dmlite_handle_obj->optimal_count && done != GLOBUS_TRUE; i++) {
		done = globus_l_gfs_dmlite_recv_next_block(dmlite_handle_obj);
	}
	globus_mutex_unlock(&dmlite_handle_obj->gfs_mutex);
	dmlite_context_free(dmlite_context_obj);
	return;

errout:
	if (dmlite_context_obj)
		dmlite_context_free(dmlite_context_obj);
	globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "recv error", result);
	globus_gridftp_server_finished_transfer(op, result);
}

/*************************************************************************
 *  send
 *  ----
 *  This interface function is called when the client requests to receive
 *  a file from the server.
 *
 *  To send a file to the client the following functions will be used in roughly
 *  the presented order.  They are doced in more detail with the
 *  gridftp server documentation.
 *
 *      globus_gridftp_server_begin_transfer();
 *      globus_gridftp_server_register_write();
 *      globus_gridftp_server_finished_transfer();
 *
 ************************************************************************/
static globus_bool_t globus_l_gfs_dmlite_send_next_block(dmlite_handle_t * dmlite_handle_obj);

static void globus_l_gfs_dmlite_write_cb(
		globus_gfs_operation_t	op,
		globus_result_t		result,
		globus_byte_t *		buffer,
		globus_size_t		nbytes,
		void *			user_arg)
{
	dmlite_handle_t * dmlite_handle_obj = (dmlite_handle_t *)user_arg;

	GlobusGFSName(globus_l_gfs_dmlite_write_cb);

	globus_free(buffer);

	globus_mutex_lock(&dmlite_handle_obj->gfs_mutex);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "write-cb :: pending %d",
		dmlite_handle_obj->pending);

	/* We're done with one more write */
	dmlite_handle_obj->pending--;

	/* Are we done? If current result is not success we are */
	if (!dmlite_handle_obj->done) {
		if (result != GLOBUS_SUCCESS) {
			dmlite_handle_obj->cur_result = result;
			dmlite_handle_obj->done = GLOBUS_TRUE;
		} else {
			globus_l_gfs_dmlite_send_next_block(dmlite_handle_obj);
		}
	}
	if (dmlite_handle_obj->pending == 0) {
		/* Ignore close errors for reading */
		dmlite_gfs_close(NULL, dmlite_handle_obj, 0);
		globus_gridftp_server_finished_transfer(op, dmlite_handle_obj->cur_result);
	}
	globus_mutex_unlock(&dmlite_handle_obj->gfs_mutex);
}

static globus_bool_t globus_l_gfs_dmlite_send_next_block(dmlite_handle_t * dmlite_handle_obj)
{
	globus_size_t		bytes_length;
	globus_off_t		bytes_read;
	globus_byte_t *		buffer = NULL;

	GlobusGFSName(globus_l_gfs_dmlite_send_next_block);

	/* Check we actually have something left to read */
	if (dmlite_handle_obj->cur_length == 0 || dmlite_feof(dmlite_handle_obj->fd))
		goto done;

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "send-next :: ofs/len = %d/%d",
			dmlite_handle_obj->cur_offset, dmlite_handle_obj->cur_length);

	/* How much should we actually read? block_size or less if bigger than remaining length */
	bytes_length = dmlite_handle_obj->block_size;
	if (dmlite_handle_obj->cur_length > 0 && dmlite_handle_obj->cur_length < dmlite_handle_obj->block_size)
		bytes_length = dmlite_handle_obj->cur_length;

	/* Allocate the read buffer */
	buffer = globus_malloc(bytes_length);
	if (!buffer) {
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				EFAULT, "failed to allocate buffer of %d bytes", bytes_length);
		goto done;
	}
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "send-next :: buffer size is %d bytes", bytes_length);

	/* Perform the actual read operation */
	bytes_read = dmlite_fread(dmlite_handle_obj->fd, buffer, bytes_length);
	if (bytes_read == 0) /* EOF */
		goto done;
	if (bytes_read < 0) { /* Check for errors */
		dmlite_handle_obj->cur_result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj,
				EFAULT, "failed read");
		goto done;
	}
	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "send-next :: read %d bytes", bytes_read);

	/* Register the write operation to push data to the client */
	dmlite_handle_obj->cur_result = globus_gridftp_server_register_write(dmlite_handle_obj->gfs_op,
			buffer, bytes_read, dmlite_handle_obj->cur_offset, -1,
			globus_l_gfs_dmlite_write_cb, dmlite_handle_obj);

	if (dmlite_handle_obj->cur_result != GLOBUS_SUCCESS)
		goto done;

	/* Update the current read offset and remaining length to be read */
	dmlite_handle_obj->pending++;
	dmlite_handle_obj->cur_offset += bytes_read;
	if (dmlite_handle_obj->cur_length >= bytes_read) /* Never go below zero */
		dmlite_handle_obj->cur_length -= bytes_read;

	return GLOBUS_FALSE;

done:
	if (buffer)
		globus_free(buffer);
	dmlite_handle_obj->done = GLOBUS_TRUE;
	if (dmlite_handle_obj->pending == 0) {
		/* Ignore close errors for reading */
		dmlite_gfs_close(NULL, dmlite_handle_obj, 0);
		globus_gridftp_server_finished_transfer(dmlite_handle_obj->gfs_op, dmlite_handle_obj->cur_result);
	}
	return GLOBUS_TRUE;
}

static void globus_l_gfs_dmlite_send(
		globus_gfs_operation_t		op,
		globus_gfs_transfer_info_t *	transfer_info,
		void *				user_arg)
{
	globus_bool_t		done = GLOBUS_FALSE;
	globus_result_t		result = GLOBUS_SUCCESS;
	dmlite_handle_t *	dmlite_handle_obj = (dmlite_handle_t *)user_arg;
	struct dmlite_context *	dmlite_context_obj;
	int			retval = 0;
	int			ctx_err, i;

	GlobusGFSName(globus_l_gfs_dmlite_send);

	dmlite_gfs_log(dmlite_handle_obj, GLOBUS_GFS_LOG_DUMP, "send :: started");

	/* Get a dmlite_context object */
	dmlite_context_obj = dmlite_get_context(dmlite_handle_obj, &ctx_err);
	if (!dmlite_context_obj) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, ctx_err,
			"failed to get context");
		goto errout;
	}

	/* These two are values coming from globus */
	globus_gridftp_server_get_block_size(op, &dmlite_handle_obj->block_size);
	globus_gridftp_server_get_optimal_concurrency(op, &dmlite_handle_obj->optimal_count);

	/* Fill in the starting handle values for the read operations */
	dmlite_handle_obj->gfs_op = op;
	/* The rest are values using during the read sequence */
	dmlite_handle_obj->cur_result = GLOBUS_SUCCESS;
	dmlite_handle_obj->done = GLOBUS_FALSE;
	dmlite_handle_obj->pending = 0;
	/* These two in particular (offset/length) are set here when there are partial reads */
	globus_gridftp_server_get_read_range(dmlite_handle_obj->gfs_op,
		 &dmlite_handle_obj->cur_offset, &dmlite_handle_obj->cur_length);

	dmlite_gfs_log(NULL, GLOBUS_GFS_LOG_DUMP, "send :: ofs/len = %d/%d",
			dmlite_handle_obj->cur_offset, dmlite_handle_obj->cur_length);

	/* Open the file */
	if (!dmlite_gfs_open(dmlite_context_obj, dmlite_handle_obj,
			transfer_info->pathname, O_RDONLY)) {
		result = dmlite_error2gfs_result(_gfs_name, dmlite_handle_obj, dmlite_context_obj);
		goto errout;
	}

	/* Seek as we might be reading partially */
	retval = dmlite_fseek(dmlite_handle_obj->fd, dmlite_handle_obj->cur_offset, SEEK_SET);
	if (retval) {
		result = posix_error2gfs_result(_gfs_name, dmlite_handle_obj, retval, "failed to seek");
		goto errout;
	}

	/* Initiate the read operations */
	globus_gridftp_server_begin_transfer(op, 0, dmlite_handle_obj);
	globus_mutex_lock(&dmlite_handle_obj->gfs_mutex);
	for(i = 0; i < dmlite_handle_obj->optimal_count && done != GLOBUS_TRUE; i++) {
		done = globus_l_gfs_dmlite_send_next_block(dmlite_handle_obj);
	}
	globus_mutex_unlock(&dmlite_handle_obj->gfs_mutex);
	dmlite_context_free(dmlite_context_obj);
	return;

errout:
	if (dmlite_handle_obj->fd)
		dmlite_gfs_close(NULL, dmlite_handle_obj, 0);
	if (dmlite_context_obj)
		dmlite_context_free(dmlite_context_obj);
	globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "send error", result);
	globus_gridftp_server_finished_transfer(op, result);
}

/*
	##################################################
	# DANGER: YOU ARE ENTERING INSANE CALLBACKS ZONE #
	##################################################
*/

static void globus_l_gfs_remote_node_release(
		globus_l_gfs_remote_node_info_t *   node_info)
{
    GlobusGFSName(globus_l_gfs_remote_node_release);

    globus_gfs_ipc_close(node_info->ipc_handle, NULL, NULL);

    if(node_info->cs)
        globus_free(node_info->cs);

    globus_free(node_info);
}

static void globus_l_gfs_remote_ipc_error_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     result,
		void *                              user_arg)
{
    dmlite_handle_t *      my_handle;

    GlobusGFSName(globus_l_gfs_remote_ipc_error_cb);

    my_handle = (dmlite_handle_t *) user_arg;
    globus_mutex_lock(&my_handle->gfs_mutex);
    my_handle->cur_result = result;
    globus_mutex_unlock(&my_handle->gfs_mutex);
    globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "IPC error", result);
}

static void globus_l_gfs_remote_node_request_kickout(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     result,
		globus_gfs_finished_info_t *        reply,
		void *                              user_arg)
{
    globus_bool_t                       callback = GLOBUS_FALSE;
    globus_l_gfs_remote_node_info_t *   node_info;

    GlobusGFSName(globus_l_gfs_remote_node_request_kickout);

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;

/* LOCKS! */
    if(result == GLOBUS_SUCCESS) {
        globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "connected to remote node\n");
        node_info->ipc_handle = ipc_handle;

        callback = GLOBUS_TRUE;
    } else {
        globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "could not connect to remote node", result);

        node_info->error_count++;
        if(node_info->error_count >= IPC_RETRY) {
            globus_gfs_log_message(GLOBUS_GFS_LOG_ERR, "retry limit reached, giving up\n");
            callback = GLOBUS_TRUE;
        } else {
            result = globus_gfs_ipc_handle_obtain(
                    &node_info->my_handle->session_info,
                    &globus_gfs_ipc_default_iface,
                    globus_l_gfs_remote_node_request_kickout,
                    node_info,
                    globus_l_gfs_remote_ipc_error_cb,
                    node_info->my_handle);
            if(result != GLOBUS_SUCCESS)
                callback = GLOBUS_TRUE;
        }
    }

    if(callback) {
        node_info->callback(node_info, result, node_info->user_arg);
        if(result != GLOBUS_SUCCESS)
            globus_free(node_info);
    }
}

static void globus_l_gfs_remote_node_error_kickout(
		void *                              user_arg)
{
    globus_l_gfs_remote_node_info_t *   node_info;

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;

    globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "could not obtain IPC handle", node_info->cached_result);

    node_info->callback(node_info, node_info->cached_result, node_info->user_arg);
}

static globus_result_t globus_l_gfs_remote_node_request(
		dmlite_handle_t *                   my_handle,
		char *                              pathname,
		globus_l_gfs_remote_node_cb         callback,
		void *                              user_arg)
{
    globus_l_gfs_remote_node_info_t *   node_info;
    globus_list_t *                     node = NULL;
    globus_result_t                     result;
    long int                            idx;
    char *                              hostname;
    int                                 r;
    
    GlobusGFSName(globus_l_gfs_remote_node_request);

    if(!callback)
        return GLOBUS_FAILURE;

    if (!pathname)
        globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "requesting node. DN: '%s' lfn: NULL\n", my_handle->session_info.subject);
    else
        globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "requesting node. DN: '%s' lfn: '%s'\n", my_handle->session_info.subject, pathname);
    
    
    r = dmlite_gfs_check_node(&hostname, my_handle, my_handle->mode != DMLITE_FILEMODE_NONE ? pathname : NULL, my_handle->mode == DMLITE_FILEMODE_READING ? O_RDONLY : O_WRONLY);

    /* Look up node by hostname */
    if(hostname) {
        globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "requested node: %s\n", hostname);
        node = globus_list_search_pred(my_handle->nodelist, dmlite_gfs_node_cmp, hostname);
    }

    /* Choose a random node if the call to dmlite_gfs_check_node failed. Otherwise
     fail, as getting no host from a successful call means that there are no good hosts to use */
    if(r) { 
        my_handle->is_replica = GLOBUS_FALSE;
        globus_gfs_log_message(GLOBUS_GFS_LOG_WARN, "rolling the dice\n");
        idx = random() % globus_list_size(my_handle->nodelist);
        node = my_handle->nodelist;
        for(;idx--;)
            node = globus_list_rest(node);
    } else if (!hostname) {
        globus_gfs_log_message(GLOBUS_GFS_LOG_WARN, "no available nodes to use\n");
        return GLOBUS_FAILURE;
    }
    
    globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "remote node: %s\n", node->datum);
    my_handle->session_info.host_id = node->datum; /* host_id */

    node_info = globus_malloc(sizeof(globus_l_gfs_remote_node_info_t));
    memset(node_info, 0, sizeof(globus_l_gfs_remote_node_info_t));
    node_info->callback = callback;
    node_info->user_arg = user_arg;
    node_info->my_handle = my_handle;

    result = globus_gfs_ipc_handle_obtain(
                &my_handle->session_info,
                &globus_gfs_ipc_default_iface,
                globus_l_gfs_remote_node_request_kickout,
                node_info,
                globus_l_gfs_remote_ipc_error_cb,
                my_handle);

    if(result != GLOBUS_SUCCESS) {
                node_info->cached_result = result;
                globus_callback_register_oneshot(
                    NULL,
                    NULL,
                    globus_l_gfs_remote_node_error_kickout,
                    node_info);
    }

    return GLOBUS_SUCCESS;
}

static void globus_l_gfs_remote_data_info_free(
		globus_gfs_data_info_t *            data_info)
{
    int                                 idx;

    if(data_info->subject != NULL)
        globus_free(data_info->subject);
    if(data_info->interface != NULL)
        globus_free(data_info->interface);
    if(data_info->pathname != NULL)
        globus_free(data_info->pathname);
    if(data_info->contact_strings != NULL) {
        for(idx = 0; idx < data_info->cs_count; idx++)
            globus_free((char *)data_info->contact_strings[idx]);
        globus_free(data_info->contact_strings);
    }
}

static void globus_l_gfs_ipc_command_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     ipc_result,
		globus_gfs_finished_info_t *        reply,
		void *                              user_arg)
{
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_l_gfs_remote_node_info_t *   node_info;

    GlobusGFSName(globus_l_gfs_ipc_command_cb);

    bounce_info = (globus_l_gfs_remote_ipc_bounce_t *) user_arg;
    node_info = bounce_info->node_info;

    globus_gridftp_server_operation_finished(bounce_info->op, reply->result, reply);

    globus_free(bounce_info);
    globus_free(node_info);
}

static void globus_l_gfs_ipc_passive_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     ipc_result,
		globus_gfs_finished_info_t *        reply,
		void *                              user_arg)
{
    globus_gfs_finished_info_t          finished_info;
    globus_bool_t                       finished = GLOBUS_FALSE;
    int                                 ndx;
    dmlite_handle_t *                   my_handle;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_l_gfs_remote_node_info_t *   node_info;

    GlobusGFSName(globus_l_gfs_ipc_passive_cb);

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;
    bounce_info = node_info->bounce;
    my_handle = bounce_info->my_handle;

    if(reply->result != GLOBUS_SUCCESS)
    {
        bounce_info->cached_result = reply->result;
    }
    else
    {
        /* XXX this is suspect if we chain DSIs another step */
        node_info->cs = globus_libc_strdup(
            reply->info.data.contact_strings[0]);
        node_info->data_arg = reply->info.data.data_arg;
    }

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        bounce_info->nodes_pending--;
        if(ipc_result == GLOBUS_SUCCESS)
            bounce_info->nodes_obtained++;

        if(!bounce_info->nodes_pending && !bounce_info->nodes_requesting)
        {
            finished = GLOBUS_TRUE;
            if(bounce_info->nodes_obtained == 0)
                goto error;

            memcpy(&finished_info, reply, sizeof(globus_gfs_finished_info_t));

            finished_info.info.data.data_arg = bounce_info->node_info;
            finished_info.info.data.cs_count = bounce_info->nodes_obtained;
            finished_info.info.data.contact_strings = (const char **)
              globus_calloc(sizeof(char *), finished_info.info.data.cs_count);

            ndx = 0;

            if(node_info != NULL)
            {
                node_info->stripe_count = 1;

                /* XXX handle case where cs_count from a single node > 1 */
                finished_info.info.data.contact_strings[ndx] = node_info->cs;
                node_info->cs = NULL;

                if(node_info->info && node_info->info_needs_free)
                {
                    globus_free(node_info->info);
                    node_info->info = NULL;
                    node_info->info_needs_free = GLOBUS_FALSE;
                }
                ndx++;
            }
            globus_assert(ndx == finished_info.info.data.cs_count);
        }
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    if(finished)
    {
        globus_gridftp_server_operation_finished(
            bounce_info->op,
            finished_info.result,
            &finished_info);

        for(ndx = 0; ndx < finished_info.info.data.cs_count; ndx++)
            globus_free((void *) finished_info.info.data.contact_strings[ndx]);
        globus_free(finished_info.info.data.contact_strings);
        globus_free(bounce_info);
    }

    return;

error:
    globus_mutex_unlock(&my_handle->gfs_mutex);
    globus_assert(finished && ipc_result != GLOBUS_SUCCESS);
    GlobusGFSErrorOpFinished(
        bounce_info->op, GLOBUS_GFS_OP_PASSIVE, ipc_result);
    globus_free(bounce_info);
}

static void globus_l_gfs_ipc_active_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     ipc_result,
		globus_gfs_finished_info_t *        reply,
		void *                              user_arg)
{
    globus_bool_t                       finished = GLOBUS_FALSE;
    int                                 ndx;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_l_gfs_remote_node_info_t *   node_info;
    globus_gfs_finished_info_t          finished_info;
    globus_gfs_data_info_t *            info;
    dmlite_handle_t *                   my_handle;

    GlobusGFSName(globus_l_gfs_ipc_active_cb);

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;
    bounce_info = node_info->bounce;
    node_info->data_arg = reply->info.data.data_arg;
    my_handle = bounce_info->my_handle;

    node_info->stripe_count = 1;

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        bounce_info->nodes_pending--;
        if(ipc_result == GLOBUS_SUCCESS)
            bounce_info->nodes_obtained++;

        if(!bounce_info->nodes_pending && !bounce_info->nodes_requesting)
        {
            finished = GLOBUS_TRUE;
            if(bounce_info->nodes_obtained == 0)
                goto error;

            memcpy(&finished_info, reply, sizeof(globus_gfs_finished_info_t));

            finished_info.info.data.data_arg = bounce_info->node_info;

            if(node_info->info && node_info->info_needs_free)
            {
                info = (globus_gfs_data_info_t *) node_info->info;
                for(ndx = 0; ndx < info->cs_count; ndx++)
                    globus_free((void *) info->contact_strings[ndx]);
                globus_free(info->contact_strings);
                globus_free(node_info->info);
                node_info->info = NULL;
                node_info->info_needs_free = GLOBUS_FALSE;
            }
        }
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    if(finished)
    {
	if(my_handle->active_delay) {
		/* return to the original callback */
		my_handle->active_delay = GLOBUS_FALSE;
		globus_l_gfs_remote_data_info_free(my_handle->active_data_info);
		my_handle->active_transfer_info->data_arg = bounce_info->node_info;
		my_handle->active_callback(
			my_handle->active_op,
			my_handle->active_transfer_info,
			my_handle->active_user_arg);
	} else {
		globus_gridftp_server_operation_finished(
			bounce_info->op,
			finished_info.result,
			&finished_info);
	}
        globus_free(bounce_info);
    }

    return;

error:
    globus_assert(finished && ipc_result != GLOBUS_SUCCESS);
    if(my_handle->active_delay) {
	/* we have to fake a failure of a different operation */
	my_handle->active_delay = GLOBUS_FALSE;
	globus_l_gfs_remote_data_info_free(my_handle->active_data_info);
	globus_gridftp_server_finished_command(my_handle->active_op, ipc_result, GLOBUS_NULL);
    } else {
	GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_ACTIVE, ipc_result);
    }
    globus_free(bounce_info);
    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_ipc_transfer_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     ipc_result,
		globus_gfs_finished_info_t *        reply,
		void *                              user_arg)
{
    dmlite_handle_t *      my_handle;
    struct dmlite_context *             my_context;
    globus_l_gfs_remote_node_info_t *   node_info;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_gfs_finished_info_t          finished_info;
    globus_gfs_operation_t              op;
    globus_bool_t                       finish = GLOBUS_FALSE;
    globus_result_t                     result;
    int                                 ctx_err;

    GlobusGFSName(globus_l_gfs_ipc_transfer_cb);

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;
    bounce_info = node_info->bounce;
    my_handle = bounce_info->my_handle;

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        bounce_info->nodes_pending--;
        if(reply->result != GLOBUS_SUCCESS)
            bounce_info->cached_result = reply->result;

        if(!bounce_info->nodes_pending && !bounce_info->nodes_requesting)
        {
            if(my_handle->cur_result == GLOBUS_SUCCESS)
                my_handle->cur_result = bounce_info->cached_result;

            memset(&finished_info, 0, sizeof(globus_gfs_finished_info_t));
            finished_info.type = reply->type;
            finished_info.id = reply->id;
            finished_info.code = reply->code;
            finished_info.msg = reply->msg;
            finished_info.result = bounce_info->cached_result;
            finish = GLOBUS_TRUE;
            op = bounce_info->op;

            if(!bounce_info->events_enabled)
            {
                if(node_info->info && node_info->info_needs_free)
                {
                    globus_free(node_info->info);
                    node_info->info = NULL;
                    node_info->info_needs_free = GLOBUS_FALSE;
                }
                if(bounce_info->eof_count != NULL)
                {
                    globus_free(bounce_info->eof_count);
                }
                globus_free(bounce_info);
            }

            /* Finished adding a new replica */
            if(my_handle->is_replica && my_handle->mode == DMLITE_FILEMODE_WRITING) {
                dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "%sregistering replica :: %s",
                    my_handle->cur_result != GLOBUS_SUCCESS ? "un" : "", my_handle->pfn);
                result = GLOBUS_SUCCESS;
                if((my_context = dmlite_get_context(my_handle, &ctx_err))) {
                    dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "transfer_cb :: putdone %s", my_handle->pfn);
                    if(dmlite_gfs_putdone(my_context, my_handle, my_handle->cur_result == GLOBUS_SUCCESS))
                        result = dmlite_error2gfs_result(_gfs_name, my_handle, my_context);
                    dmlite_context_free(my_context);
                } else {
                    result = posix_error2gfs_result(_gfs_name, my_handle, ctx_err, "failed to get context");
                }
                if(my_handle->cur_result == GLOBUS_SUCCESS && result != GLOBUS_SUCCESS)
                    my_handle->cur_result = result;
                globus_mutex_unlock(&my_handle->rep_mutex);
            }
        }
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    if(finish)
    {
        globus_gridftp_server_operation_finished(
            op,
            finished_info.result,
            &finished_info);
    }
}

static void globus_l_gfs_ipc_event_cb(
		globus_gfs_ipc_handle_t             ipc_handle,
		globus_result_t                     ipc_result,
		globus_gfs_event_info_t *           reply,
		void *                              user_arg)
{
    dmlite_handle_t *      my_handle;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_bool_t                       finish = GLOBUS_FALSE;
    globus_l_gfs_remote_node_info_t *   node_info;
    globus_gfs_transfer_info_t *        info;
    globus_gfs_event_info_t             event_info;
    globus_result_t                     result;
    int                                 ctr;

    GlobusGFSName(globus_l_gfs_ipc_event_cb);

    node_info = (globus_l_gfs_remote_node_info_t *) user_arg;
    bounce_info = node_info->bounce;
    my_handle = bounce_info->my_handle;

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        switch(reply->type)
        {
            case GLOBUS_GFS_EVENT_TRANSFER_BEGIN:
                node_info->event_arg = reply->event_arg;
                node_info->event_mask = reply->event_mask;

                bounce_info->begin_event_pending--;
                if(!bounce_info->begin_event_pending)
                {
                    if(!bounce_info->nodes_requesting)
                    {
                        bounce_info->events_enabled = GLOBUS_TRUE;
                        reply->event_arg = bounce_info;
                        reply->event_mask = 
                            GLOBUS_GFS_EVENT_TRANSFER_ABORT | 
                            GLOBUS_GFS_EVENT_TRANSFER_COMPLETE |
                            GLOBUS_GFS_EVENT_BYTES_RECVD |
                            GLOBUS_GFS_EVENT_RANGES_RECVD;

                        globus_gridftp_server_operation_event(
                            bounce_info->op,
                            GLOBUS_SUCCESS,
                            reply);
                    }
                }
                break;
            case GLOBUS_GFS_EVENT_TRANSFER_CONNECTED:
                bounce_info->event_pending--;
                if(!bounce_info->event_pending && 
                    !bounce_info->nodes_requesting)
                {
                    finish = GLOBUS_TRUE;
                }
                break;
            case GLOBUS_GFS_EVENT_PARTIAL_EOF_COUNT:
                info = (globus_gfs_transfer_info_t *) node_info->info;
                for(ctr = 0; ctr < reply->node_count; ctr++)
                {
                    bounce_info->eof_count[ctr] += reply->eof_count[ctr];
                }
                bounce_info->partial_eof_counts++;
                if(bounce_info->partial_eof_counts + 1 == 
                    bounce_info->node_count && !bounce_info->finished)
                {
                    memset(&event_info, 0, sizeof(globus_gfs_event_info_t));
                    event_info.type = GLOBUS_GFS_EVENT_FINAL_EOF_COUNT;
                    event_info.event_arg = node_info->event_arg;
                    event_info.eof_count = bounce_info->eof_count;
                    event_info.node_count = bounce_info->partial_eof_counts + 1;
                    result = globus_gfs_ipc_request_transfer_event(
                        ipc_handle,
                        &event_info);
                    bounce_info->final_eof++;
                }
                break;
            default:
                if(!bounce_info->event_pending || 
                    reply->type == GLOBUS_GFS_EVENT_BYTES_RECVD ||
                    reply->type == GLOBUS_GFS_EVENT_RANGES_RECVD)
                {
                    finish = GLOBUS_TRUE;
                }
                break;
        }
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    if(finish)
    {
        reply->event_arg = bounce_info;
        globus_gridftp_server_operation_event(
            bounce_info->op,
            GLOBUS_SUCCESS,
            reply);
    }
}

static globus_result_t globus_l_gfs_remote_init_bounce_info(
		globus_l_gfs_remote_ipc_bounce_t ** bounce,
		globus_gfs_operation_t              op,
		void *                              state,
		dmlite_handle_t *                   my_handle)
{
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_result_t                     result = GLOBUS_SUCCESS;

    GlobusGFSName(globus_l_gfs_remote_init_bounce_info);

    bounce_info = (globus_l_gfs_remote_ipc_bounce_t *)
        globus_calloc(1, sizeof(globus_l_gfs_remote_ipc_bounce_t));

    if(!bounce_info) {
        result = GlobusGFSErrorMemory("bounce_info");
        goto error;
    }

    bounce_info->op = op;
    bounce_info->state = state;
    bounce_info->my_handle = my_handle;
    *bounce = bounce_info;

error:
    return result;
}

static void globus_l_gfs_remote_active_kickout(
		globus_l_gfs_remote_node_info_t *   node_info,
		globus_result_t                     result,
		void *                              user_arg)
{
    globus_bool_t                       finish = GLOBUS_FALSE;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_gfs_data_info_t *            data_info;
    globus_gfs_data_info_t *            new_data_info;
    dmlite_handle_t *                   my_handle;
    struct dmlite_context *		my_context;
    int					ctx_err;

    GlobusGFSName(globus_l_gfs_remote_active_kickout);

    bounce_info = (globus_l_gfs_remote_ipc_bounce_t *) user_arg;
    data_info = (globus_gfs_data_info_t *) bounce_info->state;
    my_handle = bounce_info->my_handle;

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        bounce_info->nodes_requesting--;

        if(result != GLOBUS_SUCCESS)
            goto error;

        node_info->bounce = bounce_info;

        new_data_info = (globus_gfs_data_info_t *)
            globus_calloc(1, sizeof(globus_gfs_data_info_t));

        memcpy(new_data_info, bounce_info->state, sizeof(globus_gfs_data_info_t));

        new_data_info->cs_count = 1;
        new_data_info->contact_strings = (const char **) calloc(1, sizeof(char *));
        new_data_info->contact_strings[0] = globus_libc_strdup(data_info->contact_strings[bounce_info->node_ndx]);

        node_info->info = new_data_info;
        node_info->info_needs_free = GLOBUS_TRUE;
        result = globus_gfs_ipc_request_active_data(
            node_info->ipc_handle,
            new_data_info,
            globus_l_gfs_ipc_active_cb,
            node_info);
        if(result != GLOBUS_SUCCESS)
            goto error;

        node_info->node_ndx = bounce_info->node_ndx;
        bounce_info->node_info = node_info;
        bounce_info->node_ndx++;
        bounce_info->nodes_pending++;
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    return;

error:
    /* if no nodes were obtained and none are outstanding */
    if(bounce_info->nodes_requesting == 0 &&
        bounce_info->nodes_pending == 0 &&
        bounce_info->nodes_obtained == 0)
    {
        finish = GLOBUS_TRUE;
    }

    if(finish)
    {
	if(my_handle->is_replica && my_handle->mode == DMLITE_FILEMODE_WRITING) {
		dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "unregistering replica :: %s", my_handle->pfn);
		if((my_context = dmlite_get_context(my_handle, &ctx_err))) {
			/* no checks as we already handling an error */
			dmlite_gfs_putdone(my_context, my_handle, GLOBUS_FALSE);
			dmlite_context_free(my_context);
		} else {
			result = posix_error2gfs_result(_gfs_name, my_handle, ctx_err, "failed to get context");
		}
		globus_mutex_unlock(&my_handle->rep_mutex);
	}
	if(my_handle->active_delay) {
		/* we have to fake a failure of a different operation */
		my_handle->active_delay = GLOBUS_FALSE;
		globus_l_gfs_remote_data_info_free(my_handle->active_data_info);
		globus_gridftp_server_finished_command(my_handle->active_op, result, GLOBUS_NULL);
	} else {
		GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_ACTIVE, result);
	}
        globus_free(bounce_info);
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_remote_active(
		globus_gfs_operation_t              op,
		globus_gfs_data_info_t *            data_info,
		void *                              user_arg)
{
    globus_gfs_data_info_t *            new_data_info;
    dmlite_handle_t *                   my_handle;
    globus_gfs_finished_info_t          finished_info;
    int					idx;

    GlobusGFSName(globus_l_gfs_remote_active);

    my_handle = (dmlite_handle_t *) user_arg;

    globus_mutex_lock(&my_handle->gfs_mutex);
    my_handle->cur_result = GLOBUS_SUCCESS;
    my_handle->active_delay = GLOBUS_TRUE;

    new_data_info = (globus_gfs_data_info_t *) globus_calloc(1, sizeof(globus_gfs_data_info_t));
    memcpy(new_data_info, data_info, sizeof(globus_gfs_data_info_t));
    new_data_info->subject = globus_libc_strdup(data_info->subject);
    new_data_info->interface = globus_libc_strdup(data_info->interface);
    new_data_info->pathname = globus_libc_strdup(data_info->pathname);
    new_data_info->contact_strings = (const char **) calloc(data_info->cs_count, sizeof(char *));
    for(idx = 0; idx < data_info->cs_count; idx++)
        new_data_info->contact_strings[idx] = globus_libc_strdup(data_info->contact_strings[idx]);

    my_handle->active_data_info = new_data_info; /* It must be freed later */

    /* we must return OK here and initiate active connection from
       send()/recv()/list() when file name and remote node are known */

    memset(&finished_info, 0, sizeof(globus_gfs_finished_info_t));
    finished_info.type = GLOBUS_GFS_OP_ACTIVE;
    finished_info.result = GLOBUS_SUCCESS;
    finished_info.info.data.bi_directional = GLOBUS_TRUE;

    globus_gridftp_server_operation_finished(op, finished_info.result, &finished_info);

    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_remote_passive_kickout(
		globus_l_gfs_remote_node_info_t *   node_info,
		globus_result_t                     result,
		void *                              user_arg)
{
    globus_bool_t                       finished = GLOBUS_FALSE;
    dmlite_handle_t *                   my_handle;
    struct dmlite_context *		my_context;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    int					ctx_err;

    GlobusGFSName(globus_l_gfs_remote_passive_kickout);

    bounce_info = (globus_l_gfs_remote_ipc_bounce_t *)  user_arg;
    my_handle = (dmlite_handle_t *) bounce_info->my_handle;

    globus_mutex_lock(&my_handle->gfs_mutex);
    {
        bounce_info->nodes_requesting--;

        if(result != GLOBUS_SUCCESS)
            goto error;

        node_info->bounce = bounce_info;

        result = globus_gfs_ipc_request_passive_data(
            node_info->ipc_handle,
            (globus_gfs_data_info_t *) bounce_info->state,
            globus_l_gfs_ipc_passive_cb,
            node_info);
        if(result != GLOBUS_SUCCESS)
            goto error;

        bounce_info->nodes_pending++;
        bounce_info->node_info = node_info;
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);

    return;

error:
    /* if no nodes were obtained and none are outstanding */
    if(bounce_info->nodes_requesting == 0 &&
        bounce_info->nodes_pending == 0 &&
        bounce_info->nodes_obtained == 0)
    {
        finished = GLOBUS_TRUE;
    }
    if(finished)
    {
	if(my_handle->is_replica && my_handle->mode == DMLITE_FILEMODE_WRITING) {
		dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "unregistering replica :: %s", my_handle->pfn);
		if((my_context = dmlite_get_context(my_handle, &ctx_err))) {
			/* no checks as we already handling an error */
			dmlite_gfs_putdone(my_context, my_handle, GLOBUS_FALSE);
			dmlite_context_free(my_context);
		} else {
			result = posix_error2gfs_result(_gfs_name, my_handle, ctx_err, "failed to get context");
		}
		globus_mutex_unlock(&my_handle->rep_mutex);
        }
        GlobusGFSErrorOpFinished(
            bounce_info->op, GLOBUS_GFS_OP_PASSIVE, result);
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_remote_passive(
		globus_gfs_operation_t              op,
		globus_gfs_data_info_t *            data_info,
		void *                              user_arg)
{
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_result_t                     result;
    dmlite_handle_t *			my_handle;
    char *				cmd;

    GlobusGFSName(globus_l_gfs_remote_passive);

    my_handle = (dmlite_handle_t *) user_arg;

    result = globus_l_gfs_remote_init_bounce_info(
        &bounce_info, op, data_info, my_handle);

    if(result != GLOBUS_SUCCESS)
        goto error;

    bounce_info->nodes_requesting = 1;

/*  **** WARNING ****
    The following API call requires a Globus patch from https://github.com/globus/globus-toolkit/pull/98
    It is available in EPEL Globus release since version 11.8, but is yet missing from official Globus source releases.
*/
    cmd = globus_gfs_data_get_cmd_string(op);
    if(strlen(cmd) > 4)
        *(cmd + 4) = '\0'; /* All commands are 4 chars long */
    globus_gfs_log_message(GLOBUS_GFS_LOG_INFO, "passive mode: %s %s\n", cmd, data_info->pathname);

    globus_mutex_lock(&my_handle->gfs_mutex);
    my_handle->cur_result = GLOBUS_SUCCESS;

    if(!strcmp(cmd, "STOR") || !strcmp(cmd, "ESTO") || !strcmp(cmd, "APPE")) {
        globus_gfs_log_message(GLOBUS_GFS_LOG_DUMP, "incoming transfer\n");
        my_handle->mode = DMLITE_FILEMODE_WRITING;
    } else if(!strcmp(cmd, "RETR") || !strcmp(cmd, "ERET")) {
        globus_gfs_log_message(GLOBUS_GFS_LOG_DUMP, "outgoing transfer\n");
        my_handle->mode = DMLITE_FILEMODE_READING;
    } else {
        globus_gfs_log_message(GLOBUS_GFS_LOG_DUMP, "not delayed\n");
        my_handle->mode = DMLITE_FILEMODE_NONE; /* LIST and PASV in legacy passive mode */
    }
    globus_free(cmd);

    result = globus_l_gfs_remote_node_request(
            my_handle,
            data_info->pathname,
            globus_l_gfs_remote_passive_kickout,
            bounce_info);

    globus_mutex_unlock(&my_handle->gfs_mutex);

    if(result == GLOBUS_SUCCESS)
        return;

    globus_free(bounce_info);

error:
    GlobusGFSErrorOpFinished(op, GLOBUS_GFS_OP_PASSIVE, result);
}

static void globus_l_gfs_remote_list(
		globus_gfs_operation_t              op,
		globus_gfs_transfer_info_t *        transfer_info,
		void *                              user_arg)
{
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    globus_result_t                     result;
    dmlite_handle_t *                   my_handle;
    globus_l_gfs_remote_node_info_t *   node_info;

    GlobusGFSName(globus_l_gfs_remote_list);

    my_handle = (dmlite_handle_t *) user_arg;

    /* This mutex is used as a semaphore */
    if(my_handle->is_replica) {
        globus_mutex_lock(&my_handle->rep_mutex);
        globus_mutex_unlock(&my_handle->rep_mutex);
    }

    globus_mutex_lock(&my_handle->gfs_mutex);

    if(my_handle->active_delay) {
	/* Request active connection from the right node first */

	my_handle->mode = DMLITE_FILEMODE_NONE;

	result = globus_l_gfs_remote_init_bounce_info(
		&bounce_info, op, my_handle->active_data_info, my_handle);

	if(result != GLOBUS_SUCCESS)
		goto error;

	bounce_info->nodes_requesting = 1;

	result = globus_l_gfs_remote_node_request(
		my_handle,
		transfer_info->pathname,
		globus_l_gfs_remote_active_kickout,
		bounce_info);

	if(result != GLOBUS_SUCCESS) {
		globus_free(bounce_info);
		goto error;
	}

	my_handle->active_callback = globus_l_gfs_remote_list;
	my_handle->active_op = op;
	my_handle->active_transfer_info = transfer_info;
	my_handle->active_user_arg = user_arg;

	globus_mutex_unlock(&my_handle->gfs_mutex);

	return;
	/* Will be back here from active callback with delayed flag off */
    }

    /* XXX it appears no lock is needed here */
    result = globus_l_gfs_remote_init_bounce_info(
        &bounce_info, op, transfer_info, my_handle);

    node_info = (struct globus_l_gfs_remote_node_info_s *) 
        transfer_info->data_arg;

    transfer_info->data_arg = node_info->data_arg;
    transfer_info->stripe_count = 1;
    transfer_info->node_ndx = 0;
    transfer_info->node_count = 1;
    bounce_info->node_info = node_info;
    bounce_info->event_pending = 1;
    bounce_info->begin_event_pending = 1;
    bounce_info->nodes_pending = 1;
    bounce_info->node_count = 1;
    node_info->info = NULL;
    node_info->info_needs_free = GLOBUS_FALSE;
    node_info->bounce = bounce_info;

    result = globus_gfs_ipc_request_list(
        node_info->ipc_handle,
        transfer_info,
        globus_l_gfs_ipc_transfer_cb,
        globus_l_gfs_ipc_event_cb,
        node_info);

    if(result != GLOBUS_SUCCESS)
        goto error;

    globus_mutex_unlock(&my_handle->gfs_mutex);

    return;

error:
    globus_mutex_unlock(&my_handle->gfs_mutex);
    GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_TRANSFER, result);
}

static void globus_l_gfs_remote_recv(
		globus_gfs_operation_t              op,
		globus_gfs_transfer_info_t *        transfer_info,
		void *                              user_arg)
{
	globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
	globus_result_t                     result;
	dmlite_handle_t *                   my_handle;
	globus_l_gfs_remote_node_info_t *   node_info;
	globus_gfs_transfer_info_t *        new_transfer_info;

	GlobusGFSName(globus_l_gfs_remote_recv);

	my_handle = (dmlite_handle_t *) user_arg;

	/* This mutex is used as a semaphore */
	if(my_handle->is_replica) {
		globus_mutex_lock(&my_handle->rep_mutex);
		globus_mutex_unlock(&my_handle->rep_mutex);
	}

	globus_mutex_lock(&my_handle->gfs_mutex);

	if(my_handle->active_delay) {
	/* Request active connection from the right node first */

		my_handle->mode = DMLITE_FILEMODE_WRITING;

		result = globus_l_gfs_remote_init_bounce_info(
			&bounce_info, op, my_handle->active_data_info, my_handle);

		if(result != GLOBUS_SUCCESS)
			goto error;

		bounce_info->nodes_requesting = 1;

		result = globus_l_gfs_remote_node_request(
			my_handle,
			transfer_info->pathname,
			globus_l_gfs_remote_active_kickout,
			bounce_info);

		if(result != GLOBUS_SUCCESS) {
			globus_free(bounce_info);
			goto error;
		}

		my_handle->active_transfer_info = transfer_info;
		my_handle->active_op = op;
		my_handle->active_user_arg = user_arg;
		my_handle->active_callback = globus_l_gfs_remote_recv;

		globus_mutex_unlock(&my_handle->gfs_mutex);

		return;
	/* Will be back here from active callback with delayed flag off */
	}

        result = globus_l_gfs_remote_init_bounce_info(
            &bounce_info, op, transfer_info, my_handle);

        node_info = (struct globus_l_gfs_remote_node_info_s *) 
            transfer_info->data_arg;

        bounce_info->nodes_requesting = 1;
        bounce_info->node_count = 1;
        bounce_info->node_info = node_info;

        new_transfer_info = (globus_gfs_transfer_info_t *)
            globus_calloc(1, sizeof(globus_gfs_transfer_info_t));
        memcpy(new_transfer_info,transfer_info,
            sizeof(globus_gfs_transfer_info_t));

	if(my_handle->is_replica) {
		new_transfer_info->pathname = globus_libc_strdup(my_handle->pfn);
		/* This mutex ensures that dmlite_donewriting() is called before any other callback */
		globus_mutex_lock(&my_handle->rep_mutex);
	}

	dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "recv :: requesting transfer :: %s", new_transfer_info->pathname);

        new_transfer_info->data_arg = node_info->data_arg;
        new_transfer_info->node_count = 1;
        new_transfer_info->stripe_count = node_info->stripe_count;
        new_transfer_info->node_ndx = 0;
        node_info->info = new_transfer_info;
        node_info->info_needs_free = GLOBUS_TRUE;
        node_info->bounce = bounce_info;

        result = globus_gfs_ipc_request_recv(
            node_info->ipc_handle,
            new_transfer_info,
            globus_l_gfs_ipc_transfer_cb,
            globus_l_gfs_ipc_event_cb,
            node_info); 
        if(result != GLOBUS_SUCCESS)
            goto error;
        /* could maybe get away with no lock if we moved the next few lines
            above the request.  we would have to then assume that the 
            values were meaningless under error.  This way is more 
            consistant and the lock is not very costly */
        bounce_info->nodes_pending++;
        bounce_info->event_pending++;
        bounce_info->begin_event_pending++;
        bounce_info->nodes_requesting--;

	globus_mutex_unlock(&my_handle->gfs_mutex);

	return;

error:

	my_handle->cur_result = result;
	globus_mutex_unlock(&my_handle->gfs_mutex);
	GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_TRANSFER, result);
}

static void globus_l_gfs_remote_send(
		globus_gfs_operation_t              op,
		globus_gfs_transfer_info_t *        transfer_info,
		void *                              user_arg)
{
	globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
	globus_result_t                     result;
	dmlite_handle_t *                   my_handle;
	globus_l_gfs_remote_node_info_t *   node_info;
	globus_gfs_transfer_info_t *        new_transfer_info;

	GlobusGFSName(globus_l_gfs_remote_send);

	my_handle = (dmlite_handle_t *) user_arg;

	/* This mutex is used as a semaphore */
	if(my_handle->is_replica) {
		globus_mutex_lock(&my_handle->rep_mutex);
		globus_mutex_unlock(&my_handle->rep_mutex);
	}

	globus_mutex_lock(&my_handle->gfs_mutex);

	if(my_handle->active_delay) {
	/* Request active connection from the right node first */

		my_handle->mode = DMLITE_FILEMODE_READING;

		result = globus_l_gfs_remote_init_bounce_info(
			&bounce_info, op, my_handle->active_data_info, my_handle);

		if(result != GLOBUS_SUCCESS)
			goto error;

		bounce_info->nodes_requesting = 1;

		result = globus_l_gfs_remote_node_request(
			my_handle,
			transfer_info->pathname,
			globus_l_gfs_remote_active_kickout,
			bounce_info);

		if(result != GLOBUS_SUCCESS) {
			globus_free(bounce_info);
			goto error;
		}

		my_handle->active_transfer_info = transfer_info;
		my_handle->active_op = op;
		my_handle->active_user_arg = user_arg;
		my_handle->active_callback = globus_l_gfs_remote_send;

		globus_mutex_unlock(&my_handle->gfs_mutex);

		return;
	/* Will be back here from active callback with delayed flag off */
	}

        result = globus_l_gfs_remote_init_bounce_info(
            &bounce_info, op, transfer_info, my_handle);

        node_info = (struct globus_l_gfs_remote_node_info_s *)
            transfer_info->data_arg;

        bounce_info->eof_count = (int *)
            globus_calloc(1, sizeof(int) + 1);

        bounce_info->nodes_requesting = 1;
        bounce_info->node_count = 1;
        bounce_info->sending = GLOBUS_TRUE;
        bounce_info->node_info = node_info;

        new_transfer_info = (globus_gfs_transfer_info_t *)
            globus_calloc(1, sizeof(globus_gfs_transfer_info_t));
        memcpy(new_transfer_info,
            transfer_info, sizeof(globus_gfs_transfer_info_t));

	if(my_handle->is_replica)
		new_transfer_info->pathname = globus_libc_strdup(my_handle->pfn);

	dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "send :: requesting transfer :: %s", new_transfer_info->pathname);

        new_transfer_info->data_arg = node_info->data_arg;
        new_transfer_info->node_count = 1;
        new_transfer_info->stripe_count = node_info->stripe_count;
        new_transfer_info->node_ndx = 0;
        node_info->info = new_transfer_info;
        node_info->info_needs_free = GLOBUS_TRUE;
        node_info->bounce = bounce_info;

        result = globus_gfs_ipc_request_send(
                node_info->ipc_handle,
                new_transfer_info,
                globus_l_gfs_ipc_transfer_cb,
                globus_l_gfs_ipc_event_cb,
                node_info); 
        if(result != GLOBUS_SUCCESS)
            goto error;
        bounce_info->nodes_pending++;
        bounce_info->event_pending++;
        bounce_info->begin_event_pending++;
        bounce_info->nodes_requesting--;

	globus_mutex_unlock(&my_handle->gfs_mutex);

	return;

error:
	my_handle->cur_result = result;
	globus_mutex_unlock(&my_handle->gfs_mutex);
	GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_TRANSFER, result);
}

static void globus_l_gfs_remote_data_destroy(
		void *                              data_arg,
		void *                              user_arg)
{
    globus_result_t                     result;
    dmlite_handle_t *                   my_handle;
    globus_l_gfs_remote_node_info_t *   node_info;

    GlobusGFSName(globus_l_gfs_remote_data_destroy);

    my_handle = (dmlite_handle_t *) user_arg;
    node_info = (struct globus_l_gfs_remote_node_info_s *) data_arg;

    if (node_info == NULL || my_handle == NULL) {
        return;
    }

    globus_mutex_lock(&my_handle->gfs_mutex);

    if(!my_handle->active_delay) {
            result = globus_gfs_ipc_request_data_destroy(node_info->ipc_handle, node_info->data_arg);

            if(result != GLOBUS_SUCCESS)
                globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "IPC error: could not destroy data connection", result);

            node_info->data_arg = NULL;
            node_info->stripe_count = 0;

            globus_l_gfs_remote_node_release(node_info);
    }
    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_remote_trev(
		globus_gfs_event_info_t *           event_info,
		void *                              user_arg)
{
    globus_result_t                     result;
    dmlite_handle_t *                   my_handle;
    struct dmlite_context *		my_context;
    globus_l_gfs_remote_node_info_t *   node_info;
    globus_gfs_event_info_t             new_event_info;
    globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
    int					ctx_err;

    GlobusGFSName(globus_l_gfs_remote_trev);

    bounce_info = (globus_l_gfs_remote_ipc_bounce_t *) event_info->event_arg;
    node_info = bounce_info->node_info;

    memset(&new_event_info, 0, sizeof(globus_gfs_event_info_t));
    new_event_info.type = event_info->type;
    new_event_info.event_arg = node_info->event_arg;

    result = globus_gfs_ipc_request_transfer_event(node_info->ipc_handle, &new_event_info);

    if(result != GLOBUS_SUCCESS)
        globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "IPC error: could not request transfer event", result);

    my_handle = (dmlite_handle_t *) user_arg;
    globus_mutex_lock(&my_handle->gfs_mutex);

    if(event_info->type == GLOBUS_GFS_EVENT_TRANSFER_COMPLETE) {

        if(node_info->info && node_info->info_needs_free)
        {
            globus_free(node_info->info);
            node_info->info = NULL;
            node_info->info_needs_free = GLOBUS_FALSE;
        }
        node_info->event_arg = NULL;
        node_info->event_mask = 0;
        if(bounce_info->eof_count != NULL)
        {
            globus_free(bounce_info->eof_count);
        }
        globus_free(bounce_info);

        /* Finished adding a new replica *

	if(my_handle->is_replica && my_handle->mode == DMLITE_FILEMODE_WRITING) {
		dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "%sregistering replica :: %s",
		    my_handle->cur_result != GLOBUS_SUCCESS ? "un" : "", my_handle->pfn);
		result = GLOBUS_SUCCESS;
		if((my_context = dmlite_get_context(my_handle, &ctx_err))) {
			if(dmlite_gfs_putdone(my_context, my_handle, my_handle->cur_result == GLOBUS_SUCCESS))
				result = dmlite_error2gfs_result(_gfs_name, my_handle, my_context);
			dmlite_context_free(my_context);
		} else {
			result = posix_error2gfs_result(_gfs_name, my_handle, ctx_err, "failed to get context");
		}
		if(my_handle->cur_result == GLOBUS_SUCCESS && result != GLOBUS_SUCCESS)
			my_handle->cur_result = result;
		globus_mutex_unlock(&my_handle->rep_mutex);
        } */
    }

    globus_mutex_unlock(&my_handle->gfs_mutex);
}

static void globus_l_gfs_remote_command_kickout(
		globus_l_gfs_remote_node_info_t *   node_info,
		globus_result_t                     result,
		void *                              user_arg)
{
	globus_l_gfs_remote_ipc_bounce_t *  bounce_info;

	GlobusGFSName(globus_l_gfs_remote_command_kickout);

	bounce_info = (globus_l_gfs_remote_ipc_bounce_t *) user_arg;

	if(result != GLOBUS_SUCCESS)
		goto error;

	globus_mutex_lock(&bounce_info->my_handle->gfs_mutex);

	if(bounce_info->my_handle->is_replica) {
		globus_free(((globus_gfs_command_info_t *) bounce_info->state)->pathname);
		((globus_gfs_command_info_t *) bounce_info->state)->pathname = globus_libc_strdup(bounce_info->my_handle->pfn);
	}

	globus_mutex_unlock(&bounce_info->my_handle->gfs_mutex);

	result = globus_gfs_ipc_request_command(node_info->ipc_handle,
		(globus_gfs_command_info_t *) bounce_info->state,
		globus_l_gfs_ipc_command_cb, bounce_info);

	if(result != GLOBUS_SUCCESS)
		goto error;

	return;

error:
	globus_gfs_log_result(GLOBUS_GFS_LOG_ERR, "IPC error: could not connect to remote node", result);
	GlobusGFSErrorOpFinished(bounce_info->op, GLOBUS_GFS_OP_COMMAND, result);
	globus_free(bounce_info);

}

static void globus_l_gfs_remote_command(
		globus_gfs_operation_t		op,
		globus_gfs_command_info_t *	cmd_info,
		void *				user_arg)
{
	globus_l_gfs_remote_ipc_bounce_t *  bounce_info;
	globus_result_t                     result = GLOBUS_SUCCESS;
	dmlite_handle_t *                   my_handle = (dmlite_handle_t *)user_arg;
	const char *                        localpath = dmlite_gfs_fixpath(cmd_info->pathname, GLOBUS_FALSE);
	const char *                        rfn = dmlite_gfs_fixpath(cmd_info->pathname, GLOBUS_TRUE);

	GlobusGFSName(globus_l_gfs_remote_command);

	/* We only need to redirect non-DOME checksum requests */
	if(cmd_info->command != GLOBUS_GFS_CMD_CKSM || (my_handle->dome_checksum && rfn == localpath)) {
		globus_l_gfs_dmlite_command(op, cmd_info, user_arg);
		return;
	}

	dmlite_gfs_log(my_handle, GLOBUS_GFS_LOG_INFO, "requesting checksum :: %s", localpath);

	/* This mutex is used as a semaphore */
	if(my_handle->is_replica) {
		globus_mutex_lock(&my_handle->rep_mutex);
		globus_mutex_unlock(&my_handle->rep_mutex);
	}

	/* Get a dmlite_context object */
	globus_mutex_lock(&my_handle->gfs_mutex);

	my_handle->mode = DMLITE_FILEMODE_READING;

	result = globus_l_gfs_remote_init_bounce_info(&bounce_info, op, cmd_info, my_handle);

	if(result != GLOBUS_SUCCESS)
		goto error;

	bounce_info->nodes_requesting = 1;

	result = globus_l_gfs_remote_node_request(
		my_handle,
		cmd_info->pathname,
		globus_l_gfs_remote_command_kickout,
		bounce_info);

	if(result != GLOBUS_SUCCESS)
		goto error;

	globus_mutex_unlock(&my_handle->gfs_mutex);
	return;

error:
	my_handle->cur_result = result;
	globus_mutex_unlock(&my_handle->gfs_mutex);
	GlobusGFSErrorOpFinished(op, GLOBUS_GFS_OP_COMMAND, result);
	globus_free(bounce_info);
}

/*
 *  Version information.
*/

globus_version_t dmlite_gridftp_local_version =
{
	0, /* major version number */
	3, /* minor version number */
	1174988050,
	0 /* branch ID */
};

/*
 *  Interface definition for local mode.
*/

static globus_gfs_storage_iface_t globus_l_gfs_dmlite_dsi_iface =
{
	GLOBUS_GFS_DSI_DESCRIPTOR_BLOCKING | GLOBUS_GFS_DSI_DESCRIPTOR_SENDER,
	globus_l_gfs_dmlite_session_start,
	globus_l_gfs_dmlite_session_end,
	NULL, /* list */
	globus_l_gfs_dmlite_send,
	globus_l_gfs_dmlite_recv,
	NULL, /* trev */
	NULL, /* active */
	NULL, /* passive */
	NULL, /* data destroy */
	globus_l_gfs_dmlite_command,
	globus_l_gfs_dmlite_stat,
	NULL,
	NULL
};

/*
 *  Interface definition for remote mode.
*/

static globus_gfs_storage_iface_t globus_l_gfs_remote_dsi_iface =
{
	GLOBUS_GFS_DSI_DESCRIPTOR_BLOCKING | GLOBUS_GFS_DSI_DESCRIPTOR_SENDER,
	globus_l_gfs_dmlite_session_start,
	globus_l_gfs_dmlite_session_end,
	globus_l_gfs_remote_list,
	globus_l_gfs_remote_send,
	globus_l_gfs_remote_recv,
	globus_l_gfs_remote_trev,
	globus_l_gfs_remote_active,
	globus_l_gfs_remote_passive,
	globus_l_gfs_remote_data_destroy,
	globus_l_gfs_remote_command,
	globus_l_gfs_dmlite_stat,
	NULL,
	NULL
};

/*
 *  Module definition.
*/

GlobusExtensionDefineModule(globus_gridftp_server_dmlite) =
{
	"globus_gridftp_server_dmlite",
	globus_l_gfs_dmlite_activate,
	globus_l_gfs_dmlite_deactivate,
	GLOBUS_NULL,
	GLOBUS_NULL,
	&dmlite_gridftp_local_version
};

/*
 *  Called when the dmlite module is activated.
*/

int globus_l_gfs_dmlite_activate()
{
	globus_extension_registry_add(GLOBUS_GFS_DSI_REGISTRY, "dmlite",
		GlobusExtensionMyModule(globus_gridftp_server_dmlite),
		globus_gfs_config_get_string("remote_nodes") ?
			&globus_l_gfs_remote_dsi_iface : &globus_l_gfs_dmlite_dsi_iface);

	return GLOBUS_SUCCESS;
}

/*
 *  Called when the dmlite module is deactivated.
*/

int globus_l_gfs_dmlite_deactivate()
{
	globus_extension_registry_remove(GLOBUS_GFS_DSI_REGISTRY, "dmlite");

	return GLOBUS_SUCCESS;
}
