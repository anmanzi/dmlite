/// @file   DomeAdapterIO.cpp
/// @brief  Filesystem IO with dome
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/pooldriver.h>
#include <errno.h>
#include <sys/uio.h>
#include <iostream>
#include <stdio.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "utils/logger.h"
#include "utils/DomeUtils.h"

#include "DomeAdapterUtils.h"
#include "DomeAdapterHeadCatalog.h"

using namespace dmlite;
using boost::property_tree::ptree;

DomeAdapterHeadCatalogFactory::DomeAdapterHeadCatalogFactory(): davixPool_(&davixFactory_, 256) {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Ctor");
}

DomeAdapterHeadCatalogFactory::~DomeAdapterHeadCatalogFactory()
{
  // Nothing
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " ");
}

void DomeAdapterHeadCatalogFactory::configure(const std::string& key, const std::string& value)  
{
  bool gotit = true;
  LogCfgParm(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, key, value);

  if (key == "DomeHead") {
    domehead_ = value;
  }
  // if parameter starts with "Davix", pass it on to the factory
  else if( key.find("Davix") != std::string::npos) {
    Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Received davix pool parameter: " << key << "," << value);
    davixFactory_.configure(key, value);
  }
  else gotit = false;

  if (gotit)
    LogCfgParm(Logger::Lvl4, Logger::unregistered, "DomeAdapterHeadCatalogFactory", key, value);
}

Catalog* DomeAdapterHeadCatalogFactory::createCatalog(PluginManager* pm)   {
  return new DomeAdapterHeadCatalog(this);
}

DomeAdapterHeadCatalog::DomeAdapterHeadCatalog(DomeAdapterHeadCatalogFactory *factory) :
secCtx_(0), 
factory_(*factory)
{
  // Nothing
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Ctor");
  talker__ = new DomeTalker(factory_.davixPool_, factory_.domehead_, "GET", "dome_access");
}

DomeAdapterHeadCatalog::~DomeAdapterHeadCatalog()
{
  if (talker__)
    delete talker__;
}

std::string DomeAdapterHeadCatalog::getImplId() const throw ()
{
  return "DomeAdapterHeadCatalog";
}

void DomeAdapterHeadCatalog::setSecurityContext(const SecurityContext* ctx)  
{
  this->secCtx_ = ctx;
}

void DomeAdapterHeadCatalog::setStackInstance(StackInstance* si)  
{
  this->si_ = si;
}


void DomeAdapterHeadCatalog::getChecksum(const std::string& path,
                                         const std::string& csumtype,
                                         std::string& csumvalue,
                                         const std::string& pfn,
                                         const bool forcerecalc,
                                         const int waitsecs)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path '"
                                                            << absPath(path) << "', csumtype '"
                                                            << csumtype << "'");
  time_t start = time(0);
  bool recalc = forcerecalc;

  int waitsecs1 = waitsecs;
  if (waitsecs1 == 0) waitsecs1 = 1800;

  int cnt = 0;
  while(true) {
    DomeCredentials dc(secCtx_);
    
    if (!talker__)
      talker__ = new DomeTalker(factory_.davixPool_, factory_.domehead_, "GET", "dome_access");
      
    talker__->setcommand(dc, "GET", "dome_chksum");
    boost::property_tree::ptree params;
    params.put("checksum-type", csumtype);
    params.put("lfn", absPath(path));
    params.put("force-recalc", DomeUtils::bool_to_str(recalc));
    recalc = false; // no force-recalc in subsequent requests

    if(!talker__->execute(params)) {
      throw DmException(EINVAL, talker__->err());
    }

    // checksum calculation in progress
    if(talker__->status() == 202) {
      if(time(0) - start >= waitsecs1)
        throw DmException(EAGAIN, SSTR(waitsecs << "s were not sufficient to checksum '" << csumtype << ":" << absPath(path) << "'. Try again later."));
      if (cnt++ < 4)
        sleep(1);
      else {
        // Don't keep a talker allocated for potentially very long periods
        delete talker__;
        talker__ = 0;
        
        sleep(5);
      }
      continue;
    }

    try {
      csumvalue = talker__->jresp().get<std::string>("checksum");
      return;
    }
    catch(boost::property_tree::ptree_error &e) {
      throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
    }
    catch( ... ) {
      throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
    }
  }
}

std::string DomeAdapterHeadCatalog::getWorkingDir()   {
  return this->cwdPath_;
}

void DomeAdapterHeadCatalog::changeDir(const std::string& path)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. path: '" << path << "'");

  if (path.empty()) {
    this->cwdPath_.clear();
    return;
  }

  this->extendedStat(path,true);
  if (path[0] == '/')
    this->cwdPath_ = path;
  else
    this->cwdPath_ = Url::normalizePath(this->cwdPath_ + "/" + path);
}

DmStatus DomeAdapterHeadCatalog::extendedStat(ExtendedStat &xstat, const std::string& path, bool follow)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "path: " << path << " follow (ignored) :" << follow);


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getstatinfo");
  if(!talker__->execute("lfn", absPath(path))) {
    if(talker__->dmlite_code() == ENOENT) return DmStatus(ENOENT, SSTR(path << " not found"));
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    xstat = ExtendedStat();
    ptree_to_xstat(talker__->jresp(), xstat);
    return DmStatus();
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

ExtendedStat DomeAdapterHeadCatalog::extendedStat(const std::string& path, bool follow)   {
  ExtendedStat ret;
  DmStatus st = this->extendedStat(ret, path, follow);
  if(!st.ok()) throw st.exception();
  return ret;
}

bool DomeAdapterHeadCatalog::access(const std::string& sfn, int mode)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "sfn: '" << sfn << "' mode: '" << mode << "'");

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_access");
  if(!talker__->execute("path", absPath(sfn), "mode", SSTR(mode))) {
    if(talker__->status() == 403) return false;
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  return true;
}

// taken from built-in catalog and slightly modified
bool DomeAdapterHeadCatalog::accessReplica(const std::string& rfn, int mode)   {
  try {
    Replica      replica = this->getReplicaByRFN(rfn);
    // ExtendedStat xstat   = this->extendedStat(replica.fileid);

    bool replicaAllowed = true;
    mode_t perm = 0;

    if (mode & R_OK)
      perm  = S_IREAD;

    if (mode & W_OK) {
      perm |= S_IWRITE;
      replicaAllowed = (replica.status == Replica::kBeingPopulated);
    }

    if (mode & X_OK)
      perm |= S_IEXEC;

    // bool metaAllowed    = (checkPermissions(sec_, xstat.acl, xstat.stat, perm) == 0);
    return replicaAllowed;
  }
  catch (DmException& e) {
    if (e.code() != EACCES) throw;
    return false;
  }
}

Replica DomeAdapterHeadCatalog::getReplicaByRFN(const std::string& rfn)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "rfn: " << rfn);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getreplicainfo");
  if(!talker__->execute("rfn", rfn)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    Replica replica;
    ptree_to_replica(talker__->jresp(), replica);
    return replica;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: '" << e.what() << "'. Contents: '" << talker__->response() << "'"));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

Replica DomeAdapterHeadCatalog::getReplica(int64_t rid)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "rid: " << rid);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getreplicainfo");
  
  if(!talker__->execute("replicaid", SSTR(rid))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    Replica replica;
    ptree_to_replica(talker__->jresp(), replica);
    return replica;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: '" << e.what() << "'. Contents: '" << talker__->response() << "'"));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

std::vector<Replica> DomeAdapterHeadCatalog::getReplicas(const std::string& lfn)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "lfn: " << lfn);


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getreplicavec");
  
  if(!talker__->execute("lfn", absPath(lfn))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    std::vector<Replica> replicas;
    ptree entries = talker__->jresp().get_child("replicas");
    for(ptree::const_iterator it = entries.begin(); it != entries.end(); it++) {
      Replica replica;
      ptree_to_replica(it->second, replica);
      replicas.push_back(replica);
    }
    return replicas;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: '" << e.what() << "'. Contents: '" << talker__->response() << "'"));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterHeadCatalog::updateReplica(const Replica& replica)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "rfn: " << replica.rfn);


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_updatereplica");
  
  boost::property_tree::ptree params;
  params.put("rfn", replica.rfn);
  params.put("replicaid", replica.replicaid);
  params.put("status", replica.status);
  params.put("type", replica.type);
  params.put("setname", replica.setname);
  params.put("xattr", replica.serialize());

  if(!talker__->execute(params)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::addReplica(const Replica& rep)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, replica: '" << rep.rfn << "'");

  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_addreplica");
  
  boost::property_tree::ptree params;
  params.put("rfn", rep.rfn);
  params.put("status", rep.status);
  params.put("type", rep.type);
  params.put("setname", rep.setname);
  params.put("xattr", rep.serialize());

  if(!talker__->execute(params)) {
    throw DmException(EINVAL, talker__->err());
  }

}

void DomeAdapterHeadCatalog::deleteReplica(const Replica &rep)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, rfn: '" << rep.rfn << "'");


  
    DomeCredentials dc(secCtx_);
    talker__->setcommand(dc, "POST", "dome_delreplica");
  

    boost::property_tree::ptree params;
    params.put("server", DomeUtils::server_from_rfio_syntax(rep.rfn));
    params.put("pfn", DomeUtils::pfn_from_rfio_syntax(rep.rfn));


    if(!talker__->execute(params)) {
      throw DmException(EINVAL, talker__->err());
    }


}

void DomeAdapterHeadCatalog::symlink(const std::string &target, const std::string &link)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, target: '" << target << "', link: '" << link << "'");

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_symlink");
  
  if(!talker__->execute("target", absPath(target), "link", absPath(link))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::makeDir(const std::string& path, mode_t mode)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << path << "', mode: " << mode);


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_makedir");
  if(!talker__->execute("path", absPath(path), "mode", SSTR(mode))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::create(const std::string& path, mode_t mode)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << path << "', mode: " << mode);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_create");
  
  if(!talker__->execute("path", absPath(path), "mode", SSTR(mode))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::removeDir(const std::string& path)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << absPath(path));


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_removedir");
  
  if(!talker__->execute("path", absPath(path))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::setGuid(const std::string& path, const std::string& guid)   {
  throw DmException(ENOTSUP, "Not supported");
}

void DomeAdapterHeadCatalog::setOwner(const std::string& path, uid_t uid, gid_t gid, bool follow)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << absPath(path) << "', uid: " << uid << ", gid: " << gid);


  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setowner");
  
  boost::property_tree::ptree params;
  params.put("path", absPath(path));
  params.put("uid", SSTR(uid));
  params.put("gid", SSTR(gid));
  params.put("follow", DomeUtils::bool_to_str(follow));

  if(!talker__->execute(params)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::setMode(const std::string& path, mode_t mode)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << absPath(path) << "', mode: " << mode);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setmode");
  
  if(!talker__->execute("path", absPath(path), "mode", SSTR(mode))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::setSize(const std::string& path, size_t newSize)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << absPath(path) << "', newSize: " << newSize);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setsize");
  
  if(!talker__->execute("path", absPath(path), "size", SSTR(newSize))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::setChecksum(const std::string& lfn, 
                                         const std::string& ctype,
                                         const std::string& cval)   {
  Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " Entering, path: '" << absPath(lfn) << "', ctype: '" << ctype << "' cval: '" << cval);
  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setchecksum");
  
  if(!talker__->execute("lfn", absPath(lfn), "checksum-type", ctype,
    "checksum-value", cval)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

Directory* DomeAdapterHeadCatalog::openDir(const std::string& path)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. Path: " << absPath(path));
  using namespace boost::property_tree;
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "path: " << path);

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getdir");
  
  ptree params;
  params.put("path", absPath(path));
  params.put("statentries", "true");

  if(!talker__->execute(params)) {
    throw DmException(EINVAL, talker__->err());
  }

  try {
    DomeDir *domedir = new DomeDir(absPath(path));

    ptree entries = talker__->jresp().get_child("entries");
    for(ptree::const_iterator it = entries.begin(); it != entries.end(); it++) {
      ExtendedStat xstat;
      xstat.name = it->second.get<std::string>("name");

      Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "entry " << xstat.name);

      ptree_to_xstat(it->second, xstat);
      domedir->entries_.push_back(xstat);
    }
    domedir->dirents_.resize(domedir->entries_.size()); // will fill later, if needed
    return domedir;
  }
  catch(ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response - " << e.what() << " : " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterHeadCatalog::closeDir(Directory* dir)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");
  DomeDir *domedir = static_cast<DomeDir*>(dir);
  delete domedir;
}

ExtendedStat* DomeAdapterHeadCatalog::readDirx(Directory* dir)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");
  if (dir == NULL) {
    throw DmException(DMLITE_SYSERR(EFAULT), "Tried to read a null dir");
  }

  DomeDir *domedir = static_cast<DomeDir*>(dir);
  if(domedir->pos_ >= domedir->entries_.size()) {
    return NULL;
  }

  domedir->pos_++;
  return &domedir->entries_[domedir->pos_ - 1];
}

struct dirent* DomeAdapterHeadCatalog::readDir(Directory* dir)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");
  if (dir == NULL) {
    throw DmException(DMLITE_SYSERR(EFAULT), "Tried to read a null dir");
  }

  ExtendedStat *st = this->readDirx(dir);
  if(st == NULL) return NULL;

  DomeDir *domedir = static_cast<DomeDir*>(dir);
  struct dirent *entry = &domedir->dirents_[domedir->pos_ - 1];
  entry->d_ino = st->stat.st_ino;
  strncpy(entry->d_name, st->name.c_str(), sizeof(entry->d_name));

  return entry;
}

void DomeAdapterHeadCatalog::rename(const std::string& oldPath, const std::string& newPath)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_rename");
  
  if(!talker__->execute("oldpath", absPath(oldPath), "newpath", absPath(newPath))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::unlink(const std::string& path)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_unlink");
  
  if(!talker__->execute("lfn", absPath(path))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

ExtendedStat DomeAdapterHeadCatalog::extendedStatByRFN(const std::string& rfn)    {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "rfn: " << rfn );

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getstatinfo");
  
  if(!talker__->execute("rfn", rfn)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    ExtendedStat xstat;
    ptree_to_xstat(talker__->jresp(), xstat);
    return xstat;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

std::string DomeAdapterHeadCatalog::getComment(const std::string& path)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "path: " << path );
  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_getcomment");
  
  if(!talker__->execute("lfn", absPath(path))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    return talker__->jresp().get<std::string>("comment");
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterHeadCatalog::setComment(const std::string& path, const std::string& comment)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "path: " << path );

  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setcomment");
  
  if(!talker__->execute("lfn", absPath(path), "comment", comment)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterHeadCatalog::updateExtendedAttributes(const std::string& lfn,
                                                  const Extensible& ext)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");
 
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_updatexattr");
  
  if(!talker__->execute("lfn", absPath(lfn), "xattr", ext.serialize())) {
    throw DmException(EINVAL, talker__->err());
  }
}

std::string DomeAdapterHeadCatalog::readLink(const std::string& path)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");

  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_readlink");
  
  if(!talker__->execute("lfn", absPath(path))) {
    throw DmException(EINVAL, talker__->err());
  }

  try {
    return talker__->jresp().get<std::string>("target");
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterHeadCatalog::setAcl(const std::string& path, const Acl& acl)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering.");

  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_setacl");


  if(!talker__->execute("path", absPath(path), "acl", acl.serialize())) {
    throw DmException(EINVAL, talker__->err());
  }
}


std::string DomeAdapterHeadCatalog::absPath(const std::string &relpath) {
  if(relpath.size() > 0 && relpath[0] == '/') return relpath;
  return SSTR(this->cwdPath_ + "/" + relpath);
}
