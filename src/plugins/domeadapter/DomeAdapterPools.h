/// @file   DomeAdapterPools.h
/// @brief  Dome adapter
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#ifndef DOME_ADAPTER_POOLS_H
#define DOME_ADAPTER_POOLS_H

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/io.h>
#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/pooldriver.h>
#include <fstream>

#include "utils/DavixPool.h"
#include "utils/DomeTalker.h"
namespace dmlite {

  class DomeAdapterFactory;

  extern Logger::bitmask domeadapterlogmask;
  extern Logger::component domeadapterlogname;

  class DomeAdapterPoolManager : public PoolManager {
  public:
    DomeAdapterPoolManager(DomeAdapterFactory *factory);
    virtual ~DomeAdapterPoolManager();

    virtual std::string getImplId() const throw ();

    virtual void setStackInstance(StackInstance* si)  ;
    virtual void setSecurityContext(const SecurityContext*)  ;

    virtual std::vector<Pool> getPools(PoolAvailability availability = kAny)  ;
    virtual Pool getPool(const std::string&)  ;

    virtual void newPool(const Pool& pool)  ;
    virtual void updatePool(const Pool& pool)  ;
    virtual void deletePool(const Pool& pool)  ;

    virtual Location whereToRead (const std::string& path)  ;
    virtual Location whereToWrite(const std::string& path)  ;
    virtual Location chooseServer(const std::string& path)  ;
				
    virtual void cancelWrite(const Location& loc)  ;

    virtual void getDirSpaces(const std::string& path, int64_t &totalfree, int64_t &used)  ;
    
    virtual int fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, dmlite_xferinfo *progressdata)  ;
    virtual int fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, dmlite_xferinfo *progressdata)  ;
  private:
    StackInstance* si_;
    const SecurityContext* sec_;
    std::string userId_;
    
    DomeTalker *talker__;

    /// The corresponding factory.
    DomeAdapterFactory* factory_;
  };
}

#endif
