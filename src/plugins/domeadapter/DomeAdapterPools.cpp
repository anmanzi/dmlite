/// @file   DomeAdapterIO.cpp
/// @brief  Filesystem IO with dome
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/pooldriver.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional/optional.hpp>
#include <boost/foreach.hpp>
#include <boost/any.hpp>

#include "DomeAdapter.h"
#include "DomeAdapterPools.h"
#include "DomeAdapterDriver.h"

#include "utils/DomeTalker.h"
#include "DomeAdapterUtils.h"

using namespace dmlite;
using boost::property_tree::ptree;

#define SSTR(message) static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str()


DomeAdapterPoolManager::DomeAdapterPoolManager(DomeAdapterFactory *factory) {
  factory_ = factory;
  
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Ctor");
  talker__ = new DomeTalker(factory_->davixPool_, factory_->domehead_, "GET", "dome_access");
}

DomeAdapterPoolManager::~DomeAdapterPoolManager() {
  delete talker__;

}

std::string DomeAdapterPoolManager::getImplId() const throw () {
  return "DomeAdapterPoolManager";
}

void DomeAdapterPoolManager::setStackInstance(StackInstance* si)   {
  si_ = si;
}

void DomeAdapterPoolManager::setSecurityContext(const SecurityContext* secCtx)   {
  sec_ = secCtx;

  // Id mechanism
  if (factory_->tokenUseIp_)
    userId_ = sec_->credentials.remoteAddress;
  else
    userId_ = sec_->credentials.clientName;
}

static PoolManager::PoolAvailability getAvailability(const Pool &p) {
  return PoolManager::kNone; // TODO
}

std::vector<Pool> DomeAdapterPoolManager::getPools(PoolAvailability availability)   {
  if(availability == kForBoth) {
    availability = kForWrite;
  }

  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "GET", "dome_getspaceinfo");
  
  if(!talker__->execute()) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  std::vector<Pool> ret;
  try {
    // extract pools
    boost::optional<const ptree&> poolinfo = talker__->jresp().get_child_optional("poolinfo");
    if (poolinfo) {
      for(ptree::const_iterator it = poolinfo->begin(); it != poolinfo->end(); it++) {
        Pool p = deserializePool(it);
        if(availability == kAny || availability == getAvailability(p)) {
          ret.push_back(p);
        }
      }
    }
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }

  return ret;
}

Pool DomeAdapterPoolManager::getPool(const std::string& poolname)   {
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "GET", "dome_statpool");
  
  if(!talker__->execute("poolname", poolname)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    return deserializePool(talker__->jresp().get_child("poolinfo").begin());
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(DMLITE_NO_SUCH_POOL, SSTR("Pool " + poolname + " not found. (" << e.what() << ")"));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterPoolManager::newPool(const Pool& pool)   {
  
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_addpool");
  
  if(!talker__->execute("poolname", pool.name, "pool_stype", pool.getString("s_type"))) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterPoolManager::updatePool(const Pool& pool)   {
  
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_modifypool");
  

  ptree params;
  params.put("poolname", pool.name);
  params.put("pool_stype", pool.getString("s_type"));
  params.put("pool_defsize", pool.getLong("defsize"));

  if(!talker__->execute(params)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

void DomeAdapterPoolManager::deletePool(const Pool& pool)   {

  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_rmpool");
  
  
  if(!talker__->execute("poolname", pool.name)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

Location DomeAdapterPoolManager::whereToRead(const std::string& path)  
{
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "GET", "dome_get");
  
  
  if(!talker__->execute("lfn", path)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  // The 202 loop is not here, it's managed by the distant original client
  if(talker__->status() == 202) {
    throw DmException(EINPROGRESS, talker__->response());
  }

  try {
    Location loc;

    // extract pools
    ptree::const_iterator begin = talker__->jresp().begin();
    ptree::const_iterator end = talker__->jresp().end();
    for(ptree::const_iterator it = begin; it != end; it++) {
      std::string host = it->second.get<std::string>("server");
      std::string pfn = it->second.get<std::string>("pfn");

      Chunk chunk(host + ":" + pfn, 0, 0);
      chunk.url.query["token"] = dmlite::generateToken(userId_, pfn, factory_->tokenPasswd_, factory_->tokenLife_);
      loc.push_back(chunk);
    }
    return loc;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

Location DomeAdapterPoolManager::whereToWrite(const std::string& path)  
{
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. (PoolManager) Path: " << path);
  
  
  
  if (si_->contains("overwrite") && Extensible::anyToBoolean(si_->get("overwrite"))) {
    try {
      si_->getCatalog()->unlink(path);
    }
    catch (DmException& e) {
      if (e.code() != ENOENT) throw;
    }
  }
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_put");

  boost::property_tree::ptree params;
  if(si_->contains("replicate")) {
    bool val = Extensible::anyToBoolean(si_->get("replicate"));
    if (val) params.put("additionalreplica", "true");
  }

  if(si_->contains("pool")) {
    std::string val = Extensible::anyToString(si_->get("pool"));
    if (val.size()) params.put("pool", val);
  }

  if(si_->contains("filesystem")) {
    std::string val = Extensible::anyToString(si_->get("filesystem"));
    if (val.size()) {
      std::size_t found = val.find(":");
      if (found != std::string::npos ) {
        params.put("host", val.substr(0,found));
        params.put("fs", val.substr(found+1,val.size()));
      } 
    }
  }

  params.put("lfn", path);

  if(!talker__->execute(params)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    std::string host = talker__->jresp().get<std::string>("host");
    std::string pfn = talker__->jresp().get<std::string>("pfn");

    Chunk chunk(host+":"+pfn, 0, 0);
    chunk.url.query["sfn"] = path;

    std::string userId1;
    if (si_->contains("replicate"))
      userId1 = dmlite::kGenericUser;
    else
      userId1 = userId_;

    chunk.url.query["token"] = dmlite::generateToken(userId1, pfn, factory_->tokenPasswd_, factory_->tokenLife_, true);
    return Location(1, chunk);
  }
  catch(boost::property_tree::ptree &err) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}


Location DomeAdapterPoolManager::chooseServer(const std::string& path) {
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "GET", "dome_chooseserver");
  
  
  if(!talker__->execute("lfn", path)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
  
    
  try {
    Location loc;
    std::string host = talker__->jresp().get<std::string>("host");
    Chunk chunk(host+":", 0, 0);
    return Location(1, chunk);
    
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}


void DomeAdapterPoolManager::cancelWrite(const Location& loc)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. (PoolManager)");
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_delreplica");
  
  if(!talker__->execute("server", loc[0].url.domain, "pfn", loc[0].url.path)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}



void DomeAdapterPoolManager::getDirSpaces(const std::string& path, int64_t &totalfree, int64_t &used)  
{
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. (PoolManager)");
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "GET", "dome_getdirspaces");
  
  if(!talker__->execute("path", path)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }


  try {
    // extract info
    totalfree = talker__->jresp().get<int64_t>("quotafreespace");
    used = talker__->jresp().get<int64_t>("quotausedspace");
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}





int DomeAdapterPoolManager::fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, dmlite_xferinfo *progressdata)     {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. (PoolManager)");
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_filepush");
  
  if(!talker__->execute("localsrcpath", localsrcpath, "remotedesturl", remotedesturl)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
  
  return 0;
}


int DomeAdapterPoolManager::fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, dmlite_xferinfo *progressdata) {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "Entering. (PoolManager)");
  
  DomeCredentials dc(sec_);
  talker__->setcommand(dc, "POST", "dome_filepull");
  
  if(!talker__->execute("localdestpath", localdestpath, "remotesrcurl", remotesrcurl)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
  
  return 0;
}
