/// @file   DomeAdapter.cpp
/// @brief  Dome adapter plugin entry point.
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>

#include <iostream>

#include "DomeAdapter.h"
#include "DomeAdapterDiskCatalog.h"
#include "DomeAdapterHeadCatalog.h"
#include "DomeAdapterIO.h"
#include "DomeAdapterPools.h"
#include "DomeAdapterDriver.h"
#include "DomeAdapterAuthn.h"
#include "utils/Config.hh"
#include <stdio.h>

using namespace dmlite;

Logger::bitmask dmlite::domeadapterlogmask = 0;
Logger::component dmlite::domeadapterlogname = "DomeAdapter";

DomeAdapterFactory::DomeAdapterFactory()   : davixPool_(&davixFactory_, 512) {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);

}

DomeAdapterFactory::~DomeAdapterFactory() {

}

void DomeAdapterFactory::configure(const std::string& key, const std::string& value)  
{
  LogCfgParm(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, key, value);

  if(key == "DomeHead") {
    domehead_ = value;
  }
  else if(key == "TokenPassword") {
    tokenPasswd_ = value;
    CFG->SetString("glb.restclient.xrdhttpkey", (char *)value.c_str());
    
  }
  else if (key == "TokenId") {
    if (strcasecmp(value.c_str(), "ip") == 0)
      this->tokenUseIp_ = true;
    else
      this->tokenUseIp_ = false;
  }
  else if (key == "TokenLife") {
    this->tokenLife_ = (unsigned)atoi(value.c_str());
  }
  else if (key == "DavixPoolSize") {
    davixPool_.resize((unsigned)atoi(value.c_str()));
  }
  else if (key == "ThisDomeAdapterDN") {
    CFG->SetString("glb.restclient.present-as", (char *)value.c_str());
  }
  else if( key.find("Davix") != std::string::npos) {
    davixFactory_.configure(key, value);
  }
}

std::string DomeAdapterFactory::implementedPool() throw() {
  return "filesystem";
}

PoolManager* DomeAdapterFactory::createPoolManager(PluginManager*)   {
  return new DomeAdapterPoolManager(this);
}

PoolDriver* DomeAdapterFactory::createPoolDriver()   {
  return new DomeAdapterPoolDriver(this);
}

Catalog* DomeAdapterFactory::createCatalog(PluginManager*)   {
  return new DomeAdapterDiskCatalog(this);
}

Authn* DomeAdapterFactory::createAuthn(PluginManager*)   {
  return new DomeAdapterAuthn(this);
}

static void registerDomeAdapterDiskCatalog(PluginManager* pm)   {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "registerDomeAdapterDiskCatalog");

  DomeAdapterFactory *dmFactory = new DomeAdapterFactory();
  pm->registerCatalogFactory(dmFactory);
  pm->registerAuthnFactory(dmFactory);
}

static void registerIOPlugin(PluginManager* pm)   {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "registerIOPlugin");

  pm->registerIODriverFactory(new DomeIOFactory());
}

static void registerDomeAdapterPools(PluginManager* pm)   {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "registerDomeAdapterPools");

  DomeAdapterFactory *dmFactory = new DomeAdapterFactory();
  pm->registerPoolManagerFactory(dmFactory);
  pm->registerPoolDriverFactory(dmFactory);
}

static void registerDomeAdapterHeadCatalog(PluginManager* pm)   {
  domeadapterlogmask = Logger::get()->getMask(domeadapterlogname);
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, "registerDomeAdapterHeadCatalog");

  DomeAdapterHeadCatalogFactory *factory = new DomeAdapterHeadCatalogFactory();
  pm->registerCatalogFactory(factory);

  DomeAdapterFactory *dmFactory = new DomeAdapterFactory();
  pm->registerAuthnFactory(dmFactory);
}

PluginIdCard plugin_domeadapter_headcatalog = {
  PLUGIN_ID_HEADER,
  registerDomeAdapterHeadCatalog
};

PluginIdCard plugin_domeadapter_diskcatalog = {
  PLUGIN_ID_HEADER,
  registerDomeAdapterDiskCatalog
};

PluginIdCard plugin_domeadapter_io = {
  PLUGIN_ID_HEADER,
  registerIOPlugin
};

PluginIdCard plugin_domeadapter_pools = {
  PLUGIN_ID_HEADER,
  registerDomeAdapterPools
};
