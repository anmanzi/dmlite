/// @file   DomeAdapterDiskCatalog.h
/// @brief  Dome adapter
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#ifndef DOME_ADAPTER_CATALOG_H
#define	DOME_ADAPTER_CATALOG_H

#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/dmlite.h>
#include "DomeAdapter.h"

namespace dmlite {

  extern Logger::bitmask domeadapterlogmask;
  extern Logger::component domeadapterlogname;

  class DomeAdapterDiskCatalog: public Catalog {
  public:
  	/// Constructor
    DomeAdapterDiskCatalog(DomeAdapterFactory *factory)  ;

    // Overload
    std::string getImplId(void) const throw ();

    void setStackInstance(StackInstance* si)  ;
    void setSecurityContext(const SecurityContext* secCtx)  ;

    virtual void getChecksum(const std::string& path,
                             const std::string& csumtype,
                             std::string& csumvalue,
                             const std::string& pfn,
                             const bool forcerecalc = false, const int waitsecs = 0)  ;

   virtual void setChecksum(const std::string& lfn, 
                             const std::string& ctype,
                             const std::string& cval)  ;
                             
    ExtendedStat extendedStat(const std::string&, bool)  ;
    ExtendedStat extendedStatByRFN(const std::string &)  ;

    bool accessReplica(const std::string& replica, int mode)  ;
    Replica getReplicaByRFN(const std::string& rfn)  ;

    Directory* openDir (const std::string&)  ;
    void       closeDir(Directory*)          ;
    ExtendedStat*  readDirx(Directory*)  ;

    void updateExtendedAttributes(const std::string&,
                                  const Extensible&)  ;

  protected:
    struct DomeDir : public Directory {
      std::string path_;
      size_t pos_;
      std::vector<dmlite::ExtendedStat> entries_;

      virtual ~DomeDir() {}
      DomeDir(std::string path) : path_(path), pos_(0) {}
    };

    StackInstance* si_;
    const SecurityContext *sec_;
    DomeAdapterFactory* factory_;

    std::string cwd_;
  };
}


#endif
